$("input#global-search").on("keyup", function (e) {
    if (e.which == 13) {
        var searchValue = $(this).val();
        if (searchValue) {
            window.location.replace("/management/search_results/?search=" + searchValue);
        }
    }
});

$("#search-btn").on("click", function () {
    var searchValue = $("input#global-search").val().trim();
    if (searchValue) {
        window.location.replace("/management/search_results/?search=" + searchValue);
    }
});