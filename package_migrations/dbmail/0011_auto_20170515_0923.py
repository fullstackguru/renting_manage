# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('dbmail', '0010_auto_20170330_1047'),
    ]

    operations = [
        migrations.AlterField(
            model_name='mailtemplate',
            name='from_email',
            field=models.ForeignKey(on_delete=django.db.models.deletion.SET_NULL, default=None, to='dbmail.MailFromEmail', blank=True, help_text='If not specified, then used default.', null=True, verbose_name='Message from'),
        ),
    ]
