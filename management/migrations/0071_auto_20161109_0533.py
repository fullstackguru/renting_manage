# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import management.models


class Migration(migrations.Migration):

    dependencies = [
        ('management', '0070_auto_20161108_0631'),
    ]

    operations = [
        migrations.AlterField(
            model_name='invitation',
            name='invitation_date',
            field=models.DateTimeField(default=management.models.get_invite_date),
        ),
    ]
