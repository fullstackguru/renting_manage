# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
from django.db.models import Count
from website.models import UserOrganizationSocialHabitAnswers, Relationship


class Migration(migrations.Migration):

    def remove_not_unique_invites(apps, schema_editor):
        Invitation = apps.get_model("management", "Invitation")
        duplicated_invites = Invitation.objects.values('user', 'agreement_period').annotate(total=Count('user')).\
            filter(total__gt=1)
        for invite in duplicated_invites:
            invite.pop('total')
            invites = Invitation.objects.filter(**invite).values_list('id', flat=True).order_by('invitation_date')
            answers = UserOrganizationSocialHabitAnswers.objects.filter(invitation_id__in=invites).\
                values_list('invitation', flat=True).distinct()
            relationships = Relationship.objects.filter(invitation_id__in=invites).\
                values_list('invitation', flat=True).distinct()
            if answers or relationships:
                if list(answers) != list(relationships) and len(answers) + len(relationships) > 1:
                    if relationships[0] in answers:
                        answers = list(answers)
                        answers.remove(relationships[0])
                    invites_for_removing = Invitation.objects.filter(id__in=invites).exclude(id__in=answers).\
                        exclude(id__in=relationships[1:])
                    invites_ids_for_removing = invites_for_removing.values_list('id', flat=True)
                    UserOrganizationSocialHabitAnswers.objects.filter(invitation_id__in=invites_ids_for_removing).\
                        delete()
                    Relationship.objects.filter(invitation_id__in=invites_ids_for_removing).delete()
                    invites_for_removing.delete()
                else:
                    Invitation.objects.filter(id__in=invites).exclude(id__in=answers).exclude(id__in=relationships).\
                        delete()
            else:
                Invitation.objects.filter(id__in=invites[1:]).delete()

    dependencies = [
        ('management', '0014_auto_20160606_0645'),
    ]

    operations = [
        migrations.RunPython(remove_not_unique_invites),
    ]
