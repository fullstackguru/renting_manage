# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    def create_stages(apps, schema_editor):
        Stage = apps.get_model('management', 'Stage')
        Stage.objects.create(name='Stage 1', restrictions=[0])
        Stage.objects.create(name='Stage 2', restrictions=[])
        Stage.objects.create(name='Stage 3', restrictions=[])
        Stage.objects.create(name='Stage 4', restrictions=[1])

    dependencies = [
        ('management', '0054_add_stages_model'),
    ]

    operations = [
        migrations.RunPython(create_stages)
    ]
