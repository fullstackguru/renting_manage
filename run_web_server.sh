#!/bin/bash

rm /etc/redis/redis.conf
cp /usr/src/app/devops/config/redis.conf /etc/redis/redis.conf
/etc/init.d/redis-server start
uwsgi --ini /usr/src/app/devops/config/uwsgi.ini --mime-file /usr/src/app/devops/config/mime.types