from optparse import make_option
from django.core.management.base import BaseCommand, CommandError
from django.utils.translation import activate
from django.db.models import Count, Q
from openpyxl import load_workbook
from django.contrib.auth import get_user_model
User = get_user_model()
from management.models import Property, Invitation


class Command(BaseCommand):
    help = "Remove users that doesn't appears in file."

    option_list = BaseCommand.option_list + (
        make_option('--file',
                    action="store",
                    type="string",
                    dest="path",
                    help='Path to file with data'
                    ),
        make_option('--property',
                    action="store",
                    type="string",
                    dest="property",
                    help='Property name'
                    ),
        )

    def handle(self, *args, **options):
        activate('en')
        path = options.get('path') or ''
        print path
        if not path:
            raise CommandError('Please set filename!')
        try:
            workbook = load_workbook(path, read_only=True, use_iterators=True)
        except IOError:
            raise CommandError('No such file!')
        property_name = options.get('property') or ''
        try:
            prop = Property.objects.get(name=property_name)
        except Property.DoesNotExist:
            raise CommandError("Property doesn't exist")
        first_sheet = workbook.get_sheet_names()[0]
        worksheet = workbook.get_sheet_by_name(first_sheet)
        all_users = set()
        managers_ids = prop.managers.all().values_list('id', flat=True)
        for index, items_row in enumerate(worksheet.rows):  # over each row
            # print items_row
            if index == 0:
                continue
            formatted_cells = [unicode(i.value).strip() if i.value else '' for i in items_row]
            email = formatted_cells[5].lower().replace(';', '')
            if email in all_users:
                print email
            all_users.add(email)
        print all_users
        print "Start removing unnecessary users"
        unnecessary_invites = Invitation.objects.filter(agreement_period__property=prop).\
            exclude(user__email__in=all_users).exclude(user__in=managers_ids)
        print "Number of unnecessary invites: {}".format(unnecessary_invites.count())
        users_with_multiple_invites = Invitation.objects.values_list('user_id', flat=True).\
            annotate(num_of_invites=Count('id')).filter(num_of_invites__gt=1)
        users_with_multiple_invites = list(users_with_multiple_invites)
        unnecessary_users_ids = unnecessary_invites.values_list('user_id', flat=True).\
            exclude(user_id__in=users_with_multiple_invites)
        # remove many-to-many relations
        m2m_relations = User.available_properties.through.objects.filter(property_id=prop.id).\
            exclude(user__email__in=all_users).exclude(user__in=managers_ids)
        print "Number of unnecessary m2m relations: {}".format(m2m_relations.count())
        m2m_relations.delete()
        unnecessary_users = User.objects.filter(id__in=unnecessary_users_ids)
        print "Number of unnecessary users: {}".format(unnecessary_users.count())
        unnecessary_relations = User.objects.filter(Q(start_user__in=unnecessary_users_ids) |
                                                    Q(end_user__in=unnecessary_users_ids))
        print "Number of unnecessary relations: {}".format(unnecessary_relations.count())
        unnecessary_relations.delete()
        unnecessary_invites.delete()
        unnecessary_users.delete()
