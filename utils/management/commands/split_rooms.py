from optparse import make_option
from django.core.management.base import BaseCommand
from management.models import Unit, PendingAssignment, Room, Property


def split_beds_to_rooms(ids):
    rooms = Room.objects.filter(id__in=ids).prefetch_related('beds')
    for room in rooms:
        beds = room.beds.all()
        for bed in beds:
            room_name = bed.name[0]
            new_room, _ = Room.objects.get_or_create(
                unit=room.unit, name=room_name, room_type=room.room_type, square=room.square
            )
            PendingAssignment.objects.filter(bed=bed).update(room=new_room)
            bed.room = new_room
            bed.save()


class Command(BaseCommand):
    help = "Split rooms."

    option_list = BaseCommand.option_list + (
        make_option('--name',
                    action="store",
                    type="string",
                    dest="prop_name",
                    help='Property name'
                    ),
    )

    def handle(self, *args, **options):
        prop_name = options['prop_name']
        prop = Property.objects.filter(name=prop_name).first()
        if prop:
            room_ids = Unit.objects.filter(unit_type__property=prop).values_list('rooms', flat=True)
            split_beds_to_rooms(room_ids)
        Room.objects.filter(unit__unit_type__property=prop, name='WL').delete()
