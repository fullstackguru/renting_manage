# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('management', '0003_bed'),
    ]

    operations = [
        migrations.AddField(
            model_name='property',
            name='enable_email_sending',
            field=models.BooleanField(default=True),
        ),
    ]
