# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    def change_unused_relations_types(apps, schema_editor):
        Relationship = apps.get_model("website", "Relationship")
        print Relationship.objects.filter(is_roommate__in=['A', 'V']).update(is_roommate='N')

    dependencies = [
        ('website', '0020_change_relation_is_roommate_choices'),
    ]

    operations = [
        migrations.RunPython(change_unused_relations_types),
    ]
