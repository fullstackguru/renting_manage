# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
from website.models import Relationship as Relation


class Migration(migrations.Migration):

    def remove_relations_with_different_room_types(apps, schema_editor):
        Relationship = apps.get_model("website", "Relationship")
        Invitation = apps.get_model("management", "Invitation")
        relations = Relationship.objects.all()
        for relation in relations:
            start_user_invite = Invitation.objects.filter(user=relation.start_user).first()
            end_user_invite = Invitation.objects.filter(user=relation.end_user).first()
            if not start_user_invite or not end_user_invite:
                continue
            if start_user_invite.room_type != end_user_invite.room_type:
                relation.is_roommate = Relation.NOT_ROOMMATE
                relation.save()


    dependencies = [
        ('website', '0025_create_relations_for_suitemates'),
        ('management', '0025_remove_matches_with_different_units')
    ]

    operations = [
        migrations.RunPython(remove_relations_with_different_room_types)
    ]
