# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('management', '0038_add_lock_field_to_invite'),
    ]

    operations = [
        migrations.AddField(
            model_name='pendingassignment',
            name='invite',
            field=models.OneToOneField(related_name='assignment', null=True, blank=True, to='management.Invitation'),
        ),
    ]
