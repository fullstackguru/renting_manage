$('.set-roommate, .invite-btn').on('click', function (e) {
    e.preventDefault();
    var form = $(this).parents('form');
    var message = $(this).attr('confirm-text');
    var name = $(this).attr('name');
    var value = $(this).val();
    swal({
        title: "Are you sure?",
        text: message,
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: "#34a263",
        confirmButtonText: "Yes",
        closeOnConfirm: false
    }, function (isConfirm) {
        if (isConfirm) {
            var tempElement = $("<input type='hidden'/>");
            tempElement.attr("name", name)
                .val(value)
                .appendTo(form);
            form.submit();
        }
    });
});


$('#change-stage-submit').on('click', function (e) {
    e.preventDefault();
    var form = $(this).parents('form');
    swal({
        title: "Are you sure?",
        text: "Are you sure that you want to change the current stage?",
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: "#34a263",
        confirmButtonText: "Yes"
    }, function (isConfirm) {
        if (!isConfirm) {
            return;
        }
        $.ajax({
            type: "POST",
            url: form.attr('action'),
            data: form.serialize(),
            success: function (data) {
                $.notify({
                    message: 'State has been successfully changed!',
                    icon: 'fa fa-check'
                }, {
                    type: 'success',
                    z_index: 1051,
                    delay: 2000
                });
            },
            error: function (XMLHttpRequest, textStatus, errorThrown) {
                console.log(XMLHttpRequest);
                $.notify({
                    message: "Can't change current stage!",
                    icon: 'fa fa-times'
                }, {
                    type: 'danger',
                    z_index: 1051,
                    delay: 2000
                });
            }
        });
    })
});