# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('dbmail', '0010_auto_20170330_1047'),
        ('management', '0075_property_disable_email_resident'),
    ]

    operations = [
        migrations.AddField(
            model_name='property',
            name='from_email_host',
            field=models.ManyToManyField(to='dbmail.MailFromEmail', blank=True),
        ),
    ]
