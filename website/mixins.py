from django.http import HttpResponseRedirect
from django.utils import timezone
from django.core.urlresolvers import reverse
from management.models import Invitation


class CheckInviteMixin(object):
    def dispatch(self, request, *args, **kwargs):
        invite_id = request.session.get('invite_id', None)
        if not invite_id:
            return HttpResponseRedirect("%s?next=%s" % (reverse('code_index'), request.get_full_path()))
        else:
            invite = Invitation.objects.filter(id=invite_id).select_related('user').first()
            if invite:
                user = invite.user
                user.last_login = timezone.now()
                user.save()
            else:
                return HttpResponseRedirect("%s?next=%s" % (reverse('code_index'), request.get_full_path()))
        return super(CheckInviteMixin, self).dispatch(request, *args, **kwargs)
