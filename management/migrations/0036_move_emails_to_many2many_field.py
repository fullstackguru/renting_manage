# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
from django.conf import settings


class Migration(migrations.Migration):

    def move_emails(apps, schema_editor):
        Property = apps.get_model('management', 'Property')
        properties = Property.objects.all()
        email_fields = ['welcome_email', 'accept_relationship_email', 'reject_relationship_email',
                        'request_relationship_email', 'account_reminder_email', 'message_notification_email']
        for property in properties:
            for email_field in email_fields:
                email = getattr(property, email_field)
                if email.slug.startswith(settings.DB_MAILER_DEFAULT_TEMPLATE_PREFIX):
                    continue
                property.emails.add(email)

    dependencies = [
        ('management', '0035_add_emails_many2many_field_to_property'),
    ]

    operations = [
        migrations.RunPython(move_emails),
    ]
