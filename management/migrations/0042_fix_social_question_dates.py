# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from datetime import timedelta

from django.db import models, migrations
from django.db.models import Q
from django.utils import timezone


class Migration(migrations.Migration):

    def fix_social_questions_dates(apps, schema_migration):
        Answer = apps.get_model('management', 'OrganizationSocialHabitAnswer')
        Question = apps.get_model('management', 'OrganizationSocialHabitQuestion')
        answers_with_wrong_date = Answer.objects.filter(Q(created_at__lt=timezone.now() - timedelta(days=356)) |
                                                        Q(updated_at__lt=timezone.now() - timedelta(days=356)))
        questions_with_wrong_date = Question.objects.filter(Q(created_at__lt=timezone.now() - timedelta(days=356)) |
                                                        Q(updated_at__lt=timezone.now() - timedelta(days=356)))
        print answers_with_wrong_date.update(created_at=timezone.now(), updated_at=timezone.now())
        print questions_with_wrong_date.update(created_at=timezone.now(), updated_at=timezone.now())

    dependencies = [
        ('management', '0041_remove_pending_assignments_without_invites'),
    ]

    operations = [
        migrations.RunPython(fix_social_questions_dates)
    ]
