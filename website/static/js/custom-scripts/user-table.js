var resendInviteAction = '0';
var sendReminderEmailAction = '1';
var resendInviteConfirmationText = 'Are you sure that you want to resend invite for ';
var sendReminderEmailConfirmationText = 'Are you sure that you want to send account reminder email for ';
var listUrl = null;


function getCookie(name) {
    var cookieValue = null;
    if (document.cookie && document.cookie != '') {
        var cookies = document.cookie.split(';');
        for (var i = 0; i < cookies.length; i++) {
            var cookie = jQuery.trim(cookies[i]);
            // Does this cookie string begin with the name we want?
            if (cookie.substring(0, name.length + 1) == (name + '=')) {
                cookieValue = decodeURIComponent(cookie.substring(name.length + 1));
                break;
            }
        }
    }
    return cookieValue;
}


var initUserTable = function (listUrl, updateUrl, propertyId) {
    listUrl = listUrl;

    $('#user-table').on('preXhr.dt', function () {
        $('tr').attr('hidden', true);
        App.blocks('#user-table-block', 'refresh_toggle');
    });

    $('#user-table').on('xhr.dt', function () {
        $('tr').attr('hidden', false);
        App.blocks('#user-table-block', 'refresh_toggle');
        $('#check-all').prop('checked', false);
        $('.user-checkbox').prop("checked", false);
    });

    $('.filter-option').on('click', function () {
        $(this).closest('li').siblings().children('a').removeClass('selected-filter');  // Clear filter
        var selectedValue = $(this).attr("value");
        if (selectedValue) {
            $(this).addClass('selected-filter');
        }
        $('#user-table').DataTable().draw()
    });

    $('#check-all').change(function () {
        var checkedValue = $(this).is(":checked");
        var otherCheckboxes = $('.user-checkbox');
        $('.user-checkbox').prop("checked", checkedValue);
    });

    $('#selected-label').on('click', function () {
        $(this).toggleClass('selected-label');
    });

    $('#actions-form-submit').on('click', function () {
        var selectedAction = $("#actions-select option:selected").val();
        if (!selectedAction) {
            swal("Oops...", "Please select action", "error");
            return
        }
        var all = $('#selected-label').hasClass('selected-label');
        if (!all) {
            var selectedIds = [];
            $('.user-checkbox:checked').each(function () {
                selectedIds.push($(this).val());
            });
            if (selectedIds.length === 0) {
                swal("Oops...", "Please select users", "error");
                return
            }
        }
        var data = {'action': selectedAction, 'ids': selectedIds};
        if (all) {
            data['all'] = all;
            var table = $('#user-table').dataTable();
            var settings = table.fnSettings();
            var params = table._fnAjaxParameters(settings);
            for (var attrname in params) {
                data[attrname] = params[attrname];
            }
            $('.selected-filter').each(function (index, element) {
                var filterName = $(this).closest('.filter').attr('name');
                data[filterName] = $(this).attr('value');
            });
        }
        if(selectedAction == 4){
								
					swal({
							title: "Are you sure?",
							text: "Are you sure that you want to delete selected users.",
							type: "warning",
							showCancelButton: true,
							confirmButtonColor: "#DD6B55",
							confirmButtonText: "Yes, delete it!",
							cancelButtonText: "No, cancel!",
							closeOnConfirm: false
					},
					function (isConfirm) {
							if (isConfirm) {
									$.ajax({
										type: 'POST',
										headers: {
												"X-CSRFToken": getCookie('csrftoken')
										},
										data: data,
										beforeSend: function () {
												$('tr').attr('hidden', true);
												App.blocks('#user-table-block', 'refresh_toggle');
										},
										success: function (data, textStatus, request) {
												swal("Done!", data, "success");
												$('#check-all').prop('checked', false);
												$('.user-checkbox').prop("checked", false);
												$('#selected-label').removeClass('selected-label');
												$('#user-table').DataTable().draw();
										},
										error: function (jqXHR, textStatus, errorThrown) {
												swal("Oops...", jqXHR.responseText, "error");
										},
										complete: function() {
												$('tr').attr('hidden', false);
												App.blocks('#user-table-block', 'refresh_toggle');
										}
								})
							}
					});
        	
        	return;
				}
        
        $.ajax({
            type: 'POST',
            headers: {
                "X-CSRFToken": getCookie('csrftoken')
            },
            data: data,
            beforeSend: function () {
                $('tr').attr('hidden', true);
                App.blocks('#user-table-block', 'refresh_toggle');
            },
            success: function (data, textStatus, request) {
                var contentType = request.getResponseHeader('Content-Type');
                if (contentType === 'text/csv') {
                    var contentDisposition = request.getResponseHeader('Content-Disposition');
                    var filename = contentDisposition.match(/filename="(.+)"/)[1];
                    var blob = new Blob([data], {type: contentType});
                    var link = document.createElement('a');
                    link.href = window.URL.createObjectURL(blob);
                    link.download = filename;
                    document.body.appendChild(link);
                    link.click();
                    setTimeout(function () {
                        document.body.removeChild(link);
                    }, 100);
                } else {
                    swal("Done!", data, "success");
                }
                $('#check-all').prop('checked', false);
                $('.user-checkbox').prop("checked", false);
                $('#selected-label').removeClass('selected-label');
            },
            error: function (jqXHR, textStatus, errorThrown) {
                swal("Oops...", jqXHR.responseText, "error");
            },
            complete: function() {
                $('tr').attr('hidden', false);
                App.blocks('#user-table-block', 'refresh_toggle');
            }
        })
    });

    $('#user-table').DataTable({
        initComplete: function () {
            var api = this.api();
            $('#user-table_filter input')
                .off('.DT')
                .on('keyup.DT', function (e) {
                    if (e.keyCode == 13) {
                        api.search(this.value).draw();
                    }
                });
        },
        "fnDrawCallback": function () {
            $('#check-all').closest('th').removeClass("sorting_asc");
            var form = $('#actions-form');
            form.prop('hidden', false);
            $("#user-table_length").append(form);
            $("#user-table_length").children().addClass('float-left');
            $("#selected-label").text("Select all " + this.api().page.info().recordsDisplay + " users");
        },
        "responsive": true,
        "serverSide": true,
        "processing": true,
        "bSort": true,
        "bPaginate": true,
        "AutoWidth": false ,
        "sPaginationType": "full_numbers",
        "iDisplayLength": 10,
        ajax: {
            "data": function (d) {
                $('.selected-filter').each(function (index, element) {
                    var filterName = $(this).closest('.filter').attr('name');
                    d[filterName] = $(this).attr('value');
                });
            }
        },
        "aoColumns": [
            {
                "mDataProp": "id",
                "render": function (data, type, full, meta) {
                    return '<label class="css-input css-checkbox css-checkbox-primary">' +
                        '<input class="user-checkbox" value=' + data + ' type="checkbox"><span></span></label>'
                },
                "orderable": false,
                sClass: "table-checkbox text-center"
            },
            {
                "mDataProp": "email",
                "render": function (data, type, full, meta) {
                    return '<a href="' + updateUrl + full.id + '">' + data + '</a>';
                }
            },
            {"mDataProp": "first_name"},
            {"mDataProp": "last_name"},
            {
                "mDataProp": "last_login",
                "render": function (data, type, full, meta) {
                    var a = $(this);
                    $(this).attr('data-order', data);
                    return data ? moment(data, moment.ISO_8601).format('MMMM Do YYYY, h:mm:ss a') : "Never"
                }
            },
            {
                "mDataProp": "unit",
                "render": function (data, type, full, meta) {
                    if (data) {
                        return data;
                    }
                    return "WAITLIST"
                }
            },
            {
                "mDataProp": "room",
                "render": function (data, type, full, meta) {
                    if (data) {
                        return data;
                    }
                    return "WAITLIST"
                },
                "orderable": false
            },
            {
                "mDataProp": "bed",
                "render": function (data, type, full, meta) {
                    if (data) {
                        return data;
                    }
                    return "WAITLIST"
                },
                "orderable": false
            },
            {
                "mDataProp": "send_reminder_email",
                "render": function (data, type, full, meta) {
                    return '<div class="management-action" ' +
                           'onclick="sendAccountReminderEmail(' + full.id + ', \'' + full.email + '\')">' +
                                'Send reminder email <i class="fa fa-envelope-o"></i>' +
                           '</div>'
                },
                "orderable": false
            },
            {
                "mDataProp": "resend_invite",
                "render": function (data, type, full, meta) {
                    return '<div class="management-action" ' +
                           'onclick="resendInviteEmail(' + full.id + ', \'' + full.email + '\')">' +
                                'Resend invite <i class="fa fa-envelope-o"></i>' +
                           '</div>'
                },
                "orderable": false
            },
            {
                "mDataProp": "delete",
                "render": function (data, type, full, meta) {
                    return '<div class="management-action" onclick="deleteUser(' + full.id + ', \'' + full.email + '\', ' + propertyId + ')">' +
                                'Delete <i class="si si-trash"></i>' +
                           '</div>'
                },
                "orderable": false
            }
        ],
        paging: true,
        "lengthMenu": [[10, 25, 50, 100], [10, 25, 50, 100]],
    });
};

var deleteUser = function (userId, userEmail, propertyId) {
    var table = $('#user-table').DataTable();
    var url = '/management/user/' + propertyId + '/' + userId + '/delete';
    swal({
            title: "Are you sure?",
            text: "Are you sure that you want to delete " + userEmail,
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Yes, delete it!",
            cancelButtonText: "No, cancel!",
            closeOnConfirm: false
        },
        function (isConfirm) {
            if (isConfirm) {
                $.ajax({
                    url: url,
                    type: 'DELETE',
                    success: function (data) {
                        swal("Done!", data, "success");
                        table.draw('page');
                    },
                    error: function (jqXHR, textStatus, errorThrown) {
                        swal("Oops...", "Unable to delete this user!", "error");
                    }
                });
            }
        });
};


var sendEmail = function (userId, action, confirmation_text) {
    swal({
            title: "Are you sure?",
            text: confirmation_text,
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Yes, resend!",
            cancelButtonText: "No, cancel!",
            closeOnConfirm: false
        },
        function (isConfirm) {
            if (isConfirm) {
                $.ajax({
                    url: listUrl,
                    type: 'POST',
                    data: {
                        action: action,
                        ids: [userId]
                    },
                    success: function (data) {
                        swal("Done!", data, "success");
                    },
                    error: function (jqXHR, textStatus, errorThrown) {
                        swal("Oops...", "Unable to send email!", "error");
                    }
                });
            }
        });
};

var resendInviteEmail = function (userId, userEmail) {
    sendEmail(userId, resendInviteAction, resendInviteConfirmationText + userEmail);
};

var sendAccountReminderEmail = function (userId, userEmail) {
    sendEmail(userId, sendReminderEmailAction, sendReminderEmailConfirmationText + userEmail);
};
