from django.core.management.base import BaseCommand
from management.models import PendingAssignment
from website.models import User


class Command(BaseCommand):
    help = 'Remove duplicated beds'

    def handle(self, *args, **options):
        users = User.objects.all()
        for user in users:
            user_beds = user.beds.all()
            assignment = PendingAssignment.objects.filter(user=user).first()
            if assignment:
                user_beds = user_beds.exclude(id=assignment.bed_id)
            if user_beds:
                user_beds.delete()
