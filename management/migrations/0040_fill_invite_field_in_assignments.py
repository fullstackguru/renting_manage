# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    def add_invites(apps, schema_editor):
        PendingAssignment = apps.get_model('management', 'PendingAssignment')
        Invitation = apps.get_model('management', 'Invitation')
        invites = Invitation.objects.all()
        for invite in invites:
            try:
                assign = PendingAssignment.objects.get(user=invite.user, assignment_period=invite.agreement_period)
                assign.invite = invite
                assign.save()
            except PendingAssignment.DoesNotExist:
                pass

    dependencies = [
        ('management', '0039_add_invite_to_pending_assignment'),
    ]

    operations = [
        migrations.RunPython(add_invites)
    ]
