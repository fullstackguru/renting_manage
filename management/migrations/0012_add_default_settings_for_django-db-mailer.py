# -*- coding: utf-8 -*-
from __future__ import unicode_literals
import sys
reload(sys)
sys.setdefaultencoding("utf-8")

from django.db import models, migrations


class Migration(migrations.Migration):

    def add_default_templates(apps, schema_editor):
        MailCategory = apps.get_model("dbmail", "mailcategory")
        MailTemplate = apps.get_model("dbmail", "mailtemplate")
        welcome_category = MailCategory(
            name='Welcome emails'
        ).save()
        reminder_category = MailCategory(
            name='Reminder emails'
        ).save()
        accept_relationships_category = MailCategory(
            name='Accept relation emails'
        ).save()
        request_relationships_category = MailCategory(
            name='Request relation emails'
        ).save()
        reject_relationships_category = MailCategory(
            name='Reject relation emails'
        ).save()
        messages_notifications_category = MailCategory(
            name='Message notice emails'
        ).save()
        context_description = """
        A template is rendered with a context. You can use it for creating dynamic templates.
        Simply insert some variable from this list to your template (like this {{ variable_name }})
        and template engine will replace it.
        """
        welcome_emails_context = context_description + """
        1. {{ server_url }} - Server address. You must specify this tag before each url,
        like {{ server_url }}/admin - link to admin page.
        2. {{ property }} - Property object.
        3. {{ user }} - User object.
        4. {{ invite }} - Invite object.
        5. {{ invite.invite_code }} - Invite Code.
        6. {{ invite.expire_date }} - Invite expiration date. You can use formatting for it (For example: {{ invite.expire_date|date:"F d, Y" }})
        7. {{ user.first_name }} - User First Name.
        8. {{ user.last_name }} - User Last Name.
        9. {{ user.email }} - User Email.
        10. {{ property.name }} - Property Name.
        11. {{ logo_url }} - Url to Property Logo.
        12. {{ year }} - Current Year.
        13. {{ company }} - Company Name.
        14. {{ subject }} - Default Subject.
        """
        MailTemplate(
            name='Default welcome email',
            subject='Welcome to roommate matching',
            message=open('website/templates/email/welcome.html', 'r').read().replace('\n', ''),
            slug='default_welcome_email',
            num_of_retries=3,
            is_html=True,
            category=welcome_category,
            context_note=welcome_emails_context,
        ).save()
        MailTemplate(
            name='Lorenzo welcome email',
            subject='Welcome to Lorenzo roommate matching',
            message=open('website/templates/email/welcome.html', 'r').read().replace('\n', ''),
            slug='lorenzo_welcome_email',
            num_of_retries=3,
            is_html=True,
            category=welcome_category,
            context_note=welcome_emails_context,
        ).save()
        accept_relationships_context = context_description + '''
        1. {{ server_url }} - Server address. You must specify this tag before each url,
        like {{ server_url }}/admin - link to admin page.
        2. {{ code }} - Invite Code.
        3. {{ start_user }} - User that created the invitation.
        4. {{ start_user.first_name }} - User that created the invitation first name.
        5. {{ start_user.last_name }} - User that created the invitation last name.
        6. {{ start_user.email }} - User that created the invitation email.
        7. {{ end_user }} - Invited user.
        8. {{ end_user.first_name }} - Invited user first name.
        9. {{ end_user.last_name }} - Invited user last name.
        10. {{ end_user.email }} - Invited user email.
        11. {{ subject }} - Default subject.
        '''
        MailTemplate(
            name='Default accept relationship email',
            subject='Roommate Request Accepted',
            message=open('website/templates/email/accept_relationship.html', 'r').read().replace('\n', ''),
            slug='default_accept_relationship_email',
            num_of_retries=3,
            is_html=True,
            category=accept_relationships_category,
            context_note=accept_relationships_context,
        ).save()
        request_relationships_context = context_description + '''
        1. {{ server_url }} - Server address. You must specify this tag before each url,
        like {{ server_url }}/admin - link to admin page.
        2. {{ code }} - Invite Code.
        3. {{ relationship.id }} - Relationship id.
        4. {{ start_user }} - User that created the invitation.
        5. {{ start_user.first_name }} - User that created the invitation first name.
        6. {{ start_user.last_name }} - User that created the invitation last name.
        7. {{ start_user.email }} - User that created the invitation email.
        8. {{ end_user }} - Invited user.
        9. {{ end_user.first_name }} - Invited user first name.
        10. {{ end_user.last_name }} - Invited user last name.
        11. {{ end_user.email }} - Invited user email.
        12. {{ subject }} - Default subject.
        '''
        MailTemplate(
            name='Default request relationship email',
            subject="Roommate Request!",
            message=open('website/templates/email/request_relationship.html', 'r').read().replace('\n', ''),
            slug='default_request_relationship_email',
            num_of_retries=3,
            is_html=True,
            category=request_relationships_category,
            context_note=request_relationships_context,
        ).save()
        reject_relationships_context = context_description + '''
        1. {{ server_url }} - Server address. You must specify this tag before each url,
        like {{ server_url }}/admin - link to admin page.
        2. {{ code }} - Invite Code.
        3. {{ start_user }} - User that created the invitation.
        4. {{ start_user.first_name }} - User that created the invitation first name.
        5. {{ start_user.last_name }} - User that created the invitation last name.
        6. {{ start_user.email }} - User that created the invitation email.
        7. {{ end_user }} - Invited user.
        8. {{ end_user.first_name }} - Invited user first name.
        9. {{ end_user.last_name }} - Invited user last name.
        10. {{ end_user.email }} - Invited user email.
        11. {{ subject }} - Default subject.
        '''
        MailTemplate(
            name='Default reject relationship email',
            subject='Roommate Request Rejected!',
            message=open('website/templates/email/reject_relationship.html', 'r').read().replace('\n', ''),
            slug='default_reject_relationship_email',
            num_of_retries=3,
            is_html=True,
            category=reject_relationships_category,
            context_note=reject_relationships_context,
        ).save()
        reminder_context = context_description + '''
        1. {{ server_url }} - Server address. You must specify this tag before each url,
        like {{ server_url }}/admin - link to admin page.
        2. {{ property }} - Property object.
        3. {{ user }} - User object.
        4. {{ invite }} - Invite object.
        5. {{ invite.invite_code }} - Invite Code.
        6. {{ invite.expire_date }} - Invite expiration date. You can use formatting for it (For example: {{ invite.expire_date|date:"F d, Y" }})
        7. {{ user.first_name }} - User First Name.
        8. {{ user.last_name }} - User Last Name.
        9. {{ user.email }} - User Email.
        10. {{ property.name }} - Property Name.
        11. {{ logo_url }} - Url to property Logo.
        12. {{ year }} - Current Year.
        13. {{ company }} - Company Name.
        14. {{ subject }} - Default Subject.
        '''
        MailTemplate(
            name='Default account reminder email',
            subject='Account Details!',
            message=open('website/templates/email/account_reminder.html', 'r').read().replace('\n', ''),
            slug='default_account_reminder_email',
            num_of_retries=2,
            is_html=True,
            category=reminder_category,
            context_note=reminder_context,
        ).save()
        messages_notifications_context = context_description + '''
        1. {{ server_url }} - Server address. You must specify this tag before each url,
        like {{ server_url }}/admin - link to admin page.
        2. {{ code }} - Invite Code.
        3. {{ to_user }} - Message Recipient.
        4. {{ to_user.first_name }} - Message Recipient's First Name.
        5. {{ to_user.last_name }} - Message Recipient's Last Name.
        6. {{ to_user.email }} - Message Recipient Email.
        7. {{ from_user }} - Sender.
        8. {{ from_user.first_name }} - Sender's First Name.
        9. {{ from_user.last_name }} - Sender's Last Name.
        10. {{ from_user.email }} - Sender's Email.
        11. {{ subject }} - Default Subject.
        '''
        MailTemplate(
            name='Default messages notifications email',
            subject='New message',
            message=open('website/templates/email/in_app_message.html', 'r').read().replace('\n', ''),
            slug='default_messages_notifications_email',
            num_of_retries=3,
            is_html=True,
            category=messages_notifications_category,
            context_note=messages_notifications_context,
        ).save()


    dependencies = [
        ('management', '0011_fix_default_expire_date'),
    ]

    operations = [
        migrations.RunPython(add_default_templates),
    ]
