var initSocialNetworksTable = function(listUrl, updateUrl,propertyId) {
    $('#social-networks-table').on('preXhr.dt', function () {
        $('tr').attr('hidden', true);
        App.blocks('#social-networks-table-block', 'refresh_toggle');
    });
    $('#social-networks-table').on('xhr.dt', function () {
        $('tr').attr('hidden', false);
        App.blocks('#social-networks-table-block', 'refresh_toggle');
    });
    $('#social-networks-table').DataTable({
        initComplete: function () {
            var api = this.api();
            $('#social-networks-table_filter input')
                .off('.DT')
                .on('keyup.DT', function (e) {
                    if (e.keyCode == 13) {
                        api.search(this.value).draw();
                    }
                });
        },
        "serverSide": true,
        "processing": true,
        "bSort": true,
        "bPaginate": true,
        "sPaginationType": "full_numbers",
        "iDisplayLength": 10,
        ajax: {
            "dataType": 'json',
            url: listUrl,
            "dataSrc": function (json) {
                return json.data
            }
        },
        "aoColumns": [
            {
                "mDataProp": "network_name",
                "render": function (data, type, full, meta) {
                    var update_url = '<a href="' + updateUrl + full.id + '">' + data + '</a>';
                    return update_url;
                }
            },
            {
                "mDataProp": "delete",
                "render": function (data, type, full, meta) {
                    return '<div class="management-action" onclick="deleteSocialNetwork(' + full.id + ', \'' + full.network_name + '\', ' + propertyId + ')">' +
                        'Delete <i class="si si-trash"></i>' +
                        '</div>'
                },
                "orderable": false
            }

        ],
        paging: true,
        "lengthMenu": [[10, 25, 50, 100], [10, 25, 50, 100]]
    });
};

var deleteSocialNetwork = function (templateId, templateName, propertyId) {
        var table = $('#social-networks-table').DataTable();
        var url = '/management/social_networks/' + propertyId + '/' + templateId + '/delete';
        swal({
                title: "Are you sure?",
                text: "Are you sure that you want to delete " + templateName,
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: "Yes, delete it!",
                cancelButtonText: "No, cancel!",
                closeOnConfirm: false
            },
            function (isConfirm) {
                if (isConfirm) {
                    $.ajax({
                        url: url,
                        type: 'DELETE',
                        success: function (data) {
                            swal("Done!", data, "success");
                            table.draw('page');
                        },
                        error: function (jqXHR, textStatus, errorThrown) {
                            swal("Oops...", "Unable to delete this Social Network!", "error");
                        }
                    });
                }
            });
    };
