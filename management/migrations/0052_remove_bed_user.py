# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('management', '0051_auto_20161026_0336'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='bed',
            name='user',
        ),
    ]
