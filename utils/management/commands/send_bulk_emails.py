from datetime import datetime

from django.core.management.base import BaseCommand
from django.conf import settings
from django.utils.translation import activate
from boto.exception import BotoServerError
from dbmail import send_db_mail

from management.models import Invitation


class Command(BaseCommand):
    help = 'Send Emails'

    def add_arguments(self, parser):
        # Positional arguments

        # Named (optional) arguments
        parser.add_argument('--debug',
                            action='store_true',
                            dest='debug',
                            default=False,
                            help='Debug')

        parser.add_argument('--property_id',
                            action='store',
                            dest='property_id',
                            help='Property id')

        # parser.add_option('--pythonpath',
        #     help='A directory to add to the Python path, e.g. "/home/djangoprojects/myproject".'),

        parser.add_argument('--to_email',
                            action="store",
                            # type="string",
                            dest="email")

    def handle(self, *args, **options):
        activate('en')
        index = 0
        print options

        invitations = Invitation.objects.select_related('agreement_period__property').select_related('user__profile')\
            .prefetch_related('agreement_period__property__managers')\
            .filter(agreement_period__property__enable_email_sending=True).all()

        property_id = options.get('property_id', None)
        if property_id:
            invitations = invitations.filter(agreement_period__property_id=property_id)
        print invitations.count()
        debug = options.get('debug')
        email = options.get('email')
        print(debug)
        print(email)
        if debug:
            invitations = invitations[:10]
        for inv in invitations:
            property = inv.agreement_period.property
            welcome_email = property.welcome_email
            if not welcome_email:
                continue
            context = {
                'subject': 'Welcome to {} roommate matching'.format(inv.agreement_period.property),
                'invite': inv,
                'user': inv.user,
                'logo_url': property.logo.url,
                'property': property,
                'server_url': settings.SERVER_URL,
                'year': datetime.now().strftime('%Y'),
                'company': inv.agreement_period.property.name,
            }
            email_to = inv.user.email
            if debug:
                email_to = email or settings.DEFAULT_FROM_EMAIL
            try:
                send_db_mail(welcome_email.slug, email_to, context)
            except BotoServerError as e:
                print e
