# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('management', '0055_added_base_stages'),
    ]

    operations = [
        migrations.AddField(
            model_name='property',
            name='current_stage',
            field=models.ForeignKey(default=1, to='management.Stage'),
        ),
    ]
