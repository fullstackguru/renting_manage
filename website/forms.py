from django import forms
from django.forms import ValidationError
from django.contrib.auth.forms import ReadOnlyPasswordHashField
from django.contrib.auth import get_user_model
from django.db.models.fields import BLANK_CHOICE_DASH
from website.models import UserProfile
User = get_user_model()


class UserCreationForm(forms.ModelForm):
    """A form for creating new users. Includes all the required
    fields, plus a repeated password."""
    password1 = forms.CharField(label='Password', widget=forms.PasswordInput)
    password2 = forms.CharField(label='Password confirmation', widget=forms.PasswordInput)

    class Meta:
        model = User
        fields = ('email', 'first_name', 'last_name', 'gender', 'available_properties', 'allow_co_educational_groups')

    def clean_password2(self):
        # Check that the two password entries match
        password1 = self.cleaned_data.get("password1")
        password2 = self.cleaned_data.get("password2")
        if password1 and password2 and password1 != password2:
            raise forms.ValidationError("Passwords don't match")
        return password2

    def save(self, commit=True):
        # Save the provided password in hashed format
        user = super(UserCreationForm, self).save(commit=False)
        user.set_password(self.cleaned_data["password1"])
        if commit:
            user.save()
        return user


class UserChangeForm(forms.ModelForm):
    """A form for updating users. Includes all the fields on
    the user, but replaces the password field with admin's
    password hash display field.
    """
    password = ReadOnlyPasswordHashField()

    class Meta:
        model = User
        fields = ('email', 'password', 'is_active', 'is_admin', 'first_name', 'last_name', 'gender',
                  'available_properties', 'allow_co_educational_groups')

    def clean_password(self):
        # Regardless of what the user provides, return the initial value.
        # This is done here, rather than on the field, because the
        # field does not have access to the initial value
        return self.initial["password"]


class ManagerUserCreationForm(forms.ModelForm):
    is_manager = forms.BooleanField(initial=False, required=False, label='Manager')
    password1 = forms.CharField(label='Password', required=False,
                                widget=forms.PasswordInput(attrs={'class': 'form-control'}))
    password2 = forms.CharField(label='Password confirmation', required=False,
                                widget=forms.PasswordInput(attrs={'class': 'form-control'}))

    class Meta:
        model = User
        fields = ('email', 'first_name', 'last_name', 'gender', 'password1', 'password2', 'is_manager')
        widgets = {
            'email': forms.TextInput(attrs={'class': 'form-control'}),
            'first_name': forms.TextInput(attrs={'class': 'form-control'}),
            'last_name': forms.TextInput(attrs={'class': 'form-control'}),
            'gender': forms.Select(attrs={'class': 'form-control'}),
        }

    def __init__(self, *args, **kwargs):
        self.property = kwargs.pop('property', None)
        super(ManagerUserCreationForm, self).__init__(*args, **kwargs)

    def clean_password2(self):
        # Check that the two password entries match
        password1 = self.cleaned_data.get("password1")
        password2 = self.cleaned_data.get("password2")
        if password1 and password2 and password1 != password2:
            raise forms.ValidationError("Passwords don't match")
        return password2

    def clean(self):
        if not self.cleaned_data['password1'] and self.cleaned_data['is_manager']:
            self.add_error('password1', 'You must provide password for managers')
        return super(ManagerUserCreationForm, self).clean()

    def save(self, commit=True):
        # Save the provided password in hashed format
        user = super(ManagerUserCreationForm, self).save(commit=False)
        if self.cleaned_data['is_manager']:
            user.set_password(self.cleaned_data["password1"])
        else:
            user.set_unusable_password()
        if commit:
            user.save()
        if self.property:
            user.available_properties.add(self.property)
            if self.cleaned_data['is_manager']:
                self.property.managers.add(user)
        self.save_m2m()
        return user


class ManagerUserChangeForm(forms.ModelForm):
    class Meta:
        model = User
        fields = ('email', 'first_name', 'last_name', 'gender', 'points')
        widgets = {
            'email': forms.TextInput(attrs={'class': 'form-control'}),
            'first_name': forms.TextInput(attrs={'class': 'form-control'}),
            'last_name': forms.TextInput(attrs={'class': 'form-control'}),
            'gender': forms.Select(attrs={'class': 'form-control'}),
            'points': forms.NumberInput(attrs={'class': 'form-control'}),
        }


class UserInformationForm(forms.ModelForm):
    gender = forms.ChoiceField(choices=BLANK_CHOICE_DASH + list(User.GENDER_CHOICES[1:]),
                               widget=forms.Select(attrs={'class': 'form-control required required-select first-tab-element'}))

    class Meta:
        model = User
        fields = ('email', 'first_name', 'last_name', 'gender', 'allow_co_educational_groups', 'hidden',
                  'disable_emails')
        widgets = {
            'email': forms.TextInput(attrs={'class': 'form-control required first-tab-element'}),
            'first_name': forms.TextInput(attrs={'class': 'form-control required first-tab-element'}),
            'last_name': forms.TextInput(attrs={'class': 'form-control required first-tab-element'}),
            'allow_co_educational_groups': forms.CheckboxInput(attrs={'class': 'form-control first-tab-element'}),
            'hidden': forms.CheckboxInput(attrs={'class': 'form-control first-tab-element'}),
            'disable_emails': forms.CheckboxInput(attrs={'class': 'form-control first-tab-element'}),
        }


class UserProfileInformationForm(forms.ModelForm):
    class Meta:
        model = UserProfile
        fields = ('twitter_url', 'facebook_url', 'pinterest_url', 'linkedin_url', 'instagram_url',
                  'snapchat_url', 'google_plus_url')
        widgets = {
            'twitter_url': forms.TextInput(attrs={'class': 'form-control url-field first-tab-element'}),
            'facebook_url': forms.TextInput(attrs={'class': 'form-control url-field first-tab-element'}),
            'pinterest_url': forms.TextInput(attrs={'class': 'form-control url-field first-tab-element'}),
            'linkedin_url': forms.TextInput(attrs={'class': 'form-control url-field first-tab-element'}),
            'instagram_url': forms.TextInput(attrs={'class': 'form-control url-field first-tab-element'}),
            'snapchat_url': forms.TextInput(attrs={'class': 'form-control url-field first-tab-element'}),
            'google_plus_url': forms.TextInput(attrs={'class': 'form-control url-field first-tab-element'}),
        }


class UserBioForm(forms.ModelForm):
    class Meta:
        model = UserProfile
        fields = ('bio', )
        widgets = {
            'bio': forms.Textarea(attrs={'class': 'form-control', 'style': 'height: 300px;'}),
        }
