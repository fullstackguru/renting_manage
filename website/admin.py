from django.contrib import admin
from django.contrib.auth.models import Group
from django.contrib.auth.admin import UserAdmin as BaseUserAdmin
from django.views.generic.base import TemplateView
from django.db.models import Q
from django.utils.timezone import datetime
from website.models import \
    Relationship, QuizResult, UserProfile, UserConversation, ConversationMessage, SocialNetworks, \
    UserOrganizationSocialHabitAnswers
from website.forms import UserChangeForm, UserCreationForm
from website.filters import LastLoginListFilter, IsTakeQuizFilter, WithInvitesFilter, UserByAgreementPeriodFilter
from management.models import Invitation
from utils.emails import send_welcome_email, send_account_reminder_email
from django.contrib.auth import get_user_model
User = get_user_model()


class UserAdmin(BaseUserAdmin):
    # The forms to add and change user instances
    form = UserChangeForm
    add_form = UserCreationForm

    # The fields to be used in displaying the User model.
    # These override the definitions on the base UserAdmin
    # that reference specific fields on auth.User.
    list_display = ('email', 'is_admin')
    list_filter = ('is_admin', LastLoginListFilter, UserByAgreementPeriodFilter, IsTakeQuizFilter, WithInvitesFilter)
    fieldsets = (
        (None, {'fields': ('email', 'password')}),
        ('User Info', {'fields': ('first_name', 'last_name', 'gender', 'allow_co_educational_groups', 'last_login')}),
        ('Properties', {'fields': ('available_properties',)}),
        ('Permissions', {'fields': ('is_admin',)}),
    )
    # add_fieldsets is not a standard ModelAdmin attribute. UserAdmin
    # overrides get_fieldsets to use this attribute when creating a user.
    add_fieldsets = (
        (None, {
            'classes': ('wide',),
            'fields': ('email', 'password1', 'password2')}
         ),
        ('Properties', {'fields': ('available_properties',)}),
        ('User Info', {'fields': ('first_name', 'last_name', 'gender', 'allow_co_educational_groups', 'last_login')}),
    )
    search_fields = ('email', 'first_name', 'last_name', 'id')
    ordering = ('email',)
    readonly_fields = ('last_login', )
    filter_horizontal = ()
    actions = ['resend_invite_email', 'send_reminder_email']

    def resend_invite_email(self, request, queryset):
        user_ids = queryset.values_list('id', flat=True)
        invites = Invitation.objects.filter(user_id__in=user_ids).exclude(expire_date__lt=datetime.now())
        for invite in invites:
            send_welcome_email(invite)
        self.message_user(request, "Resent welcome email for %s users." % queryset.count())
    resend_invite_email.short_description = "Resend welcome email for selected users"

    def send_reminder_email(self, request, queryset):
        user_ids = queryset.values_list('id', flat=True)
        invites = Invitation.objects.filter(user_id__in=user_ids).exclude(expire_date__lt=datetime.now())
        for invite in invites:
            send_account_reminder_email(invite)
        self.message_user(request, "Sent reminder emails for %s users." % queryset.count())
    send_reminder_email.short_description = "Send reminder email"


class RelationshipAdmin(admin.ModelAdmin):
    list_display = ('start_user', 'end_user', 'invitation', 'invite_datetime', 'end_user_approved', 'approval_datetime',
                    'via_user', 'is_roommate', 'read_only')
    search_fields = ('start_user__email', 'end_user__email')
    list_filter = ('end_user_approved', )
    readonly_fields = ('via_user', )
    exclude = ('read_only', )

    def save_model(self, request, obj, form, change):
        obj.read_only = True
        obj.save()


class QuizResultAdmin(admin.ModelAdmin):
    list_display = ('user', 'traitify_assessment_id')
    search_fields = ('user__email', 'user__first_name', 'user__last_name')


class SocialNetworksAdmin(admin.TabularInline):
    model = SocialNetworks


class UserOrganizationSocialHabitAnswersAdmin(admin.TabularInline):
    model = UserOrganizationSocialHabitAnswers
    exclude = ('organization_invitation', 'organization_question', 'organization_answer', 'sort_order')
    readonly_fields = ('organization_question_text', 'organization_answer_text')
    ordering = ('sort_order', )
    extra = 0

    def has_add_permission(self, request, obj=None):
        return False

    def has_delete_permission(self, request, obj=None):
        return False


class UserProfileAdmin(admin.ModelAdmin):
    exclude = ('personality_types', 'personality_traits')
    inlines = (SocialNetworksAdmin, ) #  UserOrganizationSocialHabitAnswersAdmin)
    list_display = ('user', )
    search_fields = ('user__email', 'user__first_name', 'user__last_name')


class ConversationMessageAdmin(admin.TabularInline):
    model = ConversationMessage
    readonly_fields = ('user', 'message', 'unread', 'created_at')
    extra = 0

    def has_add_permission(self, request, obj=None):
        return False

    def has_change_permission(self, request, obj=None):
        return False

    def has_delete_permission(self, request, obj=None):
        return False


class UserConversationAdmin(admin.ModelAdmin):
    inlines = (ConversationMessageAdmin, )

    list_display = ('conversation_uuid', 'user1', 'user2', 'created_at')
    exclude = ('conversation_uuid', 'user1', 'user2', )

    def has_add_permission(self, request, obj=None):
        return False

    def has_change_permission(self, request, obj=None):
        return False


class GroupReportAdmin(TemplateView):
    template_name = 'admin/group-list.html'

    def get_context_data(self, **kwargs):
        context = super(GroupReportAdmin, self).get_context_data(**kwargs)
        admin_context = admin.site.each_context(self.request)
        context.update(admin_context)
        groups = []
        users_with_relations = set()

        relations = Relationship.objects.all().filter(end_user_approved=True).exclude(approval_datetime__isnull=True)
        for r in relations:
            users_with_relations.add(r.start_user_id)
            users_with_relations.add(r.end_user_id)

        user = User.objects.filter(id__in=users_with_relations)

        excluded_users = set()
        for u in user:
            if u in excluded_users:
                continue
            relations = Relationship.objects.filter(end_user_approved=True).exclude(approval_datetime__isnull=True).\
                filter(Q(start_user=u) | Q(end_user=u)).select_related('start_user').select_related('end_user').\
                select_related('invitation').select_related('invitation__agreement_period').\
                select_related('invitation__agreement_period__property').select_related('invitation__unit_type')
            groups.append(relations)
            for r in relations:
                excluded_users.add(r.start_user)
                excluded_users.add(r.end_user)
        context['groups'] = groups

        return context


admin.site.register_view('group_report', 'Group Report', view=GroupReportAdmin.as_view())

admin.site.register(UserProfile, UserProfileAdmin)
admin.site.register(Relationship, RelationshipAdmin)
admin.site.register(QuizResult, QuizResultAdmin)
admin.site.register(UserConversation, UserConversationAdmin)
# Now register the new UserAdmin...
admin.site.register(User, UserAdmin)
# ... and, since we're not using Django's built-in permissions,
# unregister the Group model from admin.
admin.site.unregister(Group)

from djcelery.models import (TaskState, WorkerState,
                 PeriodicTask, IntervalSchedule, CrontabSchedule)

admin.site.unregister(TaskState)
admin.site.unregister(WorkerState)
admin.site.unregister(IntervalSchedule)
admin.site.unregister(CrontabSchedule)
admin.site.unregister(PeriodicTask)
