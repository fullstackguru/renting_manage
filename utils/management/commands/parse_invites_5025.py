from optparse import make_option
import csv
from datetime import datetime

from django.core.management.base import BaseCommand, CommandError
from django.utils.translation import activate
from django.contrib.auth import get_user_model
from django.core.validators import validate_email
from django.core.exceptions import ValidationError
User = get_user_model()
from management.models import Organization, Property, AssignmentPeriod, ContractType, Invitation, UnitType, RoomType
from website.models import User, UserManager


class Command(BaseCommand):
    help = 'Parses records from file and creates user invites from them.'

    option_list = BaseCommand.option_list + (
        make_option('--file',
                    action="store",
                    type="string",
                    dest="path",
                    help='Path to file with data'
                    ),
        )

    def handle(self, *args, **options):
        activate('en')
        print options
        path = options.get('path') or ''
        print path
        if not path:
            raise CommandError('Please set filename!')
        with open(path, 'rU') as csvfile:
            reader = csv.reader(csvfile, delimiter=',', quotechar='|')
            for index, row in enumerate(reader):
                if not index:
                    continue
                print row
                organization_name = row[0].strip()
                property_name = row[1].strip()
                agreement_period_name = row[2].strip()
                agreement_period_extra = {
                    'start_date': datetime.strptime(row[3], '%x'),
                    'end_date': datetime.strptime(row[4], '%x')
                }
                user_email = row[7].lower()
                user_extra = {
                    'first_name': row[5],
                    'last_name': row[6]
                }
                unit_type_name = row[8].strip()
                contract_type_name = row[9].strip()

                org, _ = Organization.objects.get_or_create(name=organization_name)
                prop, _ = Property.objects.get_or_create(name=property_name, organization=org)
                as_per, _ = AssignmentPeriod.objects.update_or_create(name=agreement_period_name, property=prop,
                                                                      defaults=agreement_period_extra)
                unit_type, _ = UnitType.objects.get_or_create(name=unit_type_name, property=prop)
                contract_type, _ = ContractType.objects.get_or_create(name=contract_type_name)
                user_email = UserManager.normalize_email(user_email)
                try:
                    validate_email(user_email)
                except ValidationError as e:
                    print(e)
                    continue
                user, user_created = User.objects.get_or_create(email=user_email, defaults=user_extra)
                if user_created:
                    user.set_unusable_password()
                    user.save()
                user.available_properties.add(prop)
                invite, invite_created = Invitation.objects.update_or_create(
                    user=user, agreement_period=as_per, defaults={
                        'invitation_type': 'A',
                        'unit_type': unit_type,
                        'contract_type': contract_type,
                        'expire_date': agreement_period_extra['end_date'],
                    }
                )
