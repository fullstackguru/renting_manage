# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('website', '0026_remove_relations_with_different_room_types'),
    ]

    operations = [
        migrations.RenameField(
            model_name='relationship',
            old_name='disable_deleting',
            new_name='read_only',
        ),
    ]
