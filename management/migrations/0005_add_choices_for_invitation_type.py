# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('management', '0004_property_enable_email_sending'),
    ]

    operations = [
        migrations.AlterField(
            model_name='assignmentperiod',
            name='property',
            field=models.ForeignKey(related_name='assignment_period', to='management.Property'),
        ),
        migrations.AlterField(
            model_name='invitation',
            name='invitation_type',
            field=models.CharField(default=b'M', max_length=250, choices=[(b'M', b'Manual'), (b'A', b'Auto')]),
        ),
    ]
