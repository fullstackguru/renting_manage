# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('management', '0005_add_choices_for_invitation_type'),
    ]

    operations = [
        migrations.AlterField(
            model_name='invitation',
            name='room_type',
            field=models.ForeignKey(blank=True, to='management.RoomType', null=True),
        ),
    ]
