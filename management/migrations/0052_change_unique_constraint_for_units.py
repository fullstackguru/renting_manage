# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('management', '0051_unlock_unfilled_group'),
    ]

    operations = [
        migrations.AlterField(
            model_name='unit',
            name='name',
            field=models.CharField(max_length=250),
        ),
        migrations.AlterUniqueTogether(
            name='unit',
            unique_together=set([('name', 'unit_type')]),
        ),
    ]
