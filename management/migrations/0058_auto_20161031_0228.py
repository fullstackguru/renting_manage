# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('management', '0057_add_related_name_to_property_stage'),
    ]

    operations = [
        migrations.AddField(
            model_name='stage',
            name='max_points',
            field=models.PositiveSmallIntegerField(default=100),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='stage',
            name='min_points',
            field=models.PositiveSmallIntegerField(default=10),
            preserve_default=False,
        ),
    ]
