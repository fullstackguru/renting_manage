# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
from django.conf import settings


class Migration(migrations.Migration):

    dependencies = [
        ('website', '0007_assign_user_to_properties'),
    ]

    operations = [
        migrations.AddField(
            model_name='relationship',
            name='via_user',
            field=models.ForeignKey(default=None, blank=True, to=settings.AUTH_USER_MODEL, null=True),
        ),
        migrations.AlterField(
            model_name='user',
            name='available_properties',
            field=models.ManyToManyField(help_text=b'Only selected properties can send invites for this user.', related_name='assigned_users', to='management.Property'),
        ),
    ]
