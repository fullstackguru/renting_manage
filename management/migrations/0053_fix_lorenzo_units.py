# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    def fix_lorenzo_units(apps, schema_editor):
        Unit = apps.get_model("management", "Unit")
        Property = apps.get_model("management", "Property")
        PendingAssignment = apps.get_model("management", "PendingAssignment")
        Invitation = apps.get_model("management", "Invitation")
        lorenzo = Property.objects.filter(name__in=["The Lorenzo", "DobieTwenty21"]).first()
        if not lorenzo:
            return
        lorenzo_users_ids = Invitation.objects.filter(agreement_period__property=lorenzo).values_list('user', flat=True)
        lorenzo_assignments = PendingAssignment.objects.filter(user_id__in=lorenzo_users_ids).\
            select_related('unit', 'unit__unit_type').iterator()
        for assignment in lorenzo_assignments:
            if assignment.unit.unit_type.property != lorenzo:
                unit = assignment.unit
                print unit
                new_unit, created = Unit.objects.get_or_create(
                    name=unit.name,
                    unit_type=assignment.invite.unit_type,
                    max_rooms=unit.max_rooms
                )
                print new_unit.name, created
                assignment.unit = new_unit
                assignment.save()

    dependencies = [
        ('management', '0052_change_unique_constraint_for_units'),
    ]

    operations = [
        migrations.RunPython(fix_lorenzo_units)
    ]
