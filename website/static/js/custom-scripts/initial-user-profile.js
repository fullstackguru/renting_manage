$(document).ready(function () {
    var showModal = false;
    var unitList = undefined;
    var selectedUnit = undefined;

    function initStartUpUserProfileModal() {
        var $userProfileModal = $('#initial-user-profile');
        var userId = $userProfileModal.attr('user-id');
        if (userId) {
            showModal = true;
        }
        var url = "/user-profile-ajax/" + userId;

        $userProfileModal.on('show.bs.modal', function (e) {
            var $modalContent = $(this).find('.modal-content');
            var $spinner = $modalContent.find('.spinner-holder');

            $spinner.show();

            $.get(url).done(function (htmlResp) {
                $spinner.hide();
                $modalContent.append(htmlResp);
            });

        });

        $userProfileModal.on('hidden.bs.modal', function () {
            var $modalContent = $(this).find('.modal-content');
            $modalContent.children('.peer-block').remove();
        });
    }

    initStartUpUserProfileModal();
    if (showModal) {
        $('#initial-user-profile').modal({backdrop: 'static'});
    }

    var initUnitSelect = function (groupSize) {
        var data = groupSize ? {size: groupSize} : null;
        var formElement = $('#select-unit-bed-form');
        var unitElement = $('p.profile-details').find('span.user-unit');
        var loader = formElement.find('div.spinner-holder');
        var content = formElement.find('div.unit-bed-content');
        $('#select-group-size').val('');
        if (loader.hasClass('hide')) loader.removeClass('hide');
        if (!content.hasClass('hide')) content.addClass('hide');
        $.ajax({
            type: 'GET',
            url: '/select_unit/',
            data: data,
            success: function (units) {
                loader.addClass('hide');
                content.removeClass('hide');
                $('#units-select').children().remove();
                unitList = units;
                units.forEach(function (unit) {
                    $('<option/>').val(unit.id).html(unit.name).appendTo('#units-select');
                });
                if (units.length > 0) {
                    selectedUnit = units[0];
                    initBedSelect(units[0].name);
                }
            },
            error: function (jqXHR, textStatus, errorThrown) {
                loader.addClass('hide');
                content.removeClass('hide');
                $.notify({
                    message: 'Failed to query with group size!',
                    icon: 'fa fa-times'
                }, {
                    type: 'danger',
                    z_index: 1051,
                    delay: 2000
                });
                initUnitSelect();
            }
        })
    };

    var checkUnitOccupation = function () {
        var formData = $('#select-unit-bed-form').serialize();
        $.ajax({
            type: 'POST',
            url: '/select_unit/',
            data: formData,
            success: function (unit) {
            },
            error: function (jqXHR, textStatus, errorThrown) {
                $.notify({
                    message: 'Opps, this unit occupied!',
                    icon: 'fa fa-times'
                }, {
                    type: 'danger',
                    z_index: 1051,
                    delay: 2000
                });
                initUnitSelect();
            }
        });
    };

    var initBedSelect = function (unit) {
        var selectedIndex = unitList.map(function (e) {
            return e.name;
        }).indexOf(unit);
        // checkUnitOccupation(unitList[selectedIndex]);
        $.ajax({
            type: 'GET',
            url: '/select_bed/',
            data: {
                unit: unitList[selectedIndex].id
            },
            success: function (rooms) {
                var logo = $("<div></div>").addClass("push-15");
                logo.append($("<i></i>").addClass("fa fa-bed fa-2x"));
                $('#beds-select').children().remove();
                rooms.forEach(function (room) {
                    var div = $('<div></div>').addClass('row');
                    var block = $("<div></div>").addClass("block col-lg-3");
                    var ribbon = $("<div></div>").addClass("block-content block-content-full ribbon ribbon-modern ribbon-info");
                    var ribbonTitle = $("<div></div>").addClass("ribbon-box font-w600");
                    ribbonTitle.text("Room " + room.name);
                    ribbon.append(ribbonTitle);
                    block.append(ribbon);
                    var bedContainer = $("<div></div>").addClass("row items-push-2x text-center");
                    var numOfDoubleBeds = _.filter(room.beds, function (bed) {
                        return bed.contract_type && bed.contract_type.occupied_places > 1;
                    }).length;
                    var skipped = 0;
                    var beds = room.map(function (bed) {
                        // Render Bed
                        var colClass = "col-lg-" + Math.round(12 / (room.length - numOfDoubleBeds));
                        var div = $("<div></div>").addClass(colClass + " block");
                        div.css("margin-bottom", 0);
                        div.attr('data-bed-info', JSON.stringify({'bed': bed}));
                        var content = $("<div></div>").addClass("block-content block-content-full text-center");
                        var text = $("<div></div>").addClass("h6 text-muted").append(bed.name);
                        var current_logo = logo.clone();
                        if (bed.user) {
                            current_logo = current_logo.addClass("text-muted");
                            var msg = "Occupied<br />";
                            msg += "User: " + bed.user.first_name + " " + bed.user.last_name + " (" + bed.user.email + ")<br />";
                            msg += "Gender: " + bed.user.gender + " <br />";
                            /*msg += "Contract Type: " + bed.contract_type.name + "<br />";
                            msg += "Occupied " + bed.contract_type.occupied_places + " beds.<br />";
                            if (bed.user_stage) {
                                msg += "Stage: " + bed.user_stage;
                            }*/
                            div.attr("data-content", msg);
                        } else {
                            if (skipped < numOfDoubleBeds) {
                                skipped++;
                                return;
                            }
                            if (!bed.is_available) {
                                current_logo = $("<div></div>").addClass("push-15 text-danger");
                                current_logo.append($("<i></i>").addClass("fa fa-lock fa-2x"));
                                div.attr("data-content", "Blocked");
                            } else {
                                current_logo = current_logo.addClass("text-primary");
                                div.attr("data-content", "Empty");
                                content.addClass('available-bed');
                                content.attr('data-id', bed.id);
                            }
                        }
                        content.popover({
                            title: 'Status',
                            trigger: 'hover',
                            placement: "bottom",
                            html: "true",
                            content: div.attr("data-content")
                        });
                        content.append(current_logo);
                        content.append(text);
                        div.append(content);
                        return div;
                    });
                    bedContainer.append(beds);
                    block.append(bedContainer);
                    div.append(block);
                    div.appendTo('#beds-select');
                });
                $('div[data-user]').each(function () {
                    var $this = $(this);
                    var user = $this.data('user');
                    $this.popover({
                        title: 'Status',
                        trigger: 'hover',
                        placement: "bottom",
                        html: "true",
                        content: user.first_name + " " + user.last_name
                    });
                });
                $('.available-bed').on('click', function () {
                    $('.available-bed.selected').removeClass('selected');
                    var $this = $(this);
                    $this.addClass('selected');
                });
            }
        })
    };

    $('#select-unit-bed-modal').on('show.bs.modal', function (e) {
        initUnitSelect();
    });

    $('#select-group-size').keypress(function (e) {
        if (e.which == 13) {
            if ($(this).val() && Number($(this).val())) {
                $('#beds-select').children().remove();
                initUnitSelect(Number($(this).val()));
            }
        }
    });

    $('#units-select').change(function (e) {
        var unitName = $(this).find(":selected").text();
        initBedSelect(unitName);
    });

    $('#select-unit-bed-submit').on('click', function () {
        var bedId = $('.available-bed.selected').data('id');
        if (!bedId) {
            return;
        }
        var formData = $('#select-unit-bed-form').serialize();
        formData += '&bed=' + bedId;
        $.ajax({
            type: 'POST',
            url: '/select_bed/',
            data: formData,
            success: function (bed) {
                location.reload();
                $.notify({
                    message: 'Bed has been successfully selected!',
                    icon: 'fa fa-check'
                }, {
                    type: 'success',
                    z_index: 1051,
                    delay: 2000
                });
            },
            error: function (jqXHR, textStatus, errorThrown) {
                $.notify({
                    message: 'Opps, this bed occupied!',
                    icon: 'fa fa-times'
                }, {
                    type: 'danger',
                    z_index: 1051,
                    delay: 2000
                });
                initBedSelect(selectedUnit.name);
            }
        })
    });

    $('#bio_form_submit').on('click', function () {
        var formData = $('#bio_form').serialize();
        var newBio = $("#id_bio").val();
        $.ajax({
            type: 'POST',
            url: 'update-bio/',
            data: formData,
            processData: false,
            contentType: false,
            success: function (data) {
                $("#user_bio").html(newBio);
                $("#bio-modal").modal("hide");
            },
            error: function (jqXHR, textStatus, errorThrown) {
                swal("Oops...", jqXHR.responseText, "error");
            }
        });
    });
});