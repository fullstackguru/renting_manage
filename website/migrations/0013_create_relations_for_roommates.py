# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from datetime import datetime

from django.db import models, migrations
from django.db.models import Q


def update_or_create_relation(apps, start_user, end_user, via_user, invitation, disable_deleting=False):
    if start_user.id == end_user.id:
        return
    Relationship = apps.get_model("website", "Relationship")
    existed_relationship = Relationship.objects.filter(
        Q(start_user=start_user, end_user=end_user) | Q(start_user=end_user, end_user=start_user)
    )
    if existed_relationship:
        existed_relationship.update(approval_datetime=datetime.now(), end_user_approved=True,
                                    disable_deleting=disable_deleting)
    else:
        Relationship.objects.create(
            start_user=start_user, via_user=via_user,
            approval_datetime=datetime.now(),
            end_user=end_user,
            invitation=invitation, end_user_approved=True,
            disable_deleting=disable_deleting
        )


class Migration(migrations.Migration):

    def forwards_func(apps, schema_editor):
        User = apps.get_model("website", "User")
        Bed = apps.get_model("management", "Bed")
        Invitation = apps.get_model("management", "Invitation")
        db_alias = schema_editor.connection.alias
        all_beds = Bed.objects.all()
        for bed in all_beds:
            beds_in_room = Bed.objects.filter(room=bed.room).exclude(id=bed.id)
            for bed_in_room in beds_in_room:
                #  add relations for roommates
                if bed.user and bed_in_room.user:
                    invite = Invitation.objects.filter(user=bed.user).first()
                    if invite:
                        update_or_create_relation(apps, start_user=bed.user, end_user=bed_in_room.user,
                                                               via_user=None, invitation=invite, disable_deleting=True)


    dependencies = [
        ('website', '0012_change_message_field_to_text'),
    ]

    operations = [
        migrations.RunPython(forwards_func),
    ]
