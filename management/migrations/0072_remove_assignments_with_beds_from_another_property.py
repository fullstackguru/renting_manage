# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
from django.db.models import F


class Migration(migrations.Migration):

    def remove_assignments_with_beds_from_another_property(apps, schema_editor):
        PendingAssignment = apps.get_model('management', 'PendingAssignment')
        assignments = PendingAssignment.objects.filter(bed__isnull=False).\
            exclude(bed__room__unit__unit_type__property=F('assignment_period__property'))
        assignments.update(unit=None, room=None, room_type=None, bed=None)

    dependencies = [
        ('management', '0071_auto_20161109_0533'),
    ]

    operations = [
        migrations.RunPython(remove_assignments_with_beds_from_another_property)
    ]
