# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import management.fields


class Migration(migrations.Migration):

    dependencies = [
        ('management', '0053_fix_lorenzo_units'),
    ]

    operations = [
        migrations.CreateModel(
            name='Stage',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=32)),
                ('restrictions', management.fields.ChoiceArrayField(base_field=models.SmallIntegerField(blank=True, null=True, choices=[(0, b'Allow matching only inside unit'), (1, b'Allow matching only with users who have a unit assignment')]), size=None)),
            ],
        ),
    ]
