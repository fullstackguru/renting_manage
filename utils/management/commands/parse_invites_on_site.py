import requests
import json
import sys
import os
from optparse import make_option
from datetime import datetime, timedelta
from time import sleep

from django.core.management.base import BaseCommand, CommandError
from django.contrib.auth import get_user_model
from django.core.validators import validate_email
from django.core.exceptions import ValidationError

from management.models import Organization, Property, AssignmentPeriod, Invitation, UnitType, RoomType, Unit, Room, \
    PendingAssignment, Bed
from website.models import UserManager, UserProfile


COLUMNS = ['property_name', 'lease_id', 'unit_number', 'unit_type', 'lease_status', 'student_first_name',
           'student_last_name', 'student_email', 'lease_start', 'lease_end', 'tenant_signed']
DATE_FORMAT = '%Y-%m-%d'
WAIT_INTERVAL = 10
UNIT_TYPES = ['Corner Quad', 'Double Dobie', 'Longhorn', 'Private Corner', 'Split Quad', 'Texas Corner', 'Texas Double']
User = get_user_model()


def validate(date_text):
    try:
        datetime.strptime(date_text, DATE_FORMAT)
    except ValueError:
        raise CommandError("Incorrect data format, should be YYYY-MM-DD")


class Command(BaseCommand):
    help = 'Retrieve data from on-site API and then parse it.'

    option_list = BaseCommand.option_list + (
        make_option('--start_date',
                    action="store",
                    type="string",
                    dest="start_date",
                    help='Start date for report. By default is yesterday.'
                    ),
        make_option('--end_date',
                    action="store",
                    type="string",
                    dest="end_date",
                    help='End date for report. By default is today.'
                    ),
        )

    def handle(self, *args, **options):
        # Try to obtain access token
        print("Try to obtain access token!")
        oauth_dict = {
            "grant_type": "client_credentials",
            "client_id": "a9cbaf233a7155204112b84d31f069c738c1c5b07bd0723e8822fc2f23ef7bb4",
            "client_secret": "b262bbb609aa445dbfbd331e6d67a618b4e67bba6c3ab9e75d2379cd79ac7062"
        }
        r = requests.post("https://www.on-site.com/oauth/token", data=oauth_dict)
        try:
            r.raise_for_status()
            print("Successfully obtained access token!")
        except requests.HTTPError:
            raise CommandError("Unable to obtain access token: HTTP {}: {}", r.status_code, r.content)
        response_data = json.loads(r.content)
        access_token = response_data["access_token"]

        if options['start_date']:
            start_date = options['start_date']
            validate(start_date)
        else:
            start_date = datetime.now() - timedelta(days=1)
            start_date = datetime.strftime(start_date, DATE_FORMAT)

        if options['end_date']:
            end_date = options['end_date']
            validate(end_date)
        else:
            end_date = datetime.now()
            end_date = datetime.strftime(end_date, DATE_FORMAT)

        # Create report
        print("Send request for report creating!")
        reports_dict = {
            "callback_uri": "",
            "report_request_params": {
                "report_content_type": "JSON",
                "property_id": "197835",
                "sort_field": "property_name",
                "sort_direction": "ASC",
                "sort_grouping": "",
                "show_columns": ','.join(COLUMNS),
                "name": "leasing_progress",
                "useCustomRange": "1",
                "start_date": start_date,
                "end_date": end_date,
                "application_state": "all"
            }
        }
        r = requests.post('https://www.on-site.com/api/reports', data=json.dumps(reports_dict),
                          headers={
                              'Authorization': 'Bearer {}'.format(access_token),
                              'Content-Type': 'application/json',
                          })
        try:
            r.raise_for_status()
        except requests.HTTPError:
            raise CommandError("Unable to create report: HTTP {}: {}".format(r.status_code, r.content))
        report_id = json.loads(r.content)['id']
        print("Successfully created report with id - {}".format(report_id))

        # Retrieve JSON with generated report
        while True:
            sleep(WAIT_INTERVAL)

            # Get report status
            print("Try to get report status!")
            r = requests.get('https://www.on-site.com/api/reports/{}/status'.format(report_id), headers={
                'Authorization': 'Bearer {}'.format(access_token),
            })
            try:
                r.raise_for_status()
            except requests.HTTPError:
                raise CommandError("Unable to get report status: HTTP{}: {}".format(r.status_code, r.content))
            status = json.loads(r.content)['status']
            print("Got status code: {}".format(status))

            if status in ['In Progress', 'Not Started']:
                print("Report aren't ready! Try to download it again in the next {} seconds!".format(WAIT_INTERVAL))
                continue

            elif status == 'Completed':
                print("Try to download report")
                r = requests.get('https://www.on-site.com/api/reports/{}/file'.format(report_id), headers={
                    'Authorization': 'Bearer {}'.format(access_token),
                })
                try:
                    r.raise_for_status()
                except requests.HTTPError:
                    raise CommandError("Unable to download report: HTTP{}: {}".format(r.status_code, r.content))
                data = r.content
                print("Successfully downloaded report!")
                break

            else:
                raise CommandError('Unable to retrieve report from on-site API: {}', r.content)

        data = json.loads(data)
        errors = []

        for item in data:
            if item['lease_status'] != "Approved":
                continue

            # ToDo maybe change open unit for something else
            unit_name = item['unit_number'] if item['unit_number'] not in UNIT_TYPES else 'open'

            # Correct unit type showed only for records with correct unit name,
            # for users in waitlist it splited in 2 tables(unit_type, unit_number)
            unit_type_name = item['unit_type'] if item['unit_number'] not in UNIT_TYPES else "{} {}".format(
                item['unit_number'], item['unit_type'][:-1])
            org_raw = {
                'name': item['property_name'],
            }
            prop_raw = {
                'name': item['property_name'],
            }

            # ToDo Change to correct values
            assigment_period_raw = {
                'name': 'Fall 2016',
                'start_date': datetime(2016, 8, 20),
                'end_date': datetime(2017, 5, 17),
            }
            user_raw = {
                'first_name': item['student_first_name'],
                'last_name': item['student_last_name'],
                'email': item['student_email'],
            }
            unit_raw = {
                'name': unit_name,
            }
            unit_type_raw = {
                'name': unit_type_name,
            }

            # ToDo Change to correct values
            room_type_raw = {
                'name': 'Open Room',
            }

            organization, _ = Organization.objects.get_or_create(**org_raw)
            prop, _ = Property.objects.update_or_create(organization=organization, **prop_raw)
            assigment_period, _ = AssignmentPeriod.objects.get_or_create(property=prop, **assigment_period_raw)
            unit_type, _ = UnitType.objects.get_or_create(property=prop, **unit_type_raw)
            room_type, _ = RoomType.objects.get_or_create(property=prop, **room_type_raw)

            try:
                email = UserManager.normalize_email(user_raw['email'])
                try:
                    validate_email(email)
                except ValidationError as e:
                    print(e)
                    continue
                print email, user_raw['email']
                user, user_created = User.objects.get_or_create(email=email)
                if user_created:
                    user.set_unusable_password()
                    user.save()
                user.available_properties.add(prop)
                user_profile, _ = UserProfile.objects.update_or_create(user=user, defaults={
                    'first_name': user_raw['first_name'], 'last_name': user_raw['last_name']
                })
            except Exception as e:
                exc_type, exc_obj, exc_tb = sys.exc_info()
                fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
                print(user_raw['email'], e)
                print(exc_type, fname, exc_tb.tb_lineno)
                errors.append((user_raw['email'], exc_type, fname, exc_tb.tb_lineno))
                continue

            print 'CREATING/UPDATING INVITE for user: {}'.format(user.email)
            invite, invite_created = Invitation.objects.update_or_create(
                user=user, agreement_period=assigment_period, defaults={
                    'invitation_type': 'A',
                    'unit_type': unit_type,
                    'room_type': room_type
                }
            )
            if not unit_raw.get('name') == 'open':
                unit, unit_created = Unit.objects.update_or_create(unit_type=unit_type, **unit_raw)
                pa, pa_created = PendingAssignment.objects.update_or_create(
                    user=user, assignment_period=assigment_period, invite=invite,
                    defaults={'unit': unit, 'room_type': room_type}
                )
                print pa, pa_created

            for i in errors:
                print i
            print 'Total errors count {}'.format(len(errors))
