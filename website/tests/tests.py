from mock import patch
from datetime import datetime, timedelta
from django.test import TestCase
from django.db.models import Q
from website.models import User, UserProfile, Relationship, Group
from management.models import Invitation, AssignmentPeriod, Property, Bed, Room, Unit, ContractType, PendingAssignment
from website.views import SearchPeersAjaxView
from utils.tests_utils import create_users, create_property_related_objects, mock_send_welcome_email, \
    mock_send_relations_email
from utils.services import get_related_user_ids


class UserRelationshipsTestCase(TestCase):

    @classmethod
    @patch('management.signals.handlers.send_welcome_email', mock_send_welcome_email)
    def setUpClass(cls):
        super(UserRelationshipsTestCase, cls).setUpClass()
        cls.users_number = 10
        create_users(cls.users_number)
        cls.organization, cls.property, cls.unit_type, cls.assignment_period, cls.room_type = \
            create_property_related_objects()
        users = User.objects.all()
        for user in users:
            Invitation.objects.create(
                user=user,
                agreement_period=cls.assignment_period,
                unit_type=cls.unit_type,
            )

    def test_user_creation(self):
        users = User.objects.count()
        self.assertEqual(users, self.users_number)
        profiles = UserProfile.objects.count()
        self.assertEqual(profiles, self.users_number)

    @patch('website.signals.handlers.send_relationship_email', mock_send_relations_email)
    def test_via_user_unapproved_relations(self):
        users = User.objects.all()
        Relationship(
            start_user=users[0],
            invitation=Invitation.objects.get(user=users[0]),
            end_user=users[1],
        ).save()
        Relationship(
            start_user=users[1],
            invitation=Invitation.objects.get(user=users[1]),
            end_user=users[2],
        ).save()
        self.assertEqual(Relationship.objects.count(), 2, msg="Unapproved relations create via user relation")
        for relation in Relationship.objects.all():
            relation.end_user_approved = True
            relation.approval_datetime = datetime.now()
            relation.save()
        self.assertEqual(Relationship.objects.count(), 3,
                         msg="Approving unapproved relation doesn't create via user relations")

    @patch('website.signals.handlers.send_relationship_email', mock_send_relations_email)
    def test_via_user_relationship(self):
        # test simple group relations
        users = User.objects.all()
        Relationship(
            start_user=users[0],
            invitation=Invitation.objects.get(user=users[0]),
            end_user=users[1],
            end_user_approved=True,
            approval_datetime=datetime.now(),
        ).save()
        Relationship(
            start_user=users[1],
            invitation=Invitation.objects.get(user=users[1]),
            end_user=users[2],
            end_user_approved=True,
            approval_datetime=datetime.now(),
        ).save()
        self.assertEqual(Group.objects.count(), 1)
        self.assertEqual(Relationship.objects.count(), 3)
        via_user_relation = Relationship.objects.filter(via_user__isnull=False).first()
        self.assertEqual(via_user_relation.via_user, users[1])
        self.assertIn(via_user_relation.start_user, [users[2], users[0]])
        self.assertIn(via_user_relation.end_user, [users[2], users[0]])
        # create another user group
        Relationship(
            start_user=users[3],
            invitation=Invitation.objects.get(user=users[3]),
            end_user=users[4],
            end_user_approved=True,
            approval_datetime=datetime.now(),
        ).save()
        Relationship(
            start_user=users[4],
            invitation=Invitation.objects.get(user=users[4]),
            end_user=users[5],
            end_user_approved=True,
            approval_datetime=datetime.now(),
        ).save()
        self.assertEqual(Group.objects.count(), 2)
        self.assertEqual(Relationship.objects.count(), 6)
        via_user_relation = Relationship.objects.filter(via_user__isnull=False)[1]
        self.assertEqual(via_user_relation.via_user, users[4])
        self.assertIn(via_user_relation.start_user, [users[5], users[3]])
        self.assertIn(via_user_relation.end_user, [users[5], users[3]])
        # add realtion between groups
        Relationship(
            start_user=users[2],
            invitation=Invitation.objects.get(user=users[2]),
            end_user=users[5],
            end_user_approved=True,
            approval_datetime=datetime.now(),
        ).save()
        direct_relations = Relationship.objects.filter(via_user__isnull=True)
        self.assertEqual(direct_relations.count(), 5, msg="Wrong number of directed relations when merging 2 groups")
        via_user_relations = Relationship.objects.filter(via_user__isnull=False)
        self.assertEqual(via_user_relations.count(), 10, msg="Wrong number of via user relations when merging 2 groups")
        self.assertEqual(Relationship.objects.count(), 15)
        self.assertEqual(Group.objects.count(), 1)
        # test that all users has similar number of users in proposed group
        for user in users[:6]:
            proposed_group_size = Relationship.objects.filter(Q(start_user=user) | Q(end_user=user)).count()
            self.assertEqual(proposed_group_size, 5, msg="Some users hasn't all relations for their group")
        # test that updating of existed relations doesn't create new models
        Relationship.objects.update(approval_datetime=datetime.now())
        self.assertEqual(Relationship.objects.count(), 15, msg="Updating of existed relations creates new relations")
        # test deleting
        relation_for_deleting = Relationship.objects.filter(start_user=users[0], end_user=users[1]).first()
        relation_for_deleting.delete()
        self.assertEqual(Group.objects.count(), 2)
        self.assertEqual(Relationship.objects.count(), 10, msg="Delete method delete wrong number of relations")
        direct_relations = Relationship.objects.filter(via_user__isnull=True)
        self.assertEqual(direct_relations.count(), 4, msg="Delete method delete wrong number of direct relations")
        via_user_relations = Relationship.objects.filter(via_user__isnull=False)
        self.assertEqual(via_user_relations.count(), 6, msg="Delete method delete wrong number of via user relations")
        # test deleting relations between 2 groups
        relation_for_deleting = Relationship.objects.filter(start_user=users[2], end_user=users[5]).first()
        relation_for_deleting.delete()
        self.assertEqual(Relationship.objects.count(), 4,
                         msg="Delete method delete wrong number of relations when removing relation between 2 groups")
        direct_relations = Relationship.objects.filter(via_user__isnull=True)
        self.assertEqual(direct_relations.count(), 3,
                         msg="Delete method delete wrong number of direct relations when removing relation between "
                             "2 groups")
        via_user_relations = Relationship.objects.filter(via_user__isnull=False)
        self.assertEqual(via_user_relations.count(), 1,
                         msg="Delete method delete wrong number of via user relations when removing relation between "
                             "2 groups")

    @patch('website.signals.handlers.send_relationship_email', mock_send_relations_email)
    def test_roommate_relationships(self):
        users = User.objects.all()
        first_roommate_relation = Relationship.objects.create(
            start_user=users[0],
            invitation=Invitation.objects.get(user=users[0]),
            end_user=users[1],
            end_user_approved=True,
            approval_datetime=datetime.now(),
        )
        second_roommate_relation = Relationship.objects.create(
            start_user=users[0],
            invitation=Invitation.objects.get(user=users[0]),
            end_user=users[2],
            end_user_approved=True,
            approval_datetime=datetime.now(),
        )
        Relationship(
            start_user=users[0],
            invitation=Invitation.objects.get(user=users[0]),
            end_user=users[3],
            end_user_approved=True,
            approval_datetime=datetime.now(),
        ).save()
        first_roommate_relation.set_roommate_relationship(Relationship.PROPOSED_BY_END_USER)
        first_roommate_relation.set_roommate_relationship()
        roommates_relations = Relationship.objects.filter(Q(start_user=users[0]) | Q(end_user=users[0]),
                                                          is_roommate=Relationship.DIRECT_ROOMMATE)
        self.assertEqual(first_roommate_relation.is_roommate, Relationship.DIRECT_ROOMMATE)
        self.assertEqual(roommates_relations.count(), 1)
        second_roommate_relation.set_roommate_relationship(Relationship.PROPOSED_BY_START_USER)
        second_roommate_relation.set_roommate_relationship()
        roommates_relations = Relationship.objects.filter(Q(start_user=users[0]) | Q(end_user=users[0]),
                                                          is_roommate=Relationship.DIRECT_ROOMMATE)
        self.assertEqual(second_roommate_relation.is_roommate, Relationship.DIRECT_ROOMMATE)
        self.assertEqual(roommates_relations.count(), 1)

    def test_user_email_processing(self):
        user = User.objects.create(email='SomEEmaiL@YAHOO.COM')
        user.save()
        self.assertEqual(user.email, 'someemail@yahoo.com')

    @patch('management.signals.handlers.send_welcome_email', mock_send_welcome_email)
    def test_co_educational_groups_for_separate_educational_property(self):
        # Note that all created users are mans
        female_user = User.objects.create(email='test_11@mail.com', first_name='Some Name',
                                          last_name='Some Last Name', gender=User.GENDER_FEMALE)
        invite = Invitation.objects.create(user=female_user, agreement_period=self.assignment_period,
                                           unit_type=self.unit_type)
        possible_roommates = SearchPeersAjaxView._get_all_users(invite, [female_user], None)
        self.assertEqual(len(possible_roommates), 0)

    @patch('management.signals.handlers.send_welcome_email', mock_send_welcome_email)
    def test_co_educational_groups_for_co_educational_property(self):
        female_user = User.objects.create(email='test_11@mail.com', first_name='Some Name',
                                          last_name='Some Last Name', gender=User.GENDER_FEMALE,
                                          allow_co_educational_groups=False)
        co_educational_property = Property.objects.create(organization=self.organization,
                                                          name="co-educational property",
                                                          enable_co_educational_groups=True
                                                          )
        assignment_period = AssignmentPeriod.objects.create(property=co_educational_property,
                                                            name="some assignment period for co-ed property",
                                                            start_date=datetime.now(),
                                                            end_date=datetime.now() + timedelta(days=100),
                                                            )
        female_invite = Invitation.objects.create(user=female_user, agreement_period=assignment_period,
                                                  unit_type=self.unit_type)
        new_users = []
        for i in range(20, 30):
            user = User.objects.create(email='test_{}@mail.com'.format(i), first_name=str(i), last_name=str(i),
                                       gender=User.GENDER_MALE)
            new_users.append(user)
            Invitation.objects.create(user=user, agreement_period=assignment_period, unit_type=self.unit_type)
        possible_roommates = SearchPeersAjaxView._get_all_users(female_invite, [female_user], None)
        self.assertEqual(len(possible_roommates), 0)
        female_user.allow_co_educational_groups = True
        female_user.save()
        possible_roommates = SearchPeersAjaxView._get_all_users(female_invite, [female_user], None)
        self.assertEqual(len(possible_roommates), 0)
        user_ids = [u.id for u in new_users[:5]]
        User.objects.filter(id__in=user_ids).update(allow_co_educational_groups=True)
        possible_roommates = SearchPeersAjaxView._get_all_users(female_invite, [female_user], None)
        self.assertEqual(len(possible_roommates), 5)

    @patch('management.signals.handlers.send_welcome_email', mock_send_welcome_email)
    def test_suggested_roommates_from_different_units(self):
        new_user = User.objects.create(email='test_11@mail.com', first_name='Some Name', last_name='Some Last Name',
                                       gender=User.GENDER_MALE)
        invite = Invitation.objects.create(user=new_user, agreement_period=self.assignment_period,
                                           unit_type=self.unit_type)
        possible_roommates = SearchPeersAjaxView._get_all_users(invite, [new_user], None)
        all_users_count = User.objects.all().count() - 1
        self.assertEqual(len(possible_roommates), all_users_count)
        unit = Unit.objects.create(name='test_unit', unit_type=self.unit_type)
        room = Room.objects.create(room_type=self.room_type, unit=unit)
        PendingAssignment.objects.create(user=new_user, unit=unit, room=room, room_type=self.room_type,
                                         assignment_period=self.assignment_period)
        possible_roommates = SearchPeersAjaxView._get_all_users(invite, [new_user], None)
        self.assertEqual(len(possible_roommates), all_users_count)
        all_users = User.objects.all()
        another_unit = Unit.objects.create(name='another_unit', unit_type=self.unit_type)
        another_room = Room.objects.create(room_type=self.room_type, unit=another_unit)
        PendingAssignment.objects.create(user=all_users[0], unit=another_unit, room=another_room,
                                         room_type=self.room_type, assignment_period=self.assignment_period)
        possible_roommates = SearchPeersAjaxView._get_all_users(invite, [new_user], None)
        self.assertEqual(len(possible_roommates), all_users_count - 1)

    @patch('website.signals.handlers.send_relationship_email', mock_send_relations_email)
    def test_get_related_users_ids(self):
        users = set()
        for i in range(50, 55):
            user = User.objects.create(
                email="test_user_{}@mail.com".format(i),
                first_name=str(i),
                last_name=str(i),
                gender=User.GENDER_MALE,
            )
            users.add(user.id)
        new_user = User.objects.create(email='new_email@mail.com', first_name='new name', last_name='last name',
                                       gender=User.GENDER_MALE)
        new_invite = Invitation.objects.create(user=new_user, agreement_period=self.assignment_period,
                                               unit_type=self.unit_type)
        for user_id in users:
            Relationship(start_user=new_user, end_user_id=user_id, invitation=new_invite, end_user_approved=True,
                         approval_datetime=datetime.now()).save()
        users.add(new_user.id)
        self.assertEqual(get_related_user_ids(new_user), users)
