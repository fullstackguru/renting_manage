# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('website', '0017_remove_wrong_social_links'),
    ]

    operations = [
        migrations.AddField(
            model_name='relationship',
            name='is_roommate',
            field=models.CharField(default=b'N', max_length=1, choices=[(b'N', b'Not roommates'), (b'D', b'Direct roommates'), (b'V', b'Roommate via other roommates')]),
        ),
    ]
