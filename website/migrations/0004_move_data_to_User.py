# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
from django.db.models import F


class Migration(migrations.Migration):

    def forwards_func(apps, schema_editor):
        User = apps.get_model("website", "User")
        db_alias = schema_editor.connection.alias
        users = User.objects.using(db_alias).all().select_related('profile')
        for user in users:
            profile = user.profile
            user.first_name = profile.first_name
            user.last_name = profile.last_name
            user.gender = profile.gender
            user.save()

    dependencies = [
        ('website', '0003_create_name_columns_in_User'),
    ]

    operations = [
        migrations.RunPython(forwards_func),
    ]
