var initRoomTypeTable = function(listUrl, updateUrl) {
    $('#room-type-table').on('preXhr.dt', function () {
        $('tr').attr('hidden', true);
        App.blocks('#room-type-table-block', 'refresh_toggle');
    });
    $('#room-type-table').on('xhr.dt', function () {
        $('tr').attr('hidden', false);
        App.blocks('#room-type-table-block', 'refresh_toggle');
    });
    $('#room-type-table').DataTable({
        initComplete: function () {
            var api = this.api();
            $('#room-type-table_filter input')
                .off('.DT')
                .on('keyup.DT', function (e) {
                    if (e.keyCode == 13) {
                        api.search(this.value).draw();
                    }
                });
        },
        "serverSide": true,
        "processing": true,
        "bSort": true,
        "bPaginate": true,
        "sPaginationType": "full_numbers",
        "iDisplayLength": 10,
        ajax: {
            "dataType": 'json',
            url: listUrl,
            "dataSrc": function (json) {
                return json.data
            }
        },
        "aoColumns": [
            {
                "mDataProp": "name",
                "render": function (data, type, full, meta) {
                    var update_url = '<a href="' + updateUrl + full.id + '">' + data + '</a>';
                    return update_url;
                }
            },
            {"mDataProp": "max_occupancy"}

        ],
        paging: true,
        "lengthMenu": [[10, 25, 50, 100], [10, 25, 50, 100]]
    });
};
