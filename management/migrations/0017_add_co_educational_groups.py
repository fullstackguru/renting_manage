# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('management', '0016_add_unique_constraint_to_invitation_model'),
    ]

    operations = [
        migrations.AddField(
            model_name='property',
            name='enable_co_educational_groups',
            field=models.BooleanField(default=False),
        ),
    ]
