# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('management', '0050_set_activated_time'),
    ]

    operations = [
        migrations.AlterField(
            model_name='pendingassignment',
            name='bed',
            field=models.ForeignKey(related_name='assignments', blank=True, to='management.Bed', null=True),
        ),
    ]
