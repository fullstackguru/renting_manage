// Fix for bootstrap modals: remain .modal-open class present on body
// until all modals would be closed.
(function(doc, $) {
  var counter = 0;
  var $body = $(doc.body);

  $body.on('shown.bs.modal hidden.bs.modal', function (e) {
    counter += e.type === 'shown' ? 1 : -1;
    counter = Math.max(counter, 0);
    $body.toggleClass('modal-open', counter > 0);
  });
})(document, jQuery);
