# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
from django.conf import settings


class Migration(migrations.Migration):

    dependencies = [
        ('management', '0054_add_group'),
        ('website', '0029_add_disable_email_field_to_user'),
    ]

    operations = [
        migrations.CreateModel(
            name='Group',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('agreement_period', models.ForeignKey(related_name='groups', to='management.AssignmentPeriod')),
            ],
        ),
        migrations.AlterField(
            model_name='user',
            name='allow_co_educational_groups',
            field=models.BooleanField(default=False, verbose_name=b'Enable coed matching. Coed limits your matching to other coed users. '),
        ),
        migrations.AlterField(
            model_name='user',
            name='disable_emails',
            field=models.BooleanField(default=False, verbose_name=b'Turn off email notifications'),
        ),
        migrations.AlterField(
            model_name='user',
            name='hidden',
            field=models.BooleanField(default=False, verbose_name=b'Hide your profile from matching request'),
        ),
        migrations.AddField(
            model_name='group',
            name='leader',
            field=models.ForeignKey(related_name='group_leader', to=settings.AUTH_USER_MODEL),
        ),
        migrations.AddField(
            model_name='group',
            name='unit',
            field=models.ForeignKey(blank=True, to='management.Unit', null=True),
        ),
        migrations.AddField(
            model_name='group',
            name='unit_type',
            field=models.ForeignKey(to='management.UnitType'),
        ),
        migrations.AddField(
            model_name='group',
            name='users',
            field=models.ManyToManyField(related_name='groups', to=settings.AUTH_USER_MODEL),
        ),
    ]
