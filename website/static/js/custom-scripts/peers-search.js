$( document ).ready(function () {
  var searchTrigger = $({});
  var tags = [];
  var nameStr = '';
  var peersType = '';
  var searchTimeout = 600;
  var activeTab = null;
  var page = 1;

  initSearchTrigger();
  initPeersTabs();
  initPeersSearch();
  initPeersFilters();

  function initSearchTrigger() {
    searchTrigger.on('change.filters', function () {
      var $peersContainer;
      if (peersType === 'suggested') {
        $peersContainer = $('#suggested-roommates').find('.peers-list .row .peers');
        page = 1;
      }
      else {
        $peersContainer = $('#all-roommates').find('.peers-list .row .peers');
      }
      $peersContainer.addClass('hidden');
      var $loadingAnimation = $('.loading');
      $loadingAnimation.removeClass('hidden');
      var promise = doSearch();
      promise.done(renderResults);
    });
  }

  function initPeersTabs() {
    var $navTabs = $('#peers .nav-tabs a');
    $navTabs.on('shown.bs.tab', function() {
      var $target = $(this);
      peersType = $target.data('peers');
      searchTrigger.trigger('change.filters');
      activeTab = $target.attr('href');
    });

    $navTabs.filter(function () {
      return $(this).parent().hasClass('active')
    }).trigger('shown.bs.tab');
  }

  function initPeersSearch() {
    var searchHandler = delayedHandler(function () {
      nameStr = $(this).val();
      page = 1;
      peersType = 'all';
      $('.nav-tabs a[href="#all-roommates"]').tab('show');
      activeTab = '#all-roommates';
      searchTrigger.trigger('change.filters');
    }, searchTimeout);
    $('#peer-search').on('keyup', searchHandler);
  }

  function initPaginator() {
    var searchHandler = function () {
      var $target = $(this);
      page = $target[0].innerText;
      searchTrigger.trigger('change.filters');
    };
    $('.paginate_button').on('click', searchHandler);
  }

  function initPeersFilters() {
    var $filterTags = $('.filter-tags');
    var $habitsSelect = $('ul#habits');
    var $habitAnswersSelect = $('ul.habit-answers');
    var $addFilterBtn = $('#add-filter');

    var tagsHolder = $filterTags.tagsHolder({
      unique: true,
      onRemoveTag: removeHabitFilter
    }).data('tagsHolder');

    $filterTags.parent().find('.tagsinput input').attr('readonly', true);

    $(".answer-selected").on('click', onAddFilterClick);

    function onAddFilterClick() {
      var answerId = $(this).attr('value');
      var questionDropdown = $(this).closest('ul');
      var questionId = questionDropdown.attr('value');
      var title = $(this).text() + ' - ' + questionDropdown.siblings('a').text();
      var twoAnswersHabit = questionDropdown.children('li').length === 2;
      var filterIndex = getFilterIndexById(questionId);

      if (twoAnswersHabit && filterIndex > -1) {
        return modifyHabitFilter(filterIndex, answerId, title);
      }

      return addHabitFilter(questionId, answerId, title);
    }

    function addHabitFilter(q_id, a_id, title) {
      tagsHolder.addTag(title);

      tags.push({
        title: title,
        question_id: q_id,
        answer_id: a_id
      });

      searchTrigger.trigger('change.filters');

      return tags;
    }

    function modifyHabitFilter(index, new_a_id, new_title) {
      tagsHolder.modifyTag(tags[index].title, new_title);

      tags[index].title = new_title;
      tags[index].answer_id = new_a_id;

      searchTrigger.trigger('change.filters');

      return tags;
    }

    function removeHabitFilter(title) {
      tags = tags.filter(function (item) {
        return item.title !== title;
      });

      searchTrigger.trigger('change.filters');

      return tags;
    }

    function getFilterIndexById(q_id) {
      var l = tags.length;

      for (var i = 0; i < l; i++) {
        if (tags[i].question_id === q_id) {
          return i;
        }
      }

      return -1;
    }

  }

  function doSearch() {
    var searchParams = {};

    searchParams.peers = peersType;
    searchParams.name = nameStr;
    searchParams.page = page;
    searchParams.habits = tags.map(function(item) {
      return [
        item.question_id,
        item.answer_id
      ];
    });

    return $.ajax({
      type: 'POST',
      url: '/search-peers-ajax/',
      contentType: 'application/json',
      data: JSON.stringify(searchParams)
    });
  }

  function renderResults(html) {
    var $peersContainer = $(activeTab).find('.peers-list .row .peers');
    $peersContainer.html(html);
    $peersContainer.removeClass('hidden');
    var $loadingAnimation = $('.loading');
    $loadingAnimation.addClass('hidden');
    initPaginator();
    OneUI.init("uiInit");
  }
});
