import math
from django.conf import settings
from django.db.models import Q
from website.models import Relationship


COMPATIBILITY_FACTOR_MATRIX = {
    'Thoughtful': {
        'Thoughtful': 1,
        'Rational': 0.75,
        'Charismatic': 0.4,
        'Mellow': 0.8,
        'Adventurous': 0.2,
        'Reliable': 0.7,
        'Social': 0.5,
    },
    'Rational': {
        'Thoughtful': 0.75,
        'Rational': 1,
        'Charismatic': 0.4,
        'Mellow': 0.4,
        'Adventurous': 0.3,
        'Reliable': 0.9,
        'Social': 0.3,
    },
    'Charismatic': {
        'Thoughtful': 0.4,
        'Rational': 0.4,
        'Charismatic': 0.9,
        'Mellow': 0.2,
        'Adventurous': 0.8,
        'Reliable': 0.4,
        'Social': 1,
    },
    'Mellow': {
        'Thoughtful': 0.8,
        'Rational': 0.4,
        'Charismatic': 0.2,
        'Mellow': 1,
        'Adventurous': 0.4,
        'Reliable': 0.3,
        'Social': 0.3,
    },
    'Adventurous': {
        'Thoughtful': 0.2,
        'Rational': 0.3,
        'Charismatic': 0.8,
        'Mellow': 0.4,
        'Adventurous': 1,
        'Reliable': 0.3,
        'Social': 0.7,
    },
    'Reliable': {
        'Thoughtful': 0.7,
        'Rational': 0.9,
        'Charismatic': 0.4,
        'Mellow': 0.3,
        'Adventurous': 0.3,
        'Reliable': 0.9,
        'Social': 0.7,
    },
    'Social': {
        'Thoughtful': 0.5,
        'Rational': 0.3,
        'Charismatic': 1,
        'Mellow': 0.3,
        'Adventurous': 0.7,
        'Reliable': 0.7,
        'Social': 1,
    },
}

PERSONALITY_TOTAL = {
    'Thoughtful': 870,
    'Rational': 810,
    'Charismatic': 820,
    'Mellow': 680,
    'Adventurous': 740,
    'Reliable': 840,
    'Social': 900,
}


def get_user_personality(user_profile):
    """ Get personality dict for give user's profile.
    """
    personality_dict = {}
    personality_types = user_profile.get_personality_types_json().get('personality_types', [])
    for pt in personality_types:
        personality_dict[pt.personality_type.name] = pt.score
    return personality_dict


def get_ideal_personality(personality_dict):
    """ Calculate ideal personality values for a given personality.
    """
    ideal_personality_dict = {}
    for personality_name, personality_score in personality_dict.iteritems():
        ideal_personality = 0.
        compatibility_row = COMPATIBILITY_FACTOR_MATRIX[personality_name]
        for compatibility_name, compatibility_score in compatibility_row.iteritems():
            ideal_personality += (
                personality_score * compatibility_score + personality_dict[compatibility_name] * compatibility_score)
        ideal_personality_dict[personality_name] = round(
            100. * ideal_personality / PERSONALITY_TOTAL[personality_name], 2)
    return ideal_personality_dict


def get_personalities_distance(personality_dict1, personality_dict2):
    """ Calculate Euclidean distance between two personalities. Personalities are treated as vectors here.
    """
    if not personality_dict1 or not personality_dict2:
        return 0
    distance = 0.
    for personality_name, personality_score in personality_dict1.iteritems():
        try:
            diff = personality_score - personality_dict2[personality_name]
            distance += diff * diff
        except KeyError:
            pass
    return math.sqrt(distance)


def get_compatibility_score(distance1, distance2):
    """ Calculate compatibility score for two personalities represented as distances between appropriate personalities
        and idea opponent's personality. some_val coefficient meaning isn't complete clear yet, so its name means
        nothing at the moment.
    """
    # max_distance is a maximum possible distance between a personality with all value 0
    # and a personality with all values 100.
    max_distance = 264.575
    return round(100. * (2 * max_distance - (distance1 + distance2)) / (2 * max_distance), 2)


def get_compatibility_style(compatibility_score):
    """ Compatibility style is used for UI templates rendering to define color of related 'progress' bars.
    """
    if compatibility_score <= 30.:
        return 'progress-bar-red'

    if compatibility_score <= 50.:
        return 'progress-bar-yellow'

    if compatibility_score <= 70.:
        return 'progress-bar-blue'

    if compatibility_score <= 90.:
        return 'progress-bar-green'

    return 'progress-bar-dark-blue'


class RoommateMatcher(object):
    def __init__(self, users, all_users, relations=None):
        '''
        This class evaluates distance between users from users list and users from all_users list,
        than it's added suggested roommates field with best matches to each user from users.
        It's exclude users that hasn't pass test and users that in relation with users.
        If you want to filter relations you can set additional field relations.
        :param users: List[User]
        :param all_users: List[User]
        :param relations: List[Relationship]
        :return: List[User]
        '''
        self.all_relations = relations
        self.users = users
        self.all_users = all_users.filter(profile__personality_types__isnull=False)[:]

    def _set_personality_for_users(self):
        for user in self.all_users:
            user.personality_dict = get_user_personality(user.profile)
            user.personality_ideal = get_ideal_personality(user.personality_dict)
        for user in self.users:
            user.personality_dict = get_user_personality(user.profile)
            user.personality_ideal = get_ideal_personality(user.personality_dict)

    def _get_relation_user_ids(self, user):
        result_ids = {user.id}
        if self.all_relations:
            user_relations = self.all_relations.filter(Q(start_user_id=user.id) | Q(end_user_id=user.id)).\
                values('start_user_id', 'end_user_id')
        else:
            user_relations = Relationship.objects.filter(Q(start_user_id=user.id) | Q(end_user_id=user.id)).\
                values('start_user_id', 'end_user_id')
        for relation in user_relations:
            result_ids.add(relation['start_user_id'])
            result_ids.add(relation['end_user_id'])
        return list(result_ids)

    def _get_all_users(self, user):
        excluded_users = self._get_relation_user_ids(user)
        users = filter(lambda u: u.id not in excluded_users, self.all_users)
        return users

    def _get_suggested_roommates(self, peers):
        return sorted(peers, key=lambda x: x.compatibility_score, reverse=True)[:settings.SUGGESTED_ROOMMATES_CNT]

    @staticmethod
    def _set_peer_compatibility(peers, user):
        user_personality_dict = user.personality_dict
        user_personality_ideal = user.personality_ideal
        for peer in peers:
            peer_personality_dict = peer.personality_dict
            peer_personality_ideal = peer.personality_ideal
            # distance between user's ideal and peer
            distance1 = get_personalities_distance(user_personality_ideal, peer_personality_dict)
            # distance between peer's ideal and user
            try:
                distance2 = get_personalities_distance(peer_personality_ideal, user_personality_dict)
            except KeyError:
                distance2 = 0
            peer.compatibility_score = get_compatibility_score(distance1, distance2)
            peer.compatibility_style = get_compatibility_style(peer.compatibility_score)
        return user

    def get_suggested_users(self):
        '''
        Return list with users(which passed in constructor) with suggested_roommates field.
        '''
        self._set_personality_for_users()
        for user in self.users:
            peer_users = self._get_all_users(user)
            self._set_peer_compatibility(peer_users, user)
            peer_users = self._get_suggested_roommates(peer_users)
            user.suggested_roommates = peer_users
        return self.users
