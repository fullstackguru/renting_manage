# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('website', '0010_add_disable_deliting_field_to_relationship_model'),
    ]

    operations = [
        migrations.AlterField(
            model_name='userorganizationsocialhabitanswers',
            name='userprofile',
            field=models.ForeignKey(related_name='answers', to='website.UserProfile'),
        ),
    ]
