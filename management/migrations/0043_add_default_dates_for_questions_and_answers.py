# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import django.utils.timezone


class Migration(migrations.Migration):

    dependencies = [
        ('management', '0042_fix_social_question_dates'),
    ]

    operations = [
        migrations.AlterField(
            model_name='organizationsocialhabitanswer',
            name='created_at',
            field=models.DateTimeField(default=django.utils.timezone.now, editable=False, db_index=True),
        ),
        migrations.AlterField(
            model_name='organizationsocialhabitanswer',
            name='updated_at',
            field=models.DateTimeField(default=django.utils.timezone.now, editable=False, db_index=True),
        ),
        migrations.AlterField(
            model_name='organizationsocialhabitquestion',
            name='created_at',
            field=models.DateTimeField(default=django.utils.timezone.now, editable=False, db_index=True),
        ),
        migrations.AlterField(
            model_name='organizationsocialhabitquestion',
            name='updated_at',
            field=models.DateTimeField(default=django.utils.timezone.now, editable=False, db_index=True),
        ),
    ]
