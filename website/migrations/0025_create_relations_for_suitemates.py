# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
from django.db.models import Q
from django.utils import timezone


def update_or_create_relation(apps, start_user, end_user, via_user, invitation, disable_deleting=False):
    '''
    Find relations between start_user and end_user, if found it's approve founded relations else
    it's create new relation.
    '''
    Relationship = apps.get_model("website", "Relationship")
    if start_user.id == end_user.id:
        return
    existed_relationship = Relationship.objects.filter(
        Q(start_user=start_user, end_user=end_user) | Q(start_user=end_user, end_user=start_user)
    )
    if existed_relationship:
        existed_relationship.update(approval_datetime=timezone.now(), end_user_approved=True,
                                    disable_deleting=disable_deleting)
    else:
        Relationship.objects.create(
            start_user=start_user, via_user=via_user,
            approval_datetime=timezone.now(),
            end_user=end_user,
            invitation=invitation, end_user_approved=True,
            disable_deleting=disable_deleting
        )


def save_relation(apps, rel):
    '''
    Save/update relation + create all possible via friends relations for approved relation.
    '''
    Relationship = apps.get_model("website", "Relationship")
    #  approved user
    if rel.end_user_approved and rel.approval_datetime and not rel.via_user:
        user = rel.end_user
        invited_user = rel.start_user
        invited_user_relations = Relationship.objects.filter(end_user_approved=True).\
            filter(Q(start_user=invited_user) | Q(end_user=invited_user))

        #  excluded users, user already has relationships with this users
        excluded_users_for_invited_user = {invited_user.id}
        for relation in invited_user_relations:
            excluded_users_for_invited_user.add(relation.start_user_id)
            excluded_users_for_invited_user.add(relation.end_user_id)

        user_relations = Relationship.objects.filter(Q(start_user=user) | Q(end_user=user)).\
            exclude(Q(start_user_id__in=excluded_users_for_invited_user) |
                    Q(end_user_id__in=excluded_users_for_invited_user)).\
            filter(end_user_approved=True)

        #  exclude users with relations with invited user
        excluded_users = {user.id}
        for relation in user_relations:
            excluded_users.add(relation.start_user_id)
            excluded_users.add(relation.end_user_id)

        invited_user_relations = invited_user_relations.exclude(
            Q(start_user_id__in=excluded_users) | Q(end_user_id__in=excluded_users)
        )

        #  Create relations with invited users + his friends with user friends
        for relation in user_relations:
            end_user = relation.end_user if relation.end_user != user else relation.start_user
            update_or_create_relation(apps, start_user=invited_user, end_user=end_user, via_user=user,
                                      invitation=relation.invitation)
            for r in invited_user_relations:
                start_user = r.end_user if r.end_user != invited_user else r.start_user
                end_user = relation.end_user if relation.end_user != user else relation.start_user
                update_or_create_relation(apps, start_user=start_user, end_user=end_user, via_user=invited_user,
                                          invitation=relation.invitation)

        # Create relations for invited user friends and user
        for relation in invited_user_relations:
            end_user = relation.end_user if relation.end_user != invited_user else relation.start_user
            update_or_create_relation(apps, start_user=user, end_user=end_user, via_user=invited_user,
                                      invitation=relation.invitation)
    return rel.save()


def create_relations(apps, user):
    Invitation = apps.get_model("management", "Invitation")
    PendingAssignment = apps.get_model("management", "PendingAssignment")
    Relationship = apps.get_model("website", "Relationship")
    user_invite = Invitation.objects.filter(user=user).first()
    if not user_invite:
        return
    pending_assignment = PendingAssignment.objects.filter(user=user, assignment_period=user_invite.agreement_period).\
        select_related('unit').first()
    if not pending_assignment:
        return
    unit = pending_assignment.unit
    if not unit:
        return
    pending_assignments = unit.pending_assignments.exclude(id=pending_assignment.id)
    for assignment in pending_assignments:
        if Relationship.objects.filter(Q(start_user=user, end_user=assignment.user) |
                                       Q(start_user=assignment.user, end_user=user)).exists():
            continue
        suitemate = assignment.user
        relation = Relationship(
            start_user=user, end_user=suitemate, invitation=user_invite,
            approval_datetime=timezone.now(), end_user_approved=True, disable_deleting=True
        )
        save_relation(apps, relation)


class Migration(migrations.Migration):

    def create_relations_for_suitemate(apps, schema_editor):
        User = apps.get_model("website", "User")
        users = User.objects.all()
        for user in users:
            create_relations(apps, user)

    dependencies = [
        ('website', '0024_update_last_login_date'),
        ('management', '0025_remove_matches_with_different_units')
    ]

    operations = [
        migrations.RunPython(create_relations_for_suitemate)
    ]
