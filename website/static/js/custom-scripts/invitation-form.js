var formInit = function (emailUrl) {
    var resendInviteAction = '0';
    var sendReminderEmailAction = '1';
    var resendInviteConfirmationText = 'Are you sure that you send the roommate matching invitation?';
    var sendReminderEmailConfirmationText = 'Are you sure that you want to send account reminder email for this user?';

    $("#id_unit").chainedTo("#id_unit_type");
    $("#id_bed").chainedTo("#id_unit");
    var previousValue;
    var isChangingAllowed = false;

    $("#id_unit, #id_bed, #id_unit_type").on('focus', function () {
        previousValue = this.value;
    }).change(function () {
        if (!isChangingAllowed) {
            var dropdown = this;
            swal({
                    title: "Are you sure?",
                    text: "This change will effect current roommate selections.  Are you sure you want to continue?",
                    type: "warning",
                    showCancelButton: true,
                    confirmButtonColor: "#DD6B55",
                    confirmButtonText: "Yes, change it!",
                    cancelButtonText: "No, cancel!",
                    closeOnConfirm: true
                },
                function (isConfirm) {
                    if (!isConfirm) {
                        dropdown.value = previousValue;
                        $("#id_unit, #id_bed, #id_unit_type").change();
                    }
                    else {
                        isChangingAllowed = true;
                    }
                }
            );
        }
    });
    var sendEmail = function (userId, action, confirmation_text) {
        swal({
                title: "Are you sure?",
                text: confirmation_text,
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: "Yes, resend!",
                cancelButtonText: "No, cancel!",
                closeOnConfirm: false
            },
            function (isConfirm) {
                if (isConfirm) {
                    $.ajax({
                        url: emailUrl,
                        type: 'POST',
                        data: {
                            action: action,
                            ids: [userId]
                        },
                        success: function (data) {
                            swal("Done!", data, "success");
                        },
                        error: function (jqXHR, textStatus, errorThrown) {
                            swal("Oops...", "Unable to send email!", "error");
                        }
                    });
                }
            });
    };

    $('#resend-invite-btn').on('click', function () {
        var userId = $(this).attr('user-id');
        sendEmail(userId, resendInviteAction, resendInviteConfirmationText);
    });

    $('#reminder-btn').on('click', function () {
        var userId = $(this).attr('user-id');
        sendEmail(userId, sendReminderEmailAction, sendReminderEmailConfirmationText);
    });

};
