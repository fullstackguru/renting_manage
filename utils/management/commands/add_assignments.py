from datetime import datetime
from django.core.management.base import BaseCommand, CommandError
from django.db.models import Count, Q
from website.models import User, Relationship, Group
from management.models import Invitation, PendingAssignment


class Command(BaseCommand):
    help = "Adds missing assignments for students that has invites."

    def handle(self, *args, **options):
        users_with_invites = Invitation.objects.values_list('user', flat=True).distinct()
        users_with_assignments = PendingAssignment.objects.values_list('user', flat=True).distinct()
        missed_users_ids = set(users_with_invites) - set(users_with_assignments)
        invites_for_missed_users = Invitation.objects.filter(user_id__in=missed_users_ids).select_related('user')
        print 'Create assignments'
        for invite in invites_for_missed_users:
            assignment_options = {
                'user': invite.user,
                'room_type': invite.room_type,
                'assignment_period': invite.agreement_period,
                'invite': invite
            }
            user_group = Group.objects.filter(agreement_period=invite.agreement_period, users__in=[invite.user]).first()
            unit = user_group.unit if user_group else None
            assignment_options['unit'] = unit
            PendingAssignment.objects.create(**assignment_options)
        print 'Remove single groups'
        incorrect_groups = Group.objects.annotate(members_count=Count('users')).filter(members_count__lte=1)
        incorrect_groups.delete()
        print 'Add missed relations for groups'
        groups = Group.objects.select_related('unit').prefetch_related('users').all()
        for group in groups:
            users = group.users.all()
            relation = Relationship.objects.filter(start_user__in=users, end_user__in=users)
            if relation.exists():
                continue
            invite = Invitation.objects.filter(user=users[0]).first()
            Relationship.objects.create(start_user=users[0], end_user=users[1],
                                        invitation=invite,
                                        approval_datetime=datetime.now(), end_user_approved=True)
            PendingAssignment.objects.filter(user__in=users, assignment_period=invite.agreement_period).\
                update(unit=group.unit)
