# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('management', '0045_made_nullable_room_type_in_room'),
    ]

    operations = [
        migrations.AlterField(
            model_name='pendingassignment',
            name='room_type',
            field=models.ForeignKey(to='management.RoomType', null=True),
        ),
    ]
