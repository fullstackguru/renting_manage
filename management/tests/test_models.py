from django.test import TestCase
from django.conf import settings
from dbmail.models import MailTemplate, MailCategory
from website.models import User
from management.models import Invitation
from mock import patch
from utils.tests_utils import create_property_related_objects, mock_send_welcome_email


class ManagementModelsTestCase(TestCase):
    @classmethod
    def setUpClass(cls):
        super(ManagementModelsTestCase, cls).setUpClass()
        cls.organization, cls.property, cls.unit_type, cls.assignment_period, cls.room_type = \
            create_property_related_objects()

    @patch('management.signals.handlers.send_welcome_email', mock_send_welcome_email)
    def test_invite_creation(self):
        user = User(email='some_email@mail.com', first_name='some', last_name='some')
        user.save()
        self.assertEqual(user.available_properties.count(), 0)
        invite = Invitation.objects.create(user=user, agreement_period=self.assignment_period, unit_type=self.unit_type)
        self.assertEqual(user.available_properties.count(), 1)
        self.assertEqual(user.available_properties.first(), self.property)

    def test_property_get_email(self):
        welcome_category = MailCategory.objects.get(name=settings.DB_MAILER_WELCOME_EMAIL_CATEGORY_NAME)
        default_welcome_email = MailTemplate.objects.filter(category=welcome_category).first()
        self.assertEqual(self.property.get_welcome_email(), default_welcome_email)
        new_welcome_email = MailTemplate(
            name='test',
            subject='test',
            category=welcome_category,
            message='test'
        )
        new_welcome_email.save()
        self.property.emails.add(new_welcome_email)
        self.assertEqual(self.property.get_welcome_email(), new_welcome_email)
