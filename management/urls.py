from django.conf import settings
from django.conf.urls import include, url
from django.conf.urls.static import static
from django.contrib.auth.decorators import login_required
from management.views import LoginView, logout_view, DashboardView, PropertyView, InvitationListView, \
    UserListView, UserUpdateView, UserCreateView, MatchesListView, \
    UnitTypeListView, UnitTypeCreateView, UnitTypeUpdateView, RoomTypeListView, RoomTypeCreateView, RoomTypeUpdateView,\
    OrganizationSocialHabitQuestionListView, OrganizationSocialHabitQuestionCreateView, \
    OrganizationSocialHabitQuestionUpdateView, UserDeleteView, SuggestedRoommateListView, ResidentImportListView, \
    MailTemplateListView, MailTemplateCreateView, MailTemplateUpdateView, MailTemplateDeleteView, PropertyFeedListView, \
    OrganizationSocialHabitQuestionDeleteView, GlobalSearchView, PlacementListView, \
    PlacementUsersView, StageView, RealtyImportListView, UnitCreateView, UnitUpdateView, DashboardChartsView, MainView, \
    PropertySettingsView,PropertySocialNetworkListView,PropertySocialNetworkCreateView,PropertySocialNetworkUpdateView, \
    PropertySocialNetworkDeleteView

urlpatterns = [
    # url(r'^getting-started/(?P<invite_code>[^/]+)$', MainStudentDashboardView.as_view(), name='getting-started'),
    url(r'^login/$', LoginView.as_view(), name='login'),
    url(r'^logout/$', logout_view, name='logout'),
    url(r'^$', MainView.as_view(), name='main'),
    url(r'^property/(?P<pk>[0-9]+)/$', PropertyView.as_view(), name='property'),
    url(r'^dashboard/(?P<property_id>\d+)/$', login_required(DashboardView.as_view()), name='dashboard'),
    url(r'^charts/(?P<property_id>\d+)/$', DashboardChartsView.as_view(), name='dashboard-charts'),
    url(r'^invitations/(?P<property_id>\d+)/$', InvitationListView.as_view(), name='invitation-list'),
    url(r'^user/(?P<property_id>\d+)/$', UserListView.as_view(), name='user-list'),
    url(r'^user/(?P<property_id>\d+)/(?P<pk>[0-9]+)/$', UserUpdateView.as_view(), name='user-update'),
    url(r'^user/(?P<property_id>\d+)/(?P<pk>[0-9]+)/delete$', UserDeleteView.as_view(), name='user-delete'),
    url(r'^user/(?P<property_id>\d+)/add/$', UserCreateView.as_view(), name='user-add'),
    url(r'^matches/(?P<property_id>\d+)/$', MatchesListView.as_view(), name='match-list'),
    url(r'^suggested_roommates/(?P<property_id>\d+)/$', SuggestedRoommateListView.as_view(),
        name='suggested-roommates-list'),
    url(r'^unit_type/(?P<property_id>\d+)/$', UnitTypeListView.as_view(), name='unit-type-list'),
    url(r'^unit_type/(?P<property_id>\d+)/(?P<pk>[0-9]+)/$', UnitTypeUpdateView.as_view(), name='unit-type-update'),
    url(r'^unit_type/(?P<property_id>\d+)/add/$', UnitTypeCreateView.as_view(), name='unit-type-add'),
    url(r'^room_type/(?P<property_id>\d+)/$', RoomTypeListView.as_view(), name='room-type-list'),
    url(r'^room_type/(?P<property_id>\d+)/(?P<pk>[0-9]+)/$', RoomTypeUpdateView.as_view(), name='room-type-update'),
    url(r'^room_type/(?P<property_id>\d+)/add/$', RoomTypeCreateView.as_view(), name='room-type-add'),
    url(r'^org_question/(?P<property_id>\d+)/$', OrganizationSocialHabitQuestionListView.as_view(),
        name='org-questions-list'),
    url(r'^org_question/(?P<property_id>\d+)/add/$', OrganizationSocialHabitQuestionCreateView.as_view(),
        name='org-question-add'),
    url(r'^org_question/(?P<property_id>\d+)/(?P<pk>[0-9]+)/$', OrganizationSocialHabitQuestionUpdateView.as_view(),
        name='org-question-update'),
    url(r'^org_question/(?P<property_id>\d+)/(?P<pk>[0-9]+)/delete$',
        OrganizationSocialHabitQuestionDeleteView.as_view(), name='org-question-delete'),
    url(r'^resident_import/(?P<property_id>\d+)/$', ResidentImportListView.as_view(), name='resident-import-list'),
    url(r'^realty_import/(?P<property_id>\d+)/$', RealtyImportListView.as_view(), name='realty-import-list'),
    url(r'^mail/(?P<property_id>\d+)/$', MailTemplateListView.as_view(), name='mail-template-list'),
    url(r'^mail/(?P<property_id>\d+)/(?P<pk>[0-9]+)/$', MailTemplateUpdateView.as_view(), name='mail-template-update'),
    url(r'^mail/(?P<property_id>\d+)/add$', MailTemplateCreateView.as_view(), name='mail-template-add'),
    url(r'^mail/(?P<property_id>\d+)/(?P<pk>[0-9]+)/delete/$', MailTemplateDeleteView.as_view(),
        name='mail-template-delete'),
    url(r'^placements/(?P<property_id>\d+)/units/create/$', UnitCreateView.as_view(), name='unit_create'),
    url(r'^placements/(?P<property_id>\d+)/units/(?P<pk>[0-9]+)/$', UnitUpdateView.as_view(), name='unit_update'),
    url(r'^placements/(?P<property_id>\d+)/(?P<period_id>\d+)/users/$', PlacementUsersView.as_view(), name='placement_create'),
    url(r'^placements/(?P<property_id>\d+)/$', PlacementListView.as_view(), name='placement_details'),
    url(r'^feed/(?P<property_id>\d+)/$', PropertyFeedListView.as_view(), name='feed'),
    url(r'^search_results/$', GlobalSearchView.as_view(), name='search'),
    url(r'^stages/(?P<property_id>\d+)/$', StageView.as_view(), name='stages'),
    url(r'^settings/(?P<property_id>\d+)/$', PropertySettingsView.as_view(), name='property_settings'),
    url(r'^social_networks/(?P<property_id>\d+)/$', PropertySocialNetworkListView.as_view(), name='social-networks-list'),
    url(r'^social_networks/(?P<property_id>\d+)/(?P<pk>[0-9]+)/$', PropertySocialNetworkUpdateView.as_view(), name='social-networks-update'),
    url(r'^social_networks/(?P<property_id>\d+)/add/$', PropertySocialNetworkCreateView.as_view(), name='social-networks-add'),
    url(r'^social_networks/(?P<property_id>\d+)/(?P<pk>[0-9]+)/delete/$', PropertySocialNetworkDeleteView.as_view(), name='social-networks-delete'),

] + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
