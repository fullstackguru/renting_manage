# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    def remove_beds_from_users(apps, schema_editor):
        Bed = apps.get_model("management", "Bed")
        User = apps.get_model("website", "User")
        users = User.objects.all()
        for user in users:
            beds = user.beds.all()
            for bed in beds[1:]:
                bed.delete()


    dependencies = [
        ('management', '0023_remove_duplicated_beds_and_units'),
    ]

    operations = [
        migrations.RunPython(remove_beds_from_users)
    ]
