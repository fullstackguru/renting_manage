var initEmailTable = function (updateUrl, propertyId) {
    $('#email-table').on('preXhr.dt', function () {
        $('tr').attr('hidden', true);
        App.blocks('#email-table-block', 'refresh_toggle');
    });
    $('#email-table').on('xhr.dt', function () {
        $('tr').attr('hidden', false);
        App.blocks('#email-table-block', 'refresh_toggle');
    });
    $('#email-table').DataTable({
        initComplete: function () {
            var api = this.api();
            $('#email-table_filter input')
                .off('.DT')
                .on('keyup.DT', function (e) {
                    if (e.keyCode == 13) {
                        api.search(this.value).draw();
                    }
                });
        },
        "serverSide": true,
        "processing": true,
        "bSort": true,
        "bPaginate": true,
        "sPaginationType": "full_numbers",
        "iDisplayLength": 10,
        ajax: {
            "dataType": 'json',
            "dataSrc": function (json) {
                return json.data
            }
        },
        "aoColumns": [
            {
                "mDataProp": "name",
                "render": function (data, type, full, meta) {
                    var update_url = '<a href="' + updateUrl + full.id + '">' + data + '</a>';
                    return update_url;
                }
            },
            {
                "mDataProp": "delete",
                "render": function (data, type, full, meta) {
                    return '<div class="management-action" onclick="deleteEmailTemplate(' + full.id + ', \'' + full.name + '\', ' + propertyId + ')">' +
                        'Delete <i class="si si-trash"></i>' +
                        '</div>'
                },
                "orderable": false
            }
        ],
        paging: true,
        "lengthMenu": [[10, 25, 50, 100], [10, 25, 50, 100]]
    });
};

var deleteEmailTemplate = function (templateId, templateName, propertyId) {
        var table = $('#email-table').DataTable();
        var url = '/management/mail/' + propertyId + '/' + templateId + '/delete';
        swal({
                title: "Are you sure?",
                text: "Are you sure that you want to delete " + templateName,
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: "Yes, delete it!",
                cancelButtonText: "No, cancel!",
                closeOnConfirm: false
            },
            function (isConfirm) {
                if (isConfirm) {
                    $.ajax({
                        url: url,
                        type: 'DELETE',
                        success: function (data) {
                            swal("Done!", data, "success");
                            table.draw('page');
                        },
                        error: function (jqXHR, textStatus, errorThrown) {
                            swal("Oops...", "Unable to delete this email template!", "error");
                        }
                    });
                }
            });
    };