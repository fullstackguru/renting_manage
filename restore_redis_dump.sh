#! /bin/bash

/etc/init.d/redis-server stop
redis-cli shutdown
rm -f /var/lib/redis/dump.rdb
cp /usr/src/app/dump.rdb /var/lib/redis/dump.rdb
chown redis:redis /var/lib/redis/dump.rdb
/etc/init.d/redis-server start
