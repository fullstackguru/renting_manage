# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
from django.db.models import Q


class Migration(migrations.Migration):

    def set_roommate_relations(apps, schema_editor):
        Bed = apps.get_model("management", "Bed")
        Relationship = apps.get_model("website", "Relationship")
        db_alias = schema_editor.connection.alias
        all_beds = Bed.objects.all()
        relations_for_update = Relationship.objects.none()
        for bed in all_beds:
            beds_in_room = Bed.objects.filter(room=bed.room).exclude(id=bed.id)
            for bed_in_room in beds_in_room:
                if bed.user and bed_in_room.user:
                    relations = Relationship.objects.filter(Q(start_user=bed.user, end_user=bed_in_room.user) |
                                                            Q(end_user=bed.user, start_user=bed_in_room.user))
                    relations_for_update |= relations
        print relations_for_update.count()
        relations_for_update.update(is_roommate='D')

    dependencies = [
        ('website', '0018_add_is_roommate_field_for_relations'),
    ]

    operations = [
        migrations.RunPython(set_roommate_relations),
    ]
