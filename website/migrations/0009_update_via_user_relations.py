# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from datetime import datetime

from django.db import models, migrations
from django.db.models import Q


class Migration(migrations.Migration):

    def forwards_func(apps, schema_editor):
        User = apps.get_model("website", "User")
        Relationship = apps.get_model("website", "Relationship")
        db_alias = schema_editor.connection.alias
        users = User.objects.using(db_alias).all()
        new_added = True
        while new_added:
            new_added = False
            for user in users:
                # get user relationships
                user_relationships = Relationship.objects.filter(Q(start_user=user) | Q(end_user=user)). \
                    filter(end_user_approved=True).exclude(approval_datetime__isnull=True)

                # get ids of related users
                related_users_ids = set()
                for relation in user_relationships:
                    related_users_ids.add(relation.start_user_id)
                    related_users_ids.add(relation.end_user_id)
                try:
                    related_users_ids.remove(user.id)
                except KeyError:
                    pass

                # iterate through all related users
                for related_user_id in related_users_ids:

                    # get their relations
                    related_user_relationships = Relationship.objects. \
                        filter(Q(start_user_id=related_user_id) | Q(end_user_id=related_user_id)). \
                        exclude(Q(start_user_id=user.id) | Q(end_user_id=user.id)). \
                        filter(end_user_approved=True).exclude(approval_datetime__isnull=True)

                    # iterate through all related user relations and creating via friends relations
                    for relation in related_user_relationships:
                        if relation.end_user_id in related_users_ids and relation.start_user_id in related_users_ids:
                            continue

                        if relation.start_user_id == related_user_id:
                            new_user_id = relation.end_user_id
                        else:
                            new_user_id = relation.start_user_id
                        # try to get relation
                        existed_relations = Relationship.objects.filter(Q(start_user_id=user.id, end_user_id=new_user_id) |
                                                                   Q(start_user_id=new_user_id,
                                                                     end_user_id=user.id))
                        if not existed_relations:
                            r = Relationship.objects.create(
                                **{
                                    'start_user_id': user.id,
                                    'end_user_id': new_user_id,
                                    'via_user_id': related_user_id,
                                    'approval_datetime': datetime.now(),
                                    'end_user_approved': True,
                                    'invitation': relation.invitation,
                                    'invite_datetime': relation.invite_datetime,
                                })
                            new_added = True
                        else:
                            existed_relations.update(**{
                                'approval_datetime': datetime.now(),
                                'end_user_approved': True,
                                'via_user_id': related_user_id,
                            })

    dependencies = [
        ('website', '0008_add_via_user_field_to_relationship'),
    ]

    operations = [
        migrations.RunPython(forwards_func),
    ]