import cPickle as pickle
from django.core.management.base import BaseCommand, CommandError
from external.traitify.traitify import Traitify
from django.conf import settings
from website.models import QuizResult, User


class Command(BaseCommand):
    help = "Adds missing assessments for student that passed tests, but doesn't recieve result."

    def handle(self, *args, **options):
        users_with_passed_test = QuizResult.objects.values_list('user_id', flat=True)
        users = User.objects.filter(id__in=users_with_passed_test, profile__personality_types__isnull=True,
                                    profile__personality_traits__isnull=True).values_list('id', flat=True)
        results = QuizResult.objects.filter(user_id__in=users).select_related('user__profile')
        traitify = Traitify(secret_key=settings.TRAITIFY_SECRET_KEY)
        for result in results:
            try:
                profile = result.user.profile
                profile.personality_types = pickle.dumps(
                    traitify.get_personality_types(result.traitify_assessment_id))
                profile.personality_traits = pickle.dumps(
                    traitify.get_personality_traits(result.traitify_assessment_id))
                profile.save()
                print "Add assessments for user {}".format(result.user.email)
            except AttributeError:
                pass
