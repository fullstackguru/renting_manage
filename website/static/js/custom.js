$( document ).ready(function() {
  var start = new Date();

  setupAjax();
  initCustomSocNetworks();
  initQuizButtons();
  initUserProfileModal();

  function setupAjax() {
    var csrftoken = getCookie('csrftoken');
    $.ajaxSetup({
      beforeSend: function(xhr, settings) {
        if (!csrfSafeMethod(settings.type) && !this.crossDomain) {
          xhr.setRequestHeader("X-CSRFToken", csrftoken);
        }
      }
    });
  }

  function initQuizButtons() {
    $('.buttons button').click(function(ev) {
      ev.preventDefault();
      var elapsed = new Date() - start;

      $('#selection').val(this.value);
      $('#timer').val(elapsed);
      $('.quiz-form').submit();
    });
    // Autoclicker for traits test quick passing (for debug only)
    //$('.quiz-form .buttons button').eq(Math.random() * 2 >> 0).click();
  }

  function initUserProfileModal() {
    var $userProfileModal = $('#user-profile');

    $userProfileModal.on('show.bs.modal', function (e) {
      var $modalContent = $(this).find('.modal-content');
      var $spinner = $modalContent.find('.spinner-holder');
      var $targetLink = $(e.relatedTarget);
      var profileURL = $targetLink.data('href');

      $spinner.show();

      $.get(profileURL).done(function (htmlResp) {
        $spinner.hide();
        $modalContent.append(htmlResp);
      });

    });

    $userProfileModal.on('hidden.bs.modal', function () {
      var $modalContent = $(this).find('.modal-content');
      $modalContent.children('.peer-block').remove();
    });
  }

  function initCustomSocNetworks() {
    var $networksContainer = $('#other-networks');

    if (!$networksContainer.length) {
      return ;
    }

    var itemSelector = '.network-block';
    var $btnAdd = $('.btn-add-more-networks');

    var $items = $networksContainer.find(itemSelector);
    var $templateItem = $items.eq(0).clone();

    $templateItem.find('input').val('');

    $btnAdd.on('click', function() {
      var $nextItem = $templateItem.clone();
      $networksContainer.append($nextItem);
    });
  }

  $('a[data-toggle="tab"]').on('shown.bs.tab', function (e) {
    var target = $(e.target).attr("href"); // activated tab
    var header = $('.content-header');
    var description = $('.content-description');
    header.text($(e.target).data('text1'));
    description.text($(e.target).data('text2'));
  });
});
