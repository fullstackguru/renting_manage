# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import datetime
from django.conf import settings


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('management', '0065_add_related_name_to_questions'),
    ]

    operations = [
        migrations.CreateModel(
            name='RealtyImport',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('created_at', models.DateTimeField(default=datetime.datetime.now)),
                ('status', models.CharField(max_length=1, choices=[(b'Q', b'Queued'), (b'P', b'In Progress'), (b'C', b'Completed'), (b'F', b'Failed')])),
                ('file', models.FileField(upload_to=b'uploads/realty_import/')),
                ('added_by', models.ForeignKey(to=settings.AUTH_USER_MODEL)),
                ('property', models.ForeignKey(to='management.Property')),
            ],
            options={
                'abstract': False,
            },
        ),
    ]
