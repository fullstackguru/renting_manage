import csv
import operator
import openpyxl
from openpyxl.cell import get_column_letter
from actstream import action
from django.db.models import Q
from django.utils.decorators import method_decorator
from django.contrib.auth.decorators import login_required
from django.views.generic.edit import FormMixin
from django.http import JsonResponse, HttpResponse, HttpResponseBadRequest
from django.core.exceptions import PermissionDenied, SuspiciousOperation
from management.models import Property


class PropertyPermissionMixin(object):
    @method_decorator(login_required)
    def dispatch(self, request, *args, **kwargs):
        user = request.user
        user_properties = user.managed_properties.all().values_list('id', flat=True)
        prop = kwargs.get('property_id', None)
        try:
            if prop is None or int(prop) not in user_properties:
                raise PermissionDenied
        except ValueError:
            raise PermissionDenied
        self.property = Property.objects.filter(id=prop).first()
        return super(PropertyPermissionMixin, self).dispatch(request, *args, **kwargs)


class DataTableMixin(object):
    filters = []
    search_fields = []
    order_columns = []
    serializer_class = None

    def __get_params_dict(self):
        if self.request.method == 'POST':
            return self.request.POST
        else:
            return self.request.GET

    def filter(self, queryset):
        params_dict = self.__get_params_dict()
        for filter in self.filters:
            f = filter(params_dict, queryset)
            queryset = f.qs
        return queryset

    def search(self, queryset):
        params_dict = self.__get_params_dict()
        search = params_dict.get('search[value]', None)
        if search and self.search_fields:
            orm_lookups = ["{}__icontains".format(search_field) for search_field in self.search_fields]
            for search_part in search.split():
                or_queries = [Q(**{orm_lookup: search_part}) for orm_lookup in orm_lookups]
                queryset = queryset.filter(reduce(operator.or_, or_queries))
        return queryset

    def order(self, queryset):
        if not self.order_columns:
            return queryset
        params_dict = self.__get_params_dict()
        try:
            order_column = int(params_dict.get('order[0][column]', None))
        except TypeError:
            order_column = None
        if order_column is not None:
            try:
                order_by = self.order_columns[order_column]
            except IndexError:
                order_by = None
            if order_by:
                if params_dict.get('order[0][dir]') == 'asc':
                    queryset = queryset.order_by(order_by)
                else:
                    queryset = queryset.order_by('-' + order_by)

        return queryset

    def filter_by_items_per_page(self, queryset):
        params_dict = self.__get_params_dict()
        try:
            offset = int(params_dict.get('start', 0))
            limit = int(params_dict.get('length', 10))
        except ValueError:
            offset, limit = 0, 10
        if limit > 0:
            queryset = queryset[offset: offset + limit]
        return queryset

    def get_count(self, queryset):
        return queryset.count()

    def get_filtered_count(self, queryset):
        return queryset.count()

    def process_queryset(self, queryset):
        return queryset

    def get_serializer_context(self):
        return {}

    def get(self, request, **kwargs):
        if request.is_ajax():
            queryset = self.get_queryset()
            count = self.get_count(queryset)
            queryset = self.filter(queryset)
            queryset = self.search(queryset)
            queryset = self.order(queryset)
            filtered_count = self.get_filtered_count(queryset)
            queryset = self.filter_by_items_per_page(queryset)
            queryset = self.process_queryset(queryset)
            serializer_context = self.get_serializer_context()
            serializer = self.serializer_class(instance=queryset, many=True, context=serializer_context)
            return JsonResponse({
                'data': serializer.data,
                'recordsTotal': count,
                'recordsFiltered': filtered_count,
                'draw': request.GET.get('draw', 0),
            }, safe=False)
        else:
            return super(DataTableMixin, self).get(request, **kwargs)


class CSVResponseMixin(object):
    csv_filename = 'file.csv'

    def get_csv_filename(self):
        return self.csv_filename

    def render_to_csv(self, data):
        response = HttpResponse(content_type='text/csv')
        response['Content-Disposition'] = 'attachment; filename="{0}"'.format(self.get_csv_filename())
        writer = csv.writer(response)
        for row in data:
            writer.writerow([unicode(s).encode("utf-8") for s in row])
        return response


class ImportViewMixin(FormMixin):
    sample_columns = []
    task = None
    form_class = None

    def get_queryset(self):
        property_id = self.kwargs.get('property_id')
        queryset = self.model.objects.filter(property_id=property_id)
        return queryset

    def get(self, request, **kwargs):
        is_sample = request.GET.get('type', None)
        if is_sample:
            #  generating csv with sample
            response = HttpResponse(content_type='application/vnd.openxmlformats-officedocument.spreadsheetml.sheet')
            response['Content-Disposition'] = 'attachment; filename="import_sample.xlsx"'
            wb = openpyxl.Workbook()
            ws = wb.get_active_sheet()
            ws.title = "Sample"
            columns = self.sample_columns
            row_num = 0
            for col_num in xrange(len(columns)):
                c = ws.cell(row=row_num + 1, column=col_num + 1)
                c.value = columns[col_num][0]
                c.style.font.bold = True
                ws.column_dimensions[get_column_letter(col_num+1)].width = columns[col_num][1]
            wb.save(response)
            return response
        else:
            return super(ImportViewMixin, self).get(request, **kwargs)

    def post(self, request, *args, **kwargs):
        if request.is_ajax:
            property = self.property
            if not property:
                raise SuspiciousOperation("Property doesn't exist")
            resident_import_record = self.model(added_by=request.user, property=property)
            if not self.form_class:
                raise SuspiciousOperation
            form = self.form_class(request.POST, request.FILES, instance=resident_import_record)
            if form.is_valid():
                obj = form.save(commit=False)
                obj.status = self.model.QUEUED
                obj.save()
                if not self.task:
                    raise SuspiciousOperation
                self.task.delay(obj)
                action.send(self.request.user, verb='uploaded', action_object=obj, target=self.property)
                return HttpResponse('OK')
            else:
                return HttpResponseBadRequest(form.errors['__all__'][0])
        else:
            raise SuspiciousOperation('Request must be AJAX')