from itertools import chain

from django import forms
from django.template import Context
from django.template.loader import render_to_string
from django.utils.encoding import force_text
from django.utils.html import format_html
from django.utils.safestring import mark_safe


class ChainedSelect(forms.Select):
    '''
    This widget adds class attribute to options tags in html. For using this widget you must pass
    get_option_class_value parameter. It is a function that accept only one obj argument
    (it takes object from initial queryset) and returns string which will be added to class attribute
    of option tag for specified obj.
    '''
    def __init__(self, get_option_class_value=None, attrs=None, choices=()):
        self.option_class_field = get_option_class_value
        super(ChainedSelect, self).__init__(attrs, choices)

    def render_option(self, selected_choices, option_value, option_label, class_value=''):
        if option_value is None:
            option_value = ''
        option_value = force_text(option_value)
        if option_value in selected_choices:
            selected_html = mark_safe(' selected="selected"')
            if not self.allow_multiple_selected:
                # Only allow for a single selection.
                selected_choices.remove(option_value)
        else:
            selected_html = ''
        return format_html('<option value="{}" class="{}"{}>{}</option>',
                           option_value,
                           class_value,
                           selected_html,
                           force_text(option_label))

    def render_options(self, choices, selected_choices):
        # Normalize to strings.
        selected_choices = set(force_text(v) for v in selected_choices)
        output = []
        for option_value, option_label, obj in chain(self.choices, choices):
            if isinstance(option_label, (list, tuple)):
                output.append(format_html('<optgroup label="{}">', force_text(option_value)))
                for option in option_label:
                    class_value = self.option_class_field(obj) if self.option_class_field else ''
                    output.append(self.render_option(selected_choices, class_value=class_value, *option))
                output.append('</optgroup>')
            else:
                class_value = self.option_class_field(obj) if self.option_class_field else ''
                output.append(self.render_option(selected_choices, option_value, option_label, class_value=class_value))
        return '\n'.join(output)


class ManagementFilterWidget(forms.Widget):
    def __init__(self, attrs=None, choices=()):
        super(ManagementFilterWidget, self).__init__(attrs)
        self.choices = choices

    def render(self, name, value, attrs=None):
        context = Context({"choices": self.choices, "name": name, "attrs": self.build_attrs(self.attrs)})
        content = render_to_string('management/filters/default_filter.html', context)
        return content
