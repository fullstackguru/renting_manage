# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import ckeditor.fields


class Migration(migrations.Migration):

    dependencies = [
        ('management', '0001_initial'),
    ]

    operations = [
        migrations.AddField(
            model_name='property',
            name='welcome_email_body',
            field=ckeditor.fields.RichTextField(null=True, blank=True),
        ),
    ]
