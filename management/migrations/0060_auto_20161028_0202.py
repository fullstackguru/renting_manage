# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('management', '0059_merge'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='residentimport',
            name='error',
        ),
        migrations.RemoveField(
            model_name='unittype',
            name='max_rooms',
        ),
    ]
