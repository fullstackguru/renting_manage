from django.shortcuts import render


def csrf_failure(request, reason=None):
    return render(request, 'errors/403_csrf.html', status=403)
