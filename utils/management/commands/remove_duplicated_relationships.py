from django.core.management.base import BaseCommand
from django.db.models import Q
from website.models import Relationship


class Command(BaseCommand):
    help = 'Remove duplicated relationships'

    def handle(self, *args, **options):
        checked_all = False
        while not checked_all:
            checked_all = True
            relationships = Relationship.objects.all()
            for relation in relationships:
                relation_for_removing = Relationship.objects.filter(
                    Q(start_user=relation.start_user, end_user=relation.end_user) |
                    Q(start_user=relation.end_user, end_user=relation.start_user)).exclude(id=relation.id)
                if relation_for_removing:
                    checked_all = False
                    print relation_for_removing.count()
                    print relation, relation_for_removing
                    relation_for_removing.delete()
                    break
