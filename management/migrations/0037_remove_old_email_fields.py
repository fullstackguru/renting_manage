# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('management', '0036_move_emails_to_many2many_field'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='property',
            name='accept_relationship_email',
        ),
        migrations.RemoveField(
            model_name='property',
            name='account_reminder_email',
        ),
        migrations.RemoveField(
            model_name='property',
            name='message_notification_email',
        ),
        migrations.RemoveField(
            model_name='property',
            name='reject_relationship_email',
        ),
        migrations.RemoveField(
            model_name='property',
            name='request_relationship_email',
        ),
        migrations.RemoveField(
            model_name='property',
            name='welcome_email',
        ),
    ]
