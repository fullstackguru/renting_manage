# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
from django.db.models import Q


class Migration(migrations.Migration):

    def forwards_func(apps, schema_editor):
        User = apps.get_model("website", "User")
        Invitation = apps.get_model("management", "Invitation")
        Relationship = apps.get_model("website", "Relationship")
        db_alias = schema_editor.connection.alias
        users = User.objects.all()
        for user in users:
            user_relations = Relationship.objects.filter(Q(start_user=user) | Q(end_user=user))
            direct_relations = user_relations.filter(via_user__isnull=True)
            if direct_relations:
                direct_relations_ids = set()
                for relation in direct_relations:
                    direct_relations_ids.add(relation.start_user_id)
                    direct_relations_ids.add(relation.end_user_id)
                direct_relations_ids.remove(user.id)
                incorrect_relations = user_relations.filter(via_user__isnull=False).\
                    exclude(via_user__id__in=direct_relations_ids)
                incorrect_relations.delete()


    dependencies = [
        ('website', '0013_create_relations_for_roommates'),
    ]

    operations = [
        migrations.RunPython(forwards_func),
    ]
