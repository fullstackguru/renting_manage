# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('website', '0030_add_points'),
        ('website', '0032_add_size_to_group'),
    ]

    operations = [
    ]
