$( document ).ready(function () {

  initUserProfileDropzoneImages();

  function initUserProfileDropzoneImages() {
    var userImage = new Dropzone("img#userprofile_image", {
      url: "/upload-image-ajax/",
      acceptedFiles: 'image/*',
      maxFilesize: "5",
      paramName: "profile_image",
      maxFiles: 1,
      previewsContainer: false,
      init: function () {
        this.on("sending", function (file, xhr, data) {
          data.append("user", $('#userId').val());
        });
        this.on("success", function (file) {
          location.reload();
        });
        this.on("error", function (request, status, error) {
          swal("Error", "Unable to upload your photo!", "error");
        });
      },
      headers: {"X-CSRFToken": getCookie('csrftoken')},
      uploadMultiple: false,
      clickable: true
    });

    var userBackground = new Dropzone("div#userprofile_bg", {
      url: "/upload-image-ajax/",
      acceptedFiles: 'image/*',
      maxFilesize: "5",
      paramName: "profile_image_bg",
      maxFiles: 1,
      previewsContainer: false,
      init: function () {
        this.on("sending", function (file, xhr, data) {
          data.append("user", $('#userId').val());
        });
        this.on("success", function (file, data) {
          location.reload();
        });
        this.on("error", function (request, status, error) {
          swal("Error", "Unable to upload your photo!", "error");
        });
      },
      headers: {"X-CSRFToken": getCookie('csrftoken')},
      uploadMultiple: false,
      clickable: true
    });
  }
});

