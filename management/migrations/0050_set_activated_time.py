# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from datetime import timedelta
from django.db import models, migrations
from django.utils import timezone


class Migration(migrations.Migration):

    def set_activated_time(apps, schema_editor):
        Invitation = apps.get_model("management", "Invitation")
        User = apps.get_model("website", "User")
        logged_in_users = User.objects.filter(last_login__isnull=False).values_list('id', flat=True)
        invites = Invitation.objects.filter(user_id__in=logged_in_users)
        some_days_ago = timezone.now() - timedelta(days=3)
        print invites.update(activated_time=some_days_ago)

    dependencies = [
        ('management', '0049_add_activated_time_field_to_invite'),
        ('website', '0029_add_disable_email_field_to_user')
    ]

    operations = [
        migrations.RunPython(set_activated_time)
    ]
