# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('management', '0007_auto_20160509_0852'),
    ]

    operations = [
        migrations.AddField(
            model_name='property',
            name='logo',
            field=models.ImageField(null=True, upload_to=b'uploads/property-logos/', blank=True),
        ),
    ]
