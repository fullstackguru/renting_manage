# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('management', '0028_make_filefield_required_in_resident_import'),
    ]

    operations = [
        migrations.AddField(
            model_name='pendingassignment',
            name='bed',
            field=models.ForeignKey(blank=True, to='management.Bed', null=True),
        ),
    ]
