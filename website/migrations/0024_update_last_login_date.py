# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from datetime import datetime
from django.db import models, migrations


class Migration(migrations.Migration):

    def update_last_login(apps, schema_editor):
        User = apps.get_model("website", "User")
        QuizResult = apps.get_model("website", "QuizResult")
        user_takes_quiz = QuizResult.objects.values_list('user_id', flat=True)
        missed_users = User.objects.filter(id__in=user_takes_quiz, last_login=None)
        missed_users.update(last_login=datetime.now())

    dependencies = [
        ('website', '0023_convert_emails_to_lowercase'),
    ]

    operations = [
        migrations.RunPython(update_last_login)
    ]
