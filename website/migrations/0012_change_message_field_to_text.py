# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('website', '0011_add_related_name_to_user_organization_answers'),
    ]

    operations = [
        migrations.AlterField(
            model_name='conversationmessage',
            name='message',
            field=models.TextField(),
        ),
    ]
