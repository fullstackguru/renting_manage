from django.apps import AppConfig


FEED_MODELS = ['User']


class WebsiteAppConfig(AppConfig):
    name = 'website'

    def ready(self):
        # Initialize signals
        from website.signals.handlers import send_relationship_email_signal, send_in_app_message_signal, \
            create_user_profile
        from actstream import registry
        for model in FEED_MODELS:
            registry.register(self.get_model(model))
