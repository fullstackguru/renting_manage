# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
from django.db.models import Q


class Migration(migrations.Migration):

    def remove_assignments_for_users_without_invites(apps, schem_editor):
        PendingAssignment = apps.get_model('management', 'PendingAssignment')
        Invitation = apps.get_model('management', 'Invitation')
        Relationship = apps.get_model('website', 'Relationship')
        User = apps.get_model('website', 'User')
        users_with_invites = Invitation.objects.values_list('user', flat=True)
        users_without_invites = User.objects.exclude(id__in=users_with_invites)
        for u in users_without_invites:
            Relationship.objects.filter(Q(start_user=u) | Q(end_user=u)).delete()
            PendingAssignment.objects.filter(user=u).delete()

    dependencies = [
        ('management', '0040_fill_invite_field_in_assignments'),
        ('website', '0029_add_disable_email_field_to_user')
    ]

    operations = [
        migrations.RunPython(remove_assignments_for_users_without_invites)
    ]
