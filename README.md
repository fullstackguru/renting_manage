# Renting platform
This document assumes your using Apple's OSX platform. 

The tools required are:

- Python 2.7.x
- virtualenvwrapper

## Installation
```sh
$ curl -sL https://raw.githubusercontent.com/brainsik/virtualenv-burrito/master/virtualenv-burrito.sh | $SHELL
$ ruby -e "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/master/install)"
$ brew install memcached
$ brew install libGeoIp
```

## Source / Python virtualenv setup:
```sh
$ git clone
$ mkvirtualenv venv
$ pip install -r requirements.txt
$ ./manage.py syncdb
$ ./manage.py migrate
```

## Running Server:
```sh
$ workon venv
$ ./manage.py runserver
```

## Updating 3rd party requirements and database migrations
```sh
$ workon venv
$ pip install -r requirements.txt
$ ./manage.py migrate
```

## For development on local
* Running redis server
```
$ brew install redis
$ redis-server /usr/local/etc/redis.conf
```

* Running celery and server
```
$ workon venv
$ ./manage.py celeryd -Q default
$ ./manage.py runserver
```

* Import dumped db, realty and users

> This will work only when redis and celery are running