# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import datetime
from django.conf import settings


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('management', '0017_add_co_educational_groups'),
    ]

    operations = [
        migrations.CreateModel(
            name='ResidentImport',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('file', models.FileField(upload_to=b'uploads/resident_import/')),
                ('created_at', models.DateTimeField(default=datetime.datetime.now)),
                ('status', models.CharField(max_length=1, choices=[(b'Q', b'Queued'), (b'P', b'In Progress'), (b'C', b'Completed'), (b'F', b'Failed')])),
                ('added_by', models.ForeignKey(to=settings.AUTH_USER_MODEL)),
            ],
        ),
        migrations.AlterField(
            model_name='property',
            name='from_email',
            field=models.EmailField(default=b'help@xxx.com', max_length=255),
        ),
        migrations.AddField(
            model_name='residentimport',
            name='property',
            field=models.ForeignKey(to='management.Property'),
        ),
    ]
