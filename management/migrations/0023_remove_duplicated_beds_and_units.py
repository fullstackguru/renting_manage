# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    def remove_duplicated_beds_and_units(apps, schema_editor):
        Bed = apps.get_model("management", "Bed")
        Unit = apps.get_model("management", "Unit")
        units = Unit.objects.all()
        for unit in units:
            similar_units = Unit.objects.filter(name=unit.name).order_by('-id')
            if similar_units.count() > 1:
                for u in similar_units[1:]:
                    u.delete()
        beds = Bed.objects.select_related('room__unit')
        for bed in beds:
            similar_beds = Bed.objects.filter(name=bed.name, room__unit=bed.room.unit)
            if similar_beds.count() > 1:
                for b in similar_beds[1:]:
                    b.delete()



    dependencies = [
        ('management', '0022_remove_wrong_units'),
    ]

    operations = [
        migrations.RunPython(remove_duplicated_beds_and_units)
    ]
