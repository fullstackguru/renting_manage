from datetime import datetime, timedelta

from django.contrib import admin
from django.utils.translation import ugettext_lazy as _
from website.models import QuizResult
from management.models import Invitation, AssignmentPeriod


class LastLoginListFilter(admin.SimpleListFilter):
    title = _('Last Login')
    parameter_name = 'last_login'

    def lookups(self, request, model_admin):
        return (
            ('today', _('Today')),
            ('this_week', _('Past 7 days')),
            ('this_month', _('This month')),
            ('this_year', _('This year')),
            ('never', _('Never')),
        )

    def queryset(self, request, queryset):
        if self.value() == 'today':
            return queryset.filter(last_login__gte=datetime.now() - timedelta(days=1))
        if self.value() == 'this_week':
            return queryset.filter(last_login__gte=datetime.now() - timedelta(days=7))
        if self.value() == 'this_month':
            return queryset.filter(last_login__gte=datetime.now() - timedelta(days=30))
        if self.value() == 'this_year':
            return queryset.filter(last_login__gte=datetime.now() - timedelta(days=365))
        if self.value() == 'never':
            return queryset.filter(last_login__isnull=True)


class IsTakeQuizFilter(admin.SimpleListFilter):
    title = _('Quiz Taken')
    parameter_name = 'is_take_quiz'

    def lookups(self, request, model_admin):
        return (
            ('yes', _('Yes')),
            ('no', _('No')),
        )

    def queryset(self, request, queryset):
        if self.value() == 'yes':
            user_ids = QuizResult.objects.values_list('user_id', flat=True)
            return queryset.filter(id__in=user_ids)
        if self.value() == 'no':
            user_ids = QuizResult.objects.values_list('user_id', flat=True)
            return queryset.exclude(id__in=user_ids)


class WithInvitesFilter(admin.SimpleListFilter):
    title = _('Invitation Sent')
    parameter_name = 'with_invites'

    def lookups(self, request, model_admin):
        return (
            ('yes', _('Yes')),
            ('no', _('No')),
        )

    def queryset(self, request, queryset):
        if self.value() == 'yes':
            user_ids = Invitation.objects.values_list('user_id', flat=True)
            return queryset.filter(id__in=user_ids)
        if self.value() == 'no':
            user_ids = Invitation.objects.values_list('user_id', flat=True)
            return queryset.exclude(id__in=user_ids)


class UserByAgreementPeriodFilter(admin.SimpleListFilter):
    title = _('Agreement Period')
    parameter_name = 'agreement_period'

    def lookups(self, request, model_admin):
        periods = AssignmentPeriod.objects.filter(end_date__gt=datetime.now()).select_related('property')
        return ((period.id, '{} - {}'.format(period.property.name, period.name)) for period in periods)

    def queryset(self, request, queryset):
        if self.value():
            user_ids = Invitation.objects.filter(agreement_period_id=self.value()).values_list('user_id', flat=True)
            return queryset.filter(id__in=user_ids)
