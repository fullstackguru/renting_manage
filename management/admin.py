from django.contrib import admin
from django.contrib.auth import get_user_model
from django.conf import settings
from django.conf.urls import patterns, url
from dbmail.models import MailCategory, MailTemplate
from dbmail.admin import MailTemplateAdmin
from functools import update_wrapper

from forms import InvitationAdminForm, BedAdminForm, PendingAssignmentAdminForm
from management.models import (Invitation, Organization, Property, AssignmentPeriod, OrganizationSocialHabitQuestion,
                               ResidentImport, OrganizationSocialHabitAnswer, UnitType, RoomType, Room, Unit,
                               PendingAssignment, Bed, ContractType, Stage, PropertySocialNetwork)
from utils.emails import send_welcome_email, send_account_reminder_email

User = get_user_model()


class OrganizationAdmin(admin.ModelAdmin):
    list_display = ('name',)


class PropertyManagerInline(admin.TabularInline):
    model = Property.managers.through
    extra = 1


class PropertyAdmin(admin.ModelAdmin):
    list_display = ('name',)
    inlines = [
        PropertyManagerInline,
    ]
    exclude = ('managers',)


class AssignmentPeriodAdmin(admin.ModelAdmin):
    list_display = ('name', 'property', 'start_date', 'end_date')


class OrganizationInvitationAdmin(admin.ModelAdmin):
    list_display = ('organization', 'property', 'user', 'invite_code')


class InvitationAdmin(admin.ModelAdmin):
    form = InvitationAdminForm
    list_display = ('agreement_period', 'user', 'invite_code')
    search_fields = ('user__email', 'user__first_name', 'user__last_name', 'invite_code')
    list_filter = ('agreement_period', )
    actions = ['resend_invite_email', 'send_reminder_email']
    list_select_related = ('user', 'room_type', 'unit_type', 'agreement_period', 'contract_type')

    def resend_invite_email(self, request, queryset):
        for invite in queryset:
            send_welcome_email(invite)
        self.message_user(request, "Resent welcome email for %s invites." % queryset.count())
    resend_invite_email.short_description = "Resend welcome email for selected invites"

    def send_reminder_email(self, request, queryset):
        for invite in queryset:
            send_account_reminder_email(invite)
        self.message_user(request, "Sent reminder emails for %s invites." % queryset.count())
    send_reminder_email.short_description = "Send reminder email"


class PendingAssignmentAdmin(admin.ModelAdmin):
    list_display = ('user', 'unit', 'room_type', 'assignment_period')
    form = PendingAssignmentAdminForm
    list_select_related = True


class UnitTypeAdmin(admin.ModelAdmin):
    list_display = ('property', 'name', 'max_occupancy')


class RoomTypeAdmin(admin.ModelAdmin):
    list_display = ('property', 'name', 'max_occupancy')

class PropertySocialNetworkAdmin(admin.ModelAdmin):
    list_display = ('property', 'network_name')

class UnitAdmin(admin.ModelAdmin):
    list_display = ('name', 'unit_type')
    search_fields = ('name', )


class RoomAdmin(admin.ModelAdmin):
    list_display = ('room_type',)


class OrganizationSocialHabitAnswerAdmin(admin.TabularInline):
    model = OrganizationSocialHabitAnswer
    readonly_fields = ('created_at', 'updated_at')
    extra = 0

    def has_add_permission(self, request, obj=None):
        return True

    def has_delete_permission(self, request, obj=None):
        return False


class OrganizationSocialHabitQuestionAdmin(admin.ModelAdmin):
    list_display = ('organization', 'question', 'required', 'sort_order', 'status', 'created_at', 'updated_at')
    inlines = (OrganizationSocialHabitAnswerAdmin, )
    change_list_template = "admin/organizationsocialhabitquestion/change_list.html"
    def get_urls(self):
        # this is just a copy paste from the admin code
        def wrap(view):
            def wrapper(*args, **kwargs):
                return self.admin_site.admin_view(view)(*args, **kwargs)
            return update_wrapper(wrapper, view)
        # get the default urls
        urls = super(OrganizationSocialHabitQuestionAdmin, self).get_urls()
        # define my own urls
        my_urls = patterns(
            '',
            url(r'^copyhabit/$', wrap(self.get_changelist), name="copyhabit")
        )
        # return the complete list of urls
        return my_urls + urls
    
    def get_changelist(self, request, extra_context=None):
        """
        Displays the "success" page after a password change.
        """
        errormsg= False
        if request.resolver_match.url_name == "copyhabit":
            if request.POST:
                error = False

                try:
                    organization_to = int(request.POST.get('organization_to', None))
                    organization_from = int(request.POST.get('organization_from', None))
                except (ValueError, TypeError, KeyError):
                    error = True
                    errormsg = "Both Organization are required."
                if not error:
                    if organization_to == organization_from:
                        error = True
                        errormsg = "Both Organization are same."
                    else:
                        error = self.set_copyhabit(organization_from,organization_to)
                        if error:
                            errormsg = "Organization have no questions to copy."

                if not error:
                    from django.shortcuts import get_object_or_404, redirect
                    from django.core.urlresolvers import reverse
                    return redirect(reverse("admin:management_organizationsocialhabitquestion_changelist",))
            from django.template.response import TemplateResponse
            organization = Organization.objects.all()
            defaults = {
                'current_app': 'test',
                'organization': organization,
                'errors': errormsg
            }
            template_name = 'admin/organizationsocialhabitquestion/copyhabit_form.html'
            return TemplateResponse(request,template_name, defaults)
        return super(OrganizationSocialHabitQuestionAdmin, self).get_changelist(request)

    def set_copyhabit(self, organization_from, organization_to):
        habitQuestion = OrganizationSocialHabitQuestion.objects.filter(organization_id=organization_from).order_by('id')
        if len(habitQuestion) == 0:
            return True
        for question in habitQuestion:
            habitAnswer = OrganizationSocialHabitAnswer.objects.filter(question_id=question.id)
            question.pk = None
            question.organization_id = organization_to
            question.save()
            for answer in habitAnswer:
                answer.pk = None
                answer.question_id = question.id
                answer.save()
        return False


class BedAdmin(admin.ModelAdmin):
    form = BedAdminForm
    search_fields = ['user__email', 'room__unit__name']


class ResidentImportAdmin(admin.ModelAdmin):
    list_display = ('property', 'created_at', 'status')
    readonly_fields = ('file', 'property', 'created_at', 'added_by', 'status')


class MailCategoryAdmin(admin.ModelAdmin):

    def get_actions(self, request):
        actions = super(MailCategoryAdmin, self).get_actions(request)
        del actions["delete_selected"]
        return actions

    def has_change_permission(self, request, obj=None):
        if obj is not None and obj.name in settings.DB_MAILER_DEFAULT_TEMPLATE_PREFIX:
            return False
        return super(MailCategoryAdmin, self).has_change_permission(request, obj=obj)


class CustomMailTemplateAdmin(MailTemplateAdmin):

    def get_actions(self, request):
        actions = super(CustomMailTemplateAdmin, self).get_actions(request)
        del actions["delete_selected"]
        return actions

    def has_delete_permission(self, request, obj=None):
        if obj is not None and obj.slug.startswith(settings.DB_MAILER_DEFAULT_TEMPLATE_PREFIX):
            return False
        return super(CustomMailTemplateAdmin, self).has_delete_permission(request, obj=obj)


admin.site.register(Stage)
admin.site.register(Organization, OrganizationAdmin)
admin.site.register(Property, PropertyAdmin)
admin.site.register(AssignmentPeriod, AssignmentPeriodAdmin)
admin.site.register(PendingAssignment, PendingAssignmentAdmin)
admin.site.register(Unit, UnitAdmin)
admin.site.register(Room, RoomAdmin)
admin.site.register(UnitType, UnitTypeAdmin)
admin.site.register(RoomType, RoomTypeAdmin)
admin.site.register(Invitation, InvitationAdmin)
admin.site.register(OrganizationSocialHabitQuestion, OrganizationSocialHabitQuestionAdmin)
admin.site.register(Bed, BedAdmin)
admin.site.register(ContractType)
admin.site.register(ResidentImport, ResidentImportAdmin)
admin.site.register(PropertySocialNetwork,PropertySocialNetworkAdmin)

admin.site.unregister(MailCategory)
admin.site.unregister(MailTemplate)
admin.site.register(MailCategory, MailCategoryAdmin)
admin.site.register(MailTemplate, CustomMailTemplateAdmin)
