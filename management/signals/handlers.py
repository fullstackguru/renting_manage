from datetime import datetime
from django.conf import settings
from django.db.models.signals import post_save, pre_save
from dbmail.signals import pre_send
from dbmail.backends.mail import Sender as MailSender
from dbmail.models import MailTemplate
from utils.emails import send_welcome_email
from management.models import Invitation, PendingAssignment


def send_welcome_email_signal(sender, instance, **kwargs):
    # raw parameter is set to True during loaddata management operation.
    if kwargs.get('raw'):
        return
    if kwargs.get('created'):
        send_welcome_email(instance)


def create_assignment(sender, instance, created, **kwargs):
    if created:
        PendingAssignment.objects.get_or_create(user=instance.user, assignment_period=instance.agreement_period,
                                                invite=instance, room_type=instance.room_type)


def mailtemplate_post_save(sender, instance, **kwargs):
    '''
    Premailer replace jinja tags in a href. Premailer transform called in MailTemplate save method.
    So firstly we save MailTemplate using dbmail save method(it's make different processes), than replace all
    incorrect symbols to jinja tags and call basic model save method.
    '''
    if not instance.context_note:
        instance.context_note = settings.DB_MAILER_DEFAULT_CONTEXT_NOTE
    post_save.disconnect(mailtemplate_post_save, sender=sender, dispatch_uid='mailtemplate_post_save')
    instance.message = instance.message.replace('%7B%7B', '{{')
    instance.message = instance.message.replace('%7b%7b', '{{')
    instance.message = instance.message.replace('%7D%7D', '}}')
    instance.message = instance.message.replace('%7d%7d', '}}')
    instance.message = instance.message.replace('%20', ' ')
    instance.message = instance.message.replace('&quot;', '"')
    super(sender, instance).save()
    post_save.connect(mailtemplate_post_save, sender=sender, dispatch_uid='mailtemplate_post_save')


def make_email_unique(**kwargs):
    '''
    Gmail clips similar emails, so adding hidden unique string to each email.
    '''
    instance = kwargs.get('instace', None)
    if instance:
        instance._message += '<span style="color: #FFF">{}</span>'.format(datetime.now())


pre_send.connect(make_email_unique, sender=MailSender)
post_save.connect(send_welcome_email_signal, sender=Invitation, dispatch_uid="send_welcome_email")
post_save.connect(create_assignment, sender=Invitation, dispatch_uid="create_assignment")
post_save.connect(mailtemplate_post_save, sender=MailTemplate, dispatch_uid='mailtemplate_post_save')
