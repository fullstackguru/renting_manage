# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('management', '0054_add_group'),
    ]

    operations = [
        migrations.AlterField(
            model_name='invitation',
            name='contract_type',
            field=models.ForeignKey(default=1, blank=True, to='management.ContractType', null=True),
        ),
    ]
