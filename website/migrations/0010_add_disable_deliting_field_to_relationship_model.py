# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('website', '0009_update_via_user_relations'),
    ]

    operations = [
        migrations.AddField(
            model_name='relationship',
            name='disable_deleting',
            field=models.BooleanField(default=False),
        ),
    ]
