# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('management', '0064_merge'),
    ]

    operations = [
        migrations.AlterField(
            model_name='organizationsocialhabitanswer',
            name='question',
            field=models.ForeignKey(related_name='answers', to='management.OrganizationSocialHabitQuestion'),
        ),
    ]
