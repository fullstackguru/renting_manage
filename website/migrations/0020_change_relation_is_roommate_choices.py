# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('website', '0019_set_roommate_relations'),
    ]

    operations = [
        migrations.AlterField(
            model_name='relationship',
            name='is_roommate',
            field=models.CharField(default=b'N', max_length=1, choices=[(b'N', b'Not roommates'), (b'D', b'Direct roommates'), (b'E', b'Proposed by end_user'), (b'S', b'Proposed by start_user')]),
        ),
    ]
