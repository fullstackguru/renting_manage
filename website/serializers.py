from rest_framework import serializers
from management.models import Room, Bed
from management.serializers import LiteUserSerializer


class SelectBedSerializer(serializers.ModelSerializer):
    is_available = serializers.SerializerMethodField()
    user = serializers.SerializerMethodField()

    class Meta:
        model = Bed
        fields = ('id', 'name', 'is_available', 'user')

    def get_is_available(self, obj):
        return obj.is_available if hasattr(obj, 'is_available') else False

    def get_user(self, obj):
        if obj.user:
            return LiteUserSerializer(obj.user).data
        return None
