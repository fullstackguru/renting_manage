"use strict";


var initInviteConfirm = function () {
    $('.invite-btn-submit').on('click', function (e) {
        e.preventDefault();
        var form = $(this).parents('form');
        var userName = $(this).attr('user-name');
        swal({
            title: "Are you sure?",
            text: "Are you sure that you want invite " + userName + "?",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#34a263",
            confirmButtonText: "Yes!",
            closeOnConfirm: false
        }, function (isConfirm) {
            if (isConfirm) form.submit();
        });
    });
};
