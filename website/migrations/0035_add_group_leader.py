# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
from django.db.models import Prefetch


class Migration(migrations.Migration):

    def add_group_leader(apps, schema_migration):
        Group = apps.get_model('website', 'Group')
        User = apps.get_model('website', 'User')
        groups = Group.objects.prefetch_related(Prefetch('users', User.objects.all().order_by('-points')))
        for group in groups:
            users = group.users.all()
            if not users:
                continue
            leader = users[0]
            group.leader = leader
            group.save()

    dependencies = [
        ('website', '0034_make_groups_unique'),
    ]

    operations = [
        migrations.RunPython(add_group_leader)
    ]
