import os
import uuid
import random
import cPickle as pickle
from datetime import datetime
import requests
from requests.exceptions import ConnectionError
import time
from itertools import chain
from django.utils.lru_cache import lru_cache
from django.core.validators import URLValidator
from django.core.exceptions import ValidationError
from django.conf import settings
# from django.contrib.auth.models import User
from django.contrib.auth.models import (
    BaseUserManager, AbstractBaseUser
)
from django.db import models
from django.db.models import Model, Q, Prefetch, F, Sum, Case, When, Count

from management.models import Invitation, OrganizationSocialHabitQuestion, OrganizationSocialHabitAnswer, Property, \
    AssignmentPeriod, Unit, UnitType, Stage, PendingAssignment


def content_file_name(instance, filename):
    return '/'.join(['content', instance.user.username, filename])


class UserManager(BaseUserManager):
    def create_user(self, email, password=None):
        """
        Creates and saves a User with the given email, date of
        birth and password.
        """
        if not email:
            raise ValueError('Users must have an email address')

        user = self.model(
            email=self.normalize_email(email)
        )

        user.set_password(password)
        user.save(using=self._db)
        return user

    def create_superuser(self, email, password):
        """
        Creates and saves a superuser with the given email and password.
        """
        user = self.create_user(email,
            password=password
        )
        user.is_admin = True
        user.save(using=self._db)
        return user


class User(AbstractBaseUser):
    GENDER_UNKNOWN = 'U'
    GENDER_MALE = 'M'
    GENDER_FEMALE = 'F'

    GENDER_CHOICES = (
        (GENDER_UNKNOWN, 'Unknown'),
        (GENDER_MALE, 'Male'),
        (GENDER_FEMALE, 'Female')
    )

    email = models.EmailField(
        verbose_name='email address',
        max_length=255,
        unique=True,
    )
    is_active = models.BooleanField(default=True)
    is_admin = models.BooleanField(default=False)
    first_name = models.CharField('first name', max_length=30, blank=True)
    last_name = models.CharField('last name', max_length=30, blank=True)
    gender = models.CharField('gender', max_length=1, choices=GENDER_CHOICES, default=GENDER_UNKNOWN)
    available_properties = models.ManyToManyField(Property, related_name='assigned_users',
                                                  help_text="Only selected properties can send invites for this user.")
    allow_co_educational_groups = models.BooleanField(default=False, verbose_name='Enable coed matching. Coed limits your matching to other coed users. ')
    hidden = models.BooleanField(default=False, verbose_name='Hide your profile from matching request')
    disable_emails = models.BooleanField(default=False, verbose_name='Turn off email notifications')
    points = models.PositiveSmallIntegerField(default=50)
    objects = UserManager()

    USERNAME_FIELD = 'email'
    REQUIRED_FIELDS = []

    def clean(self):
        self.email = self.email.lower()
        super(User, self).clean()

    def save(self, force_insert=False, force_update=False, using=None, update_fields=None):
        self.email = self.email.lower()
        super(User, self).save(force_insert, force_update, using, update_fields)

    def get_full_name(self):
        return self.email

    def get_short_name(self):
        return self.email

    def __unicode__(self):              # __str__ on Python 3
        return self.email

    def feed_str(self):
        return self.full_name

    def has_perm(self, perm, obj=None):
        return True

    def has_module_perms(self, app_label):
        return True

    @property
    def is_staff(self):
        return self.is_admin

    @property
    def is_superuser(self):
        return self.is_admin

    @property
    def full_name(self):
        return u"{} {}".format(self.first_name.capitalize(), self.last_name.capitalize())

    @property
    def stage(self):
        return Stage.objects.filter(min_points__lte=self.points, max_points__gte=self.points).first()


class UserProfile(Model):
    user = models.OneToOneField(settings.AUTH_USER_MODEL, related_name='profile')

    # Traitify Results
    personality_types = models.TextField(null=True)
    personality_traits = models.TextField(null=True)

    # Bio
    bio = models.TextField(null=True, blank=True)

    # Social Links
    twitter_url = models.URLField(null=True, max_length=250, blank=True)
    facebook_url = models.URLField(null=True, max_length=250, blank=True)
    pinterest_url = models.URLField(null=True, max_length=250, blank=True)
    linkedin_url = models.URLField(null=True, max_length=250, blank=True)
    instagram_url = models.URLField(null=True, max_length=250, blank=True)
    snapchat_url = models.URLField(null=True, max_length=250, blank=True)
    google_plus_url = models.URLField(null=True, max_length=250, blank=True)

    # Profile Image
    profile_image = models.ImageField(blank=True, null=True, upload_to='uploads/avatar/%Y/%m/%d/')
    profile_image_bg = models.ImageField(blank=True, null=True, upload_to='uploads/avatar_bg/%Y/%m/%d/')
    image_url = models.CharField(blank=True, null=True, max_length=250)
    bg_url = models.CharField(blank=True, null=True, max_length=250)

    def __unicode__(self):
        return self.user.email

    @lru_cache(maxsize=2048)
    def get_personality_traits_json(self):
        return pickle.loads(str(self.personality_traits)) if self.personality_traits else {}

    @lru_cache(maxsize=2048)
    def get_personality_types_json(self):
        return pickle.loads(str(self.personality_types)) if self.personality_types else {}

    def _get_image_url(self, results):
        photo = {}
        for photo in results.get('photos', []):
            if photo.get('isPrimary'):
                return photo.get('url')

        return photo.get('url')

    def _get_random_background_image(self):
        photos = filter(lambda photo_file: '@' not in photo_file, os.listdir(settings.BG_ROOT))
        return os.path.join(settings.BG_URL, random.choice(photos))

    def save(self, force_insert=False, force_update=False, using=None, update_fields=None):
        try:
            results = requests.get(
                settings.FULLCONTACT_API_URL.format(self.user.email, settings.FULLCONTACT_API_KEY)).json()
        except ConnectionError:
            time.sleep(1)
            results = requests.get(
                settings.FULLCONTACT_API_URL.format(self.user.email, settings.FULLCONTACT_API_KEY)).json()
        for social_profile in results.get("socialProfiles", []):
            validate = URLValidator()
            if social_profile.get("type") == 'facebook':
                self.facebook_url = social_profile.get('url')
                try:
                    validate(self.facebook_url)
                except ValidationError, e:
                    self.facebook_url = None
            elif social_profile.get("type") == 'twitter':
                self.twitter_url = social_profile.get('url')
                try:
                    validate(self.twitter_url)
                except ValidationError, e:
                    self.twitter_url = None
            elif social_profile.get("type") == 'pinterest':
                self.pinterest_url = social_profile.get('url')
                try:
                    validate(self.pinterest_url)
                except ValidationError, e:
                    self.pinterest_url = None
            elif social_profile.get("type") == 'linkedin':
                self.linkedin_url = social_profile.get('url')
                try:
                    validate(self.linkedin_url)
                except ValidationError, e:
                    self.linkedin_url = None

            if self.bio is None and social_profile.get('bio'):
                self.bio = social_profile.get('bio')

        if self.image_url is None:
            self.image_url = self._get_image_url(results)

        if self.bg_url is None:
            self.bg_url = self._get_random_background_image()

        super(UserProfile, self).save(force_insert=False, force_update=False, using=None, update_fields=None)


class QuizResult(Model):
    user = models.ForeignKey(settings.AUTH_USER_MODEL)
    traitify_assessment_id = models.CharField(null=True, max_length=50)

class RelationshipBaseManager(models.Manager):
    pass

class RelationshipNotDeletedManager(RelationshipBaseManager):
    def get_queryset(self):
        return super(RelationshipNotDeletedManager, self).get_queryset().filter(is_deleted=False)

class RelationshipAllEntriesManager(RelationshipBaseManager):
    pass

class Relationship(Model):
    NOT_ROOMMATE = 'N'
    DIRECT_ROOMMATE = 'D'
    PROPOSED_BY_END_USER = 'E'
    PROPOSED_BY_START_USER = 'S'
    ROOMMATE_CHOICES = (
        (NOT_ROOMMATE, 'Not roommates'),
        (DIRECT_ROOMMATE, 'Direct roommates'),
        (PROPOSED_BY_END_USER, 'Proposed by end_user'),
        (PROPOSED_BY_START_USER, 'Proposed by start_user'),
    )

    group = models.ForeignKey('Group', null=True, blank=True, related_name='relations')
    start_user = models.ForeignKey(settings.AUTH_USER_MODEL, related_name='start_user')
    # organization_invitation = models.ForeignKey(OrganizationInvitation, blank=True, null=True)
    invitation = models.ForeignKey(Invitation)
    end_user = models.ForeignKey(settings.AUTH_USER_MODEL, related_name='end_user')
    end_user_approved = models.BooleanField(default=False)
    invite_datetime = models.DateTimeField(default=datetime.now)
    approval_datetime = models.DateTimeField(null=True)
    # This field used for relations via friends, for direct relation this value is None
    via_user = models.ForeignKey(settings.AUTH_USER_MODEL, null=True, blank=True, default=None)
    # This field used for restricting deleting of relations
    read_only = models.BooleanField(default=False)
    is_roommate = models.CharField(max_length=1, choices=ROOMMATE_CHOICES, default=NOT_ROOMMATE)

    is_deleted = models.BooleanField(default=False)

    objects = RelationshipNotDeletedManager()
    all_objects = RelationshipAllEntriesManager()

    def __unicode__(self):
        return u"Relation between {} and {} via user {}.".format(self.start_user, self.end_user, self.via_user)

    def save(self, force_insert=False, force_update=False, using=None, update_fields=None):
        print("relationship save")
        if self.start_user == self.end_user:
            return
        if self.end_user_approved and self.approval_datetime:
            from utils.services import get_group_unit
            print("start user: ")
            print(self.start_user.id)
            print(self.start_user)
            print("end user: ")
            print(self.end_user.id)
            print(self.end_user)
            groups = Group.objects.filter(agreement_period=self.invitation.agreement_period)
            start_user_group = groups.filter(users__in=[self.start_user]).first()
            if start_user_group:
                print("there is a start_user_group")
            end_user_group = groups.filter(users__in=[self.end_user]).first()
            if end_user_group:
                print("there is an end_user_group")
            leader_group = groups.filter(leader=self.end_user).first()
            if leader_group:
                print("there is a leader group")
            leader_start_user_group = groups.filter(leader=self.start_user).first()
            if leader_start_user_group:
                print("there is a leader group")

            if start_user_group:
                start_user_group.delete()
            if end_user_group:
                end_user_group.delete()
            if leader_group:
                leader_group.delete()
            if leader_start_user_group:
                leader_start_user_group.delete()

            start_user_group_members = start_user_group.users.exclude(id=self.start_user.id) if start_user_group else []
            end_user_group_members = end_user_group.users.exclude(id=self.end_user.id) if end_user_group else []
            group = Group.objects.create(agreement_period=self.invitation.agreement_period,
                                         unit_type=self.invitation.unit_type)
            print("group = ")
            print(group.id)
            relations = []
            print("for another group member in end user group members: ")
            for another_group_member in end_user_group_members:
                print(another_group_member)
                if self.start_user == another_group_member:
                    continue
                relation = Relationship(start_user=self.start_user, end_user=another_group_member,
                                        invitation=self.invitation, via_user=self.end_user, end_user_approved=True,
                                        approval_datetime=datetime.now(), read_only=True)
                relations.append(relation)
            print("for another group member in start user group members: ")
            for another_group_member in start_user_group_members:
                print(another_group_member)
                if self.end_user == another_group_member:
                    continue
                relation = Relationship(start_user=self.end_user, end_user=another_group_member,
                                        invitation=self.invitation, via_user=self.start_user, end_user_approved=True,
                                        approval_datetime=datetime.now(), read_only=True)
                relations.append(relation)
            print("for member in start user group members: ")
            for member in start_user_group_members:
                print(member)
                for another_group_member in end_user_group_members:
                    if member == another_group_member or member == self.end_user or \
                                    another_group_member == self.start_user:
                        continue
                    relation = Relationship(
                        start_user=member, 
                        end_user=another_group_member,
                        invitation=self.invitation, 
                        via_user=self.start_user, 
                        end_user_approved=True,
                        approval_datetime=datetime.now(), 
                        read_only=True)
                    relations.append(relation)
            if relations:
                print("if relations:")
                Relationship.objects.bulk_create(relations)
            members = list(chain(start_user_group_members, end_user_group_members, [self.start_user, self.end_user]))
            Relationship.objects.filter(
                start_user__in=members, 
                end_user__in=members, 
                end_user_approved=True,
                approval_datetime__isnull=False,
                invitation__agreement_period=group.agreement_period).update(group=group)
            print(members)
            unit_id = get_group_unit(members)
            print(unit_id)
            group.unit_id = unit_id
            print(self.id)
            print(group.id)
            try:
                group.save()
            except Exception as e:
                print(e)
                return
            print("saved")
            group.users.add(*members)
        return super(Relationship, self).save(force_insert, force_update, using, update_fields)

    def set_roommate_relationship(self, relation_type=DIRECT_ROOMMATE):
        '''
        Set is_roommate flag for this relation and via roommate relations.
        '''
        direct_statuses = [self.PROPOSED_BY_START_USER, self.PROPOSED_BY_END_USER]
        other_statuses = [self.NOT_ROOMMATE, self.PROPOSED_BY_START_USER, self.PROPOSED_BY_END_USER]
        if relation_type == self.DIRECT_ROOMMATE and self.is_roommate in direct_statuses:
            users = [self.start_user, self.end_user]
            Relationship.objects.filter(
                    Q(start_user__in=users) | Q(end_user__in=users)
                ).update(is_roommate=self.NOT_ROOMMATE)
            self.is_roommate = self.DIRECT_ROOMMATE
            super(Relationship, self).save()
        elif relation_type in other_statuses:
            self.is_roommate = relation_type
            super(Relationship, self).save()

    def delete(self, using=None):
        '''
        Delete relation + all related relations (via friends)
        '''
        print("relationship normal delete")
        if self.end_user_approved and self.approval_datetime:
            from utils.services import get_related_user_ids

            user = self.start_user
            relate_user = self.end_user

            for _ in range(2):
                # Remove relations
                deleted_ids = {relate_user.id}
                relate_user_relations = Relationship.objects.filter(
                        Q(start_user=relate_user)| Q(end_user=relate_user)
                    ).exclude(
                        Q(start_user=user) | Q(end_user=user)
                    ).exclude(
                        via_user=user)
                relate_user_relations_ids = set()
                for r in relate_user_relations:
                    relate_user_relations_ids.add(r.start_user_id)
                    relate_user_relations_ids.add(r.end_user_id)
                if relate_user_relations_ids:
                    relate_user_relations_ids.remove(relate_user.id)

                while True:
                    relate_user_friends = Relationship.objects.filter(
                        Q(start_user=user) | Q(end_user=user),
                        via_user__id__in=deleted_ids
                    )
                    if relate_user_friends:
                        for relation in relate_user_friends:
                            deleted_ids.add(relation.start_user_id)
                            deleted_ids.add(relation.end_user_id)
                        relate_user_friends.delete()
                    else:
                        break

                weak_relations = Relationship.objects.filter(
                    Q(start_user_id__in=relate_user_relations_ids) | Q(end_user_id__in=relate_user_relations_ids),
                    via_user=user
                )
                weak_relations.delete()
                user, relate_user = relate_user, user
            result = super(Relationship, self).delete(using)

            agreement_period = self.invitation.agreement_period
            start_user_group_members = get_related_user_ids(self.start_user)
            end_user_group_members = get_related_user_ids(self.end_user)
            PendingAssignment.objects.filter(assignment_period=agreement_period,
                                             user__in=end_user_group_members).update(unit=None, room=None, bed=None)
            group = Group.objects.filter(agreement_period=agreement_period,
                                         users__in=[self.start_user, self.end_user]).first()
            if not group:
                return result
            start_user_group = Group(agreement_period=agreement_period, unit_type=group.unit_type, unit=group.unit)
            end_user_group = Group(agreement_period=agreement_period, unit_type=group.unit_type, unit=None)
            if len(start_user_group_members) > 1:
                start_user_group.save()
                start_user_group.users.add(*chain(start_user_group_members, [self.start_user.id]))
                Relationship.objects.filter(start_user__in=start_user_group_members,
                                            end_user__in=start_user_group_members, approval_datetime__isnull=False,
                                            end_user_approved=True).update(group=start_user_group)
            if len(end_user_group_members) > 1:
                end_user_group.save()
                end_user_group.users.add(*chain(end_user_group_members, [self.end_user.id]))
                Relationship.objects.filter(start_user__in=end_user_group_members,
                                            end_user__in=end_user_group_members, approval_datetime__isnull=False,
                                            end_user_approved=True).update(group=end_user_group)
            group.delete()
            return result
        return super(Relationship, self).delete(using)

    def delete_single(self):
        """
        Delete single relation only
        """
        print("delete_single")
        self.is_deleted = True
        return super(Relationship, self).save(
            force_insert=False,
            force_update=False,
            using=None,
            update_fields=None)

class Group(Model):
    agreement_period = models.ForeignKey(AssignmentPeriod, related_name='groups')
    leader = models.ForeignKey(User, null=True, blank=True, related_name='group_leader')
    users = models.ManyToManyField(User, related_name='groups')
    unit = models.ForeignKey(Unit, null=True, blank=True)
    unit_type = models.ForeignKey(UnitType)
    size = models.PositiveSmallIntegerField(null=False, default=0)
    is_locked = models.BooleanField(default=False)

    class Meta:
        unique_together = (('agreement_period', 'unit'),)

    def save(self, **kwargs):
        print("group save")
        if self.unit:
            print("if self.unit: ")
            self.unit_type = self.unit.unit_type
        print("return super")
        return super(Group, self).save(**kwargs)

    @property
    def empty_places(self):
        print("empty_places")
        empty_number = self.unit_type.max_occupancy - self.size
        print(empty_number)
        return empty_number

    def change_unit(self, unit):
        self.unit = unit
        self.save()
        users = self.users.all()
        PendingAssignment.objects.filter(assignment_period=self.agreement_period, user__in=users).\
            update(unit=unit, room=None, bed=None)

    def allowed_units(self):
        if self.size > self.unit_type.max_occupancy:
            return Unit.objects.none()
        blocked_beds = self.agreement_period.blocked_beds.values_list('id', flat=True)
        units = Unit.objects.filter(unit_type=self.unit_type)
        units_with_blocked_beds = units.annotate(
            blocked_beds=Sum(
                Case(
                    When(
                        rooms__beds__id__in=blocked_beds,
                        then=1
                    ),
                    default=0,
                    output_field=models.IntegerField()
                )
            ),
        ).values('id', 'blocked_beds').order_by('id')
        units_with_free_places = units.annotate(
            free_places=F('unit_type__max_occupancy') - Sum(
                Case(
                    When(
                        pending_assignments__assignment_period=self.agreement_period,
                        then=F('pending_assignments__invite__contract_type__occupied_places'),
                    ),
                    default=0,
                    output_field=models.IntegerField()
                )
            )
        ).values('id', 'free_places').order_by('id')
        units_ids = []
        for blocked_beds, free_places in zip(units_with_blocked_beds, units_with_free_places):
            if free_places['free_places'] - blocked_beds['blocked_beds'] - self.size >= 0:
                units_ids.append(free_places['id'])
        units = units.filter(id__in=units_ids)
        return units

    def lock(self):
        users = self.users.all()
        if self.size < self.unit_type.max_occupancy:
            raise Exception('Unable to lock unfilled group!')
        members_relations = Relationship.objects.filter(Q(start_user__in=users) |
                                                        Q(end_user__in=users))
        #  remove pending invites
        members_relations.filter(approval_datetime__isnull=True, end_user_approved=False).delete()
        #  set group relations as readonly
        members_relations.update(read_only=True)
        #  lock invites
        Invitation.objects.filter(user__in=users).update(locked=True)
        self.is_locked = True
        self.save()

    def group_roommates(self, user=None):
        if not self.unit:
            return
        members = self.users.all()
        all_roommates = Relationship.objects.filter(start_user__in=members, end_user__in=members,
                                                    invitation__agreement_period=self.agreement_period,
                                                    is_roommate=Relationship.DIRECT_ROOMMATE)
        if user:
            all_roommates = all_roommates.filter(Q(start_user=user) | Q(end_user=user))
        for roommates in all_roommates:
            assignments = PendingAssignment.objects.filter(assignment_period=self.agreement_period).\
                select_related('bed', 'bed__room', 'invite__contract_type', 'user')
            start_user_assignment = assignments.filter(user=roommates.start_user).first()
            end_user_assignment = assignments.filter(user=roommates.end_user).first()
            if user and end_user_assignment.user == user:
                start_user_assignment, end_user_assignment = end_user_assignment, start_user_assignment
            excluded_rooms = []
            if not all([start_user_assignment, end_user_assignment]):
                continue
            if all([start_user_assignment.bed, end_user_assignment.bed]):
                if start_user_assignment.bed.room == end_user_assignment.bed.room:
                    continue
            if start_user_assignment.bed:
                excluded_rooms.append(start_user_assignment.bed.room.id)
                available_beds = start_user_assignment.bed.room.get_available_beds(self.agreement_period)
                if available_beds:
                    end_user_assignment.bed = available_beds[0]
                    end_user_assignment.room = start_user_assignment.bed.room
                    end_user_assignment.save()
                    continue
            if end_user_assignment.bed:
                excluded_rooms.append(end_user_assignment.bed.room.id)
                available_beds = end_user_assignment.bed.room.get_available_beds(self.agreement_period)
                if available_beds:
                    start_user_assignment.bed = available_beds[0]
                    start_user_assignment.room = end_user_assignment.bed.room
                    start_user_assignment.save()
                    continue

            other_rooms = self.unit.rooms.exclude(id__in=excluded_rooms)
            space_needed = start_user_assignment.invite.contract_type.occupied_places + \
                           end_user_assignment.invite.contract_type.occupied_places
            for room in other_rooms:
                available_beds = room.get_available_beds(self.agreement_period)
                if len(available_beds) >= space_needed:
                    start_user_assignment.bed = available_beds[0]
                    start_user_assignment.room = room
                    end_user_assignment.bed = available_beds[1]
                    end_user_assignment.room = room
                    start_user_assignment.save()
                    end_user_assignment.save()
                    break


class UserConversation(Model):
    user1 = models.ForeignKey(settings.AUTH_USER_MODEL, related_name='user1')
    user2 = models.ForeignKey(settings.AUTH_USER_MODEL, related_name='user2')
    conversation_uuid = models.UUIDField(default=uuid.uuid4, db_index=True)
    created_at = models.DateTimeField(auto_now=True, editable=False, db_index=True)


class ConversationMessage(Model):
    conversation = models.ForeignKey(UserConversation, related_name='user_conversation')
    # Here the user is message sender
    user = models.ForeignKey(settings.AUTH_USER_MODEL)
    message = models.TextField(null=False)
    unread = models.BooleanField(default=True)
    created_at = models.DateTimeField(auto_now=True, editable=False, db_index=True)


class SocialNetworks(Model):
    userprofile = models.ForeignKey(UserProfile, related_name='userprofile')
    network_name = models.CharField(null=False, max_length=50)
    network_url = models.CharField(null=False, max_length=250)


class UserOrganizationSocialHabitAnswers(Model):
    userprofile = models.ForeignKey(UserProfile, related_name='answers')
    # organization_invitation = models.ForeignKey(OrganizationInvitation, blank=True, null=True)
    invitation = models.ForeignKey(Invitation)
    organization_question = models.ForeignKey(OrganizationSocialHabitQuestion)
    organization_question_text = models.CharField(null=False, max_length=250)
    organization_answer = models.ForeignKey(OrganizationSocialHabitAnswer)
    organization_answer_text = models.CharField(null=False, max_length=250)
    sort_order = models.IntegerField(default=1)
    created_at = models.DateTimeField(editable=False, db_index=True)


class UserSocialHabitAnswersHistory(Model):
    # This table not in real use at the moment, just accumulate users answers
    id = models.AutoField(primary_key=True)
    user_id = models.IntegerField(null=False)
    organization_id = models.IntegerField(null=False)
    invitation_id = models.IntegerField(null=False)
    question_id = models.IntegerField(null=False)
    answer_id = models.IntegerField(null=False)
    answered_at = models.DateTimeField(editable=False)
    question_text = models.CharField(null=False, max_length=250)
    answer_text = models.CharField(null=False, max_length=250)
    archived_at = models.DateTimeField(editable=False)

    @classmethod
    def restore_answers(cls, user_id):
        answers_history = UserSocialHabitAnswersHistory.objects.filter(user_id=user_id)
        question_ids = answers_history.order_by('question_id').values_list('question_id', flat=True).\
            distinct('question_id')
        if not question_ids: return False
        for question_id in question_ids:
            archived_answer = answers_history.filter(question_id=question_id).order_by('-archived_at').first()
            if not archived_answer:
                continue
            user = User.objects.get(id=user_id)
            invite = Invitation.objects.filter(user=user).first()
            if not invite:
                return False
            UserOrganizationSocialHabitAnswers.objects.update_or_create(userprofile=user.profile, invitation=invite,
                                                                        organization_question_id=question_id, defaults={
                    'organization_question_text': archived_answer.question_text,
                    'organization_answer_id': archived_answer.answer_id,
                    'organization_answer_text': archived_answer.answer_text,
                    'created_at': datetime.now()
                })
        return True
