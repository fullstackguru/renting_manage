# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
from django.conf import settings


class Migration(migrations.Migration):

    dependencies = [
        ('management', '0053_fix_lorenzo_units'),
    ]

    operations = [
        migrations.AddField(
            model_name='residentimport',
            name='error',
            field=models.CharField(default=1, max_length=256),
            preserve_default=False,
        ),
        migrations.AlterField(
            model_name='invitation',
            name='user',
            field=models.ForeignKey(related_name='invites', to=settings.AUTH_USER_MODEL),
        ),
    ]
