var initInvitationTable = function(listUrl, updateUrl) {
    $('#invitation-table').on('preXhr.dt', function () {
        $('tr').attr('hidden', true);
        App.blocks('#invitation-table-block', 'refresh_toggle');
    });
    $('#invitation-table').on('xhr.dt', function () {
        $('tr').attr('hidden', false);
        App.blocks('#invitation-table-block', 'refresh_toggle');
    });
    $('#invitation-table').DataTable({
        initComplete: function () {
            var api = this.api();
            $('#invitation-table_filter input')
                .off('.DT')
                .on('keyup.DT', function (e) {
                    if (e.keyCode == 13) {
                        api.search(this.value).draw();
                    }
                });
        },
        "serverSide": true,
        "processing": true,
        "bSort": true,
        "bPaginate": true,
        "sPaginationType": "full_numbers",
        "iDisplayLength": 10,
        ajax: {
            "dataType": 'json',
            url: listUrl,
            "dataSrc": function (json) {
                return json.data
            }
        },
        "aoColumns": [
            {
                "mDataProp": "agreement_period"
            },
            {
                "mDataProp": "unit",
                "render": function (data, type, full, meta) {
                    if (data) {
                        return data;
                    }
                    return "WAITLIST"
                }
            },
            {
                "mDataProp": "room",
                "render": function (data, type, full, meta) {
                    if (data) {
                        return data;
                    }
                    return "WAITLIST"
                },
                "orderable": false
            },
            {
                "mDataProp": "bed",
                "render": function (data, type, full, meta) {
                    if (data) {
                        return data;
                    }
                    return "WAITLIST"
                },
                "orderable": false
            },
            {"mDataProp": "first_name"},
            {"mDataProp": "last_name"},
            {
                "mDataProp": "email",
                "render": function (data, type, full, meta) {
                    var update_url = '<a href="' + updateUrl + full.user + '">' + data + '</a>';
                    return update_url;
                }
            },
            {"mDataProp": "unit_type"},
            {"mDataProp": "room_type"},
            {"mDataProp": "gender"}
        ],
        paging: true,
        "lengthMenu": [[10, 25, 50, 100], [10, 25, 50, 100]]
    });
};
