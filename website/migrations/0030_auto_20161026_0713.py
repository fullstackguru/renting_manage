# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('website', '0029_add_disable_email_field_to_user'),
    ]

    operations = [
        migrations.AlterField(
            model_name='user',
            name='allow_co_educational_groups',
            field=models.BooleanField(default=False, verbose_name=b'Enable coed matching. Coed limits your matching to other coed users. '),
        ),
        migrations.AlterField(
            model_name='user',
            name='disable_emails',
            field=models.BooleanField(default=False, verbose_name=b'Turn off email notifications'),
        ),
        migrations.AlterField(
            model_name='user',
            name='hidden',
            field=models.BooleanField(default=False, verbose_name=b'Hide your profile from matching request'),
        ),
    ]
