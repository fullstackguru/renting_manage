from django import template
from collections import defaultdict


register = template.Library()


@register.simple_tag
def print_beds(beds):
    if beds:
        units = defaultdict(list)
        for bed in beds:
            units[bed.room.unit.name].append(bed.name)
        return ';'.join(["Unit {}: Beds {}".format(unit, ','.join(beds)) for unit, beds in units.iteritems()])
    else:
        return 'WAITLIST'


@register.simple_tag
def active(request, pattern):
    import re
    if re.search(pattern, request.path):
        return 'active'
    return ''


@register.simple_tag
def open(request, *patterns):
    import re
    if any([re.search(pattern, request.path) for pattern in patterns]):
        return 'open'
    return ''
