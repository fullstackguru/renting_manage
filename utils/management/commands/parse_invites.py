from optparse import make_option
import sys, os
from datetime import datetime, timedelta

from django.core.exceptions import ValidationError
from django.core.management.base import BaseCommand, CommandError
from django.utils.translation import activate
from django.db.models import Count, Q
from django.core.validators import validate_email
from openpyxl import load_workbook, cell

from django.utils import timezone

from django.contrib.auth import get_user_model
User = get_user_model()
from website.models import UserManager
from management.models import Organization, Property, AssignmentPeriod, Invitation, UnitType, RoomType, Unit, Room, \
    PendingAssignment, Bed, ContractType
from utils.services import create_relations_for_suitemates, break_unnecessary_relations


class Command(BaseCommand):
    help = 'Parses records from file and creates user invites from them.'

    option_list = BaseCommand.option_list + (
        make_option('--file',
                    action="store",
                    type="string",
                    dest="path",
                    help='Path to file with data'
                    ),
        make_option('--remove_unnecessary_users',
                    action="store",
                    type="string",
                    dest="remove_unnecessary_users",
                    help="Set variable to true if you want to remove users that doesn't appears in file.",
                    default=False,
                    ),
        )

    def handle(self, *args, **options):
        activate('en')
        print options
        path = options.get('path') or ''
        print path
        if not path:
            raise CommandError('Please set filename!')
        try:
            workbook = load_workbook(path, read_only=True, use_iterators=True)
        except IOError:
            raise CommandError('No such file!')
        first_sheet = workbook.get_sheet_names()[0]

        worksheet = workbook.get_sheet_by_name(first_sheet)

        errors = []

        all_users = set()
        for index, items_row in enumerate(worksheet.rows):  # over each row
            # print items_row
            if index == 0:
                continue
            formatted_cells = [unicode(i.value).strip() if i.value else '' for i in items_row]
            org_raw = {
                'name': formatted_cells[0]
            }
            prop_raw = {
                'name': formatted_cells[0]
            }
            agreement_period_raw = {
                'name': formatted_cells[1],
            }
            user_raw = {
                'first_name': formatted_cells[4],
                'last_name': formatted_cells[5],
                'email': formatted_cells[6].lower().replace(';', '')
            }
            unit_raw = {
                'name': formatted_cells[2] if formatted_cells[2] != 'open' else 'open',
            }

            # try:
            #     unit_type, room_type = map(unicode.strip, formatted_cells[7].split('-'))
            # except ValueError:
            #     unit_type, room_type = formatted_cells[7], 'Open Room'

            unit_type_raw = {
                'name': formatted_cells[7],
            }
            room_type_raw = {
                'name': formatted_cells[8],
            }
            bed_raw = {
                'name': formatted_cells[3],
            }
            contract_type_raw = {
                'name': formatted_cells[10],
            }

            org, org_created = Organization.objects.get_or_create(name=org_raw['name'])

            prop, prop_created = Property.objects.update_or_create(name=prop_raw['name'], organization=org)

            # print agreement_period

            as_per, as_per_created = AssignmentPeriod.objects.update_or_create(
                name=agreement_period_raw['name'], property=prop,
                    defaults={'start_date': timezone.now(),
                              'end_date': timezone.now() + timedelta(days=364)})

            unit_type, unit_type_created = UnitType.objects.get_or_create(name=unit_type_raw['name'], property=prop)

            room_type, room_type_created = RoomType.objects.get_or_create(name=room_type_raw['name'], property=prop)
            contract_type, contract_type_created = ContractType.objects.get_or_create(name=contract_type_raw['name'])
            try:
                email = UserManager.normalize_email(user_raw['email'])
                print email
                try:
                    validate_email(email)
                except ValidationError as e:
                    print(e)
                    continue
                print email, user_raw['email']
                user, user_created = User.objects.get_or_create(email=email,
                                                                defaults={'first_name': user_raw['first_name'],
                                                                          'last_name': user_raw['last_name'],
                                                                          'gender': formatted_cells[9][0]})
                all_users.add(user.id)

                if user_created:
                    user.set_unusable_password()
                    user.save()
                user.available_properties.add(prop)
            except Exception as e:
                exc_type, exc_obj, exc_tb = sys.exc_info()
                fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
                print(user_raw['email'], e)
                print(exc_type, fname, exc_tb.tb_lineno)
                errors.append((index, index, user_raw['email'], exc_type, fname, exc_tb.tb_lineno))
                continue

            # User is not assigned to any bed/room/unit, so we just create invite

            print 'CREATING/UPDATING INVITE for user: {}'.format(user.email)
            try:
                invite, invite_created = Invitation.objects.update_or_create(
                    user=user, agreement_period=as_per, defaults={
                        'invitation_type': 'A',
                        'unit_type': unit_type,
                        'room_type': room_type,
                        'expire_date': as_per.end_date,
                        'contract_type': contract_type
                    }
                )
            except Invitation.MultipleObjectsReturned:
                invite, invite_created = Invitation.objects.filter(user=user, agreement_period=as_per).first(), False
            # Will assign or update assignment user to room
            if not unit_raw.get('name') == 'open' and not bed_raw.get('name') == 'WL':
                unit, unit_created = Unit.objects.update_or_create(name=unit_raw['name'], unit_type=unit_type,
                                                                   defaults={'max_rooms': int(unit_type.name[0])})
                room, room_created = Room.objects.get_or_create(unit=unit, room_type=room_type)
                bed, bed_created = Bed.objects.update_or_create(name=bed_raw['name'], room=room, defaults={'user': user})
                pa, pa_created = PendingAssignment.objects.update_or_create(user=user, assignment_period=as_per,
                                                                            invite=invite,
                                                                            defaults={'unit': unit, 'room_type': room_type})
                create_relations_for_suitemates(user)
                break_unnecessary_relations(user)
                print pa, pa_created

            if not user_created:
                    continue

            print '/n/n/n'
            for i in errors:
                print i
            print '/n/n/n'

            print 'Total errors count {}'.format(len(errors))

        print len(all_users)
        remove_unnecessary_users = options.get('remove_unnecessary_users', None)
        if remove_unnecessary_users and prop:
            print "Start removing unnecessary users"
            unnecessary_invites = Invitation.objects.filter(agreement_period__property=prop).\
                exclude(user_id__in=all_users)
            print "Number of unnecessary invites: {}".format(unnecessary_invites.count())
            users_with_multiple_invites = Invitation.objects.values_list('user_id', flat=True).\
                annotate(num_of_invites=Count('id')).filter(num_of_invites__gt=1)
            users_with_multiple_invites = list(users_with_multiple_invites)
            unnecessary_users_ids = unnecessary_invites.values_list('user_id', flat=True).\
                exclude(user_id__in=users_with_multiple_invites)
            print unnecessary_users_ids
            # remove many-to-many relations
            m2m_relations = User.available_properties.through.objects.filter(property_id=prop.id).\
                exclude(user_id__in=all_users)
            print "Number of unnecessary m2m relations: {}".format(m2m_relations.count())
            m2m_relations.delete()
            unnecessary_users = User.objects.filter(id__in=unnecessary_users_ids)
            print "Number of unnecessary users: {}".format(unnecessary_users.count())
            unnecessary_relations = User.objects.filter(Q(start_user_id__in=unnecessary_users_ids) |
                                                        Q(end_user_id__in=unnecessary_users_ids))
            print "Number of unnecessary relations: {}".format(unnecessary_relations.count())
            unnecessary_relations.delete()
            unnecessary_invites.delete()
            unnecessary_users.delete()
