# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('management', '0066_realtyimport'),
    ]

    operations = [
        migrations.AddField(
            model_name='realtyimport',
            name='error',
            field=models.CharField(max_length=256, null=True),
        ),
        migrations.AddField(
            model_name='residentimport',
            name='error',
            field=models.CharField(max_length=256, null=True),
        ),
    ]
