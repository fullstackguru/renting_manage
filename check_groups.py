import os
os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'renting.settings')
os.environ.setdefault('PYTHONUNBUFFERED', '1')

import django
django.setup()
from website.models import User, QuizResult, UserOrganizationSocialHabitAnswers
from management.models import Bed, Invitation, PendingAssignment, Unit
from utils.services import get_group_size, get_related_user_ids


def get_group_unit(group_members):
    group_unit = PendingAssignment.objects.filter(user_id__in=group_members).order_by('unit').\
        values_list('unit', flat=True).distinct()
    if not group_unit:
        return None
    return group_unit


def check_2_beds():
    users = User.objects.all()
    for u in users:
        beds = Bed.objects.filter(user=u)
        if beds.count() > 1:
            print(u, beds)


def check_2_invites():
    users = User.objects.all()
    for u in users:
        invites = Invitation.objects.filter(user=u)
        if invites.count() > 1:
            print invites


def check_similar_names():
    users = User.objects.all()
    for u in users:
        users_with_same_name = User.objects.filter(first_name=u.first_name, last_name=u.last_name).exclude(id=u.id)
        if users_with_same_name.exists():
            print u, users_with_same_name


def check_overflowed_groups():
    users = User.objects.all()
    for u in users:
        group = get_related_user_ids(u) #, with_pending_requests=True)
        group_size = get_group_size(group)
        unit = get_group_unit(group)
        if not unit:
            continue
        if len(unit) > 1:
            print u, unit
        try:
            unit = Unit.objects.get(id=unit[0])
        except Unit.DoesNotExist:
            continue
        try:
            invite = Invitation.objects.get(user=u)
        except Invitation.DoesNotExist:
            continue

        if unit.unit_type.max_occupancy < group_size and invite.agreement_period.id == 1:
            print u, invite.agreement_period


def check_groups_with_different_units():
    users = User.objects.all()
    for u in users:
        user_ids = get_related_user_ids(u)
        units = get_group_unit(user_ids)
        if not units:
            continue
        units = set(units)
        if None in units:
            units.remove(None)
        if len(units) > 1:
            print u, units


def check_users_with_many_assignments():
    users = User.objects.all()
    for u in users:
        if PendingAssignment.objects.filter(user=u).count() > 1:
            print u


def check_users_with_missing_personality():
    users_take_quiz = QuizResult.objects.values_list('user', flat=True)
    users = User.objects.filter(id__in=users_take_quiz).exclude(last_login__isnull=True).select_related('profile')
    c = 0
    for user in users:
        if not UserOrganizationSocialHabitAnswers.objects.filter(userprofile=user.profile).exists():
            # print user
            c += 1
    print c


def check_assignment_unit_with_group_unit():
    users = User.objects.all()
    for user in users:
        a = PendingAssignment.objects.filter(user=user).first()
        if not a:
            continue
        group = get_related_user_ids(user)
        unit = get_group_unit(group)
        if not a.unit:
            continue
        if a.unit and a.unit.id != unit[0]:
            print user, a.unit.id


def check_group_members_and_assignments():
    users = User.objects.all()
    for user in users:
        a = PendingAssignment.objects.filter(user=user).first()
        if not a or not a.unit:
            continue
        unit = a.unit
        suitemates = unit.get_suitmates().all()
        group = get_related_user_ids(user)
        for guy in suitemates:
            if not guy.user.id in group:
                print unit


if __name__ == '__main__':
    print "Check 2 beds"
    check_2_beds()
    print "Check 2 invites"
    check_2_invites()
    #print "Check similar names"
    #check_similar_names()
    print "Check overflowed groups"
    check_overflowed_groups()
    print "Check users with many assignments"
    check_users_with_many_assignments()
    print "Check groups with different units"
    check_groups_with_different_units()
    print "Check users with missing personality"
    check_users_with_missing_personality()
    print "Check assignment unit and group unit"
    check_assignment_unit_with_group_unit()
    print "Check group members and assignments"
    check_group_members_and_assignments()
