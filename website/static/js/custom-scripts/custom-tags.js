(function (win, $) {
  var defaultOptions = {
    unique: false,
    tagTemplate: '<span class="btn btn-sm btn-rounded btn-default push-5-l push-5">' +
    '{{ text }}' +
    '&nbsp;' +
    '<a class="tag-remove" href="#"><i class="fa fa-times"></i></a>' +
    '</span>',
    tagClassName: 'tag',
    onAddTag: function () {

    },
    onModifyTag: function () {

    },
    onRemoveTag: function () {

    }
  };

  var TagsHolder = function (el, opts) {
    this.options = $.extend(defaultOptions, opts);
    this.tags = [];
    this.el = el;

    this.bindEvents();
  };

  TagsHolder.prototype.bindEvents = function () {
    var tagClass = '.' + this.options.tagClassName;

    $(this.el).on('click.tag-holder', tagClass + ' .tag-remove', $.proxy(this.onRemoveBtnClick, this));
  };

  TagsHolder.prototype.destroy = function () {
    $(this.el).off('click.tag-holder');
  };

  TagsHolder.prototype.addTag = function (tag) {
    if (this.options.unique && this.isTagExist(tag)) {
      return ;
    }

    this.tags.push(tag);
    this.renderTag(tag);

    this.options.onAddTag(tag);

    return tag;
  };

  TagsHolder.prototype.modifyTag = function (oldTag, newTag) {
    if (!this.isTagExist(oldTag) && typeof newTag === 'string') {
      return ;
    }
    var tagClass = '.' + this.options.tagClassName;
    var $tags = $(this.el).children(tagClass);
    var modify = $.proxy(function(i, newTag) {
      this.tags[i] = newTag;
      $tags.eq(i).replaceWith(this.createTag(newTag));
    }, this);

    if (typeof oldTag === 'number') {
      modify(oldTag, newTag);
    } else {
      this.tags.forEach(function (tag, i) {
        if (oldTag === tag) {
          modify(i, newTag);
        }
      });
    }

    this.options.onModifyTag(oldTag, newTag);
  };

  TagsHolder.prototype.removeTag = function (tag) {
    var removedTag, index;
    var tagClass = '.' + this.options.tagClassName;
    var $tags = $(this.el).children(tagClass);

    if (typeof tag === 'string') {
      index = this.indexOfTag(tag);
    }

    if (tag instanceof $) {
      index = $tags.index(tag);
    }

    removedTag = index > -1 ? this.tags.splice(index, 1)[0] : undefined;
    $tags.eq(index).remove();

    this.options.onRemoveTag(removedTag);

    return removedTag
  };

  TagsHolder.prototype.onRemoveBtnClick = function (e) {
    e.preventDefault();

    var tagClass = '.' + this.options.tagClassName;
    var $tagEl = $(e.currentTarget).closest(tagClass);

    this.removeTag($tagEl);
  };

  TagsHolder.prototype.isTagExist = function (tag) {
    return this.indexOfTag(tag) > -1;
  };

  TagsHolder.prototype.indexOfTag = function (tag) {
    return this.tags.indexOf(tag);
  };

  TagsHolder.prototype.renderTag = function (tag) {
    var $tag = this.createTag(tag);
    $tag.appendTo(this.el);
  };

  TagsHolder.prototype.createTag = function (tag) {
    var $tag = $(this.options.tagTemplate.replace('{{ text }}', tag));
    $tag.data('tag', tag);
    $tag.addClass(this.options.tagClassName);

    return $tag;
  };

  TagsHolder.prototype.updateTags = function () {
    var tags = this.tags.map(this.createTag);
    var $el = $(this.el);
    $el.children().remove();
    $el.append(tags);
  };

  TagsHolder.prototype.getTags = function () {
    return this.tags;
  };

  $.fn.tagsHolder = function (opts) {
    return $(this).each(function () {
      var tagsHolder = new TagsHolder(this, opts);
      $(this).data('tagsHolder', tagsHolder);
    });
  };

  return win.TagsHolder = TagsHolder;
})(window, jQuery);
