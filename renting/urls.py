from django.conf.urls import include, patterns, url
from django.contrib import admin
from django.conf.urls.static import static
from django.conf import settings
from renting.authentication import logout_view, login_view

from management import urls as management_urls
from website import urls as student_urls

from adminplus.sites import AdminSitePlus

admin.site = AdminSitePlus()
admin.autodiscover()

# Segment.io
import analytics
analytics.write_key = 'WvODpdaIThxlqclZyp9Ri6BxvacynhDm'


urlpatterns = [

    #Authentication
    # url(r'^login/$', login_view, name='login'),
    # url(r'^logout/$', logout_view, name='logout'),

    # Website apps
    url(r'^management/', include(management_urls, namespace='management')),
    url(r'^', include(student_urls)),


    # Django Admin URLS
    url(r'^admin/', include(admin.site.urls)),
    url(r'^media/(?P<path>.*)$', 'django.views.static.serve', {'document_root': 'media'}),
] + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)

urlpatterns += patterns(
    '', url(r'^dbmail/', include('dbmail.urls')),
)
