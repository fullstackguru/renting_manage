# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('management', '0078_stage_label'),
    ]

    operations = [
        migrations.AddField(
            model_name='property',
            name='file',
            field=models.FileField(null=True, upload_to=b'uploads/resources_link/', blank=True),
        ),
    ]
