from django.test import TestCase
from django.conf import settings
from dbmail.models import MailTemplate


class DBMailerSignalsTests(TestCase):
    def setUp(self):
        super(DBMailerSignalsTests, self).setUp()

    def test_jinja_template_tags_in_a_href(self):
        message_body = '''
        <html>
            <head></head>
            <body>
                <h1>{{ date|date:{}F d, Y{} }}</h1>
                <a href="{{{{ some_var }}}}"></a>
            </body>
        </html>
        '''
        mt = MailTemplate(
            name='test',
            subject='test',
            message=message_body.format('&quot;', '&quot;'),
        )
        mt.save()
        self.assertHTMLEqual(mt.message, message_body.format('"', '"'))

    def test_context_note_creation(self):
        mt = MailTemplate(
            slug='test2',
            name='test',
            subject='test',
            message='test',
        )
        mt.save()
        self.assertEqual(mt.context_note, settings.DB_MAILER_DEFAULT_CONTEXT_NOTE)
        mt = MailTemplate(
            slug='test3',
            name='test',
            subject='test',
            message='test',
            context_note='test',
        )
        mt.save()
        self.assertEqual(mt.context_note, 'test')
