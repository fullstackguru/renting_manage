# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
from django.db.models import Q


def delete_relation(apps, rel):
    '''
    Delete relation + all related relations (via friends).
    Custom Save method from Relationship model.
    '''
    Relationship = apps.get_model("website", "Relationship")
    user = rel.start_user
    relate_user = rel.end_user
    if user.email == 'cheweil@usc.edu' or relate_user.email == 'cheweil@usc.edu':
        print 'cheweil@usc.edu'

    for _ in range(2):
        # Remove relations
        deleted_ids = {relate_user.id}
        relate_user_relations = Relationship.objects.filter(Q(start_user=relate_user) | Q(end_user=relate_user)).\
            exclude(Q(start_user=user) | Q(end_user=user)).exclude(via_user=user)
        relate_user_relations_ids = set()
        for r in relate_user_relations:
            relate_user_relations_ids.add(r.start_user_id)
            relate_user_relations_ids.add(r.end_user_id)
        if relate_user_relations_ids:
            relate_user_relations_ids.remove(relate_user.id)

        while True:
            relate_user_friends = Relationship.objects.filter(
                Q(start_user=user) | Q(end_user=user),
                via_user__id__in=deleted_ids
            )
            if relate_user_friends:
                for relation in relate_user_friends:
                    deleted_ids.add(relation.start_user_id)
                    deleted_ids.add(relation.end_user_id)
                relate_user_friends.delete()
            else:
                break

        weak_relations = Relationship.objects.filter(
            Q(start_user_id__in=relate_user_relations_ids) | Q(end_user_id__in=relate_user_relations_ids),
            via_user=user
        )
        weak_relations.delete()
        user, relate_user = relate_user, user
    rel.delete()


class Migration(migrations.Migration):

    def remove_matches_with_different_units(apps, schema_editor):
        Invitation = apps.get_model('management', "Invitation")
        Bed = apps.get_model("management", "Bed")
        Relationship = apps.get_model("website", "Relationship")
        PendingAssignment = apps.get_model("management", "PendingAssignment")
        invites = Invitation.objects.all()
        excluded_users = set()
        for invite in invites:
            user_id = invite.user_id
            if user_id in excluded_users:
                continue
            user_relations = Relationship.objects.filter(Q(start_user_id=user_id) | Q(end_user_id=user_id)).\
                order_by('id')
            group_users = {user_id}
            for relation in user_relations:
                group_users.add(relation.start_user_id)
                group_users.add(relation.end_user_id)
            excluded_users.update(group_users)
            group_beds = Bed.objects.filter(user_id__in=group_users).select_related('room__unit')
            units = set([bed.room.unit.id for bed in group_beds])
            units.update(PendingAssignment.objects.filter(user_id__in=group_users).order_by('unit__name').
                         values_list('unit_id', flat=True).distinct())
            if len(units) > 1:
                #  Find first relation with units
                first_added_unit = None
                users_with_beds = [bed.user_id for bed in group_beds]
                for relation in user_relations:
                    if relation.start_user_id in users_with_beds:
                        first_added_unit = filter(lambda bed: bed.user.id == relation.start_user_id, group_beds)[0].\
                            room.unit
                        break
                    if relation.end_user_id in users_with_beds:
                        first_added_unit = filter(lambda bed: bed.user.id == relation.end_user_id, group_beds)[0].\
                            room.unit
                        break
                if first_added_unit:
                    users_from_another_unit = Bed.objects.filter(user_id__in=group_users).\
                        filter(room__unit_id=first_added_unit).values_list('user_id', flat=True)
                    #  Remove users from another units
                    relations_for_deleting = user_relations.filter(
                        Q(start_user_id__in=users_from_another_unit) | Q(end_user_id__in=users_from_another_unit)
                    )
                    for relation in relations_for_deleting:
                        delete_relation(apps, relation)

    dependencies = [
        ('management', '0024_remove_beds_from_user_with_many_beds'),
        ('website', '0024_update_last_login_date')
    ]

    operations = [
        migrations.RunPython(remove_matches_with_different_units)
    ]
