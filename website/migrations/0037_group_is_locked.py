# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('website', '0036_link_relations_with_group'),
    ]

    operations = [
        migrations.AddField(
            model_name='group',
            name='is_locked',
            field=models.BooleanField(default=False),
        ),
    ]
