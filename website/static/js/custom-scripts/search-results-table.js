var initSearchResultsTable = function (search_value) {

    $('#search-results-table').on('preXhr.dt', function () {
        $('tr').attr('hidden', true);
        App.blocks('#match-table-block', 'refresh_toggle');
    });
    $('#search-results-table').on('xhr.dt', function () {
        $('tr').attr('hidden', false);
        App.blocks('#match-table-block', 'refresh_toggle');
    });
    $('#search-results-table').DataTable({
        "deferRender": true,
        "bSort": false,
        "bPaginate": true,
        "searching": false,
        "sPaginationType": "full_numbers",
        "iDisplayLength": 10,
        ajax: {
            "dataType": 'json',
            data: {
                "search": search_value,
            },
            "dataSrc": function (json) {
                this.aaData = json.data;
                return json.data
            }
        },
        "aoColumns": [
            {
                "mDataProp": "user",
                bSearchable: false
            },
            {
                "mDataProp": "property",
                bSearchable: false
            },
            {
                "mDataProp": "user_url",
                "render": function (data, type, full, meta) {
                    return '<a href="' + data + '">Profile Page</a>';
                },
                bSearchable: false
            }
        ],
        paging: true,
        "lengthMenu": [[10, 25, 50, 100, -1], [10, 25, 50, 100, "All"]],
    });
};
