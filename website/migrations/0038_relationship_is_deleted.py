# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('website', '0037_group_is_locked'),
    ]

    operations = [
        migrations.AddField(
            model_name='relationship',
            name='is_deleted',
            field=models.BooleanField(default=False),
        ),
    ]
