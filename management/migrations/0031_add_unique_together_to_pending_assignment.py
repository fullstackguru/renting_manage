# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('management', '0030_set_bedand_room_for_pending_assignment'),
    ]

    operations = [
        migrations.AlterUniqueTogether(
            name='pendingassignment',
            unique_together=set([('user', 'assignment_period')]),
        ),
    ]
