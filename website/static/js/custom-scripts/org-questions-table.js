var initOrgQuestionsTable = function(listUrl, updateUrl) {
    $('#org-questions-table').on('preXhr.dt', function () {
        $('tr').attr('hidden', true);
        App.blocks('#org-questions-table-block', 'refresh_toggle');
    });
    $('#org-questions-table').on('xhr.dt', function () {
        $('tr').attr('hidden', false);
        App.blocks('#org-questions-table-block', 'refresh_toggle');
    });
    $('#org-questions-table').DataTable({
        initComplete: function () {
            var api = this.api();
            $('#org-questions-table_filter input')
                .off('.DT')
                .on('keyup.DT', function (e) {
                    if (e.keyCode == 13) {
                        api.search(this.value).draw();
                    }
                });
        },
        "serverSide": true,
        "processing": true,
        "bSort": true,
        "bPaginate": true,
        "sPaginationType": "full_numbers",
        "iDisplayLength": 25,
        ajax: {
            "dataType": 'json',
            url: listUrl,
            "dataSrc": function (json) {
                return json.data
            }
        },
        "aoColumns": [
            {
                "mDataProp": "question",
                "render": function (data, type, full, meta) {
                    var update_url = '<a href="' + updateUrl + full.id + '">' + data + '</a>';
                    return update_url;
                }
            },
            {"mDataProp": "required"},
            {"mDataProp": "sort_order"},
            {"mDataProp": "status"},
            {
                "mDataProp": "created_at",
                "render": function (data, type, full, meta) {
                    return moment(data, moment.ISO_8601).format('MMMM Do YYYY, h:mm:ss a');
                }
            },
            {
                "mDataProp": "updated_at",
                "render": function (data, type, full, meta) {
                    return moment(data, moment.ISO_8601).format('MMMM Do YYYY, h:mm:ss a');
                }
            }
        ],
        paging: true,
        "lengthMenu": [[10, 25, 50, 100], [10, 25, 50, 100]]
    });
};
