# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('management', '0008_add_logo_to_property'),
        ('website', '0005_remove_name_columns_from_UserProfile'),
    ]

    operations = [
        migrations.AddField(
            model_name='user',
            name='available_properties',
            field=models.ManyToManyField(related_name='assigned_users', to='management.Property'),
        ),
    ]
