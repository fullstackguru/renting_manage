# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('management', '0008_add_logo_to_property'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='property',
            options={'ordering': ('name',), 'verbose_name_plural': 'Properties'},
        ),
        migrations.AddField(
            model_name='organizationsocialhabitquestion',
            name='required',
            field=models.BooleanField(default=True),
        ),
    ]
