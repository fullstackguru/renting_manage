# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('management', '0048_add_matching_restriction_to_lorenzo'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='invitation',
            name='is_reminder_email_sent',
        ),
        migrations.AddField(
            model_name='invitation',
            name='activated_time',
            field=models.DateTimeField(default=None, null=True, blank=True),
        ),
    ]
