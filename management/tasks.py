from datetime import timedelta
from django.contrib.auth import get_user_model
from django.core.exceptions import ValidationError
from django.core.validators import validate_email
from django.utils import timezone
from openpyxl import load_workbook
from celery import task, Task
from actstream import action

User = get_user_model()
from website.models import UserManager
from management.models import Organization, Property, AssignmentPeriod, Invitation, UnitType, RoomType, Unit, Room, \
    PendingAssignment, Bed, ResidentImport, ContractType
from utils.services import create_relations_for_suitemates, split_groups


class ParseInvitesErrorHandlingTask(Task):
    abstract = True

    def on_failure(self, exc, task_id, targs, tkwargs, einfo):
        try:
            resident_import = targs[0]
            resident_import.error = exc
            resident_import.status = ResidentImport.FAILED
            resident_import.save()
        except IndexError:
            pass

    def on_success(self, retval, task_id, targs, tkwargs):
        try:
            resident_import = targs[0]
            if not resident_import.error:
                resident_import.status = ResidentImport.COMPLETED
            else:
                resident_import.status = ResidentImport.FAILED
            resident_import.save()
        except IndexError:
            pass


@task(base=ParseInvitesErrorHandlingTask, name='management.parse_invites')
def parse_invites(resident_import):
    resident_import.status = ResidentImport.IN_PROGRESS
    resident_import.save()
    workbook = load_workbook(resident_import.file, read_only=True, use_iterators=True)
    first_sheet = workbook.get_sheet_names()[0]
    worksheet = workbook.get_sheet_by_name(first_sheet)
    for index, items_row in enumerate(worksheet.rows):
        if index == 0:
            continue
        formatted_cells = [unicode(i.value).strip() if i.value else '' for i in items_row]
        if not any(formatted_cells):
            break
        try:
            gender_restriction = formatted_cells[10][0]
        except IndexError:
            gender_restriction = User.GENDER_UNKNOWN

        agreement_period_name = formatted_cells[0]
        unit_type_name = formatted_cells[8]
        room_type_name = formatted_cells[9]
        contract_type_name = formatted_cells[11]
        unit_name = formatted_cells[1] if formatted_cells[1].lower() != 'waitlist' and formatted_cells[1] else None
        room_name = formatted_cells[2] if formatted_cells[2] else None
        bed_name = formatted_cells[3] if formatted_cells[3] else None
        locked = 1 if formatted_cells[12].lower() == 't' and formatted_cells[12] else 0
					
        try:
            user_raw = {
                'first_name': formatted_cells[4],
                'last_name': formatted_cells[5],
                'email': formatted_cells[6].lower(),
                'points': int(formatted_cells[7])
            }
        except ValueError:
            resident_import.error = "Points must be positive number! User: {}".format(formatted_cells[6].lower())
            resident_import.save()
            return

        prop = resident_import.property

        try:
            as_per = AssignmentPeriod.objects.get(name__iexact=agreement_period_name, property=prop)
        except AssignmentPeriod.DoesNotExist:
            resident_import.error = "Agreement period with name {} doesn't exist!".format(agreement_period_name)
            resident_import.save()
            return

        try:
            unit_type = UnitType.objects.get(name__iexact=unit_type_name, property=prop)
        except UnitType.DoesNotExist:
            resident_import.error = "Unit Type with name {} doesn't exist!".format(unit_type_name)
            resident_import.save()
            return

        if room_type_name:
            try:
                room_type = RoomType.objects.get(name__iexact=room_type_name, property=prop)
            except RoomType.DoesNotExist:
                resident_import.error = "Room Type with name {} doesn't exist!".format(room_type_name)
                resident_import.save()
                return
        else:
            room_type = None

        if contract_type_name:
            try:
                contract_type = ContractType.objects.get(name__iexact=contract_type_name)
            except ContractType.DoesNotExist:
                resident_import.error = "Contract Type with name {} doesn't exist!".format(contract_type_name)
                resident_import.save()
                return
        else:
            contract_type = None

        try:
            email = UserManager.normalize_email(user_raw['email'])
            try:
                validate_email(email)
            except ValidationError as e:
                resident_import.error = "Incorrect email address {}!".format(email)
                resident_import.save()
                return
            user, user_created = User.objects.get_or_create(email=email,
                                                            defaults={'first_name': user_raw['first_name'],
                                                                      'last_name': user_raw['last_name'],
                                                                      'points': user_raw['points'],
                                                                      'gender': gender_restriction})
            if user_created:
                user.set_unusable_password()
                user.save()
                action.send(resident_import.added_by, verb='created profile for', action_object=user, target=prop)
            else:
                user.first_name = user_raw['first_name']
                user.last_name = user_raw['last_name']
                user.points = user_raw['points']
                user.save()
                action.send(resident_import.added_by, verb='updated profile for', action_object=user, target=prop)
            if user.gender == User.GENDER_UNKNOWN:
                user.gender = gender_restriction
                user.save()
            user.available_properties.add(prop)
        except Exception as e:
            resident_import.error = "Error {}".format(e)
            resident_import.save()
            return
        try:
            invite, invite_created = Invitation.objects.update_or_create(
                user=user, agreement_period=as_per, defaults={
                    'invitation_type': 'M',
                    'unit_type': unit_type,
                    'room_type': room_type,
                    'expire_date': as_per.end_date,
                    'contract_type': contract_type,
                    'locked': locked
                    
                }
            )
        except Invitation.MultipleObjectsReturned:
            invite, invite_created = Invitation.objects.filter(user=user, agreement_period=as_per).first(), False

        if invite_created:
            action.send(resident_import.added_by, verb='created', action_object=invite, target=prop)
        else:
            action.send(resident_import.added_by, verb='updated', action_object=invite, target=prop)

        if unit_name:
            try:
                unit = Unit.objects.get(name__iexact=unit_name, unit_type=unit_type)
            except Unit.DoesNotExist:
                resident_import.error = "Unit with name {} doesn't exist!".format(unit_name)
                resident_import.save()
                return

            if not room_name and bed_name:
                room_name = bed_name[0]
            if room_name:
                try:
                    room = Room.objects.get(name__iexact=room_name, unit=unit)
                except Room.DoesNotExist:
                    resident_import.error = "Room with name {} doesn't exist in unit {}!".format(room_name, unit_name)
                    resident_import.save()
                    return
            else:
                room = None
        else:
            unit, room = None, None

        if bed_name and room:
            try:
                bed = Bed.objects.get(name__iexact=bed_name, room=room)
            except Bed.DoesNotExist:
                resident_import.error = "Bed with name {} doesn't exist in room {}!".format(bed_name, room_name)
                resident_import.save()
                return
        else:
            bed = None
        old_pa = PendingAssignment.objects.filter(assignment_period=as_per, unit=unit, bed=bed, room=room).\
            exclude(user=user)
        old_pa.delete()
        pa, pa_created = PendingAssignment.objects.update_or_create(user=user, assignment_period=as_per, invite=invite,
                                                                    defaults={'unit': unit, 'room_type': room_type,
                                                                              'bed': bed, 'room': room})
    try:
        user_ids = Invitation.objects.filter(agreement_period__property=prop).values_list('user')
        users = User.objects.filter(id__in=user_ids)
        split_groups(users)
        for user in users:
            create_relations_for_suitemates(user, as_per)
    except NameError:
        return


def parse_invites_trinity(file):
    workbook = load_workbook(file, read_only=True, use_iterators=True)
    students_sheet, property_info_sheet = workbook.get_sheet_names()
    worksheet = workbook.get_sheet_by_name(property_info_sheet)
    for index, items_row in enumerate(worksheet.rows):
        if index in [0, 1]:
            continue
        formatted_cells = [i.value if i.value else '' for i in items_row]
        if not any(formatted_cells):
            break
        org_name = formatted_cells[1].strip()
        try:
            prop_name = str(int(formatted_cells[2]))
        except ValueError:
            prop_name = formatted_cells[2]
        prop_name = prop_name.strip()
        agreement_period_name = formatted_cells[3].strip()
        unit_type_name = formatted_cells[6].strip()
        agreement_period_raw = {
            'start_date': formatted_cells[4],
            'end_date': formatted_cells[5],
        }
        unit_type_raw = {
            'max_occupancy': formatted_cells[7],
        }
        try:
            room_type_name = formatted_cells[8].strip()
            room_type_raw = {
                'max_occupancy': formatted_cells[9],
            }
        except IndexError:
            room_type_name = None
            room_type_raw = None
        if org_name:
            org, org_created = Organization.objects.get_or_create(name=org_name)
        if prop_name:
            prop, prop_created = Property.objects.update_or_create(name=prop_name, organization=org)
        if agreement_period_name:
            as_per, as_per_created = AssignmentPeriod.objects.update_or_create(
                name=agreement_period_name, property=prop,
                defaults=agreement_period_raw)
        if unit_type_name:
            unit_type, unit_type_created = UnitType.objects.update_or_create(name=unit_type_name, property=prop,
                                                                             defaults=unit_type_raw)
        if room_type_name:
            room_type, room_type_created = RoomType.objects.update_or_create(name=room_type_name, property=prop,
                                                                             defaults=room_type_raw)

    worksheet = workbook.get_sheet_by_name(students_sheet)
    for index, items_row in enumerate(worksheet.rows):
        if index in [0, 1]:
            continue
        formatted_cells = [i.value if i.value else '' for i in items_row]
        if not any(formatted_cells):
            break
        org_raw = {
            'name': formatted_cells[1].strip()
        }
        try:
            prop_name = str(int(formatted_cells[2]))
        except ValueError:
            prop_name = formatted_cells[2]
        prop_name = prop_name.strip()
        prop_raw = {
            'name': prop_name
        }
        agreement_period_raw = {
            'name': formatted_cells[3].strip()
        }
        user_raw = {
            'first_name': formatted_cells[6].strip(),
            'last_name': formatted_cells[7].strip(),
            'email': formatted_cells[8].strip().lower()
        }
        unit_type_raw = {
            'name': formatted_cells[9].strip(),
        }
        try:
            room_type_raw = {
                'name': formatted_cells[10].strip(),
            }
        except IndexError:
            room_type_raw = {
                'name': 'Open Room',
            }
        org, org_created = Organization.objects.get_or_create(name=org_raw['name'])
        prop, prop_created = Property.objects.get_or_create(name=prop_raw['name'], organization=org)
        as_per, as_per_created = AssignmentPeriod.objects.get_or_create(name=agreement_period_raw['name'], property=prop)
        unit_type, unit_type_created = UnitType.objects.get_or_create(name=unit_type_raw['name'], property=prop)
        room_type, room_type_created = RoomType.objects.get_or_create(name=room_type_raw['name'], property=prop)
        try:
            email = UserManager.normalize_email(user_raw['email'])
            try:
                validate_email(email)
            except ValidationError as e:
                print(e)
                continue
            user, user_created = User.objects.update_or_create(email=email,
                                                                   defaults={'first_name': user_raw['first_name'],
                                                                             'last_name': user_raw['last_name']})
            if user_created:
                user.set_unusable_password()
                user.save()
            user.available_properties.add(prop)
        except Exception as e:
            print(user_raw['email'], e)
            continue
        try:
            invite, invite_created = Invitation.objects.update_or_create(
                    user=user, agreement_period=as_per, defaults={
                        'invitation_type': 'A',
                        'unit_type': unit_type,
                        'room_type': room_type,
                        'expire_date': formatted_cells[5],
                    }
                )
        except Invitation.MultipleObjectsReturned:
            pass


@task(base=ParseInvitesErrorHandlingTask, name='management.parse_units')
def parse_realty(realty_import):
    realty_import.status = ResidentImport.IN_PROGRESS
    realty_import.save()
    workbook = load_workbook(realty_import.file, read_only=True, use_iterators=True)
    first_sheet = workbook.get_sheet_names()[0]
    worksheet = workbook.get_sheet_by_name(first_sheet)
    prop = realty_import.property
    for index, items_row in enumerate(worksheet.rows):
        if index == 0:
            continue
        formatted_cells = [unicode(i.value).strip() if i.value else '' for i in items_row]
        unit_name = formatted_cells[0]
        unit_type_name = formatted_cells[1]
        if unit_type_name == '':
            continue
        room_name = formatted_cells[2]
        room_type_name = formatted_cells[3]
        bed_name = formatted_cells[4]
        unit_type, _ = UnitType.objects.get_or_create(defaults={'name':unit_type_name}, name__iexact=unit_type_name, property=prop)
        unit, _ = Unit.objects.update_or_create(defaults={'name':unit_name},name__iexact=unit_name, unit_type__property=prop,unit_type=unit_type)
        room_type, _ = RoomType.objects.get_or_create(defaults={'name':room_type_name},name__iexact=room_type_name, property=prop)
        room, _ = Room.objects.update_or_create(defaults={'name':room_name}, name__iexact=room_name,unit=unit,room_type=room_type)
        bed, _ = Bed.objects.update_or_create(defaults={'name':bed_name}, name__iexact=bed_name, room=room)

#
