# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('website', '0027_rename_disable_deleting_field_to_read_only_in_relationship'),
    ]

    operations = [
        migrations.AddField(
            model_name='user',
            name='hidden',
            field=models.BooleanField(default=False),
        ),
    ]
