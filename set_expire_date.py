import os
os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'renting.settings')
os.environ.setdefault('PYTHONUNBUFFERED', '1')

import django
django.setup()

from django.utils import timezone
from management.models import Invitation, Property


def change_expiration():
    expire_date = timezone.datetime(2017, 06, 17)
    try:
        property = Property.objects.get(name='The Lorenzo')
        Invitation.objects.filter(user__available_properties__in=[property]).update(expire_date=expire_date)
        print 'Success update expire date of all invites for Property "The Lorenzo"!'
    except Property.DoesNotExist:
        print 'Property "The Lorenzo" does nor exist!'

change_expiration()