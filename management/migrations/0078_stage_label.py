# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('management', '0077_propertysocialnetwork'),
    ]

    operations = [
        migrations.AddField(
            model_name='stage',
            name='label',
            field=models.CharField(max_length=256, null=True, blank=True),
        ),
    ]
