var initUnitFormsets = function (propId) {
    var roomForms = [];

    var outerFormset = $(".room-form").not('.actions')
        .djangoFormset({
            on: {
                formAdded: function (event, form) {
                    roomForms.push(form);
                    var innerFormsetElem = form.elem.children('.bed-forms').first();
                    var innerFormset = innerFormsetElem.children('div').djangoFormset();
                    var id = form.index;
                    var selectId = "#id_rooms-" + id + "-room_type";
                    $(selectId).change(function (e) {
                        for (var i = innerFormset.forms.length - 1; i >= 0; i--) {
                            innerFormset.forms[i].delete();
                        }
                        var $this = $(this);
                        var roomTypeId = $this.val();
                        if (!roomTypeId) {
                            return;
                        }
                        $.ajax({
                            url: "/management/room_type/" + propId + "/",
                            method: "GET",
                            data: {id: roomTypeId},
                            success: function (result) {
                                var types = result.data;
                                if (types.length) {
                                    var beds = types[0].max_occupancy;
                                    for (var i = 0; i < beds; i++) {
                                        innerFormset.addForm();
                                    }
                                }
                            }
                        });
                    });
                }
            }
        });

    for (var j = 0; j < outerFormset.forms.length; j++) {
        var form = outerFormset.forms[j];
        roomForms.push(form);
        var innerFormsetElem = form.elem.children('.bed-forms').first();
        var innerFormset = innerFormsetElem.children('div').djangoFormset();
        var id = form.index;
        var selectId = "#id_rooms-" + id + "-room_type";
        $(selectId).change(function (e) {
            for (var i = innerFormset.forms.length - 1; i >= 0; i--) {
                innerFormset.forms[i].delete();
            }
            var $this = $(this);
            var roomTypeId = $this.val();
            if (!roomTypeId) {
                return;
            }
            $.ajax({
                url: "/management/room_type/" + propId + "/",
                method: "GET",
                data: {id: roomTypeId},
                success: function (result) {
                    var types = result.data;
                    if (types.length) {
                        var beds = types[0].max_occupancy;
                        for (var i = 0; i < beds; i++) {
                            innerFormset.addForm();
                        }
                    }
                }
            });
        });
    }

    $("#id_unit_type").change(function (e) {
        var $this = $(this);
        roomForms.forEach(function (elem) {
            elem.delete()
        });
        roomForms = [];
        var unitTypeId = $this.val();
        if (!unitTypeId) {
            return;
        }
        $.ajax({
            url: "/management/unit_type/" + propId + "/",
            method: "GET",
            data: {id: unitTypeId},
            success: function (result) {
                var types = result.data;
                if (types.length) {
                    var rooms = types[0].max_rooms;
                    for (var i = 0; i < rooms; i++) {
                        outerFormset.addForm();
                    }
                }
            }
        });
    });
};