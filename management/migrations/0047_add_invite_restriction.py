# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('management', '0046_made_nullable_room_type_in_pending_assignment'),
    ]

    operations = [
        migrations.AddField(
            model_name='invitation',
            name='matching_restriction',
            field=models.CharField(default=b'A', max_length=1, choices=[(b'A', b'All'), (b'Y', b'With Assignments'), (b'N', b'Without Assignments')]),
        ),
    ]
