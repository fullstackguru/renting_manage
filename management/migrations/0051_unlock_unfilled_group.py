# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
from django.db.models import Sum, F, Q


def get_related_user_ids(apps, user, with_pending_requests=False):
    Relationship = apps.get_model('website', 'Relationship')
    users = {user.id}
    relations = Relationship.objects.filter(Q(start_user=user) | Q(end_user=user))
    if not with_pending_requests:
        relations = relations.filter(approval_datetime__isnull=False, end_user_approved=True)
    relations = relations.values('start_user_id', 'end_user_id')
    for relation in relations:
        users.add(relation['start_user_id'])
        users.add(relation['end_user_id'])
    return users


def get_group_size(apps, group_members):
    Invitation = apps.get_model('management', 'Invitation')
    result = Invitation.objects.filter(user__in=group_members).filter(contract_type__occupied_places__gt=1). \
        aggregate(extra_size=Sum(F('contract_type__occupied_places') - 1))
    return len(group_members) + (result['extra_size'] if result['extra_size'] else 0)


class Migration(migrations.Migration):

    def unlock_unfilled_groups(apps, schema_editor):
        Invitation = apps.get_model('management', 'Invitation')
        locked_invites = Invitation.objects.filter(locked=True).select_related('user', 'unit_type')
        for invite in locked_invites:
            group = get_related_user_ids(apps, invite.user)
            group_size = get_group_size(apps, group)
            if group_size < invite.unit_type.max_occupancy:
                print invite.user
                invite.locked = False
                invite.save()

    dependencies = [
        ('management', '0050_set_activated_time'),
        ('website', '0029_add_disable_email_field_to_user'),
    ]

    operations = [
        migrations.RunPython(unlock_unfilled_groups)
    ]
