# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('website', '0021_remove_relationships_with_incorrect_is_roommate_value'),
    ]

    operations = [
        migrations.AddField(
            model_name='user',
            name='allow_co_educational_groups',
            field=models.BooleanField(default=False),
        ),
    ]
