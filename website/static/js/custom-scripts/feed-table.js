var VERB_STYLES = {
    'created': "si si-plus text-success",
    'updated': "fa fa-edit text-info",
    'deleted': "si si-trash text-danger",
    'uploaded': "fa fa-upload text-info",
    'finished': "si si-badge text-warning",
    'sent': "fa fa-send text-info",
    'rejected': 'fa fa-user-times text-danger',
    'added': 'fa fa-user-plus text-success'
};


var initFeedTable = function() {
    $('#feed-table').on('preXhr.dt', function () {
        $('tr').attr('hidden', true);
        App.blocks('#feed-table-block', 'refresh_toggle');
    });
    $('#feed-table').on('xhr.dt', function () {
        $('tr').attr('hidden', false);
        App.blocks('#feed-table-block', 'refresh_toggle');
    });
    $('#feed-table').DataTable({
        "serverSide": true,
        "processing": true,
        bFilter: false,
        "bPaginate": true,
        "sPaginationType": "full_numbers",
        "iDisplayLength": 100,
        ajax: {
            "dataType": 'json',
            "dataSrc": function (json) {
                return json.data
            }
        },
        "aoColumns": [
            {
                "mDataProp": "activity",
                "render": function (data, type, full, meta) {
                    return "<i class='" + VERB_STYLES[full.verb.split(' ')[0]] + "'></i> <strong>" + full.actor + " </strong>" + full.verb + " <strong>" + full.object + "</strong>" +
                            "<span class='text-muted help-block'>" + moment(full.timestamp).fromNow() + "</span>";
                }
            },
            {
                "mDataProp": "timestamp",
                "render": function (data, type, full, meta) {
                    return data ? moment(data, moment.ISO_8601).format('MMMM Do YYYY, h:mm:ss a') : "Never"
                }
            }

        ],
        paging: true,
        "lengthMenu": [[10, 25, 50, 100], [10, 25, 50, 100]]
    });
};
