# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('management', '0081_property_enable_star'),
    ]

    operations = [
        migrations.AddField(
            model_name='unit',
            name='unit_image',
            field=models.ImageField(null=True, upload_to='uploads/unit_image/%Y/%m/%d/', blank=True),
        ),
    ]
