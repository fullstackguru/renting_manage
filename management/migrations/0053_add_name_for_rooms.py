# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('management', '0052_remove_bed_user'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='unit',
            name='max_rooms',
        ),
        migrations.AddField(
            model_name='room',
            name='name',
            field=models.CharField(max_length=100, null=True),
        ),
        migrations.AddField(
            model_name='unittype',
            name='max_rooms',
            field=models.IntegerField(default=1),
        ),
    ]
