# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    def remove_incorrect_contract_types(apps, schema_editor):
        ContractType = apps.get_model('management', 'ContractType')
        Invitation = apps.get_model('management', 'Invitation')
        base_contract_types = ['Single', 'Double', 'Double XL']
        default_contract_type = ContractType.objects.get(name='Double')
        incorrect_contract_types = ContractType.objects.exclude(name__in=base_contract_types)
        Invitation.objects.filter(models.Q(contract_type__isnull=True) |
                                  models.Q(contract_type__in=incorrect_contract_types)).\
            update(contract_type=default_contract_type)
        incorrect_contract_types.delete()

    dependencies = [
        ('management', '0055_added_default_contract_type'),
    ]

    operations = [
        migrations.RunPython(remove_incorrect_contract_types)
    ]
