# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    def remove_unused_room_types(apps, schema_migration):
        RoomType = apps.get_model('management', 'RoomType')
        Room = apps.get_model('management', 'Room')
        Bed = apps.get_model('management', 'Bed')
        Invitation = apps.get_model('management', 'Invitation')
        types = RoomType.objects.all()
        for type in types:
            rooms = Room.objects.filter(room_type=type)
            if (rooms.count() and Bed.objects.filter(room__in=rooms).filter(assignments__gte=1).exists()) or \
                    Invitation.objects.filter(room_type=type).exists():
                continue
            type.delete()

    dependencies = [
        ('management', '0055_add_missed_beds'),
    ]

    operations = [
        migrations.RunPython(remove_unused_room_types)
    ]
