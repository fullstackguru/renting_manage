# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('management', '0013_add_emails_templates_to_property_model'),
    ]

    operations = [
        migrations.AlterField(
            model_name='property',
            name='accept_relationship_email',
            field=models.ForeignKey(related_name='accept_relationship_email', blank=True, to='dbmail.MailTemplate', null=True),
        ),
        migrations.AlterField(
            model_name='property',
            name='account_reminder_email',
            field=models.ForeignKey(related_name='account_reminder_email', blank=True, to='dbmail.MailTemplate', null=True),
        ),
        migrations.AlterField(
            model_name='property',
            name='message_notification_email',
            field=models.ForeignKey(related_name='message_notification_email', blank=True, to='dbmail.MailTemplate', null=True),
        ),
        migrations.AlterField(
            model_name='property',
            name='reject_relationship_email',
            field=models.ForeignKey(related_name='reject_relationship_email', blank=True, to='dbmail.MailTemplate', null=True),
        ),
        migrations.AlterField(
            model_name='property',
            name='request_relationship_email',
            field=models.ForeignKey(related_name='request_relationship_email', blank=True, to='dbmail.MailTemplate', null=True),
        ),
        migrations.AlterField(
            model_name='property',
            name='welcome_email',
            field=models.ForeignKey(related_name='welcome_email', blank=True, to='dbmail.MailTemplate', null=True),
        ),
    ]
