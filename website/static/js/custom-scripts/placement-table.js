'use strict';


var reload = false;
var currentOffset = null;


var initPlacementTable = function (propId) {
    $('#agreement-period-select').on('change', function () {
         $('#placement-table').DataTable().draw();
    });
    $('#placement-table').on('preXhr.dt', function () {
        $('tr').attr('hidden', true);
        App.blocks('#placement-table-block', 'refresh_toggle');
    });
    $('#placement-table').on('xhr.dt', function () {
        $('tr').attr('hidden', false);
        App.blocks('#placement-table-block', 'refresh_toggle');
    });
    $('#placement-table').on('draw.dt', function () {
        $(".block .user-popover").each(function () {
            var $this = $(this);
            var bedInfo = $(this).data("bed-info");
            var $content = $this.find(".block-content");
            $content.popover({
                title: 'Status',
                trigger: 'hover',
                placement: "bottom",
                html: "true",
                content: $this.attr("data-content")
            });
            if (!bedInfo.bed.blocked) {
                $content.on('click', function (e) {
                    openModal(bedInfo);
                    currentOffset = $(document).scrollTop();
                });
            }
            $this.find(".exclude-button").each(function () {
                var $this = $(this);
                $this.on('click', function (e) {
                    excludeBed(bedInfo.bed);
                    currentOffset = $(document).scrollTop();
                })
            });
        });
        if (currentOffset) {
            $('html, body').scrollTop(currentOffset);
            currentOffset = null;
        }
    });
    $('#placement-table').DataTable({
        initComplete: function () {
            var api = this.api();
            $('#placement-table_filter input')
                .off('.DT')
                .on('keyup.DT', function (e) {
                    if (e.keyCode == 13) {
                        api.search(this.value).draw();
                    }
                });
        },
        "serverSide": true,
        "processing": true,
        "bPaginate": true,
        "sPaginationType": "full_numbers",
        "iDisplayLength": 100,
        ajax: {
            data: function(d){
                d.period = $('#agreement-period-select').val();
            },
            "dataType": 'json',
            "dataSrc": function (json) {
                return json.data
            }
        },
        "aoColumns": [
            {
                "mDataProp": "name",
                "render": function (data, type, full, meta) {
                    return '<a href="/management/placements/' + propId + '/units/' + full.id + '/">' + data + '</a>';
                }
            },
            {
                "mDataProp": "unit_type",
                "render": function (data, type, full, meta) {
                    return data;
                }
            },
            {
                "mDataProp": "rooms",
                "render": function (data, type, full, meta) {
                    var row = $("<div></div>").addClass("row");
                    var logo = $("<div></div>").addClass("push-15");
                    logo.append($("<i></i>").addClass("fa fa-bed fa-2x"));
                    data.forEach(function (room) {
                        var block = $("<div></div>").addClass("block col-lg-3");
                        var ribbon = $("<div></div>").addClass("block-content block-content-full ribbon ribbon-modern ribbon-info");
                        var ribbonTitle = $("<div></div>").addClass("ribbon-box font-w600");
                        ribbonTitle.text("Room " + room.name);
                        ribbon.append(ribbonTitle);
                        block.append(ribbon);
                        var bedContainer = $("<div></div>").addClass("row items-push-2x text-center");
                        var numOfDoubleBeds = _.filter(room.beds, function (bed) {
                            return bed.contract_type && bed.contract_type.occupied_places > 1;
                        }).length;
                        var skipped = 0;
                        var beds = _.map(room.beds, function (bed) {
                            // Render Bed
                            var colClass = "col-lg-" + Math.round(12 / (room.beds.length - numOfDoubleBeds));
                            var div = $("<div></div>").addClass(colClass + " block user-popover");
                            div.css("margin-bottom", 0);
                            div.attr('data-bed-info', JSON.stringify({'bed': bed, 'unit': full.name}));
                            var header = $("<div></div>").addClass("block-header");
                            var headerOptions = $("<ul></ul>").addClass("block-options");
                            var excludeButton = $("<button></button>").addClass("exclude-button").html('<i class="si si-close"></i>');
                            excludeButton.on('click', function(e) {
                                excludeBed(bed);
                            });
                            var option = $("<li></li>").append(excludeButton);
                            headerOptions.append(option);
                            header.append(headerOptions);
                            var content = $("<div></div>").addClass("block-content block-content-full text-center");
                            var text = $("<div></div>").addClass("h6 text-muted").append(bed.name);
                            var current_logo = logo.clone();
                            if (bed.user) {
                                current_logo = current_logo.addClass("text-muted");
                                var msg = "Occupied<br />";
                                msg += "User: " + bed.user.first_name + " " + bed.user.last_name + " (" + bed.user.email + ")<br />";
                                msg += "Gender: " + bed.user.gender+ " <br />";
                                msg += "Contract Type: " + bed.contract_type.name + "<br />";
                                msg += "Occupied " + bed.contract_type.occupied_places + " beds.<br />";
                                if (bed.user_stage) {
                                    msg += "Stage: " + bed.user_stage;
                                }
                                div.attr("data-content", msg);
                            } else {
                                if (skipped < numOfDoubleBeds) {
                                    skipped++;
                                    return;
                                }
                                if (bed.blocked) {
                                    current_logo = $("<div></div>").addClass("push-15 text-danger");
                                    current_logo.append($("<i></i>").addClass("fa fa-lock fa-2x"));
                                    div.attr("data-content", "Blocked");
                                } else {
                                    current_logo = current_logo.addClass("text-success");
                                    div.attr("data-content", "Empty");
                                }
                            }
                            content.append(current_logo);
                            content.append(text);
                            div.append(header);
                            div.append(content);
                            return div;
                        });
                        bedContainer.append(beds);
                        block.append(bedContainer);
                        row.append(block);
                    });
                    return row[0].outerHTML;
                },
                "orderable": false
            }

        ],
        paging: true,
        "lengthMenu": [[10, 25, 50, 100, -1], [10, 25, 50, 100, "All"]]
    });
};


var excludeBed = function (bed) {
    var sendRequest = function () {
        $.ajax({
            url: window.location.pathname + $('#agreement-period-select').val() +  '/users/',
            data: {'bed_id': bed.id},
            type: 'PATCH',
            success: function (data) {
                $('#placement-table').DataTable().draw('page');
            }
        });
    };
    if (!bed.user) {
        sendRequest();
        return
    }
    swal({
        title: "Are you sure?",
        text: "Are you sure that you want to block " + bed.name + " ? It will break all user relations.",
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: "#DD6B55",
        confirmButtonText: "Yes, block it!",
        cancelButtonText: "No, cancel!",
        closeOnConfirm: true
        },
        function (isConfirm) {
            if (isConfirm) {
                sendRequest();
            }
        });
};


$('#bed-status-modal').on('hide.bs.modal', function (e) {
    if (reload) {
        $('#placement-table').DataTable().draw('page');
    }
    reload = false;
    $('#delete-button').off('click');

    $('#add-to-bedspace-button').off('click');

    $('#with-groups-checkbox, #with-beds-checkbox').off('change');
});


var getAvailableUsers = function (bed, with_groups, callback) {
    var data = {
        'bed_id': bed
    };
    if ($('#with-groups-checkbox').is(':checked')) {
        data['with_group'] = true;
    }
    if ($('#with-beds-checkbox').is(':checked')) {
        data['with_beds'] = true;
    }
    $.ajax({
        url: window.location.pathname + $('#agreement-period-select').val() + '/users/',
        data: data,
        type: 'GET',
        success: function (data) {
            callback(data);
        }
    });
};


var deleteFromBedspace = function (bed) {
    swal({
            title: "Are you sure?",
            text: "Are you sure that you want to remove " + bed.user.first_name + " " + bed.user.last_name +
            " from bedspace? It will break all user relations.",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Yes, remove it!",
            cancelButtonText: "No, cancel!",
            closeOnConfirm: true
        },
        function (isConfirm) {
            if (isConfirm) {
                $.ajax({
                    url: window.location.pathname + $('#agreement-period-select').val() + '/users/',
                    data: {'bed_id': bed.id},
                    type: 'DELETE',
                    success: function (data) {
                        reload = true;
                        $('#bed-status-modal').modal('hide');
                    }
                });
            }
        });
};


var addToBedspace = function (bed) {
    var data = {
        'bed_id': bed.id,
        'user_id': $('#user-select').find(":selected").val()
    };
    if ($('#with-groups-checkbox').is(':checked')) {
        data['with_group'] = true;
    }
    if ($('#with-beds-checkbox').is(':checked')) {
        data['with_beds'] = true;
    }
    var makeRequest = function () {
        $.ajax({
            url: window.location.pathname + $('#agreement-period-select').val() + '/users/',
            data: data,
            type: 'POST',
            success: function (data) {
                reload = true;
                $('#bed-status-modal').modal('hide');
            },
            error: function (jqXHR, textStatus, errorThrown) {
            		reload = true;
            		$('#bed-status-modal').modal('hide');
            		var errorText = jqXHR.responseText.split("\n")
                swal("Oops...", errorThrown+" : "+errorText[1], "error");
                
            }
        });
    };
    if (data['with_beds']) {
        swal({
                title: "Are you sure?",
                text: "Are you sure that you want to add " + $('#user-select').find(":selected").text() +
                " to bedspace and remove current user assignment? It will break all user relations.",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: "Yes, add it!",
                cancelButtonText: "No, cancel!",
                closeOnConfirm: true
            },
            function (isConfirm) {
                if (isConfirm) {
                    makeRequest();
                }
            });
    } else {
        makeRequest();
    }
};


var addToBedspaceModalInit = function (bedInfo) {
    var $modal = $('#bed-status-modal');
    $modal.find('.user-assigned').hide();
    $modal.find('.user-doesnt-assigned').show();

    var calback = function (users) {
        var $select = $('#user-select');
        $select.children().remove();
        users.forEach(function (user) {
            $('<option/>').val(user.id).html(user.first_name + ' ' + user.last_name + ' (' + user.email + ')').appendTo('#user-select');
        });
        $select.select2({
            width: '550px'
        });
    };

    $('#with-groups-checkbox, #with-beds-checkbox').change(function (e) {
        getAvailableUsers(bedInfo.bed.id, false, calback);
    });

    getAvailableUsers(bedInfo.bed.id, false, calback);
};


var removeFromBedspaceModalInit = function (bedInfo) {
    var $modal = $('#bed-status-modal');
    var user = bedInfo.bed.user;
    var assignedBlock = $modal.find('.user-assigned');
    assignedBlock.show();
    assignedBlock.find('h3').text(user.first_name + ' ' + user.last_name);
    $modal.find('.user-doesnt-assigned').hide();
};


var openModal = function (bedInfo) {
    reload = false;
    var $modal = $('#bed-status-modal');
    $modal.find('.modal-title').text('Bed ' + bedInfo.bed.name + ' in Unit ' + bedInfo.unit);
    $("#with-groups-checkbox").prop('checked', false);
    $("#with-beds-checkbox").prop('checked', false);
    if (bedInfo.bed.user) {
        removeFromBedspaceModalInit(bedInfo);
    } else {
        addToBedspaceModalInit(bedInfo);
    }

    $('#delete-button').on('click', function (e) {
        deleteFromBedspace(bedInfo.bed);
    });

    $('#add-to-bedspace-button').on('click', function (e) {
        addToBedspace(bedInfo.bed);
    });

    $modal.modal('show');
};
