# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
from django.db.utils import IntegrityError
from django.db import transaction


class Migration(migrations.Migration):

    def convert_emails_to_lowercase(apps, schema_editor):
        User = apps.get_model("website", "User")
        users = User.objects.all()
        for user in users:
            try:
                user.email = user.email.lower()
                with transaction.atomic():
                    user.save()
            except IntegrityError:
                print user.email
                user.delete()

    dependencies = [
        ('website', '0022_add_co_educational_groups'),
    ]

    operations = [
        migrations.RunPython(convert_emails_to_lowercase),
    ]
