from optparse import make_option
from django.core.management.base import BaseCommand, CommandError


from management.models import Property


class Command(BaseCommand):
    help = 'Create test property with same settings.'

    option_list = BaseCommand.option_list + (
        make_option('--property',
                    action="store",
                    type="string",
                    dest="property",
                    help='Name of property that you want to copy.'),

        make_option('--new-property',
                    action="store",
                    type="string",
                    dest="new_property",
                    help='Name of new property.'),
    )

    def handle(self, *args, **options):
        prop_name = options.get('property')
        new_prop_name = options.get('new_property')
        if not prop_name or not new_prop_name:
            raise CommandError('You must provide property name and new property name.')
        prop = Property.objects.filter(name=prop_name).first()
        if not prop:
            raise CommandError("Property with specified name doesn't exist")
        if Property.objects.filter(name=new_prop_name).exists():
            raise CommandError("Property with name {} already exists.".format(new_prop_name))
        managers = prop.managers.all()
        emails = prop.emails.all()
        new_prop = prop
        new_prop.id = None
        new_prop.name = new_prop_name
        new_prop.save()
        for manager in managers:
            new_prop.managers.add(manager)
        for email in emails:
            new_email = email
            new_email.id = None
            new_email.slug = '{}_{}'.format(new_prop.name, new_email.name).lower().replace(' ', '_')
            new_email.name += "(for %s)" % new_prop.name
            new_email.save()
            new_prop.emails.add(new_email)
