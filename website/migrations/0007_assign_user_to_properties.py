# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    def assign_users_to_properties(apps, schema_editor):
        Invitation = apps.get_model("management", "Invitation")
        db_alias = schema_editor.connection.alias
        invites = Invitation.objects.using(db_alias).select_related('user').select_related('agreement_period__property')
        for invite in invites:
            property = invite.agreement_period.property
            user = invite.user
            user.available_properties.add(property)

    dependencies = [
        ('website', '0006_add_available_properties_to_user'),
    ]

    operations = [
        migrations.RunPython(assign_users_to_properties),
    ]
