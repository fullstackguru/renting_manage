from optparse import make_option

from django.core.management.base import BaseCommand, CommandError
from django.utils.translation import activate
from openpyxl import load_workbook

from django.contrib.auth import get_user_model
User = get_user_model()
from management.models import Property, UnitType, RoomType, Unit, Room, Bed


class Command(BaseCommand):
    help = "Parses records from file and creates units/rooms/beds. " \
           "It's remove other property realty that doesn't specified in file."

    option_list = BaseCommand.option_list + (
        make_option('--file',
                    action="store",
                    type="string",
                    dest="path",
                    help='Path to file with data'
                    ),
        make_option('--property',
                    action="store",
                    type="string",
                    dest="prop",
                    help='Property name'
                    ),
        )

    def handle(self, *args, **options):
        activate('en')
        path = options.get('path') or ''
        if not path:
            raise CommandError('Please set filename!')
        try:
            workbook = load_workbook(path, read_only=True, use_iterators=True)
        except IOError:
            raise CommandError('No such file!')
        prop = options.get('prop')
        if not prop:
            raise CommandError('Please set property name!')
        prop = Property.objects.filter(name=prop).first()
        if not prop:
            raise CommandError("Property with specified name doesn't exist")
        first_sheet = workbook.get_sheet_names()[0]
        worksheet = workbook.get_sheet_by_name(first_sheet)
        existed_beds = []
        existed_rooms = []
        existed_units = []
        for index, items_row in enumerate(worksheet.rows):
            if index == 0:
                continue
            formatted_cells = [unicode(i.value).strip() if i.value else '' for i in items_row]
            unit_name = formatted_cells[0]
            unit_type_name = formatted_cells[1]
            room_name = formatted_cells[2]
            room_type_name = formatted_cells[3]
            bed_name = formatted_cells[4]
            unit_type, _ = UnitType.objects.get_or_create(name=unit_type_name, property=prop)
            unit, _ = Unit.objects.get_or_create(name=unit_name, unit_type=unit_type)
            existed_units.append(unit.id)
            room_type, _ = RoomType.objects.get_or_create(name=room_type_name, property=prop)
            room, _ = Room.objects.get_or_create(unit=unit, name=room_name, defaults={'room_type': room_type})
            existed_rooms.append(room.id)
            bed, _ = Bed.objects.get_or_create(name=bed_name, room=room)
            existed_beds.append(bed.id)
        units_for_delete = Unit.objects.filter(unit_type__property=prop).exclude(id__in=existed_units)
        print units_for_delete
        units_for_delete.delete()
        rooms_for_delete = Room.objects.filter(unit__unit_type__property=prop).exclude(id__in=existed_rooms)
        print rooms_for_delete
        rooms_for_delete.delete()
        beds_for_delete = Bed.objects.filter(room__unit__unit_type__property=prop).exclude(id__in=existed_beds)
        print beds_for_delete
        beds_for_delete.delete()
