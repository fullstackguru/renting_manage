# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('website', '0035_add_group_leader'),
    ]

    operations = [
        migrations.AddField(
            model_name='relationship',
            name='group',
            field=models.ForeignKey(related_name='relations', blank=True, to='website.Group', null=True),
        ),
    ]
