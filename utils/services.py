from django.utils import timezone
from django.db.models import Q, Sum, F
from management_models import PendingAssignment, Invitation, Unit
from website.models import Relationship, User


def get_related_user_ids(user, ap=None, with_pending_requests=False):
    users = {user.id}
    relations = Relationship.objects.filter(Q(start_user=user) | Q(end_user=user)).\
        filter(invitation__agreement_period=ap)
    if not with_pending_requests:
        relations = relations.filter(approval_datetime__isnull=False, end_user_approved=True)
    relations = relations.values('start_user_id', 'end_user_id')
    for relation in relations:
        users.add(relation['start_user_id'])
        users.add(relation['end_user_id'])
    return users


def get_group_size(group_members):
    result = Invitation.objects.filter(user__in=group_members).filter(contract_type__occupied_places__gt=1). \
        aggregate(extra_size=Sum(F('contract_type__occupied_places') - 1))
    return len(group_members) + (result['extra_size'] if result['extra_size'] else 0)


def get_group_unit(group_members):
    group_unit = PendingAssignment.objects.filter(user_id__in=group_members).order_by('unit'). \
        values_list('unit', flat=True).distinct()
    if not group_unit:
        return None
    return group_unit[0]


def create_relations_for_suitemates(user, ap):
    user_invite = Invitation.objects.filter(user=user, agreement_period=ap).first()
    if not user_invite:
        return
    pending_assignment = PendingAssignment.objects.filter(user=user, assignment_period=user_invite.agreement_period). \
        select_related('unit').first()
    if not pending_assignment:
        return
    unit = pending_assignment.unit
    if not unit:
        return
    user_relations_users = get_related_user_ids(user, ap)
    pending_assignments = unit.pending_assignments.filter(assignment_period=ap).exclude(id=pending_assignment.id)
    for assignment in pending_assignments:
        suitemate = assignment.user
        suitemate_relation_users = get_related_user_ids(suitemate)
        new_relations_users = suitemate_relation_users - user_relations_users
        for new_user in new_relations_users:
            relation = Relationship(
                start_user=user, end_user_id=new_user, invitation=user_invite,
                approval_datetime=timezone.now(), end_user_approved=True, read_only=True
            )
            try:
                relation.save()
            except:
                pass
    all_suitemates = unit.pending_assignments.filter(assignment_period=ap).values_list('user', flat=True)
    Relationship.objects.filter(start_user__in=all_suitemates, end_user__in=all_suitemates).update(read_only=True,
                                                                                                   via_user=None)


def break_unnecessary_relations(user):
    '''
    Check if user has pending requests that can conflict with current group and remove it.
    '''
    print("break unnecessary relations")
    user_group = get_related_user_ids(user)
    print("user group: ")
    print(user_group)
    group_unit = get_group_unit(user_group)
    print("group_unit: ")
    print(group_unit)
    group_size = get_group_size(user_group)
    print("group_size: ")
    print(group_size)
    user_invite = Invitation.objects.filter(user=user).select_related('unit_type').first()
    max_allowed = user_invite.unit_type.max_occupancy - group_size
    pending_relations = Relationship.objects.filter(Q(start_user=user) | Q(end_user=user)). \
        exclude(end_user_approved=True, approval_datetime__isnull=False)
    potential_suitemates = []
    for relation in pending_relations:
        potential_suitemates.append((relation.start_user if relation.end_user == user else relation.end_user, relation))
    for potential_suitemate, relation in potential_suitemates:
        potential_suitemate_group = get_related_user_ids(potential_suitemate, with_pending_requests=True)
        potential_suitemate_unit = get_group_unit(potential_suitemate_group)
        potential_suitemate_group_size = get_group_size(potential_suitemate_group)
        if (potential_suitemate_unit and group_unit and potential_suitemate_unit != group_unit) or \
                        potential_suitemate_group_size > max_allowed:
            print relation
            print("gonna delete relation: ")
            print(relation)
            relation.delete()


def split_groups(users=None, with_pending_requests=False):
    if users is None:
        users = User.objects.all()
    for user in users:
        group = get_related_user_ids(user, with_pending_requests=with_pending_requests)
        group_size = get_group_size(group)
        units = PendingAssignment.objects.filter(user_id__in=group).order_by('unit'). \
            values_list('unit', flat=True).distinct()
        if not units:
            units = [-1]
        #  Remove groups with different units
        if len(units) > 1:
            users_from_another_unit = PendingAssignment.objects.filter(unit_id__in=units[1:]).\
                values_list('user', flat=True)
            user_relations = Relationship.objects.filter(Q(start_user=user) | Q(end_user=user))
            relations_for_deleting = user_relations.filter(Q(start_user_id__in=users_from_another_unit) |
                                                           Q(end_user_id__in=users_from_another_unit))
            for relation in relations_for_deleting:
                relation.delete()
            group = get_related_user_ids(user, with_pending_requests=with_pending_requests)
            group_size = get_group_size(group)
        unit = Unit.objects.filter(id=units[0]).select_related('unit_type').first()
        if not unit:
            invite = Invitation.objects.filter(user=user).first()
            if not invite:
                continue
            max_allowed = invite.unit_type.max_occupancy
        else:
            max_allowed = unit.unit_type.max_occupancy
        #  Remove overflowed groups by users from waitlist
        if group_size > max_allowed:
            assigned_users = PendingAssignment.objects.filter(user_id__in=group)
            if unit:
                assigned_users.filter(unit=unit)
            assigned_users = set(assigned_users.values_list('user', flat=True))
            waitlist_users = list(group - assigned_users)[:group_size - max_allowed]
            if not assigned_users:
                assigned_users = group - set(waitlist_users)
            relations_for_deleting = Relationship.objects.filter(Q(start_user_id__in=waitlist_users, end_user_id__in=assigned_users) |
                                                                 Q(start_user_id__in=assigned_users, end_user_id__in=waitlist_users))
            for relation in relations_for_deleting:
                relation.delete()


def lock_group(user, check_overflowed=False):
    group_members = get_related_user_ids(user)
    if check_overflowed:
        group_size = get_group_size(group_members)
        group_unit_id = get_group_unit(group_members)
        group_unit = Unit.objects.filter(id=group_unit_id).select_related('unit_type').first()
        if group_unit:
            max_occupancy = group_unit.unit_type.max_occupancy
        else:
            max_occupancy = Invitation.objects.filter(id__in=group_members).values('unit_type__max_occupancy').first()
            max_occupancy = max_occupancy['unit_type__max_occupancy'] if max_occupancy else 0
        if group_size < max_occupancy:
            raise Exception('Unable to lock unfilled group!')
    members_relations = Relationship.objects.filter(Q(start_user_id__in=group_members) |
                                                    Q(end_user_id__in=group_members))
    #  remove pending invites
    members_relations.filter(approval_datetime__isnull=True, end_user_approved=False).delete()
    #  set group relations as readonly
    members_relations.update(read_only=True)
    #  lock invites
    Invitation.objects.filter(user_id__in=group_members).update(locked=True)
