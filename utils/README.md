This folder is used to store different utility commands available through Django's ./manage.py script.

The commands could be related to __website__ or __management__ or other future Django applications.
This folder is used only because __management__ name conflicts with standard Django's conventions about
management commands. All commands must be located in some management/ folder and because __website__ and
__management__ applications has references for each other so there is a problem with import Django custom
commands.
