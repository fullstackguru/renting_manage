var initImportTable = function (listUrl) {
    $('#import-table').on('preXhr.dt', function () {
        $('tr').attr('hidden', true);
        App.blocks('#resident-import-table-block', 'refresh_toggle');
    });
    $('#import-table').on('xhr.dt', function () {
        $('tr').attr('hidden', false);
        App.blocks('#resident-import-table-block', 'refresh_toggle');
    });
    $('#import-table').on('draw.dt', function () {
        $("div[data-error]").each(function () {
            var $this = $(this);
            var error = $(this).data("error");
            $this.popover({
                title: 'Error',
                trigger: 'hover',
                placement: "left",
                html: "true",
                content: error
            });
        });
    });
    $('#import-table').DataTable({
        initComplete: function () {
            var api = this.api();
            $("form").submit(function () {
                var formData = new FormData($(this)[0]);
                $.ajax({
                    url: window.location.pathname,
                    type: 'POST',
                    data: formData,
                    async: false,
                    success: function (data) {
                        swal("Done!", "Your file added to queue", "success");
                        $(":file").filestyle('clear');
                        api.draw();
                    },
                    error: function (jqXHR, textStatus, errorThrown) {
                        swal("Oops...", jqXHR.responseText, "error");
                    },
                    cache: false,
                    contentType: false,
                    processData: false
                });
                return false;
            });
            $("#sample-button").on("click", function () {
                window.open(window.location.pathname + "?type=sample");
            });
        },
        "serverSide": true,
        "processing": true,
        "searching": false,
        "bSort": true,
        "bPaginate": true,
        "sPaginationType": "full_numbers",
        "iDisplayLength": 10,
        "order": [[ 1, "desc" ]],
        ajax: {
            "dataType": 'json',
            url: listUrl,
            "dataSrc": function (json) {
                return json.data
            }
        },
        "aoColumns": [
            {
                "mDataProp": "file",
                "render": function (data, type, full, meta) {
                    return '<a href="' + full.file_url + '">' + data + '</a>';
                }
            },
            {
                "mDataProp": "created_at",
                "render": function (data, type, full, meta) {
                    return moment(data, moment.ISO_8601).format('MMMM Do YYYY, h:mm:ss a');
                }
            },
            {
                "mDataProp": "status",
                "render": function (data, type, full, meta) {
                    var div = $('<div></div>');
                    if (full.error) {
                        div.attr("data-error", full.error);
                    }
                    div.html(data);
                    return div[0].outerHTML;
                }
            }
        ],
        paging: true,
        "lengthMenu": [[10, 25, 50, 100], [10, 25, 50, 100]]
    });
};
