from optparse import make_option
from django.core.management.base import BaseCommand
from django.db.models import Q
from website.models import Relationship, User
from management.models import PendingAssignment, Unit
from utils.services import create_relations_for_suitemates, split_groups


class Command(BaseCommand):
    help = 'Split groups with different units and group user from same unit'

    option_list = BaseCommand.option_list + (
        make_option('--split',
                    action="store",
                    type="string",
                    dest="split",
                    help='Split groups with different units.',
                    default=False
                    ),
        make_option('--group',
                    action="store",
                    type="string",
                    dest="group",
                    help="Group user from same unit.",
                    default=False,
                    ),
        )

    def handle(self, *args, **options):
        if options['split']:
            print 'Split groups'
            users = User.objects.all()
            split_groups(users)
        if options['group']:
            print 'Group users from same unit'
            users = User.objects.all()
            for user in users:
                create_relations_for_suitemates(user)

