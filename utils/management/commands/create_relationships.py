from itertools import permutations
from datetime import datetime

from django.core.management.base import BaseCommand

from website.models import Relationship
from management.models import Invitation, Unit



class Command(BaseCommand):
    help = 'Updates Relationships'

    def handle(self, *args, **options):
        index = 0
        print options
        for unit in Unit.objects.all():
            unit_assignments_count = unit.pending_assignments.all().count() or 0
            if unit_assignments_count > 1:
                for i in permutations(unit.pending_assignments.all(), 2):
                    end_user_invite = Invitation.objects.get(user=i[1].user, agreement_period=i[1].assignment_period)
                    rs_dict = {
                        'start_user': i[0].user,
                        'invitation': end_user_invite,
                        'end_user': end_user_invite.user
                    }
                    rs_defaults = {
                        'end_user_approved': True,
                        'approval_datetime': datetime.now()
                    }
                    rs, rs_created = Relationship.objects.update_or_create(defaults=rs_defaults, **rs_dict)
                    index += 1
            print 'Unit: {}, assignments count: {}; Total: {}'.format(unit.name, unit_assignments_count, index)