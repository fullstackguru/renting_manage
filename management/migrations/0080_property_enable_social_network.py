# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('management', '0079_property_file'),
    ]

    operations = [
        migrations.AddField(
            model_name='property',
            name='enable_social_network',
            field=models.BooleanField(default=False),
        ),
    ]
