# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


def split_beds_to_rooms(Room, PendingAssignment, ids):
    rooms = Room.objects.filter(id__in=ids).prefetch_related('beds')
    for room in rooms:
        beds = room.beds.all()
        for bed in beds:
            room_name = bed.name[0]
            new_room, _ = Room.objects.get_or_create(
                unit=room.unit, name=room_name, room_type=room.room_type, square=room.square
            )
            PendingAssignment.objects.filter(bed=bed).update(room=new_room)
            bed.room = new_room
            bed.save()


class Migration(migrations.Migration):

    def add_rooms(apps, schema_migration):
        Unit = apps.get_model('management', 'Unit')
        Room = apps.get_model('management', 'Room')
        PendingAssignment = apps.get_model('management', 'PendingAssignment')
        Property = apps.get_model('management', 'Property')
        props = []
        lorenzo = Property.objects.filter(name='The Lorenzo').first()
        if lorenzo:
            room_ids = Unit.objects.filter(unit_type__property=lorenzo).values_list('rooms', flat=True)
            split_beds_to_rooms(Room, PendingAssignment, room_ids)
            props.append(lorenzo)
        dobie = Property.objects.filter(name='DobieTwenty21').first()
        if dobie:
            room_ids = Unit.objects.filter(unit_type__property=dobie).values_list('rooms', flat=True)
            split_beds_to_rooms(Room, PendingAssignment, room_ids)
            props.append(dobie)
        cottonwood = Property.objects.filter(name='Cottonwood').first()
        if cottonwood:
            room_ids = Unit.objects.filter(unit_type__property=cottonwood).values_list('rooms', flat=True)
            split_beds_to_rooms(Room, PendingAssignment, room_ids)
            props.append(cottonwood)
        Room.objects.filter(unit__unit_type__property__in=props, name__isnull=True).delete()

    dependencies = [
        ('management', '0053_add_name_for_rooms'),
    ]

    operations = [
        migrations.RunPython(add_rooms)
    ]
