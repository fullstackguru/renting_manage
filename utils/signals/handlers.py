import re
from django.db.models.signals import pre_save
from dbmail.models import MailTemplate


BASE64_REGEX = r'([\"\']data:image\/\w+;base64,.+?[\"\'])'
HTML_STYLE_REGEX = r'(<style.*>[\s\S]+?<.style>)'
HTML_SCRIPT_REGEX = r'(<script.*>[\s\S]+?<.script>)'


def remove_base64_images_signal(sender, instance, **kwargs):
    #  remove all style blocks
    matches = re.findall(HTML_STYLE_REGEX, instance.message)
    for match in matches[1:]:
        instance.message = instance.message.replace(match, '')
    #  remove js
    instance.message = re.sub(HTML_SCRIPT_REGEX, '', instance.message)
    #  remove base64 images
    instance.message = re.sub(BASE64_REGEX, '""', instance.message)


pre_save.connect(remove_base64_images_signal, sender=MailTemplate, dispatch_uid='remove_base64_images')
