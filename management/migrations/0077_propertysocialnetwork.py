# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('management', '0076_property_from_email_host'),
    ]

    operations = [
        migrations.CreateModel(
            name='PropertySocialNetwork',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('network_name', models.CharField(max_length=50)),
                ('property', models.ForeignKey(to='management.Property')),
            ],
        ),
    ]
