FROM python:2.7-slim

RUN mkdir -p /usr/src/app
WORKDIR /usr/src/app
COPY requirements.txt /usr/src/app/

# Run 'pip install' before copying code, use docker cache for external packages
# gcc, libc6-dev is required to 'pip install uwsgi' and other C-based packages
# libpq-dev is required for 'pip install psycopg'
# libjpeg-dev zlib1g-dev are required for 'pip install Pillow'
# Remove build dependencies right after 'pip install' to save ~100MB
RUN set -x \
    && buildDeps='gcc libc6-dev libpq-dev' \
    && runtimeDeps='libpq5 libjpeg-dev zlib1g-dev libxml2-dev libxslt1-dev redis-server libgeoip-dev supervisor' \
    && apt-get update \
    && apt-get install -y --no-install-recommends $buildDeps $runtimeDeps \
    && pip install -r requirements.txt
    # && apt-get purge -y --auto-remove $buildDeps \
    # && rm -rf /var/lib/apt/lists/*

RUN mkdir -p /var/log/uwsgi

RUN mkdir -p /var/log/supervisor
COPY ./devops/config/supervisord.conf /etc/supervisor/conf.d/supervisord.conf

RUN rm /etc/redis/redis.conf
COPY ./devops/config/redis.conf /etc/redis/redis.conf

COPY . /usr/src/app
RUN ./manage.py collectstatic --noinput
RUN chmod +x ./run_web_server.sh

# uWSGI HTTP protocol, native protocol, stats
EXPOSE 8000
ENV C_FORCE_ROOT="true"
CMD ["/usr/bin/supervisord"]
