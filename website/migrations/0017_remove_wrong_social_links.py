# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
from django.core.validators import URLValidator
from django.core.exceptions import ValidationError


class Migration(migrations.Migration):

    def remove_wrong_social_links(apps, schema_editor):
        UserProfile = apps.get_model("website", "UserProfile")
        user_profiles = UserProfile.objects.all()
        validate = URLValidator()
        attrs = ['twitter_url', 'facebook_url', 'pinterest_url', 'linkedin_url', 'instagram_url',
                 'snapchat_url', 'google_plus_url']
        for user_profile in user_profiles:
            for attr in attrs:
                try:
                    link = getattr(user_profile, attr)
                    if link and not any(map(lambda sn: sn in link, ['twitter', 'facebook', 'pinterest',
                                                                    'linkedin', 'instagram', 'snapchat',
                                                                    'google'])):
                        validate(link)
                except ValidationError:
                    print "Incorrect link: {}".format(getattr(user_profile, attr))
                    setattr(user_profile, attr, None)
            user_profile.save()

    dependencies = [
        ('website', '0016_add_social_links_validators'),
    ]

    operations = [
        migrations.RunPython(remove_wrong_social_links),
    ]
