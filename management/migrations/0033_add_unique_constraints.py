# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('management', '0032_remove_duplicated_beds'),
    ]

    operations = [
        migrations.AlterUniqueTogether(
            name='assignmentperiod',
            unique_together=set([('property', 'name')]),
        ),
        migrations.AlterUniqueTogether(
            name='bed',
            unique_together=set([('name', 'room')]),
        ),
    ]
