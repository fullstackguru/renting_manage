from django.apps import AppConfig
from dbmail.models import MailTemplate


FEED_MODELS = ['AssignmentPeriod', 'UnitType', 'RoomType', 'Unit', 'Room', 'Bed', 'ContractType', 'Invitation',
               'OrganizationSocialHabitQuestion', 'ResidentImport', 'RealtyImport', 'Property']


class ManagementAppConfig(AppConfig):
    name = 'management'

    def ready(self):
        # Initialize signals
        from management.signals.handlers import send_welcome_email_signal, mailtemplate_post_save, make_email_unique
        from actstream import registry
        for model in FEED_MODELS:
            registry.register(self.get_model(model))
        registry.register(MailTemplate)
