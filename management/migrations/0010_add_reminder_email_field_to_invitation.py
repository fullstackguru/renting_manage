# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('management', '0009_add_required_field_to_org_questions'),
    ]

    operations = [
        migrations.AddField(
            model_name='invitation',
            name='is_reminder_email_sent',
            field=models.BooleanField(default=False),
        ),
    ]
