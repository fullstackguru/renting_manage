from django.core.management.base import BaseCommand, CommandError
from website.models import Group, User, Invitation
from management.models import Property, Unit
from utils.services import get_related_user_ids, get_group_unit


class Command(BaseCommand):
    help = "Create groups."

    def handle(self, *args, **options):
        managers_ids = Property.objects.values_list('managers', flat=True)
        users = User.objects.exclude(id__in=managers_ids)
        for u in users:
            unit = None
            if Group.objects.filter(users__in=[u]).exists():
                continue
            group_users = get_related_user_ids(u)
            if group_users == 1:
                continue
            group_unit = get_group_unit(group_users)
            invite = Invitation.objects.filter(user=u).first()
            if not invite:
                continue
            if group_unit:
                unit = Unit.objects.filter(id=group_unit).first()
            group = Group(
                agreement_period=invite.agreement_period,
                unit=unit,
                unit_type=unit.unit_type if unit else invite.unit_type
            )
            group.save()
            for group_member in group_users:
                group.users.add(group_member)
            group.save()
