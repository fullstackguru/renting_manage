# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import ckeditor.fields


class Migration(migrations.Migration):

    dependencies = [
        ('dbmail', '0009_auto_20160311_0918'),
    ]

    operations = [
        migrations.AlterField(
            model_name='mailbasetemplate',
            name='message',
            field=ckeditor.fields.RichTextField(help_text='Basic template for mail messages. {{content}} tag for msg.', verbose_name='Body'),
        ),
        migrations.AlterField(
            model_name='mailtemplate',
            name='message',
            field=ckeditor.fields.RichTextField(verbose_name='Body'),
        ),
    ]
