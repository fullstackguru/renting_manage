'use strict';

var initCharts = function (url) {
    var studentsChart, placementChart, stagesChart;

    var getCharts = function () {
        var period = $('#agreement-period-select').val();
        if (!period) {
            return;
        }
        $.get(url, {'period': period}, function (data) {
            var ctx = null;
            var students = data.students;
            var placements = data.placements;
            var stages = data.stages;
            var realEstate = data.real_estate;
            var defaultOptions = {
                animation: {
                    duration: 500,
                    easing: "easeOutQuart"
                }
            };
            var totalTenants = students.count;
            var totalBeds = placements.count;
            $('#total-students').text(totalTenants);
            $('#total-beds').text(totalBeds);
            if (studentsChart) {
                studentsChart.destroy();
                placementChart.destroy();
                stagesChart.destroy();
            }

            //init students chart
            var $studentsChart = $("#students-chart");
            $studentsChart.empty();
            ctx = $studentsChart.get(0).getContext("2d");
            studentsChart = new Chart(ctx, {
                type: 'pie',
                data: students,
                options: defaultOptions
            });

            //init placements chart
            var $placementChart = $("#placements-chart");
            $placementChart.empty();
            ctx = $placementChart.get(0).getContext("2d");
            placementChart = new Chart(ctx, {
                type: 'pie',
                data: placements,
                options: defaultOptions
            });

            // init stages chart
            var stagesOptions = {
                events: false,
                animation: {
                    duration: 500,
                    easing: "easeOutQuart",
                    onComplete: function () {
                        var ctx = this.chart.ctx;
                        ctx.font = Chart.helpers.fontString(14, 'normal', Chart.defaults.global.defaultFontFamily);
                        ctx.textAlign = 'center';
                        ctx.textBaseline = 'bottom';
                        var origin_data = this.data;

                        this.data.datasets.forEach(function (dataset) {

                            for (var i = 0; i < dataset.data.length; i++) {
                                var model = dataset._meta[Object.keys(dataset._meta)[0]].data[i]._model,
                                    total = dataset._meta[Object.keys(dataset._meta)[0]].total,
                                    mid_radius = model.innerRadius + (model.outerRadius - model.innerRadius) / 2,
                                    start_angle = model.startAngle,
                                    end_angle = model.endAngle,
                                    mid_angle = start_angle + (end_angle - start_angle) / 2;

                                var x = mid_radius * Math.cos(mid_angle);
                                var y = mid_radius * Math.sin(mid_angle);

                                ctx.fillStyle = '#fff';
                                ctx.fillText(origin_data.labels[i], model.x + x, model.y + y);
                            }
                        });
                    }
                },
                legend: {
                    display: false
                }
            };
            var $stagesChart = $("#stages-chart");
            $stagesChart.empty();
            ctx = $stagesChart.get(0).getContext("2d");
            stagesChart = new Chart(ctx, {
                type: 'pie',
                data: stages,
                options: stagesOptions
            });

            // init real estate
            $('#num-of-units').text(realEstate.units);
            $('#num-of-rooms').text(realEstate.rooms);
            $('#num-of-beds').text(realEstate.beds);
        });
    };

    $('#agreement-period-select').on('change', function () {
        getCharts();
    });

    getCharts();
};