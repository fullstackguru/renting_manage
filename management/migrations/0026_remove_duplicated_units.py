# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    def remove_duplicated_units(apps, schema_editor):
        PendingAssignment = apps.get_model("management", "PendingAssignment")
        Unit = apps.get_model("management", "Unit")
        units = Unit.objects.all()
        units_for_delete = set()
        for unit in units:
            if unit.id in units_for_delete:
                continue
            same_units = Unit.objects.filter(name=unit.name)
            if same_units.count() > 1:
                for same_unit in same_units[1:]:
                    PendingAssignment.objects.filter(unit=same_unit).update(unit=same_units[0])
                    units_for_delete.add(same_unit.id)
        Unit.objects.filter(id__in=units_for_delete).delete()

    dependencies = [
        ('management', '0025_remove_matches_with_different_units'),
    ]

    operations = [
        migrations.RunPython(remove_duplicated_units)
    ]
