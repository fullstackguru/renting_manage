from optparse import make_option
from django.core.management.base import BaseCommand, CommandError
from django.utils.translation import activate
from django.contrib.auth import get_user_model
from management.tasks import parse_invites_trinity
User = get_user_model()


class Command(BaseCommand):
    help = 'Parses records from file and creates user invites from them.'

    option_list = BaseCommand.option_list + (
        make_option('--file',
                    action="store",
                    type="string",
                    dest="path",
                    help='Path to file with data'
                    ),
        make_option('--remove_unnecessary_users',
                    action="store",
                    type="string",
                    dest="remove_unnecessary_users",
                    help="Set variable to true if you want to remove users that doesn't appears in file.",
                    default=False,
                    ),
        )

    def handle(self, *args, **options):
        activate('en')
        print options
        path = options.get('path') or ''
        print path
        if not path:
            raise CommandError('Please set filename!')
        try:
            file = open(path, 'r')
        except IOError:
            raise CommandError('No such file!')
        parse_invites_trinity(file)
