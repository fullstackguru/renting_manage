from celery import task
from utils.emails import send_account_reminder_email


@task(name='website.send_reminder_email')
def send_delayed_reminder_email(invite):
    send_account_reminder_email(invite)
