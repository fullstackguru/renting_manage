import django_filters
from django_filters.filters import MethodFilter, DateRangeFilter
from django.db.models import Sum
from website.models import User, Relationship, Group
from management.models import Invitation, AssignmentPeriod, PendingAssignment, UnitType, RoomType, PropertySocialNetwork
from management.widgets import ManagementFilterWidget
from utils.services import get_related_user_ids, get_group_size


BOOLEAN_CHOICES = (
    ('', 'All'),
    ('yes', 'Yes'),
    ('no', 'No'),
)


GROUP_SIZE_CHOICES = [
    ('', 'All')
]
GROUP_SIZE_CHOICES.extend([(str(i), str(i)) for i in range(1, 10)])
GROUP_SIZE_CHOICES.append(('>10', '10+'))


class AdvancedDateRangeFilter(DateRangeFilter):
    def __init__(self, *args, **kwargs):
        self.options.update({
            6: ('Never', lambda qs, name: qs.filter(**{
                '%s' % name: None,
            }))
        })
        super(AdvancedDateRangeFilter, self).__init__(*args, **kwargs)


class UserListFilter(django_filters.FilterSet):
        is_take_quiz = MethodFilter(action='check_is_take_quiz', widget=ManagementFilterWidget(choices=BOOLEAN_CHOICES),
                                    label='Taken Quiz')
        with_invite = MethodFilter(action='check_with_invite', widget=ManagementFilterWidget(choices=BOOLEAN_CHOICES))
        last_login = AdvancedDateRangeFilter(widget=ManagementFilterWidget())
        agreement_period = MethodFilter(action='check_agreement_period')
        group_size = MethodFilter(action='check_group_size', widget=ManagementFilterWidget(choices=GROUP_SIZE_CHOICES))
        has_roommate_requests = MethodFilter(action='check_roommate_requests',
                                             widget=ManagementFilterWidget(choices=BOOLEAN_CHOICES))
        with_lock_invites = MethodFilter(action='check_locked_invites',
                                         widget=ManagementFilterWidget(choices=BOOLEAN_CHOICES))
        with_assignments = MethodFilter(action='check_with_assignments', widget=ManagementFilterWidget(
            choices=BOOLEAN_CHOICES))

        class Meta:
            model = User
            fields = ['is_take_quiz', 'with_invite', 'last_login']

        def __init__(self, *args, **kwargs):
            property = kwargs.pop('property', None)
            super(UserListFilter, self).__init__(*args, **kwargs)
            if property:
                periods = AssignmentPeriod.objects.filter(property=property)
            else:
                periods = AssignmentPeriod.objects.all()
            periods_choices = [('', 'All')]
            periods_choices.extend((period.id, period.name) for period in periods)
            self.filters['agreement_period'].widget = ManagementFilterWidget(choices=periods_choices)

        def check_is_take_quiz(self, queryset, value):
            if value.lower() == 'yes':
                return queryset.filter(profile__personality_types__isnull=False)
            elif value.lower() == 'no':
                return queryset.filter(profile__personality_types__isnull=True)
            else:
                return queryset

        def check_with_invite(self, queryset, value):
            user_ids = Invitation.objects.values_list('user_id', flat=True)
            if value.lower() == 'yes':
                return queryset.filter(id__in=user_ids)
            elif value.lower() == 'no':
                return queryset.exclude(id__in=user_ids)
            else:
                return queryset

        def check_agreement_period(self, queryset, value):
            if value:
                user_ids = Invitation.objects.filter(agreement_period_id=value).values_list('user_id', flat=True)
                queryset = queryset.filter(id__in=user_ids)
            return queryset

        def check_group_size(self, queryset, value):
            if not value:
                return queryset
            users = User.objects.all()
            processed_users = set()
            filtered_user_ids = set()
            for user in users:
                if user.id in processed_users:
                    continue
                group_members = get_related_user_ids(user)
                group_size = get_group_size(group_members)
                if value == u'>10':
                    if group_size >= 10:
                        filtered_user_ids.update(group_members)
                    continue
                value = int(value)
                if group_size == value:
                    filtered_user_ids.update(group_members)
            return queryset.filter(id__in=filtered_user_ids)

        def check_roommate_requests(self, queryset, value):
            user_with_suitemates_requests = Relationship.objects.\
                filter(approval_datetime__isnull=True, end_user_approved=False).values_list('end_user', flat=True)
            start_users_with_roommate_requests = Relationship.objects.\
                filter(is_roommate=Relationship.PROPOSED_BY_END_USER).values_list('start_user', flat=True)
            end_users_with_roommate_requests = Relationship.objects.\
                filter(is_roommate=Relationship.PROPOSED_BY_START_USER).values_list('end_user', flat=True)
            if value.lower() == 'yes':
                users_with_pending_requests = list(user_with_suitemates_requests) + \
                                              list(start_users_with_roommate_requests) + \
                                              list(end_users_with_roommate_requests)
                return queryset.filter(id__in=users_with_pending_requests)
            elif value.lower() == 'no':
                users_with_pending_requests = list(user_with_suitemates_requests) + \
                                              list(start_users_with_roommate_requests) + \
                                              list(end_users_with_roommate_requests)
                return queryset.exclude(id__in=users_with_pending_requests)
            else:
                return queryset

        def check_locked_invites(self, queryset, value):
            user_ids = Invitation.objects.filter(locked=True).values_list('user_id', flat=True)
            if value.lower() == 'yes':
                return queryset.filter(id__in=user_ids)
            elif value.lower() == 'no':
                return queryset.exclude(id__in=user_ids)
            else:
                return queryset

        def check_with_assignments(self, queryset, value):
            users_with_assignments = PendingAssignment.objects.filter(unit__isnull=False).values_list('user', flat=True)
            if value.lower() == 'yes':
                return queryset.filter(id__in=users_with_assignments)
            elif value.lower() == 'no':
                return queryset.exclude(id__in=users_with_assignments)
            else:
                return queryset


class UnitTypeFilter(django_filters.FilterSet):
    class Meta:
        model = UnitType
        fields = ('id', )


class RoomTypeFilter(django_filters.FilterSet):
    class Meta:
        model = RoomType
        fields = ('id', )

class PropertySocialNetworkFilter(django_filters.FilterSet):
    class Meta:
        model = PropertySocialNetwork
        fields = ('id', )


class MatchesFilter(django_filters.FilterSet):
    agreement_period = MethodFilter(action='check_agreement_period')
    unit_type = MethodFilter(action='check_unit_type')
    with_group = MethodFilter(action='check_with_group', widget=ManagementFilterWidget(choices=BOOLEAN_CHOICES))
    is_locked = MethodFilter(action='check_is_locked', widget=ManagementFilterWidget(choices=BOOLEAN_CHOICES))

    class Meta:
        abstract = True

    def __init__(self, *args, **kwargs):
        property = kwargs.pop('property', None)
        super(MatchesFilter, self).__init__(*args, **kwargs)
        if property:
            periods = AssignmentPeriod.objects.filter(property=property)
            unit_types = UnitType.objects.filter(property=property)
        else:
            periods = AssignmentPeriod.objects.all()
            unit_types = UnitType.objects.all()
        periods_choices = [('', 'All')]
        unit_types_choices = [('', 'All')]
        periods_choices.extend((period.id, period.name) for period in periods)
        unit_types_choices.extend((unit_type.id, unit_type.name) for unit_type in unit_types)
        self.filters['agreement_period'].widget = ManagementFilterWidget(choices=periods_choices)
        self.filters['unit_type'].widget = ManagementFilterWidget(choices=unit_types_choices)

    def check_agreement_period(self, queryset, value):
        if value:
            queryset = queryset.filter(agreement_period_id=value)
        return queryset

    def check_unit_type(self, queryset, value):
        if value:
            queryset = queryset.filter(unit_type_id=value)
        return queryset

    def check_with_group(self, queryset, value):
        pass

    def check_is_locked(self, queryset, value):
        pass


class GroupFilter(MatchesFilter):
    class Meta:
        model = Group
        fields = []

    def check_with_group(self, queryset, value):
        if value.lower() == 'no':
            return queryset.none()
        else:
            return queryset

    def check_is_locked(self, queryset, value):
        if value.lower() == 'yes':
            return queryset.filter(is_locked=True)
        elif value.lower() == 'no':
            return queryset.exclude(is_locked=True)
        else:
            return queryset


class InviteFilter(MatchesFilter):
    class Meta:
        model = Invitation

    def check_with_group(self, queryset, value):
        if value.lower() == 'yes':
            return queryset.none()
        else:
            return queryset

    def check_is_locked(self, queryset, value):
        if value.lower() == 'yes':
            return queryset.filter(locked=True)
        elif value.lower() == 'no':
            return queryset.exclude(locked=True)
        else:
            return queryset
