# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    def update_assignments(apps, schema_editor):
        PendingAssignment = apps.get_model('management', 'PendingAssignment')
        assignments = PendingAssignment.objects.select_related('bed', 'bed__room').all()
        for assignment in assignments:
            bed = assignment.bed
            if not bed:
                continue
            assignment.room = assignment.bed.room
            assignment.room_type = assignment.bed.room.room_type
            assignment.unit = assignment.bed.room.unit
            assignment.save()

    dependencies = [
        ('management', '0057_fix_room_in_different_property'),
    ]

    operations = [
        migrations.RunPython(update_assignments)
    ]
