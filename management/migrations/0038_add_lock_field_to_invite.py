# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('management', '0037_remove_old_email_fields'),
    ]

    operations = [
        migrations.AddField(
            model_name='invitation',
            name='locked',
            field=models.BooleanField(default=False),
        ),
    ]
