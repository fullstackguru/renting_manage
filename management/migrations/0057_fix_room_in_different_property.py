# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    def fix_rooms(apps, schema_editor):
        Unit = apps.get_model('management', 'Unit')
        UnitType = apps.get_model('management', 'UnitType')
        Property = apps.get_model('management', 'Property')
        units = Unit.objects.select_related('unit_type__property').\
            prefetch_related('rooms', 'rooms__room_type__property').all()
        for unit in units:
            prop = unit.unit_type.property
            rooms = unit.rooms.all()
            for room in rooms:
                if not room.room_type:
                    room_prop_id = room.beds.values_list('assignments__assignment_period__property', flat=True)
                    room_prop_id = filter(lambda val: val is not None, room_prop_id)
                    room_prop = Property.objects.filter(id__in=room_prop_id).first()
                else:
                    room_prop = room.room_type.property
                if not room_prop:
                    continue
                if prop != room_prop:
                    new_type = UnitType.objects.filter(property=room_prop).first()
                    new_unit, _ = Unit.objects.get_or_create(
                        name=unit.name,
                        unit_type=new_type
                    )
                    room.unit = new_unit
                    room.save()


    dependencies = [
        ('management', '0056_remove_unused_room_types'),
    ]

    operations = [
        migrations.RunPython(fix_rooms)
    ]
