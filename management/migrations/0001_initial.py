# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import datetime
from django.conf import settings
import uuid


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
    ]

    operations = [
        migrations.CreateModel(
            name='AssignmentPeriod',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=250)),
                ('start_date', models.DateTimeField()),
                ('end_date', models.DateTimeField()),
            ],
        ),
        migrations.CreateModel(
            name='Invitation',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('invite_code', models.CharField(default=uuid.uuid4, max_length=250)),
                ('invitation_type', models.CharField(max_length=250)),
                ('invitation_date', models.DateTimeField(default=datetime.datetime.now)),
                ('expire_date', models.DateTimeField(default=datetime.datetime.now)),
                ('agreement_period', models.ForeignKey(to='management.AssignmentPeriod')),
            ],
        ),
        migrations.CreateModel(
            name='Organization',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=250)),
            ],
        ),
        migrations.CreateModel(
            name='OrganizationSocialHabitAnswer',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('answer', models.CharField(max_length=250)),
                ('sort_order', models.IntegerField(default=1)),
                ('status', models.BooleanField(default=True)),
                ('created_at', models.DateTimeField(auto_now=True, db_index=True)),
                ('updated_at', models.DateTimeField(auto_now=True, db_index=True)),
            ],
        ),
        migrations.CreateModel(
            name='OrganizationSocialHabitQuestion',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('question', models.CharField(max_length=250)),
                ('sort_order', models.IntegerField(default=1)),
                ('status', models.BooleanField(default=True)),
                ('created_at', models.DateTimeField(auto_now=True, db_index=True)),
                ('updated_at', models.DateTimeField(auto_now=True, db_index=True)),
                ('organization', models.ForeignKey(to='management.Organization')),
            ],
        ),
        migrations.CreateModel(
            name='PendingAssignment',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('assignment_period', models.ForeignKey(to='management.AssignmentPeriod')),
            ],
            options={
                'ordering': ['unit', 'room_type'],
            },
        ),
        migrations.CreateModel(
            name='Property',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=250)),
                ('from_email', models.EmailField(default=b'noreply@xxx.com', max_length=255)),
                ('managers', models.ManyToManyField(related_name='managed_properties', to=settings.AUTH_USER_MODEL)),
                ('organization', models.ForeignKey(to='management.Organization')),
            ],
            options={
                'verbose_name_plural': 'Properties',
            },
        ),
        migrations.CreateModel(
            name='Room',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('square', models.IntegerField(default=0)),
            ],
        ),
        migrations.CreateModel(
            name='RoomType',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=250)),
                ('max_occupancy', models.IntegerField(default=1)),
                ('property', models.ForeignKey(to='management.Property')),
            ],
        ),
        migrations.CreateModel(
            name='Unit',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=250)),
                ('max_rooms', models.IntegerField(default=1)),
            ],
        ),
        migrations.CreateModel(
            name='UnitType',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=250)),
                ('max_occupancy', models.IntegerField(default=1)),
                ('property', models.ForeignKey(to='management.Property')),
            ],
        ),
        migrations.AddField(
            model_name='unit',
            name='unit_type',
            field=models.ForeignKey(to='management.UnitType'),
        ),
        migrations.AddField(
            model_name='room',
            name='room_type',
            field=models.ForeignKey(to='management.RoomType'),
        ),
        migrations.AddField(
            model_name='room',
            name='unit',
            field=models.ForeignKey(related_name='rooms', to='management.Unit'),
        ),
        migrations.AddField(
            model_name='pendingassignment',
            name='room',
            field=models.ForeignKey(blank=True, to='management.Room', null=True),
        ),
        migrations.AddField(
            model_name='pendingassignment',
            name='room_type',
            field=models.ForeignKey(to='management.RoomType'),
        ),
        migrations.AddField(
            model_name='pendingassignment',
            name='unit',
            field=models.ForeignKey(related_name='pending_assignments', blank=True, to='management.Unit', null=True),
        ),
        migrations.AddField(
            model_name='pendingassignment',
            name='user',
            field=models.ForeignKey(to=settings.AUTH_USER_MODEL),
        ),
        migrations.AddField(
            model_name='organizationsocialhabitanswer',
            name='question',
            field=models.ForeignKey(to='management.OrganizationSocialHabitQuestion'),
        ),
        migrations.AddField(
            model_name='invitation',
            name='room_type',
            field=models.ForeignKey(to='management.RoomType'),
        ),
        migrations.AddField(
            model_name='invitation',
            name='unit_type',
            field=models.ForeignKey(to='management.UnitType'),
        ),
        migrations.AddField(
            model_name='invitation',
            name='user',
            field=models.ForeignKey(to=settings.AUTH_USER_MODEL),
        ),
        migrations.AddField(
            model_name='assignmentperiod',
            name='property',
            field=models.ForeignKey(to='management.Property'),
        ),
    ]
