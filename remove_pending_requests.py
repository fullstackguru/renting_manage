import os
os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'renting.settings')
os.environ.setdefault('PYTHONUNBUFFERED', '1')

import django
django.setup()
from django.db.models import Q
from management.models import Invitation
from website.models import User, Relationship
from utils.services import get_related_user_ids, get_group_size


def remove_requests():
    users = User.objects.all()
    for user in users:
        try:
            invite = Invitation.objects.get(user=user)
        except Invitation.DoesNotExist:
            continue
        group = get_related_user_ids(user, with_pending_requests=True)
        max_allowed_group_size = invite.unit_type.max_occupancy - get_group_size(group)
        pending_requests = Relationship.objects.filter(Q(start_user=user) | Q(end_user=user)).\
            exclude(end_user_approved=True, approval_datetime__isnull=False)
        for request in pending_requests:
            total_group_size = 0
            pending_user = request.start_user if request.end_user == user else request.end_user
            group_size = get_group_size(get_related_user_ids(pending_user, with_pending_requests=True))
            total_group_size += group_size
            if group_size > max_allowed_group_size:
                print request
                request.delete()
            if total_group_size > max_allowed_group_size:
                print total_group_size


if __name__ == '__main__':
    remove_requests()