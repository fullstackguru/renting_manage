# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import management.fields


class Migration(migrations.Migration):

    dependencies = [
        ('management', '0069_set_unit_type_max_rooms'),
    ]

    operations = [
        migrations.AlterField(
            model_name='stage',
            name='restrictions',
            field=management.fields.ChoiceArrayField(size=None, base_field=models.SmallIntegerField(blank=True, null=True, choices=[(0, b'Allow matching only inside unit'), (1, b'Allow matching only with users who have a unit assignment'), (2, b'Group leader can select unit'), (3, b'Users with unit can select bed')]), blank=True),
        ),
    ]
