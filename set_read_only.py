import os
os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'renting.settings')
os.environ.setdefault('PYTHONUNBUFFERED', '1')

import django
django.setup()

from website.models import User, Relationship
from management.models import PendingAssignment
from django.db.models import Q


def set_read_only():
    users = User.objects.all()
    for user in users:
        assignment = PendingAssignment.objects.filter(user=user).first()
        if not assignment:
            continue
        unit = assignment.unit
        if not unit:
            continue
        suitemates_assignments = PendingAssignment.objects.filter(unit=unit)
        for assign in suitemates_assignments:
            print Relationship.objects.filter(Q(start_user=assign.user, end_user=user) |
                                              Q(start_user=user, end_user=assign.user)).\
                update(read_only=True, via_user=None)

set_read_only()
