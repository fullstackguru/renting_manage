# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('management', '0056_add_stage_to_property'),
    ]

    operations = [
        migrations.AlterField(
            model_name='property',
            name='current_stage',
            field=models.ForeignKey(related_name='properties', default=1, to='management.Stage'),
        ),
    ]
