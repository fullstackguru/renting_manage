# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('management', '0056_remove_incorrect_contract_types'),
        ('management', '0058_update_assignments'),
    ]

    operations = [
    ]
