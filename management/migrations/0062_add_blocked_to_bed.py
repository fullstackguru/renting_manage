# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import management.fields


class Migration(migrations.Migration):

    dependencies = [
        ('management', '0061_merge'),
    ]

    operations = [
        migrations.AddField(
            model_name='bed',
            name='blocked',
            field=models.ManyToManyField(related_name='blocked_beds', verbose_name=b'Periods where this bed blocked', to='management.AssignmentPeriod'),
        ),
        migrations.AlterField(
            model_name='property',
            name='emails',
            field=models.ManyToManyField(to='dbmail.MailTemplate', blank=True),
        ),
        migrations.AlterField(
            model_name='stage',
            name='restrictions',
            field=management.fields.ChoiceArrayField(size=None, base_field=models.SmallIntegerField(blank=True, null=True, choices=[(0, b'Allow matching only inside unit'), (1, b'Allow matching only with users who have a unit assignment')]), blank=True),
        ),
    ]
