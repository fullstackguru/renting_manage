from datetime import datetime
from django import forms
from django.forms import extras
from django.forms import ValidationError
from django.db.models import Q
from django.conf import settings
from django.forms.models import BaseInlineFormSet
from nested_formset import BaseNestedFormset
from dbmail.models import MailTemplate, MailFromEmail, MailCategory
from ckeditor.fields import RichTextFormField
from fields import ChainedModelChoiceField
from widgets import ChainedSelect
from management.models import Invitation, UnitType, RoomType, AssignmentPeriod, Bed, OrganizationSocialHabitQuestion, \
    OrganizationSocialHabitAnswer, ResidentImport, PendingAssignment, Room, Unit, Property, RealtyImport, ContractType, \
    PropertySocialNetwork
from website.models import User, Relationship, Group
from utils.services import create_relations_for_suitemates, get_related_user_ids, get_group_size


def class_value_for_unit_type(obj):
    try:
        return ' '.join((str(ap.id) for ap in obj.property.assignment_period.all()))
    except AttributeError:
        return ''


def class_value_for_room_type(obj):
    try:
        return ' '.join((str(ap.id) for ap in obj.property.assignment_period.all()))
    except AttributeError:
        return ''


def class_value_for_user(obj):
    try:
        assignment_periods = []
        for prop in obj.available_properties.all():
            assignment_periods.extend([str(ap.id) for ap in prop.assignment_period.all()])
        return ' '.join(assignment_periods)
    except AttributeError:
        return ''


def class_value_for_bed(obj):
    try:
        return obj.room.unit_id
    except AttributeError:
        return ''


def class_value_for_unit(obj):
    try:
        return obj.unit_type_id
    except AttributeError:
        return ''


class InvitationAdminForm(forms.ModelForm):
    unit_type = ChainedModelChoiceField(
        queryset=UnitType.objects.select_related('property').prefetch_related('property__assignment_period'),
        get_option_class_value=class_value_for_unit_type
    )
    room_type = ChainedModelChoiceField(
        queryset=RoomType.objects.select_related('property').prefetch_related('property__assignment_period'),
        get_option_class_value=class_value_for_room_type,
        required=False,
    )
    user = ChainedModelChoiceField(
        queryset=User.objects.prefetch_related('available_properties').
            prefetch_related('available_properties__assignment_period').
            order_by('email'),
        get_option_class_value=class_value_for_user,
    )

    class Meta:
        model = Invitation
        fields = ('agreement_period', 'user', 'invite_code', 'invitation_date', 'expire_date', 'unit_type',
                  'room_type', 'contract_type')

    class Media:
        js = [
            'js/plugins/jquery-chained/jquery.chained.django.min.js',
            'js/custom-scripts/chain-select-admin-invitations.js'
        ]


class BedAdminForm(forms.ModelForm):
    class Meta:
        model = Bed
        fields = '__all__'

    def clean_room(self):
        room = self.cleaned_data['room']
        if self.changed_data:
            room_max_occupancy = room.room_type.max_occupancy
            unit_max_occupancy = room.unit.unit_type.max_occupancy
            room_occupancy = Bed.objects.filter(room=room).count()
            unit_occupancy = Bed.objects.filter(room__in=room.unit.rooms.all()).count()
            if room_occupancy >= room_max_occupancy or unit_occupancy >= unit_max_occupancy:
                raise forms.ValidationError("This room/unit is filled!")
        return room


class PendingAssignmentAdminForm(forms.ModelForm):
    def __init__(self, *args, **kwargs):
        super(PendingAssignmentAdminForm, self).__init__(*args, **kwargs)
        self.fields['bed'].queryset = Bed.objects.all().select_related('room__unit__unit_type', 'room__room_type')
        self.fields['room'].queryset = Room.objects.all().select_related('unit', 'room_type')
        self.fields['unit'].queryset = Unit.objects.all().select_related('unit_type')
        self.fields['assignment_period'].queryset = AssignmentPeriod.objects.all().select_related('property')

    class Meta:
        model = PendingAssignment
        fields = '__all__'


class InvitationManagerForm(forms.ModelForm):
    unit_type = forms.ModelChoiceField(
        queryset=UnitType.objects.all(),
        widget=forms.Select(attrs={'class': 'form-control'}),
    )
    room_type = forms.ModelChoiceField(
        queryset=RoomType.objects.all(),
        required=False,
        widget=forms.Select(attrs={'class': 'form-control'}),
    )
    unit = ChainedModelChoiceField(
        queryset=Unit.objects.all(),
        required=False,
        get_label_from_instance=lambda unit: unit.name,
        widget=ChainedSelect(get_option_class_value=class_value_for_unit, attrs={'class': 'form-control'}),
    )
    bed = ChainedModelChoiceField(
        queryset=Bed.objects.all(),
        required=False,
        get_label_from_instance=lambda bed: bed.name,
        widget=ChainedSelect(get_option_class_value=class_value_for_bed, attrs={'class': 'form-control'}),
    )
    create = forms.BooleanField(required=False, initial=True)

    def __init__(self, property, *args, **kwargs):
        super(InvitationManagerForm, self).__init__(*args, **kwargs)
        self.property = property
        self.fields['matching_restriction'].label = "Matching Options"
        self.fields['unit_type'].queryset = UnitType.objects.select_related('property').filter(property=property)
        self.fields['contract_type'].queryset = ContractType.objects.filter(property=self.property)
        self.fields['room_type'].queryset = RoomType.objects.select_related('property').filter(property=property)
        self.fields['unit'].queryset = Unit.objects.select_related('unit_type').filter(unit_type__property=property).\
            order_by('name')
        self.fields['bed'].queryset = Bed.objects.select_related('room__room_type', 'room__unit').\
            filter(room__unit__unit_type__property=property)
        instance = kwargs.get('instance', None)
        if instance:
            assignment = PendingAssignment.objects.filter(user=instance.user,
                                                          assignment_period=property.current_agreement_period).first()
            if assignment and assignment.unit:
                self.fields['unit'].initial = assignment.unit
            if assignment and assignment.bed:
                self.fields['bed'].initial = assignment.bed
        if instance and instance.pk:
            self.fields['create'].widget = forms.HiddenInput()

    def clean_bed(self):
        bed = self.cleaned_data.get("bed")
        if 'bed' in self.changed_data:
            if bed and PendingAssignment.objects.filter(bed=bed).exists():
                raise ValidationError("Another user has this bed")
        return bed

    def clean_unit(self):
        unit = self.cleaned_data.get("unit")
        if "unit" in self.changed_data and unit and self.instance:
            assignment = PendingAssignment.objects.filter(unit=unit).first()
            if not assignment:
                return unit
            group = get_related_user_ids(assignment.user)
            group_size = get_group_size(group)
            max_allowed_group_size = unit.unit_type.max_occupancy - group_size
            occupied_places = self.instance.contract_type.occupied_places if self.instance.contract_type else 1
            if max_allowed_group_size < occupied_places:
                raise ValidationError("This unit filled!")
        return unit

    def save(self, commit=True):
        obj = super(InvitationManagerForm, self).save(commit)
        if 'bed' in self.changed_data or 'unit' in self.changed_data or 'unit_type' in self.changed_data:
            bed = self.cleaned_data.get("bed", None)
            unit = self.cleaned_data.get("unit", None)
            room = bed.room if bed else None
            room_type = obj.room_type if obj.room_type else room.room_type if room else \
                RoomType.objects.filter(property=self.property).first()
            user = obj.user
            user_assignments = PendingAssignment.objects.filter(user=user, assignment_period=
            self.property.current_agreement_period)
            #  update assignments
            if user_assignments.exists():
                user_assignments.update(unit=unit, room=room, bed=bed)
            else:
                PendingAssignment.objects.create(user=user, unit=unit, bed=bed, room_type=room_type,
                                                 assignment_period=obj.agreement_period, invite=obj)
            # update unit and relations
            # remove old
            Relationship.objects.filter(Q(start_user=user) | Q(end_user=user)).\
                filter(end_user_approved=True, approval_datetime__isnull=False).delete()
            groups = Group.objects.filter(users__in=[user])
            for group in groups:
                group.users.remove(user)
                if group.users.count() < 2:
                    group.delete()
            # create new
            if unit:
                another_assignment = PendingAssignment.objects.filter(unit=unit, assignment_period=obj.agreement_period,
                                                                  user__groups__agreement_period=obj.agreement_period).\
                    exclude(user=user).first()
                if another_assignment:
                    r = Relationship(start_user=another_assignment.user, end_user=user, end_user_approved=True,
                                 approval_datetime=datetime.now(), invitation=obj, read_only=True)
                    r.save()  # it will automatically create other relations
        return obj

    class Meta:
        model = Invitation
        exclude = ('user', 'agreement_period', 'invitation_type', 'activated_time')
        widgets = {
            'agreement_period': forms.Select(attrs={'class': 'form-control'}),
            'contract_type': forms.Select(attrs={'class': 'form-control'}),
            'invite_code': forms.TextInput(attrs={'class': 'form-control'}),
            'invitation_date': extras.SelectDateWidget(attrs={'class': 'form-control',
                                                              'style': 'width: 25%; float: left;'}),
            'expire_date': extras.SelectDateWidget(attrs={'class': 'form-control',
                                                          'style': 'width: 25%; float: left;'}),
            'matching_restriction': forms.Select(attrs={'class': 'form-control'}),
        }


class UnitTypeManagerForm(forms.ModelForm):
    class Meta:
        model = UnitType
        exclude = ('property', )
        widgets = {
            'name': forms.TextInput(attrs={'class': 'form-control'}),
            'max_occupancy': forms.NumberInput(attrs={'class': 'form-control'}),
        }


class RoomTypeManagerForm(forms.ModelForm):
    class Meta:
        model = RoomType
        exclude = ('property', )
        widgets = {
            'name': forms.TextInput(attrs={'class': 'form-control'}),
            'max_occupancy': forms.NumberInput(attrs={'class': 'form-control'}),
        }

class PropertySocialNetworkManagerForm(forms.ModelForm):
    class Meta:
        model = PropertySocialNetwork
        exclude = ('property', )
        widgets = {
            'network_name': forms.TextInput(attrs={'class': 'form-control'}),
        }



class OrganizationSocialHabitAnswerInline(forms.ModelForm):
    class Meta:
        model = OrganizationSocialHabitAnswer
        fields = '__all__'
        widgets = {
            'answer': forms.TextInput(attrs={'class': 'form-control'}),
            'sort_order': forms.TextInput(attrs={'class': 'form-control'}),
        }


class OrganizationSocialHabitQuestionManagerForm(forms.ModelForm):
    class Meta:
        model = OrganizationSocialHabitQuestion
        exclude = ('organization', 'created_at', 'updated_at')
        widgets = {
            'question': forms.TextInput(attrs={'class': 'form-control'}),
            'sort_order': forms.TextInput(attrs={'class': 'form-control'}),
        }


class ImportModelForm(forms.ModelForm):
    class Meta:
        fields = ('file', )
        widgets = {
            'file': forms.FileInput(attrs={'class': 'filestyle'}),
        }

    def clean(self):
        file = self.cleaned_data.get('file', None)
        if not file:
            raise ValidationError("You must attach file!")
        if file.content_type != 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet':
            raise ValidationError("Only xlsx files allowed!")


class ResidentImportForm(ImportModelForm):
    class Meta(ImportModelForm.Meta):
        model = ResidentImport


class RealtyImportForm(ImportModelForm):
    class Meta(ImportModelForm.Meta):
        model = RealtyImport


class MailTemplateManagerForm(forms.ModelForm):
    message = RichTextFormField()

    class Meta:
        model = MailTemplate
        fields = ('name', 'subject', 'from_email', 'category', 'message', 'context_note')
        widgets = {
            'name': forms.TextInput(attrs={'class': 'form-control'}),
            'subject': forms.TextInput(attrs={'class': 'form-control'}),
            'from_email': forms.Select(attrs={'class': 'form-control'}),
            'category': forms.Select(attrs={'class': 'form-control'}),
            'context_note': forms.Textarea(attrs={'readonly': True, 'cols': 100}),
        }

    def __init__(self, property, *args, **kwargs):
        self.property = property
        super(MailTemplateManagerForm, self).__init__(*args, **kwargs)
        self.fields['context_note'].initial = settings.DB_MAILER_DEFAULT_CONTEXT_NOTE
        self.fields['from_email'].queryset = MailFromEmail.objects.filter(property=self.property)
        self.fields['category'].queryset = MailCategory.objects.all()

    def clean_context_note(self):
        instance = getattr(self, 'instance', None)
        if instance and instance.pk:
            return instance.context_note
        else:
            return self.cleaned_data['context_note']

    def save(self, commit=True):
        if not self.instance.slug:
            self.instance.slug = '{}_{}'.format(self.property.name, self.instance.name).lower().replace(' ', '_')
        obj = super(MailTemplateManagerForm, self).save(commit)
        self.property.emails.add(obj)
        return obj


class StageForm(forms.ModelForm):
    class Meta:
        model = Property
        fields = ('current_stage', )
        widgets = {
            'current_stage': forms.Select(attrs={'class': 'form-control'}),
        }


class BedForm(forms.ModelForm):
    class Meta:
        model = Bed
        fields = ('name', )
        widgets = {
            'name': forms.TextInput(attrs={'class': 'form-control'}),
        }


class RoomForm(forms.ModelForm):
    property = None

    class Meta:
        model = Room
        fields = ('name', 'room_type')
        widgets = {
            'room_type': forms.Select(attrs={'class': 'form-control'}),
            'name': forms.TextInput(attrs={'class': 'form-control'})
        }

    def __init__(self, *args, **kwargs):
        super(RoomForm, self).__init__(*args, **kwargs)
        if self.property:
            self.fields['room_type'].queryset = RoomType.objects.filter(property=self.property)
        instance = getattr(self, 'instance', None)
        if instance and instance.pk:
            self.fields['room_type'].widget.attrs['disabled'] = 'disabled'
            self.fields['room_type'].required = False


class UnitForm(forms.ModelForm):
    class Meta:
        model = Unit
        fields = ('name', 'unit_type')
        widgets = {
            'name': forms.TextInput(attrs={'class': 'form-control'}),
            'unit_type': forms.Select(attrs={'class': 'form-control'})
        }

    def __init__(self, property, *args, **kwargs):
        super(UnitForm, self).__init__(*args, **kwargs)
        self.property = property
        self.fields['unit_type'].queryset = UnitType.objects.filter(property=property)
        instance = getattr(self, 'instance', None)
        if instance and instance.pk:
            self.fields['unit_type'].widget.attrs['disabled'] = 'disabled'
            self.fields['unit_type'].required = False

    def clean_name(self):
        name = self.cleaned_data['name']
        if not self.instance and Unit.objects.filter(name=name, unit_type__property=self.property).exists():
            raise ValidationError("Unit with this name already exists")
        return name


class BedFormSet(BaseInlineFormSet):
    def clean(self):
        if any(self.errors):
            return
        bed_names = [form.cleaned_data['name'] for form in self.forms]
        if len(set(bed_names)) != len(self.forms):
            raise ValidationError('Bed names must be unique')


class RoomFormSet(BaseNestedFormset):
    def clean(self):
        if any(self.errors):
            return
        room_names = [form.cleaned_data['name'] for form in self.forms]
        if len(set(room_names)) != len(self.forms):
            raise ValidationError('Room names must be unique')


class PropertySettingsForm(forms.ModelForm):
    def __init__(self, *args, **kwargs):
        super(PropertySettingsForm, self).__init__(*args, **kwargs)
        self.fields['file'].label = "Upload a global resource (typically a pdf that tenants can view for clarity, house rules, site plans)"
        self.fields['enable_social_network'].label = "Allow tenant to enhance their profile by adding external social network links"
        self.fields['enable_star_setting'].label = "Show the roommate matching star (only used without placement)"
        self.fields['enable_email_sending'].label = "Allow the site to send emails"
        self.fields['disable_email_resident'].label = "Allow tenant to opt out of emails"

    class Meta:
        model = Property
        fields = ['enable_email_sending', 'disable_email_resident', 'enable_social_network',
                  'enable_star_setting', 'file']

