# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('management', '0027_make_unit_name_unique'),
    ]

    operations = [
        migrations.AlterField(
            model_name='residentimport',
            name='file',
            field=models.FileField(upload_to=b'uploads/resident_import/'),
        ),
    ]
