# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
from management.models import Invitation as inv


class Migration(migrations.Migration):

    def add_matching_restriction(apps, schema_editor):
        PendingAssignment = apps.get_model('management', 'PendingAssignment')
        Property = apps.get_model('management', 'Property')
        Invitation = apps.get_model('management', 'Invitation')
        lorenzo_property = Property.objects.filter(name="The Lorenzo").first()
        if not lorenzo_property:
            return
        assigned_users = PendingAssignment.objects.filter(assignment_period__property=lorenzo_property,
                                                          unit__isnull=False).values_list('user', flat=True)
        unassigned_users_invites = Invitation.objects.filter(agreement_period__property=lorenzo_property).\
            exclude(user_id__in=assigned_users)
        print unassigned_users_invites.update(matching_restriction=inv.MATCHING_RESTRICTION_WITH_ASSIGNMENTS)

    dependencies = [
        ('management', '0047_add_invite_restriction'),
    ]

    operations = [
        migrations.RunPython(add_matching_restriction)
    ]
