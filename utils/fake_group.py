class FakeGroup(object):
    def __init__(self, property, agreement_period, unit_type, group_count, user, unit, locked):
        self.property = property
        self.agreement_period = agreement_period
        self.unit_type = unit_type
        self.size = group_count
        self.suitemates = [user]
        self.roommates = []
        self.unit = unit
        self.is_locked = locked
