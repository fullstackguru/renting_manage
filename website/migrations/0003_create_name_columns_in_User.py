# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('website', '0002_auto_20160428_0345'),
    ]

    operations = [
        migrations.AddField(
            model_name='user',
            name='first_name',
            field=models.CharField(max_length=30, verbose_name=b'first name', blank=True),
        ),
        migrations.AddField(
            model_name='user',
            name='gender',
            field=models.CharField(default=b'U', max_length=1, verbose_name=b'gender', choices=[(b'U', b'Unknown'), (b'M', b'Male'), (b'F', b'Female')]),
        ),
        migrations.AddField(
            model_name='user',
            name='last_name',
            field=models.CharField(max_length=30, verbose_name=b'last name', blank=True),
        ),
        migrations.AlterField(
            model_name='userprofile',
            name='bg_url',
            field=models.CharField(max_length=250, null=True, blank=True),
        ),
        migrations.AlterField(
            model_name='userprofile',
            name='bio',
            field=models.TextField(null=True, blank=True),
        ),
        migrations.AlterField(
            model_name='userprofile',
            name='image_url',
            field=models.CharField(max_length=250, null=True, blank=True),
        ),
    ]
