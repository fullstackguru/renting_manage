# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('website', '0015_change_incorrect_emails'),
    ]

    operations = [
        migrations.AlterField(
            model_name='userprofile',
            name='facebook_url',
            field=models.URLField(max_length=250, null=True, blank=True),
        ),
        migrations.AlterField(
            model_name='userprofile',
            name='google_plus_url',
            field=models.URLField(max_length=250, null=True, blank=True),
        ),
        migrations.AlterField(
            model_name='userprofile',
            name='instagram_url',
            field=models.URLField(max_length=250, null=True, blank=True),
        ),
        migrations.AlterField(
            model_name='userprofile',
            name='linkedin_url',
            field=models.URLField(max_length=250, null=True, blank=True),
        ),
        migrations.AlterField(
            model_name='userprofile',
            name='pinterest_url',
            field=models.URLField(max_length=250, null=True, blank=True),
        ),
        migrations.AlterField(
            model_name='userprofile',
            name='snapchat_url',
            field=models.URLField(max_length=250, null=True, blank=True),
        ),
        migrations.AlterField(
            model_name='userprofile',
            name='twitter_url',
            field=models.URLField(max_length=250, null=True, blank=True),
        ),
    ]
