from datetime import datetime
from django.core.management.base import BaseCommand, CommandError
from django.db.models import Count, Q
from website.models import User, Relationship, Group
from utils.services import get_related_user_ids
from management.models import Invitation, PendingAssignment


class Command(BaseCommand):
    help = "Adds missing assignments for students that has invites."

    def handle(self, *args, **options):
        groups = Group.objects.prefetch_related('users')
        for group in groups:
            print Relationship.objects.filter(start_user__in=group.users.all(), end_user__in=group.users.all(),
                                              end_user_approved=True, approval_datetime__isnull=False,
                                              invitation__agreement_period=group.agreement_period).update(group=group)
