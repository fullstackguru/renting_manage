from django.core.management.base import BaseCommand

from website.models import UserProfile


class Command(BaseCommand):
    help = 'Initialize random background images for users.'

    def add_arguments(self, parser):
        parser.add_argument('--update_all', default=False, action='store_true',
                            help='Update all users. By default only users with empty bg_url field are updated.')

    def handle(self, *args, **options):
        if options['update_all']:
            profiles = UserProfile.objects.all()
        else:
            profiles = UserProfile.objects.filter(
                bg_url__isnull=True
            )

        for profile in profiles:
            # just .save is enough because bg_url initialized when user's profile is saved
            profile.save()
