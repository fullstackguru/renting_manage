import csv
from django.http import HttpResponse, HttpResponseBadRequest
from django.utils import timezone
from actstream import action
from management.models import Invitation, Property
from website.models import User, UserOrganizationSocialHabitAnswers
from utils.emails import send_welcome_email, send_account_reminder_email, send_email
from utils.compatibility import RoommateMatcher


def resend_invites(user, user_ids, property):
    invites = Invitation.objects.filter(user_id__in=user_ids).filter(agreement_period__property=property).\
        exclude(expire_date__lt=timezone.now()).select_related('user')
    for invite in invites:
        send_welcome_email(invite)
        action.send(user, verb='sent welcome email to', action_object=invite.user, target=property)

def delete_users(user, user_ids, property):
    User.objects.filter(id__in=user_ids).delete()
    return HttpResponse('Users Deleted!')

def account_reminder_emails(user, user_ids, property):
    invites = Invitation.objects.filter(user_id__in=user_ids).filter(agreement_period__property=property).\
        exclude(expire_date__lt=timezone.now()).select_related('user')
    for invite in invites:
        send_account_reminder_email(invite)
        action.send(user, verb='sent reminder email to', action_object=invite.user, target=property)


def send_custom_email(user, user_ids, property, template_id):
    emails = property.emails.all()
    selected_template = filter(lambda e: e.id == template_id, emails)
    if not selected_template:
        raise TypeError("Wrong email template id!")
    selected_template = selected_template[0]
    invites = Invitation.objects.filter(user_id__in=user_ids).filter(agreement_period__property=property).\
        exclude(expire_date__lt=timezone.now()).select_related('user')
    for invite in invites:
        send_email(selected_template, invite)
        action.send(user, verb='sent {} to'.format(selected_template.name), action_object=invite.user, target=property)


def generate_profile_report(user, user_ids, property):
    response = HttpResponse(content_type='text/csv')
    response['Content-Disposition'] = 'attachment; filename="user_report.csv"'
    writer = csv.writer(response)
    all_users_ids = Invitation.objects.filter(agreement_period__property=property).\
        values_list('user', flat=True)
    all_users = User.objects.filter(id__in=all_users_ids).select_related('profile')
    users = User.objects.filter(id__in=user_ids).select_related('profile')
    matcher = RoommateMatcher(users, all_users)
    users = matcher.get_suggested_users()
    writer.writerow(['Name', 'Social habits answers', 'Quiz Result', 'Suggested roommates'])
    for user in users:
        answers = UserOrganizationSocialHabitAnswers.objects.filter(userprofile=user.profile).\
            select_related('organization_question', 'organization_answer').order_by('organization_question')
        if answers:
            answers = '\n'.join(
                [u"{}) {} - {}".format(index, answer.organization_question.question, answer.organization_answer.answer)
                 for index, answer in enumerate(answers, start=1)]
            )
        else:
            answers = ""
        personality_types = user.profile.get_personality_types_json()
        if personality_types:
            quiz_result = [u"Type: {}".format(personality_types['personality_blend'].name), '']
            attributes = personality_types['personality_types']
            for attribute in attributes:
                quiz_result.append(u"{} - {:.2f}%".format(attribute.personality_type.name, attribute.score))
            quiz_result = '\n'.join(quiz_result)
            suggested_roommates = '\n'.join([u"{}) {}".format(index, suggested_user.full_name)
                                            for index, suggested_user in enumerate(user.suggested_roommates, start=1)])
        else:
            quiz_result = "No Quiz"
            suggested_roommates = ''
        row = [user.full_name, answers, quiz_result, suggested_roommates]
        writer.writerow([unicode(s).encode("utf-8") for s in row])
    return response


def export_users(user, user_ids, property):
    if not property:
        return HttpResponseBadRequest('Incorrect property id!')
    response = HttpResponse(content_type='text/csv')
    response['Content-Disposition'] = 'attachment; filename="{}_{}.csv"'.format(property.name,
                                                                                timezone.now().strftime('%d_%m_%Y'))
    writer = csv.writer(response)
    invites = Invitation.objects.filter(agreement_period__property=property)
    users_with_invites = invites.values_list('user', flat=True)
    managers = property.managers.all().values_list('id', flat=True)
    invites = invites.filter(user__in=user_ids).values('agreement_period__name', 'user__first_name', 'user__last_name',
                                                       'user__email', 'user__gender', 'user__points', 'unit_type__name',
                                                       'room_type__name', 'contract_type__name', 'assignment',
                                                       'assignment__unit__name', 'assignment__room__name',
                                                       'assignment__bed__name','locked').\
        order_by('assignment__unit__name').iterator()
    writer.writerow(['Agreement Period', 'Unit', 'Room', 'Bed', 'First Name', 'Last Name', 'Email Address', 'Points',
                     'Unit Type', 'Room Type', 'Gender Restriction', 'Contract Type', 'Locked'])
    for invite in invites:
        row = []
        row.append(invite['agreement_period__name'])
        row.append(invite['assignment__unit__name'] if invite['assignment__unit__name'] else 'WAITLIST')
        row.append(invite['assignment__room__name'] if invite['assignment__room__name'] else 'WAITLIST')
        row.append(invite['assignment__bed__name'] if invite['assignment__bed__name'] else 'WAITLIST')
        row.append(invite['user__first_name'])
        row.append(invite['user__last_name'])
        row.append(invite['user__email'])
        row.append(invite['user__points'])
        row.append(invite['unit_type__name'])
        row.append(invite['room_type__name'] if invite['room_type__name'] else '')
        row.append('Female' if invite['user__gender'] == User.GENDER_FEMALE else 'Male'
                   if invite['user__gender'] == User.GENDER_MALE else 'Unknown')
        row.append(invite['contract_type__name'] if invite['contract_type__name'] is not None else '')
        row.append('t' if invite['locked'] else 'f')
        writer.writerow([unicode(s).encode("utf-8") for s in row])
    users_without_invites = User.objects.filter(available_properties__in=[property]).\
        exclude(id__in=users_with_invites).exclude(id__in=managers).values('first_name', 'last_name', 'email', 'gender')
    for user in users_without_invites:
        row = []
        row.append('')
        row.append('')
        row.append('')
        row.append(user['first_name'])
        row.append(user['last_name'])
        row.append(user['email'])
        row.append('')
        row.append('')
        row.append('Female' if user['gender'] == User.GENDER_FEMALE else 'Male' if user['gender'] == User.GENDER_MALE
                   else 'Unknown')
        row.append('')
        writer.writerow([unicode(s).encode("utf-8") for s in row])
    return response
