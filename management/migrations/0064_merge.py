# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('management', '0063_update_stage_restrictions'),
        ('management', '0060_auto_20161028_0202'),
    ]

    operations = [
    ]
