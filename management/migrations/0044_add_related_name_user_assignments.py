# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
from django.conf import settings


class Migration(migrations.Migration):

    dependencies = [
        ('management', '0043_add_default_dates_for_questions_and_answers'),
    ]

    operations = [
        migrations.AlterField(
            model_name='pendingassignment',
            name='user',
            field=models.ForeignKey(related_name='assignments', to=settings.AUTH_USER_MODEL),
        ),
    ]
