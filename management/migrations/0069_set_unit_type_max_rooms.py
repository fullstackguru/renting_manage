# -*- coding: utf-8 -*-
from __future__ import unicode_literals, division

from django.db import models, migrations


class Migration(migrations.Migration):

    def set_max_rooms(apps, schema_editor):
        UnitType = apps.get_model('management', 'UnitType')
        Unit = apps.get_model('management', 'Unit')
        types = UnitType.objects.all()
        for type in types:
            units = Unit.objects.filter(unit_type=type)
            total_units = units.count()
            if not total_units:
                continue
            rooms = units.aggregate(models.Count('rooms'))['rooms__count'] or 0
            type.max_rooms = int(round(rooms /total_units)) or 1
            type.save()

    dependencies = [
        ('management', '0068_unittype_max_rooms'),
    ]

    operations = [
        migrations.RunPython(set_max_rooms)
    ]
