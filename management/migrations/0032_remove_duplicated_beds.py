# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    def remove_duplicated_beds(apps, schema_editor):
        Bed = apps.get_model("management", "Bed")
        PendingAssignment = apps.get_model("management", "PendingAssignment")
        beds = Bed.objects.all()
        beds_for_deleting = set()
        for bed in beds:
            if bed.id in beds_for_deleting:
                continue
            similar_beds = Bed.objects.filter(name=bed.name, room=bed.room)
            if similar_beds.count() == 1:
                continue
            similar_beds_ids = set(similar_beds.values_list('id', flat=True))
            similar_beds_with_assignments = set(PendingAssignment.objects.filter(bed_id__in=similar_beds_ids).
                                                values_list('id', flat=True))
            beds_for_deleting.update(similar_beds_ids - similar_beds_with_assignments)
        Bed.objects.filter(id__in=beds_for_deleting).delete()

    dependencies = [
        ('management', '0031_add_unique_together_to_pending_assignment'),
    ]

    operations = [
        migrations.RunPython(remove_duplicated_beds)
    ]
