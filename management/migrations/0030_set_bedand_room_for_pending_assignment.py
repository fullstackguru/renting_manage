# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    def add_beds_to_pending_assignment(apps, schema_editor):
        Bed = apps.get_model("management", "Bed")
        PendingAssignment = apps.get_model("management", "PendingAssignment")
        beds = Bed.objects.all().select_related('user', 'room', 'room__unit')
        for bed in beds:
            user = bed.user
            room = bed.room
            unit = bed.room.unit
            PendingAssignment.objects.filter(user=user, unit=unit).update(room=room, bed=bed)

    dependencies = [
        ('management', '0029_add_bed_to_pending_assignment'),
    ]

    operations = [
        migrations.RunPython(add_beds_to_pending_assignment)
    ]
