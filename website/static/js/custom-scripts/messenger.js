'use strict';

$(document).ready(function () {
    var $messenger = $('#messenger');
    var userId = $('#userId').val();
    var peerId, chatURL;

    var renderMessages = function (messages) {
        var $chatTalk = $messenger.find('.js-chat-talk');
        var logTemplate = $messenger.find('.template.messenger-log').html();
        var logHTML = _.template(logTemplate, {variable: 'data'})({
            userId: userId,
            messages: messages
        });
        $chatTalk.html(logHTML);
    };

    $messenger.on('show.bs.modal', function (e) {
        var $chatModal = $(this);
        var $targetLink = $(e.relatedTarget);

        peerId = $targetLink.data('peer-id');
        chatURL = $targetLink.data('href');

        var $relatedPeerBlock = $targetLink.closest('.peer-block');
        var peerName = $relatedPeerBlock.find('.block-header .block-title').html();

        $chatModal.find('.block-header .block-title').html(peerName);
        $chatModal.find('.js-chat-form form').attr('action', chatURL);
        $chatModal.find('input[name=user2_id]').val(peerId);
        $.get(chatURL)
            .done(function (resp) {
                renderMessages(resp.messages);
            });
    });

    $messenger.on('shown.bs.modal', function () {
        var $chatModal = $(this);
        var $chatTalk = $chatModal.find('.js-chat-talk');

        // scroll to end of log
        $chatTalk.prop('scrollTop', $chatTalk.prop('scrollHeight'));
    });

    $("#message-form").on("submit", function (e) {
        e.preventDefault();
        var $form = $(this);
        var messageInput = $form.find('.js-chat-input');
        var isEmpty = !messageInput.val().length;
        $form.toggleClass('has-error', isEmpty);
        if (isEmpty) {
            return false;
        }
        $.ajax({
            type: "POST",
            url: chatURL,
            data: $form.serialize(),
            success: function (data) {
                renderMessages(data.messages);
                var $chatTalk = $messenger.find('.js-chat-talk');
                $chatTalk.prop('scrollTop', $chatTalk.prop('scrollHeight'));
                $("#message-input").val('');
                $.notify({
                    message: 'Message has been successfully sent!',
                    icon: 'fa fa-check'
                }, {
                    type: 'success',
                    z_index: 1051,
                    delay: 2000
                });
            },
            error: function () {
                $.notify({
                    message: 'Opps, an error occured!',
                    icon: 'fa fa-times'
                }, {
                    type: 'danger',
                    z_index: 1051,
                    delay: 2000
                });

            }
        });
    });
});
