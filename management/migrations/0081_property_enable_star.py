# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('management', '0080_property_enable_social_network'),
    ]

    operations = [
        migrations.AddField(
            model_name='property',
            name='enable_star_setting',
            field=models.BooleanField(default=False),
        ),
    ]
