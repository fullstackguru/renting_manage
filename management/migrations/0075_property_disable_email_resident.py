# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('management', '0074_property_contract_type'),
    ]

    operations = [
        migrations.AddField(
            model_name='property',
            name='disable_email_resident',
            field=models.BooleanField(default=False),
        ),
    ]
