from django import template
from collections import defaultdict


register = template.Library()


@register.simple_tag
def print_group_users(group):
    group_users = set()
    for relation in group:
        group_users.add(relation.start_user)
        group_users.add(relation.end_user)
    return ''.join(['<li>{}</li>'.format(u.email) for u in group_users])
