# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('management', '0019_make_file_field_optional'),
    ]

    operations = [
        migrations.CreateModel(
            name='ContractType',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=64)),
                ('occupied_places', models.IntegerField(default=1)),
            ],
        ),
        migrations.AddField(
            model_name='invitation',
            name='contract_type',
            field=models.ForeignKey(blank=True, to='management.ContractType', null=True),
        ),
    ]
