from datetime import timedelta
from django.core.management.base import BaseCommand, CommandError
from django.utils.translation import activate
from django.utils import timezone
from management.models import Invitation
from utils.emails import send_account_reminder_email


class Command(BaseCommand):
    help = 'Send reminder emails to users.'

    def handle(self, *args, **options):
        activate('en')
        now = timezone.now()
        new_activated_invites = Invitation.objects.filter(activated_time__lt=now - timedelta(hours=22),
                                                          activated_time__gte=now - timedelta(days=1))
        print new_activated_invites.count()
        for invite in new_activated_invites:
            print "Send reminder email for {}".format(invite.user)
            try:
                send_account_reminder_email(invite)
            except Exception:
                pass
