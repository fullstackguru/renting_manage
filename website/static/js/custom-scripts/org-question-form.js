var initForm = function () {
    var prefix = "answers";

    function updateElementIndex(el, prefix, ndx) {
        var id_regex = new RegExp('(' + prefix + '-\\d+-)');
        var replacement = prefix + '-' + ndx + '-';
        if ($(el).attr("for")) $(el).attr("for", $(el).attr("for").replace(id_regex,
            replacement));
        if (el.id) el.id = el.id.replace(id_regex, replacement);
        if (el.name) el.name = el.name.replace(id_regex, replacement);
    }

    function deleteForm(btn) {
        var formCount = parseInt($('#id_' + prefix + '-TOTAL_FORMS').val());
        if (formCount > 0) {
            $(btn).parents('tr').remove();
            var forms = $('#form_set tr'); // Get all the forms
            $('#id_' + prefix + '-TOTAL_FORMS').val(forms.length);
            var i = 0;
            for (formCount = forms.length; i < formCount; i++) {
                $(forms.get(i)).children().children().each(function () {
                    updateElementIndex(this, prefix, i);
                });
            }
        }
    }

    function bindDeleteEvent() {
        $(".delete").click(function () {
            return deleteForm(this);
        });
    }

    $('#add_more').click(function () {
        var formCount = parseInt($('#id_' + prefix + '-TOTAL_FORMS').val());
        $('#form_set').append($('#empty_form table tbody').html().replace(/__prefix__/g, formCount));
        $("#id_" + prefix + "-TOTAL_FORMS").val(formCount + 1);
        bindDeleteEvent();
    });

    $('#delete-btn').on('click', function () {
        var url = $(this).attr('url');
        swal({
                title: "Are you sure?",
                text: "Are you sure that you want to delete this question",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: "Yes, delete it!",
                cancelButtonText: "No, cancel!",
                closeOnConfirm: false
            },
            function (isConfirm) {
                if (isConfirm) {
                    $.ajax({
                        url: url,
                        type: 'DELETE',
                        success: function (data) {
                            window.location.href = data;
                        },
                        error: function (jqXHR, textStatus, errorThrown) {
                            swal("Oops...", "Unable to delete this question!", "error");
                        }
                    });
                }
            });
    });

    bindDeleteEvent();
};

initForm();
