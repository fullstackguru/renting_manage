from mock import patch
from django.test import Client, TestCase
from django.core.urlresolvers import reverse
from website.models import User
from management.models import Invitation
from utils.tests_utils import create_property_related_objects, mock_send_reminder_email, mock_send_welcome_email


class WebsiteViewsTest(TestCase):
    @patch('management.signals.handlers.send_welcome_email', mock_send_welcome_email)
    def setUp(self):
        self.client = Client()
        self.user = User.objects.create(email='some_email@mail.com', first_name='Somename', last_name='Somename')
        self.organization, self.property, self.unit_type, self.assignment_period, self.room_type = \
            create_property_related_objects()
        self.invite = Invitation.objects.create(user=self.user, agreement_period=self.assignment_period,
                                                unit_type=self.unit_type)

    @patch('website.tasks.send_delayed_reminder_email', mock_send_reminder_email)
    def test_login_without_redirecting(self):
        response = self.client.get(reverse('code_index'))
        self.assertEqual(response.status_code, 200)
        response = self.client.post(reverse('code_index'), {'code': self.invite.invite_code})
        self.assertRedirects(response, reverse('getting-started', kwargs={'invite_code': self.invite.invite_code}))

    @patch('website.tasks.send_delayed_reminder_email', mock_send_reminder_email)
    def test_login_with_redirecting(self):
        redirect_url = '/profile_v2/?user-profile=1'
        response = self.client.get("%s?next=%s" % (reverse('code_index'), redirect_url))
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.context['next'], redirect_url)
        response = self.client.post(reverse('code_index'), {'code': self.invite.invite_code, 'next': redirect_url})
        self.assertRedirects(response, redirect_url)

    @patch('website.tasks.send_delayed_reminder_email', mock_send_reminder_email)
    def test_wrong_login_with_redirecting(self):
        redirect_url = '/profile_v2/?user-profile=1'
        incorrect_code = 'incorrect_code'
        response = self.client.get("%s?next=%s" % (reverse('code_index'), redirect_url))
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.context['next'], redirect_url)
        response = self.client.post(reverse('code_index'), {'code': incorrect_code, 'next': redirect_url})
        self.assertRedirects(response, "%s?code=%s&next=%s" % (reverse('code_index'), incorrect_code, redirect_url))
