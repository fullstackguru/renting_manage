from mock import patch
from django.test import TestCase
from django.utils import timezone
from django.db.models import Q
from django.conf import settings
from website.models import User, Relationship
from management.models import Invitation, Unit, Bed, Room, PendingAssignment
from utils.tests_utils import create_property_related_objects, create_users, mock_send_welcome_email, \
    mock_send_relations_email
from management.forms import InvitationManagerForm, MailTemplateManagerForm


class ManagerInvitationFormTestCase(TestCase):

    @classmethod
    @patch('management.signals.handlers.send_welcome_email', mock_send_welcome_email)
    def setUpClass(cls):
        super(ManagerInvitationFormTestCase, cls).setUpClass()
        cls.users_number = 10
        create_users(cls.users_number)
        cls.organization, cls.property, cls.unit_type, cls.assignment_period, cls.room_type = \
            create_property_related_objects()
        users = User.objects.all()
        for user in users:
            Invitation.objects.create(
                user=user,
                agreement_period=cls.assignment_period,
                unit_type=cls.unit_type,
            )

    @patch('management.signals.handlers.send_welcome_email', mock_send_welcome_email)
    @patch('website.signals.handlers.send_relationship_email', mock_send_relations_email)
    def test_removing_relations_for_user_from_different_properties(self):
        users = User.objects.all()
        users[0].available_properties.add(self.property)
        invite = Invitation.objects.filter(user=users[0]).first()
        unit = Unit.objects.create(name='unit', unit_type=self.unit_type)
        form_data = {
            'user': invite.user_id,
            'invite_code': invite.invite_code,
            'invitation_date': invite.invitation_date,
            'expire_date': invite.expire_date,
            'agreement_period': invite.agreement_period_id,
            'unit_type': invite.unit_type_id,
            'matching_restriction': Invitation.MATCHING_RESTRICTION_ALL,
            'unit': unit.id
        }
        #  test placement to occupied bed
        form = InvitationManagerForm(instance=invite, property=self.property, data=form_data)
        self.assertTrue(form.is_valid())
        room = Room.objects.create(unit=unit, room_type=self.room_type)
        bed = Bed.objects.create(name='A1', user=users[2], room=room)
        assignment = PendingAssignment.objects.create(user=users[2], unit=unit, room=room, bed=bed, room_type=self.room_type,
                                                      assignment_period=self.assignment_period)
        form_data['bed'] = bed.id
        form = InvitationManagerForm(instance=invite, property=self.property, data=form_data)
        self.assertFalse(form.is_valid())
        del form_data['bed']

        # test save
        first_group_invite = Invitation.objects.get(user=users[0])
        Relationship.objects.create(start_user=users[0], end_user=users[1], invitation=first_group_invite,
                                    end_user_approved=True, approval_datetime=timezone.now())
        second_group_invite = Invitation.objects.get(user=users[2])
        Relationship.objects.create(start_user=users[2], end_user=users[3], invitation=second_group_invite,
                                    end_user_approved=True, approval_datetime=timezone.now())
        Relationship(start_user=users[0], end_user=users[2], invitation=first_group_invite, end_user_approved=True,
                     approval_datetime=timezone.now()).save()
        new_unit = Unit.objects.create(name='new unit', unit_type=self.unit_type)
        form_data['unit'] = new_unit.id
        form = InvitationManagerForm(instance=invite, property=self.property, data=form_data)
        self.assertTrue(form.is_valid())
        invite = form.save()
        self.assertEqual(PendingAssignment.objects.count(), 2)
        first_group_size = Relationship.objects.filter(Q(start_user=users[0]) | Q(end_user=users[0])).count()
        second_group_size = Relationship.objects.filter(Q(start_user__in=[users[2], users[3]]) |
                                                        Q(end_user__in=[users[2], users[3]])).count()
        self.assertEqual(Relationship.objects.count(), 3)
        self.assertEqual(first_group_size, 0)
        self.assertEqual(second_group_size, 3)


class ManagerMailTemplateFormTestCase(TestCase):

    @classmethod
    @patch('management.signals.handlers.send_welcome_email', mock_send_welcome_email)
    def setUpClass(cls):
        super(ManagerMailTemplateFormTestCase, cls).setUpClass()
        cls.users_number = 10
        create_users(cls.users_number)
        cls.organization, cls.property, cls.unit_type, cls.assignment_period, cls.room_type = \
            create_property_related_objects()
        users = User.objects.all()
        for user in users:
            Invitation.objects.create(
                user=user,
                agreement_period=cls.assignment_period,
                unit_type=cls.unit_type,
            )

    def test_context_note(self):
        form_data = {
            'name': 'test',
            'subject': 'test',
            'message': 'test'
        }
        form = MailTemplateManagerForm(property=self.property, data=form_data)
        self.assertTrue(form.is_valid())
        obj = form.save()
        self.assertEqual(obj.context_note, settings.DB_MAILER_DEFAULT_CONTEXT_NOTE)
        self.assertEqual(obj.slug, 'some_property_test')
        property_emails = self.property.emails.all()
        self.assertTrue(obj in property_emails)
