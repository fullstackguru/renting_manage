from django.apps import AppConfig
from dbmail.models import MailTemplate


class UtilsAppConfig(AppConfig):
    name = 'utils'

    def ready(self):
        # Initialize signals
        from utils.signals.handlers import remove_base64_images_signal
