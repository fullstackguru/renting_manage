# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('management', '0034_make_user_optional_in_bed_model'),
    ]

    operations = [
        migrations.AddField(
            model_name='property',
            name='emails',
            field=models.ManyToManyField(to='dbmail.MailTemplate'),
        ),
    ]
