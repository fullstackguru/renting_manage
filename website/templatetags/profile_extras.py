from django import template
from collections import defaultdict


register = template.Library()


@register.filter(name='direct_length')
def cut(users):
    return len(filter(lambda user: user.via_user is None, users))


@register.filter(name='via_users_length')
def cut(users):
    return len(filter(lambda user: user.via_user, users))
