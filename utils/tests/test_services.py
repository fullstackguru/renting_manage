from mock import patch
from django.test import TestCase
from django.utils import timezone
from website.models import User, Relationship
from management_models import Invitation, Unit, PendingAssignment
from utils.services import create_relations_for_suitemates, lock_group, get_group_size, break_unnecessary_relations, \
    get_related_user_ids
from utils.tests_utils import create_property_related_objects, create_users, mock_send_welcome_email


class UtilServicesTestCase(TestCase):

    @classmethod
    @patch('management.signals.handlers.send_welcome_email', mock_send_welcome_email)
    def setUpClass(cls):
        super(UtilServicesTestCase, cls).setUpClass()
        cls.users_number = 10
        create_users(cls.users_number)
        cls.organization, cls.property, cls.unit_type, cls.assignment_period, cls.room_type = \
            create_property_related_objects()
        users = User.objects.all()
        for user in users:
            Invitation.objects.create(
                user=user,
                agreement_period=cls.assignment_period,
                unit_type=cls.unit_type,
            )

    def test_get_related_user_ids(self):
        group_member = User.objects.all().first()
        invite = Invitation.objects.get(user=group_member)
        users = list(User.objects.exclude(id=group_member.id)[:4])
        group_ids = {group_member.id}
        self.assertEqual(get_related_user_ids(group_member), group_ids)
        for user in users[:3]:
            Relationship.objects.create(start_user=group_member, end_user=user, invitation=invite,
                                        approval_datetime=timezone.now(), end_user_approved=True)
            group_ids.add(user.id)
            self.assertEqual(get_related_user_ids(group_member), group_ids)
        pending_member = users[-1]
        Relationship.objects.create(start_user=group_member, end_user=pending_member, invitation=invite)
        self.assertEqual(get_related_user_ids(group_member), group_ids)
        group_ids.add(pending_member.id)
        self.assertEqual(get_related_user_ids(group_member, with_pending_requests=True), group_ids)

    @patch('management.signals.handlers.send_welcome_email', mock_send_welcome_email)
    def test_creating_relations_for_suitemates(self):
        users = User.objects.all()
        new_user = User.objects.create(email='some_email@mail.com', first_name='some_first_name',
                                       last_name='some_last_name')
        invite = Invitation.objects.create(user=new_user, agreement_period=self.assignment_period,
                                           unit_type=self.unit_type)
        unit = Unit.objects.create(name='test_unit', unit_type=self.unit_type, max_rooms=3)
        PendingAssignment.objects.create(user=new_user, unit=unit, room_type=self.room_type,
                                         assignment_period=self.assignment_period)
        create_relations_for_suitemates(new_user)
        relations = Relationship.objects.all()
        self.assertEqual(relations.count(), 0)
        for user in users[:3]:
            PendingAssignment.objects.create(user=user, unit=unit, room_type=self.room_type,
                                             assignment_period=self.assignment_period)
        create_relations_for_suitemates(new_user)
        relations = Relationship.objects.all()
        self.assertEqual(relations.count(), 6)
        create_relations_for_suitemates(new_user)
        relations = Relationship.objects.all()
        self.assertEqual(relations.count(), 6)
        relations = relations.filter(read_only=True, via_user__isnull=True)
        self.assertEqual(relations.count(), 6)
        other_user = User.objects.create(email='other_user@email.com', first_name='other', last_name='user')
        Invitation.objects.create(user=other_user, agreement_period=self.assignment_period, unit_type=self.unit_type)
        Relationship.objects.create(start_user=new_user, end_user=other_user, invitation=invite,
                                    approval_datetime=timezone.now(), end_user_approved=True)
        create_relations_for_suitemates(new_user)
        relations = Relationship.objects.all()
        self.assertEqual(relations.count(), 10)
        relations = relations.filter(read_only=True, via_user__isnull=True)
        self.assertEqual(relations.count(), 6)
        PendingAssignment.objects.create(user=other_user, unit=unit, room_type=self.room_type,
                                         assignment_period=self.assignment_period)
        create_relations_for_suitemates(new_user)
        relations = Relationship.objects.all()
        self.assertEqual(relations.count(), 10)
        relations = relations.filter(read_only=True, via_user__isnull=True)
        self.assertEqual(relations.count(), 10)

    def test_lock_group(self):
        user1, user2, user3, user4 = User.objects.all()[:4]
        invite = Invitation.objects.get(user=user1)
        Relationship.objects.create(start_user=user1, end_user=user2, invitation=invite, end_user_approved=True,
                                    approval_datetime=timezone.now())
        Relationship.objects.create(start_user=user1, end_user=user3, invitation=invite, end_user_approved=True,
                                    approval_datetime=timezone.now())
        Relationship.objects.create(start_user=user1, end_user=user4, invitation=invite)
        all_relations = Relationship.objects.all()
        group_users_invites = Invitation.objects.filter(user__in=[user1, user2, user3, user4])
        self.assertEqual(all_relations.count(), 4)
        approved_relations = all_relations.filter(approval_datetime__isnull=False, end_user_approved=True)
        self.assertEqual(approved_relations.count(), 3)
        self.assertEqual(group_users_invites.filter(locked=True).count(), 0)
        self.assertRaises(Exception, lock_group, user1, check_overflowed=True)  # unfilled group
        self.assertEqual(all_relations.count(), 4)
        unit = Unit.objects.create(name='test_unit', unit_type=self.unit_type, max_rooms=3)
        PendingAssignment.objects.create(user=user1, unit=unit, room_type=self.room_type,
                                         assignment_period=self.assignment_period)
        self.assertRaises(Exception, lock_group, user1, check_overflowed=True)  # unfilled group
        self.assertEqual(all_relations.count(), 4)
        self.unit_type.max_occupancy = 3
        self.unit_type.save()
        lock_group(user1)
        self.assertEqual(all_relations.count(), 3)
        self.assertEqual(approved_relations.filter(read_only=True).count(), 3)
        self.assertEqual(group_users_invites.filter(locked=True).count(), 3)

    def test_group_size(self):
        from management.models import ContractType
        users = User.objects.values_list('id', flat=True)
        double_xl_contract_type = ContractType.objects.create(name='Double XL', occupied_places=2)
        Invitation.objects.filter(user__in=users[:2]).update(contract_type=double_xl_contract_type)
        self.assertEqual(get_group_size(users), len(users) + 2)

    def test_break_unnecessary_relations(self):
        user1, user2, user3, user4, user5, user6 = User.objects.all()[:6]
        invite = Invitation.objects.get(user=user1)
        Relationship.objects.create(start_user=user1, end_user=user2, invitation=invite, end_user_approved=True,
                                    approval_datetime=timezone.now())
        first_unit = Unit.objects.create(name='first_unit', unit_type=self.unit_type, max_rooms=3)
        PendingAssignment.objects.create(user=user3, unit=first_unit, room_type=self.room_type,
                                         assignment_period=self.assignment_period)
        Relationship.objects.create(start_user=user3, end_user=user4, invitation=invite, end_user_approved=True,
                                    approval_datetime=timezone.now())
        second_unit = Unit.objects.create(name='second_unit', unit_type=self.unit_type, max_rooms=3)
        PendingAssignment.objects.create(user=user5, unit=second_unit, room_type=self.room_type,
                                         assignment_period=self.assignment_period)
        Relationship.objects.create(start_user=user5, end_user=user6, invitation=invite, end_user_approved=True,
                                    approval_datetime=timezone.now())
        self.assertEqual(get_group_size(get_related_user_ids(user1, with_pending_requests=True)), 2)
        break_unnecessary_relations(user1)
        self.assertEqual(get_group_size(get_related_user_ids(user1, with_pending_requests=True)), 2)
        first_relation = Relationship.objects.create(start_user=user1, end_user=user3, invitation=invite)
        second_relation = Relationship.objects.create(start_user=user1, end_user=user5, invitation=invite)
        break_unnecessary_relations(user1)
        self.assertEqual(get_group_size(get_related_user_ids(user1, with_pending_requests=True)), 4)
        first_relation.end_user_approved = True
        first_relation.approval_datetime = timezone.now()
        first_relation.save()
        break_unnecessary_relations(user1)
        self.assertEqual(get_group_size(get_related_user_ids(user1, with_pending_requests=True)), 4)
        self.assertEqual(get_related_user_ids(user1, with_pending_requests=True),
                         {user1.id, user2.id, user3.id, user4.id})
