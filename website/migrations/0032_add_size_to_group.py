# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
from django.conf import settings


class Migration(migrations.Migration):

    dependencies = [
        ('website', '0031_merge'),
    ]

    operations = [
        migrations.AddField(
            model_name='group',
            name='size',
            field=models.PositiveSmallIntegerField(default=0),
        ),
        migrations.AlterField(
            model_name='group',
            name='leader',
            field=models.ForeignKey(related_name='group_leader', blank=True, to=settings.AUTH_USER_MODEL, null=True),
        ),
    ]
