import os, binascii


class ChartBuilder(object):
    def __init__(self, num_of_segments, params=None):
        self.num_of_segments = num_of_segments
        self.params = params if params else {}
        self.data = []
        self.labels = []
        self.background_colors = []
        self.hover_background_colors = []

    def __check_input_array(self, arr):
        if len(arr) != self.num_of_segments:
            raise ValueError

    def set_data(self, data):
        self.__check_input_array(data)
        self.data = data

    def set_labels(self, labels):
        self.__check_input_array(labels)
        self.labels = labels

    def set_background_color(self, colors):
        self.__check_input_array(colors)
        self.background_colors = colors

    def set_hover_background_colors(self, colors):
        self.__check_input_array(colors)
        self.hover_background_colors = colors

    def __gen_random_colors(self):
        return ['#{}'.format(binascii.b2a_hex(os.urandom(3))).upper() for _ in range(0, self.num_of_segments)]

    def build(self):
        if not all([self.labels, self.data]):
            raise ValueError
        background_colors = self.background_colors if self.background_colors else self.__gen_random_colors()
        hover_background_colors = self.hover_background_colors if self.hover_background_colors \
            else self.__gen_random_colors()
        chart = {
            'labels': self.labels,
            'datasets': [{
                'data': self.data,
                'backgroundColor': background_colors,
                'hoverBackgroundColor': hover_background_colors,
                'label': 'My dataset'
            }]
        }
        chart.update(self.params)
        return chart
