#! /bin/bash

cd ~/redis_backups

ts=$(date +%Y-%m-%d.%H%M)

# Do the dump
docker exec rc_app redis-cli bgsave
docker cp rc_app:/var/lib/redis/dump.rdb ~/redis_backups/dump.$ts.rdb

# Delete sql older than a week
find -iname "dump\.*\.rdb" -ctime +7 -delete
