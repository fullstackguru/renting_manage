# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    def add_missed_beds(apps, schema_migration):
        Room = apps.get_model('management', 'Room')
        Bed = apps.get_model('management', 'Bed')
        Property = apps.get_model('management', 'Property')
        props = Property.objects.filter(name__in=['The Lorenzo', 'DobieTwenty21', 'Cottonwood'])
        rooms = Room.objects.filter(unit__unit_type__property__in=props).prefetch_related('beds')
        for room in rooms:
            beds = room.beds.all()
            if len(beds) in [0, 2]:
                continue
            bed = beds[0]
            new_name = bed.name[:-1] + ('1' if bed.name[-1] == '2' else '2')
            Bed.objects.get_or_create(room=bed.room, name=new_name)

    dependencies = [
        ('management', '0054_add_rooms'),
    ]

    operations = [
        migrations.RunPython(add_missed_beds)
    ]
