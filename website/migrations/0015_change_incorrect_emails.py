# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
from django.db.utils import IntegrityError
from django.db import transaction


class Migration(migrations.Migration):

    def forwards_func(apps, schema_editor):
        User = apps.get_model("website", "User")
        Invitation = apps.get_model("management", "Invitation")
        users = User.objects.all()
        for user in users:
            try:
                user.email = user.email.replace(';', '')
                with transaction.atomic():
                    user.save()
            except IntegrityError as e:
                print "Remove {} user and invitation".format(user.email)
                Invitation.objects.filter(user=user).delete()
                user.delete()

    dependencies = [
        ('website', '0014_remove_incorrect_relations'),
    ]

    operations = [
        migrations.RunPython(forwards_func),
    ]
