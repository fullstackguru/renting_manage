# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    def remove_wrong_units(apps, schema_editor):
        Unit = apps.get_model("management", "Unit")
        wrong_units = Unit.objects.filter(name__icontains="waitlist")
        wrong_units.delete()

    dependencies = [
        ('management', '0021_add_contract_types'),
    ]

    operations = [
        migrations.RunPython(remove_wrong_units)
    ]
