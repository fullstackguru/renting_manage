from datetime import datetime, timedelta
from management_models import Organization, Property, UnitType, RoomType, AssignmentPeriod
from website.models import User


def create_users(count):
    for i in range(1, count + 1):
        User.objects.create(
            email="test_user_{}@mail.com".format(i),
            first_name=str(i),
            last_name=str(i),
            gender=User.GENDER_MALE,
        )


def create_property_related_objects():
    organization = Organization.objects.create(
        name="some organization",
    )
    prop = Property.objects.create(
        organization=organization,
        name="some property",
    )
    unit_type = UnitType.objects.create(
        property=prop,
        name="some unit type",
        max_occupancy=8,
    )
    room_type = RoomType.objects.create(property=prop, name='rt', max_occupancy=6)
    assignment_period = AssignmentPeriod.objects.create(
        property=prop,
        name="some assignment period",
        start_date=datetime.now(),
        end_date=datetime.now() + timedelta(days=100),
    )
    return organization, prop, unit_type, assignment_period, room_type


def mock_send_welcome_email(instance):
    print "SENT WELCOME EMAIL for {}".format(instance)


def mock_send_relations_email(instance, created):
    print "SENT RELATIONSHIP EMAIL between {} and {}".format(instance.start_user, instance.end_user)


def mock_send_reminder_email(instance):
    print "SENT REMINDER EMAIL for {}".format(instance)
