# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('management', '0067_auto_20161103_0459'),
    ]

    operations = [
        migrations.AddField(
            model_name='unittype',
            name='max_rooms',
            field=models.IntegerField(default=1),
        ),
    ]
