$(document).ready(function () {
    var paginationTrigger = $({});
    var page = 1;

    initPaginationTrigger();
    initPaginator();
    paginationTrigger.trigger('change.page');

    function initPaginationTrigger() {
        paginationTrigger.on('change.page', function () {
            var promise = downloadPage();
            promise.done(renderResults);
        });
    }

    function initPaginator() {
        var handler = function () {
            var $target = $(this);
            var pageValue = $target.val();
            var totalPages = $('#user-messages-total-pages').val();
            if (pageValue === 'next' && page < totalPages) {
                page += 1;
                paginationTrigger.trigger('change.page');
            } else if (pageValue === 'previous' && page > 1) {
                page -= 1;
                paginationTrigger.trigger('change.page');
            } else if (pageValue === 'refresh') {
                paginationTrigger.trigger('change.page');
            }
        };
        $('.user-messages-paginate-button').on('click', handler);
    }

    function downloadPage() {
        return $.ajax({
            type: 'GET',
            url: '/user-messages-ajax/?page=' + page,
            contentType: 'application/json'
        });
    }

    function renderResults(html) {
        var $messagesContainer = $('#user-messages');
        $messagesContainer.html(html);
        initPaginator();
        OneUI.init("uiBlocks");
    }
});