# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import management.models


class Migration(migrations.Migration):

    dependencies = [
        ('management', '0010_add_reminder_email_field_to_invitation'),
    ]

    operations = [
        migrations.AlterField(
            model_name='invitation',
            name='expire_date',
            field=models.DateTimeField(default=management.models.get_expire_date),
        ),
    ]
