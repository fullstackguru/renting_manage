(function ($) {
  $(document).ready(function(){
    $("#id_unit_type").chainedTo("#id_agreement_period");
    $("#id_room_type").chainedTo("#id_agreement_period");
    $("#id_user").chainedTo("#id_agreement_period");
  });
})(django.jQuery);