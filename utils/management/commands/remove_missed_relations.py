from django.core.management.base import BaseCommand
from django.db.models import Q
from website.models import Relationship, User


class Command(BaseCommand):
    help = 'Remove missed relationships'

    def handle(self, *args, **options):
        users = User.objects.all()
        for user in users:
            user_relations = Relationship.objects.filter(Q(start_user=user) | Q(end_user=user)).\
                filter(end_user_approved=True).filter(approval_datetime__isnull=False)
            direct_relations = user_relations.filter(via_user__isnull=True)
            direct_relations_users = {user}
            for relation in direct_relations:
                direct_relations_users.add(relation.start_user)
                direct_relations_users.add(relation.end_user)
            direct_relations_users.remove(user)
            relations_for_deleting = user_relations.filter(via_user__isnull=False).\
                exclude(via_user__in=direct_relations_users)
            relations_for_deleting.delete()
