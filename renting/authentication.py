from django.contrib.auth import logout, authenticate, login
from django.contrib.auth.decorators import login_required
from django.contrib.auth import get_user_model
User = get_user_model()
from django.core.urlresolvers import reverse
from django.http import HttpResponseRedirect


def logout_view(request):
    logout(request)
    return HttpResponseRedirect(reverse('website:static', {'page':'index'}))


def login_view(request):
    username = request.POST.get('username')
    password = request.POST.get('password')
    user = authenticate(username=username, password=password)
    return login_user(request, user)


def login_user(request, user):
    if user is not None:
        if user.is_active:
            login(request, user)
            # Redirect to a success page.
            return HttpResponseRedirect(reverse('website:static', {'page': 'quiz'}))
        else:
            # Return a 'disabled account' error message
            return HttpResponseRedirect(reverse('website:static', {'page': 'index'}))
    else:
        # Return an 'invalid login' error message.
        return HttpResponseRedirect(reverse('website:static', {'page': 'index'}))


class LoginRequiredMixin(object):
    pass
    # @classmethod
    # def as_view(cls, **initkwargs):
    #     view = super(LoginRequiredMixin, cls).as_view(**initkwargs)
    #     return login_required(view)