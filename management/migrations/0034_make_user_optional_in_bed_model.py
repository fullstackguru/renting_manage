# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
from django.conf import settings


class Migration(migrations.Migration):

    dependencies = [
        ('management', '0033_add_unique_constraints'),
    ]

    operations = [
        migrations.AlterField(
            model_name='bed',
            name='user',
            field=models.ForeignKey(related_name='beds', blank=True, to=settings.AUTH_USER_MODEL, null=True),
        ),
    ]
