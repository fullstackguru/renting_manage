# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import datetime
from django.conf import settings
import uuid


class Migration(migrations.Migration):

    dependencies = [
        ('management', '__first__'),
        ('website', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='ConversationMessage',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('message', models.CharField(max_length=250)),
                ('unread', models.BooleanField(default=True)),
                ('created_at', models.DateTimeField(auto_now=True, db_index=True)),
            ],
        ),
        migrations.CreateModel(
            name='QuizResult',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('traitify_assessment_id', models.CharField(max_length=50, null=True)),
                ('user', models.ForeignKey(to=settings.AUTH_USER_MODEL)),
            ],
        ),
        migrations.CreateModel(
            name='Relationship',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('end_user_approved', models.BooleanField(default=False)),
                ('invite_datetime', models.DateTimeField(default=datetime.datetime.now)),
                ('approval_datetime', models.DateTimeField(null=True)),
                ('end_user', models.ForeignKey(related_name='end_user', to=settings.AUTH_USER_MODEL)),
                ('invitation', models.ForeignKey(to='management.Invitation')),
                ('start_user', models.ForeignKey(related_name='start_user', to=settings.AUTH_USER_MODEL)),
            ],
        ),
        migrations.CreateModel(
            name='SocialNetworks',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('network_name', models.CharField(max_length=50)),
                ('network_url', models.CharField(max_length=250)),
            ],
        ),
        migrations.CreateModel(
            name='UserConversation',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('conversation_uuid', models.UUIDField(default=uuid.uuid4, db_index=True)),
                ('created_at', models.DateTimeField(auto_now=True, db_index=True)),
                ('user1', models.ForeignKey(related_name='user1', to=settings.AUTH_USER_MODEL)),
                ('user2', models.ForeignKey(related_name='user2', to=settings.AUTH_USER_MODEL)),
            ],
        ),
        migrations.CreateModel(
            name='UserOrganizationSocialHabitAnswers',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('organization_question_text', models.CharField(max_length=250)),
                ('organization_answer_text', models.CharField(max_length=250)),
                ('sort_order', models.IntegerField(default=1)),
                ('created_at', models.DateTimeField(editable=False, db_index=True)),
                ('invitation', models.ForeignKey(to='management.Invitation')),
                ('organization_answer', models.ForeignKey(to='management.OrganizationSocialHabitAnswer')),
                ('organization_question', models.ForeignKey(to='management.OrganizationSocialHabitQuestion')),
            ],
        ),
        migrations.CreateModel(
            name='UserProfile',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('first_name', models.CharField(max_length=30, verbose_name=b'first name', blank=True)),
                ('last_name', models.CharField(max_length=30, verbose_name=b'last name', blank=True)),
                ('gender', models.CharField(default=b'U', max_length=1, verbose_name=b'gender', choices=[(b'U', b'Unknown'), (b'M', b'Male'), (b'F', b'Female')])),
                ('personality_types', models.TextField(null=True)),
                ('personality_traits', models.TextField(null=True)),
                ('bio', models.TextField(null=True)),
                ('twitter_url', models.CharField(max_length=250, null=True, blank=True)),
                ('facebook_url', models.CharField(max_length=250, null=True, blank=True)),
                ('pinterest_url', models.CharField(max_length=250, null=True, blank=True)),
                ('linkedin_url', models.CharField(max_length=250, null=True, blank=True)),
                ('instagram_url', models.CharField(max_length=250, null=True, blank=True)),
                ('snapchat_url', models.CharField(max_length=250, null=True, blank=True)),
                ('google_plus_url', models.CharField(max_length=250, null=True, blank=True)),
                ('profile_image', models.ImageField(null=True, upload_to=b'uploads/avatar/%Y/%m/%d/', blank=True)),
                ('profile_image_bg', models.ImageField(null=True, upload_to=b'uploads/avatar_bg/%Y/%m/%d/', blank=True)),
                ('image_url', models.CharField(max_length=250, null=True)),
                ('bg_url', models.CharField(max_length=250, null=True)),
                ('user', models.OneToOneField(related_name='profile', to=settings.AUTH_USER_MODEL)),
            ],
        ),
        migrations.CreateModel(
            name='UserSocialHabitAnswersHistory',
            fields=[
                ('id', models.AutoField(serialize=False, primary_key=True)),
                ('user_id', models.IntegerField()),
                ('organization_id', models.IntegerField()),
                ('invitation_id', models.IntegerField()),
                ('question_id', models.IntegerField()),
                ('answer_id', models.IntegerField()),
                ('answered_at', models.DateTimeField(editable=False)),
                ('question_text', models.CharField(max_length=250)),
                ('answer_text', models.CharField(max_length=250)),
                ('archived_at', models.DateTimeField(editable=False)),
            ],
        ),
        migrations.AddField(
            model_name='userorganizationsocialhabitanswers',
            name='userprofile',
            field=models.ForeignKey(to='website.UserProfile'),
        ),
        migrations.AddField(
            model_name='socialnetworks',
            name='userprofile',
            field=models.ForeignKey(related_name='userprofile', to='website.UserProfile'),
        ),
        migrations.AddField(
            model_name='conversationmessage',
            name='conversation',
            field=models.ForeignKey(related_name='user_conversation', to='website.UserConversation'),
        ),
        migrations.AddField(
            model_name='conversationmessage',
            name='user',
            field=models.ForeignKey(to=settings.AUTH_USER_MODEL),
        ),
    ]
