var initSuggestedMatchesTable = function(listUrl) {
    $('#suggested-matches-table').on('preXhr.dt', function () {
        $('tr').attr('hidden', true);
        App.blocks('#suggested-matches-table-block', 'refresh_toggle');
    });
    $('#suggested-matches-table').on('xhr.dt', function () {
        $('tr').attr('hidden', false);
        App.blocks('#suggested-matches-table-block', 'refresh_toggle');
    });
    $('#suggested-matches-table').DataTable({
        initComplete: function () {
            var api = this.api();
            $('#suggested-matches-table_filter input')
                .off('.DT')
                .on('keyup.DT', function (e) {
                    if (e.keyCode == 13) {
                        api.search(this.value).draw();
                    }
                });
        },
        "serverSide": true,
        "processing": true,
        "bSort": true,
        "bPaginate": true,
        "sPaginationType": "full_numbers",
        "iDisplayLength": 10,
        ajax: {
            "dataType": 'json',
            url: listUrl,
            "dataSrc": function (json) {
                return json.data
            }
        },
        "aoColumns": [
            {
                "mDataProp": "email",
                "render": function (data, type, full, meta) {
                    return  full.email + ' - ' + full.first_name + ' ' + full.last_name;
                },
                "orderable": true
            },
            {
                "mDataProp": "suggested_roommates",
                "render": function (data, type, full, meta) {
                    var matches = [];
                    data.forEach(function(entry){
                        matches.push('<li>' + entry.email + ' - ' + entry.first_name + ' ' + entry.last_name + ' - ' + entry.score + '%</li>');
                    });
                    return '<ol>' + matches.join('') + '</ol>';
                },
                "orderable": false
            }
        ],
        paging: true,
        "lengthMenu": [[10, 25, 50, 100], [10, 25, 50, 100]]
    });
};
