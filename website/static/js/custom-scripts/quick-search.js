(function (win, $) {
  var QuickSearch = function QuickSearch(el, opts) {
    var options = $.extend({
      events: 'keyup search',
      timeout: 300,
      params: []
    }, opts);

    this.input = $(el);
    this.items = $(options.items);
    this.params = options.params;
    this.expr = this.input.val();
    this.events = options.events;
    this.timeout = options.timeout;

    this.done = options.done;

    this.init();
  };

  QuickSearch.prototype.init = function() {
    var self = this;
    this.searchHandler = delayedHandler(function() {
      self.expr = self.input.val();
      self.doSearch(self.expr);
    }, this.timeout);

    this.input.on(this.events, this.searchHandler);
  };

  QuickSearch.prototype.destroy = function() {
    this.input.off(this.events, this.searchHandler);
    this.expr = '';
  };

  QuickSearch.prototype.doSearch = function(expr) {
    var result = this.searchFunction(expr);
    typeof this.done == 'function' && this.done(result);

    return result;
  };

  QuickSearch.prototype.searchFunction = function (expr) {
    var params = this.params;
    var l = params.length;
    var rexp = new RegExp(expr, 'i');

    var resultedItems = this.items.filter(function() {
      var text = '',
        i = 0;

      if (!l) {
        text = $(this).text();
      } else {
        for (i; i < l; i++) {
          text += $(this).find(params[i]).text() + ' ';
        }
      }

      return text.search(rexp) > -1;
    });

    resultedItems.show();
    this.items.not(resultedItems).hide();

    return resultedItems;
  };

  $.fn.quickSearch = function (options) {
    return $(this).data('quick-search', new QuickSearch(this, options));
  };

  win.QuickSearch = QuickSearch;
})(window, jQuery);

