var initMatchTable = function (listUrl, propertyId) {

    $('.filter-option').on('click', function () {
        $(this).closest('li').siblings().children('a').removeClass('selected-filter');  // Clear filter
        var selectedValue = $(this).attr("value");
        if (selectedValue) {
            $(this).addClass('selected-filter');
        }
        $('#match-table').DataTable().ajax.reload();
    });

    $('#match-table').on('preXhr.dt', function () {
        $('tr').attr('hidden', true);
        App.blocks('#match-table-block', 'refresh_toggle');
    });
    $('#match-table').on('xhr.dt', function () {
        $('tr').attr('hidden', false);
        App.blocks('#match-table-block', 'refresh_toggle');
    });
    $('#match-table').DataTable({
        initComplete: function () {
            var table = this;
            var api = this.api();
            $('#match-table_filter input')
                .off('.DT')
                .on('keyup.DT', function (e) {
                    if (e.keyCode == 13) {
                        table.fnFilter(this.value);
                    }
                });
        },
        "deferRender": true,
        "bSort": true,
        "bPaginate": true,
        "sPaginationType": "full_numbers",
        "iDisplayLength": 100,
        ajax: {
            "data": function(d) {
                $('.selected-filter').each(function (index, element) {
                    var filterName = $(this).closest('.filter').attr('name');
                    d[filterName] = $(this).attr('value');
                });
            },
            "dataType": 'json',
            url: listUrl,
            "dataSrc": function (json) {
                this.aaData = json.data;
                return json.data
            }
        },
        "aoColumns": [
            {
                "mDataProp": "property",
                bSearchable: false
            },
            {
                "mDataProp": "agreement_period",
                bSearchable: false
            },
            {
                "mDataProp": "unit_type",
                bSearchable: false
            },
            {
                "mDataProp": "group_count",
                bSearchable: false
            },
            {
                "mDataProp": "group",
                "render": function (data, type, full, meta) {
                        var printUser = function (user) {
                            return "<dd>" + user.name + " (" + user.gender + "); " + user.email + "</dd>"
                        };
                        var arr = ["<dt>Roommates:</dt>"];
                        full.roommates.forEach(function (user) {
                            arr.push(printUser(user));
                        });
                        arr.push("<dt>Suite mates:</dt>");
                        full.suitemates.forEach(function (user) {
                            arr.push(printUser(user));
                        });
                        return "<dl>" + arr.join("") + "</dl>";
                },
                "orderable": false
            },
            {
                "mDataProp": "unit"
            },
            {
                "mDataProp": "lock",
                "render": function (data, type, full, meta) {
                    if (full.locked) {
                        return '<div class="text-center"><i class="fa fa-lock fa-2x"></i></div>';
                    }
                    else {
                        return '<div class="management-action text-center lock-button" group-id="' + full.id + '">' +
                            '<p class="lock-button"><i class="fa fa-unlock fa-2x"></i></p>' +
                            '</div>'
                    }
                },
                "orderable": false,
                bSearchable: false
            }
        ],
        paging: true,
        "lengthMenu": [[10, 25, 50, 100, -1], [10, 25, 50, 100, "All"]],
        "drawCallback": function (settings) {
            setLockOnClickEvent(propertyId);
        }
    });
};

var setLockOnClickEvent = function (propertyId) {
    $('.lock-button').on('click', function () {
        var table = $('#match-table').DataTable();
        var clickedBlock = this;
        var groupId = $(clickedBlock).attr('group-id');
        if (!groupId) {  // Only groups have ids
            return;
        }
        var url = '/management/matches/' + propertyId + '/';
        var data = {'groupId': groupId};
        swal({
                title: "Are you sure?",
                text: "Are you sure that you want to lock this group",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: "Yes, lock it!",
                cancelButtonText: "No, cancel!",
                closeOnConfirm: false
            },
            function (isConfirm) {
                if (isConfirm) {
                    $.ajax({
                        url: url,
                        type: 'POST',
                        data: data,
                        success: function (data) {
                            swal("Done!", data, "success");
                            $(clickedBlock).replaceWith('<div class="text-center"><i class="fa fa-lock fa-2x"></i></div>');
                        },
                        error: function (jqXHR, textStatus, errorThrown) {
                            swal("Oops...", "Unable to lock unfilled group", "error");
                        }
                    });
                }
            });
    });
};