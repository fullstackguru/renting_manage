# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('website', '0033_merge'),
    ]

    operations = [
        migrations.AlterField(
            model_name='user',
            name='points',
            field=models.PositiveSmallIntegerField(default=50),
        ),
        migrations.AlterUniqueTogether(
            name='group',
            unique_together=set([('agreement_period', 'unit')]),
        ),
    ]
