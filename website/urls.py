from django.conf import settings
from django.views.static import serve
from django.conf.urls.static import static
from django.conf.urls import url
from website.views import MainStudentDashboardView, ProfileView, InviteUpdateView, StaticView, \
    QuizProfileInformation, QuizUpdateSlideView, RoomMateMatchView, CodeIndex, UserConversationView, \
    PeerProfileAjaxView, SearchPeersAjaxView, GoogleVerification, UploadUserProfileImageView, UserMessagesAjax, \
    update_bio, SelectUnitView, SelectBedView

urlpatterns = [
    url(r'^getting-started/(?P<invite_code>[^/]+)$', MainStudentDashboardView.as_view(), name='getting-started'),
    url(r'^profile/$', ProfileView.as_view(), name='user_profile'),
    url(r'^profile_v2/$', MainStudentDashboardView.as_view(), name='user_profile_v2'),
    url(r'^invite_update/$', InviteUpdateView.as_view(), name='invite_update'),
    url(r'^quiz-profile/$', QuizProfileInformation.as_view(), name='quiz_profile'),
    url(r'^quiz-slide/$', QuizUpdateSlideView.as_view(), name='quiz_slide'),
    url(r'^room-mate-matching/$', RoomMateMatchView.as_view(), name='roommate_selection'),
    url(r'^user-conversation/(?P<conversation_uuid>.*)/$', UserConversationView.as_view(), name='user-conversation'),
    url(r'^user-profile-ajax/(?P<peer_id>.*)/$', PeerProfileAjaxView.as_view(), name='user-profile-ajax'),
    url(r'^search-peers-ajax/$', SearchPeersAjaxView.as_view(), name='search-peers-ajax'),
    url(r'^upload-image-ajax/$', UploadUserProfileImageView.as_view(), name='upload-image-ajax'),
    url(r'^user-messages-ajax/$', UserMessagesAjax.as_view(), name='user-messages-ajax'),
    url(r'^google0f68347ff74b158a.html$', GoogleVerification.as_view(), name='google0f68347ff74b158a.html'),
    url(r'update-bio/$', update_bio, name='update-bio'),
    url(r'^select_unit/$', SelectUnitView.as_view(), name='select_unit'),
    url(r'^select_bed/$', SelectBedView.as_view(), name='select_bed'),
    url(r'^$', CodeIndex.as_view(), name='code_index'),
    url(r'^(?P<page>.+\.html)$', StaticView.as_view(), name='static'),
    url(r'^(?P<page>.+\.txt)$', StaticView.as_view(), name='static'),
    url(r'^(?P<page>)$', StaticView.as_view(), name='static'),
] + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
