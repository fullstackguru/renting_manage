# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('management', '0073_auto_20161121_0654'),
    ]

    operations = [
        migrations.AddField(
            model_name='property',
            name='contract_type',
            field=models.ManyToManyField(to='management.ContractType', blank=True),
        ),
    ]
