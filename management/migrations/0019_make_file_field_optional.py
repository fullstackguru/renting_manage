# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('management', '0018_add_resident_import_model'),
    ]

    operations = [
        migrations.AlterField(
            model_name='residentimport',
            name='file',
            field=models.FileField(null=True, upload_to=b'uploads/resident_import/', blank=True),
        ),
    ]
