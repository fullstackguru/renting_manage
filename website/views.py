import json
import cPickle as pickle
import operator
from uuid import uuid4
from itertools import chain
from datetime import datetime, timedelta
from collections import defaultdict

from django.conf import settings
from django.contrib.auth import login
from django.contrib.auth import get_user_model
from django.core.urlresolvers import reverse
from django.core.exceptions import SuspiciousOperation
from django.db.models import Q, F, Prefetch, Sum, Case, When, Count
from django.db import models
from django.utils.decorators import method_decorator
from django.views.decorators.http import require_http_methods
from django.http import Http404, HttpResponseRedirect, JsonResponse, HttpResponse
from django.template import TemplateDoesNotExist
from django.views.generic import TemplateView, View
from django.views.generic.edit import BaseUpdateView
from django.views.generic.list import ListView
from django.core.exceptions import PermissionDenied
from django.utils import timezone
from actstream import action

from external.traitify.traitify import Traitify
from management.models import Invitation, OrganizationSocialHabitQuestion, OrganizationSocialHabitAnswer, \
    PendingAssignment, Stage, Unit, Bed, Room, PropertySocialNetwork
from website.mixins import CheckInviteMixin
from website.serializers import SelectBedSerializer
from website.models import \
    QuizResult, Relationship, UserProfile, UserConversation, ConversationMessage, SocialNetworks, \
    UserOrganizationSocialHabitAnswers, UserSocialHabitAnswersHistory, Group
from website.forms import UserInformationForm, UserProfileInformationForm, UserBioForm
from utils import compatibility
from utils.services import get_related_user_ids, get_group_size, break_unnecessary_relations
User = get_user_model()


# ============================ Student Views ============================
class StaticView(TemplateView):
    template_name = 'base.html'

    def get(self, request, *args, **kwargs):
        page = kwargs.get('page')
        if page is not None:
            self.template_name = page

        response = super(StaticView, self).get(request, *args, **kwargs)
        try:
            return response.render()
        except TemplateDoesNotExist:
            raise Http404()


class CodeIndex(TemplateView):
    template_name = 'login_by_quiz_code.html'

    def get_context_data(self, **kwargs):
        context = super(CodeIndex, self).get_context_data(**kwargs)
        if self.request.GET.get('code'):
            context['code'] = self.request.GET.get('code')
        context['next'] = self.request.GET.get('next', '')
        return context

    def post(self, request, *args, **kwargs):
        form_data = request.POST
        next = request.POST.get('next', '')
        invite = Invitation.objects.filter(invite_code=form_data.get('code'), expire_date__gte=timezone.now()).first()
        if invite:
            self.request.session['invite_id'] = invite.id
            if next:
                return HttpResponseRedirect(next)
            else:
                return HttpResponseRedirect(reverse('getting-started', kwargs={'invite_code': form_data.get('code')}))
        else:
            path = request.get_full_path()
            invite_code = form_data.get('code', None)
            params = {'code': invite_code, 'next': next}
            if invite_code and invite_code in path:
                params['next'] = None
            params_string = '&'.join(['{}={}'.format(k, v) for k, v in params.items() if v])
            params_string = '?{}'.format(params_string) if params_string else ''
            return HttpResponseRedirect('{}{}'.format(reverse('code_index'), params_string))


class ProfileView(TemplateView):
    template_name = 'user_profile.html'

    def get_context_data(self, **kwargs):
        context = super(ProfileView, self).get_context_data(**kwargs)
        try:
            invite = Invitation.objects.get(id=self.request.session.get('invite_id'))
        except Invitation.DoesNotExist:
            raise Http404("No Invitation matches the given query.")

        user = User.objects.get(username=invite.user.username) # Probably do some sort of token email
        obj_manager_result = QuizResult.objects.get_or_create(user=user)

        quiz_result = obj_manager_result[0]
        if not quiz_result.traitify_assessment_id:
            traitify = Traitify(secret_key=settings.TRAITIFY_SECRET_KEY, host=settings.TRAITIFY_HOST)
            assessment = traitify.create_assessment(settings.TRAITIFY_DECK)
            quiz_result.traitify_assessment_id = assessment.id
            quiz_result.save()

        self.request.session['invite_id'] = invite.id
        self.request.session['quiz_result_id'] = quiz_result.id

        context['quiz'] = quiz_result

        peer_invites = Invitation.objects.filter(
            agreement_period=invite.agreement_period
        ).exclude(
            user=invite.user
        )

        context["peer_invites"] = peer_invites
        return context


class AjaxableResponseMixin(object):
    def form_invalid(self, form):
        response = super(AjaxableResponseMixin, self).form_invalid(form)
        if self.request.is_ajax():
            return JsonResponse(form.errors, status=400)
        else:
            return response

    def form_valid(self, form):
        # We make sure to call the parent's form_valid() method because
        # it might do some processing (in the case of CreateView, it will
        # call form.save() for example).

        self.object = form.save(commit=False)
        image_types = self.request.FILES.keys()
        for image_type in image_types:
            if hasattr(self.object, image_type) and self.request.FILES.get(image_type):
                setattr(self.object, image_type, self.request.FILES.get(image_type))

        response = super(AjaxableResponseMixin, self).form_valid(form)
        if self.request.is_ajax():
            data = {
                'pk': self.object.pk,
                'profile_image_url': self.object.profile_image.url if self.object.profile_image else None,
                'profile_image_bg_url': self.object.profile_image_bg.url if self.object.profile_image_bg else None
            }
            return JsonResponse(data)
        else:
            return response


class UploadUserProfileImageView(AjaxableResponseMixin, BaseUpdateView):
    model = UserProfile
    fields = ['profile_image', 'profile_image_bg']

    @method_decorator(require_http_methods(["POST"]))
    def dispatch(self, request, *args, **kwargs):
        return super(UploadUserProfileImageView, self).dispatch(request, *args, **kwargs)

    def get_success_url(self):
        return reverse('user_profile_v2')

    def get_object(self):
        return UserProfile.objects.get(user__id=self.request.POST.get('user'))

    def form_invalid(self, form):
        raise SuspiciousOperation('Unable to load images!')


# ============================ Quiz Views ============================
class MainStudentDashboardView(TemplateView):
    template_name = 'profile_v2.html'
    traitify = None
    userExists = []
    to_be_deleted = []

    def get(self, request, **kwargs):
        print("MainStudentDashboardView:get")
        # Get Invite Object
        user_invite = self._get_user_invite(kwargs)
        if not user_invite:
            print("not user invite")
            path = request.get_full_path()
            invite_code = kwargs.get('invite_code', None)
            params = {'code': invite_code, 'next': path}
            if invite_code and invite_code in path:  # Redirecting to getting_started page
                params['next'] = None
            params_string = '&'.join(['{}={}'.format(k, v) for k, v in params.items() if v])
            params_string = '?{}'.format(params_string) if params_string else ''
            return HttpResponseRedirect('{}{}'.format(reverse('code_index'), params_string))
        #  update last login
        user = user_invite.user
        user.last_login = timezone.now()
        user.save()
        #  activate invite
        if not user_invite.activated_time:
            user_invite.activated_time = timezone.now()
            user_invite.save()

        self.request.session['invite_id'] = user_invite.id
        context = self.get_context_data(user_invite=user_invite)
        return self.render_to_response(context)

    def get_context_data(self, **kwargs):
        context = super(MainStudentDashboardView, self).get_context_data(**kwargs)
        # TODO: all code below must be cleaned as soon as we switch to new AJAX scheme for users
        user_invite = kwargs.get('user_invite')

        # Init Traitify
        self.traitify = Traitify(secret_key=settings.TRAITIFY_SECRET_KEY, host=settings.TRAITIFY_HOST)

        # Get Quiz Result Object
        quiz_result = self._get_quiz_results(user_invite.user)
        context['quiz'] = quiz_result
        self.request.session['quiz_result_id'] = quiz_result.id

        assessment = self._get_assessment(quiz_result)
        context['assessment'] = assessment

        prop = user_invite.agreement_period.property
        context['property'] = prop
        user = user_invite.user

        if assessment.completed_at:
            # Get Approved Users
            self.to_be_deleted = []
            self.userExists = []
            
            approved_users = self._get_approved_users(user_invite)
            context['approved_relationships'] = approved_users

            # Get Same Unit Users
            unit_users = self._get_sameunit_users(user_invite)
            context['sameunit_relationships'] = unit_users

            # Get Pending Users
            pending_users = self._get_pending_users(user_invite)
            context['pending_relationships'] = pending_users

            extrauser = 0
            if len(self.to_be_deleted) > 0:
                for uid in self.to_be_deleted:
                    if 'su_' in uid:
                        extrauser = extrauser + 1
                context['user_to_be_deleted'] = "."+" , .".join(self.to_be_deleted)
            context['approved_relationships_via_users'] = approved_users.exclude(via_user=None).count() + len(unit_users) - extrauser

            habits_questions = self._get_habits_questions(user_invite)
            context['habits_questions'] = habits_questions

            # Make a accessible user object
            user.assessment = assessment

        # Set user's personality
        self._set_user_personality(user)
        context['user'] = user
        context['invite'] = user_invite
        context['assignment'] = PendingAssignment.objects.\
            filter(user=user, assignment_period=user_invite.agreement_period).\
            select_related('unit', 'room', 'bed').first()
        context['form'] = UserBioForm(instance=user.profile)
        context['max_occupancy'] = user_invite.unit_type.max_occupancy
        context['initial_user_profile'] = self.request.GET.get('user-profile', '')
        stages = Stage.objects.all().order_by('-min_points')
        context['stages'] = stages
        context['current_stage'] = prop.current_stage
        progress = (100 / stages.count()) * (list(stages).index(user.stage) + 0.5)
        context['progress'] = progress
        groups = Group.objects.filter(agreement_period=user_invite.agreement_period)
        user_group = groups.filter(users__in=[user]).first()
        
        if user_group:
            context['show_group_overflow_alert'] = not user_group.allowed_units() and not user_group.unit
        else:
            context['show_group_overflow_alert'] = False
        context['is_user_stage'] = user.points >= prop.current_stage.min_points
        context['is_leader'] = Stage.CAN_SELECT_UNIT in prop.current_stage.restrictions and \
                               (groups.filter(leader=user).exists() or not user_group)
        context['can_select_bed'] = Stage.CAN_SELECT_BED in prop.current_stage.restrictions
        context['can_select_unit'] = Stage.CAN_SELECT_UNIT in prop.current_stage.restrictions

        if context['show_group_overflow_alert']:
          context['can_select_bed'] = False
          context['can_select_unit'] = False
          context['is_leader'] = False

        if user_invite.locked:
          if context['assignment'].bed:
            context['can_select_bed'] = False
          if context['assignment'].unit:
            context['can_select_unit'] = False
            context['is_leader'] = False
        return context

    def _get_user_invite(self, kwargs):
        invite_code = kwargs.get('invite_code')
        invite_id = self.request.session.get('invite_id')
        select_related = ['agreement_period__property__organization', 'agreement_period__property', 'contract_type',
                          'unit_type', 'user', 'user__profile', 'agreement_period__property__current_stage']
        try:
            if invite_code:
                return Invitation.objects.select_related(*select_related).\
                    get(invite_code=invite_code)
            elif invite_id:
                return Invitation.objects.select_related(*select_related).\
                    get(id=invite_id)
        except Invitation.DoesNotExist:
            return None

    def _get_pending_users(self, user_invite):
        print("_get_pending_users")
        pending_users = Relationship.objects.filter(
            invitation__agreement_period__property=user_invite.agreement_period.property,
            approval_datetime__isnull=True,
        ).filter(Q(start_user=user_invite.user) | Q(end_user=user_invite.user)).\
            select_related('start_user', 'end_user')
        print(pending_users)
        for relation in pending_users:
            user = relation.start_user if relation.end_user == user_invite.user else relation.end_user
            invite = Invitation.objects.filter(user=user).values('assignment__unit__name', 'assignment__bed__name').\
                first()
            user.assignment = u"Unit {}: Bed {}".format(invite['assignment__unit__name'],
                                                        invite['assignment__bed__name']) if \
                invite['assignment__unit__name'] and invite['assignment__bed__name'] else ''
            if user not in self.userExists:
                self.userExists.append(user)
            else:
                self.to_be_deleted.append('pu_'+str(relation.id))
        print("_get_pending_users: ")
        print(pending_users)
        return pending_users

    def _get_sameunit_users(self, user_invite):
        print("_get_sameunit_users")
        unituser=[]
        userunit = PendingAssignment.objects.filter(user=user_invite.user, assignment_period=user_invite.agreement_period).\
            exclude(unit__isnull=True).select_related('unit', 'room', 'bed').first()
        if userunit:
            unituser = PendingAssignment.objects.filter(unit=userunit.unit).exclude(
                user=user_invite.user
            ).select_related('user')
        for unitusers in unituser:
            if unitusers.user not in self.userExists:
                self.userExists.append(unitusers.user)
            else:
                self.to_be_deleted.append('su_'+str(unitusers.id))
        return unituser

    def _get_approved_users(self, user_invite):
        print("_get_approved_users")
        print("user_invite.agreement_period.property: ")
        print(user_invite.agreement_period.property.id)
        print("user_invite.user: ")
        print(user_invite.user.id)

        approved_users = Relationship.objects.filter(
            invitation__agreement_period__property=user_invite.agreement_period.property,
            end_user_approved=True,
        ).filter(Q(start_user=user_invite.user) | Q(end_user=user_invite.user)).select_related('start_user', 'end_user').annotate(Count("start_user"))
        user_room_type = user_invite.room_type_id
        for user in approved_users:
            print("this user: ")
            print(user)
            print(user.id)
            potential_roommate = user.start_user if user.end_user == user_invite.user else user.end_user
            potential_roommate_invite = Invitation.objects.filter(user=potential_roommate).\
                values('room_type', 'assignment__unit__name', 'assignment__bed__name').first()
            assignment = u""
            if potential_roommate_invite['assignment__unit__name']:
                assignment += u"Unit {}".format(potential_roommate_invite['assignment__unit__name'])
                if potential_roommate_invite['assignment__bed__name']:
                    assignment += u": Bed {}".format(potential_roommate_invite['assignment__bed__name'])
            potential_roommate.assignment = assignment
            if user_room_type:
                room_type = potential_roommate_invite['room_type'] if potential_roommate_invite else None
                if not room_type or room_type == user_room_type:
                    user.is_roommate_relations_allowed = True
                else:
                    user.is_roommate_relations_allowed = False
            else:
                user.is_roommate_relations_allowed = True
            if potential_roommate not in self.userExists:
                self.userExists.append(potential_roommate)
            else:
                self.to_be_deleted.append('ap_'+str(potential_roommate.id))
        print(approved_users)
        return approved_users

    def _get_org_users(self, user_invite):
        print("_get_org_users")
        org_users = Invitation.objects.filter(
            agreement_period=user_invite.agreement_period
        ).exclude(
            user=user_invite.user
        ).values('user__id')

        return org_users

    def _get_quiz_results(self, user):
        obj_manager_result = QuizResult.objects.get_or_create(user=user)
        quiz_result = obj_manager_result[0]

        return quiz_result

    def _get_assessment(self, quiz_result):
        # Get Assessment
        if not quiz_result.traitify_assessment_id:
            assessment = self.traitify.create_assessment(settings.TRAITIFY_DECK)
            quiz_result.traitify_assessment_id = assessment.id
            quiz_result.save()
        else:
            assessment = self.traitify.get_assessment(quiz_result.traitify_assessment_id)

        return assessment

    def _get_users(self, users, exclude_users=[]):
        matches = QuizResult.objects.filter(
            user__in=users,
        )
        if len(exclude_users) > 0:
            matches = matches.exclude(
                user__in=exclude_users
            )
        matches = list(matches)
        # Set user's personality
        for match in matches:
            self._set_user_personality(match.user)
        return matches

    def _get_no_quiz_users(self):
        no_quiz_users = UserProfile.objects.filter(
            personality_types__isnull=True
        ).values('user_id')

        return [user['user_id'] for user in no_quiz_users]

    def _get_relation_ids(self, relations):
        """ Get list of IDs for all users which have relations.
        """
        user_ids = []
        for relation in relations:
            user_ids.append(relation.start_user.id)
            user_ids.append(relation.end_user.id)
        return user_ids

    def _get_relation_users(self, relations):
        """ Get list of all users which have relations.

            NOTE: WrapObj is used because there are several methods which expect list of 'something' with user
            attribute set. Unfortunately Relationship model doesn't have user link because it has a pair of users -
            start_user and end_user. Here created a list of WrapObj objects which has user attribute set to mock
            some behaviour of other models.
        """
        WrapObj = type('WrapObj', (), {})
        users = []
        for relation in relations:
            wo = WrapObj()
            wo.user = relation.start_user
            users.append(wo)

            wo = WrapObj()
            wo.user = relation.end_user
            users.append(wo)
        return users

    def _get_suggested_roommates(self, peers):
        return sorted(peers, key=lambda x: x.compatibility_score, reverse=True)[:settings.SUGGESTED_ROOMMATES_CNT]

    def _get_habits_questions(self, invite):
        return OrganizationSocialHabitQuestion.objects.filter(
            organization=invite.agreement_period.property.organization,
            status=True).prefetch_related('answers').order_by('sort_order')

    def _set_user_personality(self, user):
        """ Set user's personality attributes dict.
        """
        personality = []
        answers = UserOrganizationSocialHabitAnswers.objects.filter(userprofile=user.profile).order_by('sort_order')
        for answer in answers:
            personality.append((answer.organization_question_text, answer.organization_answer_text))

        personality.append({'': ''})
        user.profile.personality = zip(personality[::2], personality[1::2])

    def _set_users_personality(self, wrapped_users):
        """ Set user's personality attributes for all users in wrapped_users list.
            wrapped_users list contains entities which has a user attribute set.
        """
        for wrapped_user in wrapped_users:
            self._set_user_personality(wrapped_user.user)

    def _set_peers_relations(self, peers, relation_ids):
        """ Set has_relation attribute for each user which has any relations.
        """
        for peer in peers:
            peer.has_relation = (peer.user.id in relation_ids)

    def _set_peer_conversations(self, peers, user):
        """ Set conversation URL for each peer.
        """
        for peer in peers:
            try:
                conversation = UserConversation.objects.get(
                    Q(user1=user, user2=peer.user) | Q(user1=peer.user, user2=user)
                )
                peer.conversation_url = '/user-conversation/{}/'.format(conversation.conversation_uuid)
            except UserConversation.DoesNotExist:
                peer.conversation_url = '/user-conversation/{}/'.format(uuid4())

    def _set_peer_compatibility(self, peers, user):
        """ Calculate compatibility score and set appropriate progress bar style for each peer.
            Compatibility score progress bar style is used for the UI templates only.
        """
        user_personality_dict = compatibility.get_user_personality(user.profile)
        user_personality_ideal = compatibility.get_ideal_personality(user_personality_dict)
        for peer in peers:
            peer_personality_dict = compatibility.get_user_personality(peer.user.profile)
            peer_personality_ideal = compatibility.get_ideal_personality(peer_personality_dict)
            # distance between user's ideal and peer
            distance1 = compatibility.get_personalities_distance(user_personality_ideal, peer_personality_dict)

            # distance between peer's ideal and user
            distance2 = compatibility.get_personalities_distance(peer_personality_ideal, user_personality_dict)

            peer.compatibility_score = compatibility.get_compatibility_score(distance1, distance2)
            peer.compatibility_style = compatibility.get_compatibility_style(peer.compatibility_score)

    def post(self, request, *args, **kwargs):
        print("post main student dashboard view")
        form_data = request.POST
        try:
            end_user_id = int(form_data.get('end_user_id'))
        except ValueError:
            end_user_id = 0

        quiz_result_id = self.request.session.get('quiz_result_id')
        if quiz_result_id:
            quiz_result = QuizResult.objects.get(id=quiz_result_id)
            # There are some corner cases when it is possible to send an invite to himself
            if end_user_id and quiz_result.user.id != end_user_id:
                invite = Invitation.objects.filter(id=self.request.session.get('invite_id')).\
                    select_related('agreement_period__property', 'user', 'unit_type').first()

                max_occupancy = invite.unit_type.max_occupancy
                print("max occupancy: ")
                print(max_occupancy)
                current_group_size = get_group_size(group_members=get_related_user_ids(invite.user))
                print("current_group_size: ")
                print(current_group_size)
                max_allowed_group_size = max_occupancy - current_group_size
                print("max_allowed_group_size")
                print(max_allowed_group_size)
                try:
                    end_user = User.objects.get(id=end_user_id)
                except User.DoesNotExist:
                    raise SuspiciousOperation('Wrong user id')
                end_user_group_size = get_group_size(group_members=get_related_user_ids(end_user))
                print("end_user_group_size: ")
                print(end_user_group_size)
                if end_user_group_size > max_allowed_group_size:
                    raise SuspiciousOperation('Unable to set this user as roommate')
                relationship, created = Relationship.objects.get_or_create(
                    invitation=invite,
                    start_user=invite.user,
                    end_user=end_user,
                )
                action.send(invite.user, verb='sent suitemates request to', action_object=relationship.end_user,
                            target=invite.agreement_period.property)
        return HttpResponseRedirect(reverse('user_profile_v2'))


def update_bio(request):
    if not request.is_ajax() or request.method != 'POST':
        raise SuspiciousOperation('Incorrect request!')
    invite_id = request.session.get('invite_id', None)
    if not invite_id:
        raise SuspiciousOperation('Incorrect request!')
    invite = Invitation.objects.filter(id=invite_id).select_related('user', 'user__profile').first()
    if not invite:
        raise SuspiciousOperation('Incorrect request!')
    form = UserBioForm(instance=invite.user.profile, data=request.POST)
    if form.is_valid():
        form.save()
        return HttpResponse('Ok')
    raise SuspiciousOperation('Incorrect request!')


class InviteUpdateView(CheckInviteMixin, TemplateView):
    template_name = 'profile_v2.html'

    def dispatch(self, request, *args, **kwargs):
        print("INVITEUPDATEVIEW DISPATCHHHH")
        self.invite = Invitation.objects.filter(id=self.request.session.get('invite_id')).\
            select_related('agreement_period__property', 'user').first()
        return super(InviteUpdateView, self).dispatch(request, *args, **kwargs)

    def get(self, request, **kwargs):
        print("get invite update view")
        relationship = self._update_relationship(self.request.GET)
        if relationship:
            return HttpResponseRedirect(
                reverse('getting-started', kwargs={'invite_code': self.request.GET.get('invite_code')}))
        return HttpResponseRedirect(reverse('code_index'))

    def post(self, request, *args, **kwargs):
        print("POST")
        form_data = request.POST
        if form_data.get('delete'):
            self._delete_relationship(form_data)
        elif form_data.get('set_roommate'):
            self._set_roommate_relationship(form_data)
        else:
            self._update_relationship(form_data)
        return HttpResponseRedirect(reverse('user_profile_v2'))

    def _update_relationship(self, data):
        print("_update_relationship")
        relation_ship_id = data.get('relation_ship_id')
        if relation_ship_id:
            print("if relation ship id")
            try:
                relationship = Relationship.objects.get(id=data.get('relation_ship_id'))
                relationship.end_user_approved = (data.get('accepted') == "true")
                relationship.approval_datetime = datetime.now()
                relationship.save()
                print("got past relationship.save")
                if data.get('accepted') == "true":
                    print("accepted == true")
                    relation_invite = Invitation.objects.get(id=relationship.invitation_id)
                    start_user_exist = True
                    try:
                        start_user_unit = PendingAssignment.objects.get(user_id=relationship.start_user_id,assignment_period_id=relation_invite.agreement_period_id)
                        if not start_user_unit.unit:
                            start_user_exist = False
                    except PendingAssignment.DoesNotExist:
                        start_user_exist = False
                    try:
                        end_user_unit = PendingAssignment.objects.get(user_id=relationship.end_user_id,assignment_period_id=relation_invite.agreement_period_id)
                    except PendingAssignment.DoesNotExist:
                        end_user_unit = False
                    if start_user_exist:
                        if end_user_unit:
                            end_user_unit.unit = start_user_unit.unit
                            end_user_unit.save()
                        else:
                            invite = Invitation.objects.get(user=relationship.end_user)
                            user_assignment_create = PendingAssignment.objects.get_or_create(user=relationship.end_user, assignment_period=start_user_unit.assignment_period, invite=invite, room_type=start_user_unit.room_type, unit=start_user_unit.unit)
                break_unnecessary_relations(relationship.start_user)
                break_unnecessary_relations(relationship.end_user)
                action.send(self.invite.user, verb='added to suitemates', action_object=relationship.start_user if
                            relationship.start_user != self.invite.user else relationship.end_user,
                            target=self.invite.agreement_period.property)
                return relationship
            except Relationship.DoesNotExist:
                return None

    def _delete_relationship(self, data):
        print("delete_relationship")
        print(data)
        relation_ship_id = data.get('relation_ship_id')
        print(relation_ship_id)
        if relation_ship_id:
            try:
                relationship = Relationship.objects.get(id=data.get('relation_ship_id'))
                print(relationship)
                print(relationship.group_id)
                print(self.invite.user)
                print(relationship.start_user)
                print(relationship.end_user)
                action.send(self.invite.user, verb='deleted from suitemates', action_object=relationship.start_user if
                            relationship.start_user != self.invite.user else relationship.end_user,
                            target=self.invite.agreement_period.property)
                print(relationship.id)
                relationship.delete_single()
            except Relationship.DoesNotExist:
                return None

    def _set_roommate_relationship(self, data):
        relation_ship_id = data.get('relation_ship_id')
        if relation_ship_id:
            try:
                relationship = Relationship.objects.get(id=data.get('relation_ship_id'))
                relation_type = data.get('relation_type', None)
                if relation_type:
                    relationship.set_roommate_relationship(relation_type)
                else:
                    relationship.set_roommate_relationship()
                if relationship.is_roommate == Relationship.DIRECT_ROOMMATE:
                    action.send(self.invite.user, verb='added to roommates', action_object=relationship.start_user if
                    relationship.start_user != self.invite.user else relationship.end_user,
                                target=self.invite.agreement_period.property)
                    group = Group.objects.filter(users__in=[relationship.start_user, relationship.end_user],
                                                 agreement_period=relationship.invitation.agreement_period).first()
                    if group:
                        group.group_roommates()
                elif relationship.is_roommate == Relationship.NOT_ROOMMATE:
                    action.send(self.invite.user, verb='rejected roommate request from', action_object=relationship.start_user
                                if relationship.start_user != self.invite.user else relationship.end_user,
                                target=self.invite.agreement_period.property)
                else:
                    action.send(self.invite.user, verb='sent roommate request to', action_object=relationship.start_user
                                if relationship.start_user != self.invite.user else relationship.end_user,
                                target=self.invite.agreement_period.property)
            except Relationship.DoesNotExist:
                return None


class QuizStartView(CheckInviteMixin, TemplateView):
    template_name = 'quiz.html'

    def get_context_data(self, **kwargs):
        context = super(QuizStartView, self).get_context_data(**kwargs)
        invite = Invitation.objects.get(invite_code=kwargs.get('invite_code'))

        if invite:
            # Authenticate
            user = User.objects.get(username=invite.user.username)  # Probably do some sort of token email
            user.backend = None
            login(self.request, user)

            obj_manager_result = QuizResult.objects.get_or_create(user=user)
            quiz_result = obj_manager_result[0]

            traitify = Traitify(secret_key=settings.TRAITIFY_SECRET_KEY, host=settings.TRAITIFY_HOST)
            if not quiz_result.traitify_assessment_id:
                assessment = traitify.create_assessment(settings.TRAITIFY_DECK)
                quiz_result.traitify_assessment_id = assessment.id
                quiz_result.save()
            else:
                assessment = traitify.get_assessment(quiz_result.traitify_assessment_id)
            self.request.session['invite_id'] = invite.id
            self.request.session['quiz_result_id'] = quiz_result.id
            context['assessment'] = assessment
            context['quiz'] = quiz_result

            peer_invites = Invitation.objects.filter(
                agreement_period=invite.agreement_period
            ).exclude(
                user=invite.user
            )

            context["peer_invites"] = peer_invites
            return context

        return Http404()


#  ToDo: Replace this big form with django form
class QuizProfileInformation(CheckInviteMixin, TemplateView):
    template_name = 'quiz_profile.html'

    def get_context_data(self, **kwargs):
        context = super(QuizProfileInformation, self).get_context_data(**kwargs)
        invite = Invitation.objects.filter(id=self.request.session['invite_id']).\
            select_related('user', 'agreement_period__property').first()
        if invite:
            self._get_other_social_networks(invite)
            self._get_property_social_networks(invite)
            user = User.objects.get(email=invite.user.email)  # Probably do some sort of token email
            obj_manager_result = QuizResult.objects.get_or_create(user=user)

            quiz_result = obj_manager_result[0]

            self.request.session['invite_id'] = invite.id
            self.request.session['quiz_result_id'] = quiz_result.id

            questions = self._get_organization_questions(invite)
            questions = self._get_question_answers(questions, user)

            context['quiz'] = quiz_result
            context['invite'] = invite
            context['questions'] = questions
            context['user_form'] = UserInformationForm(instance=invite.user)
            #context['user_profile_form'] = UserProfileInformationForm(instance=invite.user.profile)
            return context

        return Http404()

    def post(self, request, *args, **kwargs):
        form_data = request.POST
        invite = Invitation.objects.get(id=self.request.session['invite_id'])
        user_form = UserInformationForm(request.POST, instance=invite.user)
        user_profile_form = UserProfileInformationForm(request.POST, instance=invite.user.profile)
        if all([user_form.is_valid(), user_profile_form.is_valid()]):
            user_form.save()
            user_profile_form.save()

        userprofile = invite.user.profile
        userprofile.bio = form_data.get('bio')
        userprofile.save()

        self._update_other_social_networks(form_data, userprofile)
        self._archive_previous_answers(invite)
        self._update_social_habits(form_data, invite)

        action.send(invite.user, verb='updated profile', target=invite.agreement_period.property)

        redirect = request.GET.get('redirect', reverse('quiz_slide'))
        return HttpResponseRedirect(redirect)

    def _get_other_social_networks(self, invite):
        other_networks = list(SocialNetworks.objects.filter(userprofile=invite.user.profile))
        invite.other_networks = other_networks

    def _get_property_social_networks(self, invite):
        social_networks = PropertySocialNetwork.objects.filter(property=invite.agreement_period.property)
        exitsnetwork = []
        for i in invite.other_networks:
					exitsnetwork.append(i.network_name)
        for i in social_networks:
					if i.network_name not in exitsnetwork:
						invite.other_networks.append(i)
        invite.other_networks.reverse()
        
    def _get_organization_questions(self, invite):
        orgquest = OrganizationSocialHabitQuestion.objects.filter(
            organization=invite.agreement_period.property.organization,
            status=True).order_by('sort_order')
        return orgquest

    def _get_question_answers(self, questions, user):
        user_answers = user.profile.answers.values('organization_question_id', 'organization_answer_id')
        return_questions = []
        for question in questions:
            these_answers = []
            new_answers = OrganizationSocialHabitAnswer.objects.filter(
                question=question,
                status=True).order_by('sort_order')
            for each_answer in new_answers:
                these_answers.append({
                        'id': each_answer.id,
                        'answer': each_answer.answer
                    })

            print("question {} has {} answers".format(question.id, len(new_answers)))
            print(dir(question))

            try:
                user_answer = filter(lambda a: a['organization_question_id'] == question.id,
                                         user_answers)[0]['organization_answer_id']
            except IndexError:
                user_answer = None
                #question.user_answer = user_answer
            #question.answers = new_answers
            return_questions.append({
                    'id': question.id,
                    'required': question.required,
                    'question': question.question,
                    'answers': these_answers,
                    'user_answer': user_answer
                })
        return return_questions

    def _update_other_social_networks(self, form_data, userprofile):
        for network_name, network_url in zip(form_data.getlist('network_name'), form_data.getlist('network_url')):
            if not network_name:
                continue
            try:
                social_network = SocialNetworks.objects.get(userprofile=userprofile, network_name=network_name)
                if not network_url:
                    social_network.delete()
                else:
                    social_network.network_url = network_url
                    social_network.save()
            except SocialNetworks.DoesNotExist:
                if network_name and network_url:
                    SocialNetworks.objects.create(
                        userprofile=userprofile,
                        network_name=network_name,
                        network_url=network_url)

    def _update_social_habits(self, form_data, invite):
        answers_time = datetime.utcnow()
        for key, value in form_data.items():
            if not key.startswith('question-'):
                continue
            if not value:
                continue
            if len(key.split('-')) < 2 or len(value.split('-')) < 2:
                raise SuspiciousOperation("Wrong answer id")
            question_id = int(key.split('-')[-1])
            answer_id = int(value.split('-')[-1])
            question = OrganizationSocialHabitQuestion.objects.get(id=question_id)
            answer = OrganizationSocialHabitAnswer.objects.get(id=answer_id)
            UserOrganizationSocialHabitAnswers.objects.create(
                userprofile=invite.user.profile,
                invitation=invite,
                organization_question=question,
                organization_question_text=question.question,
                organization_answer=answer,
                organization_answer_text=answer.answer,
                sort_order=question.sort_order,
                created_at=answers_time)

    def _archive_previous_answers(self, invite):
        """ Archive old user's answers if he\she takes a new quiz.
        """
        archive_time = datetime.utcnow()
        old_answers = UserOrganizationSocialHabitAnswers.objects.filter(
            userprofile=invite.user.profile).order_by('sort_order')
        for old_answer in old_answers:
            UserSocialHabitAnswersHistory.objects.create(
                user_id=invite.user.id,
                organization_id=invite.agreement_period.property.organization.id,
                invitation_id=invite.id,
                question_id=old_answer.organization_question.id,
                answer_id=old_answer.organization_answer.id,
                answered_at=old_answer.created_at,
                question_text=old_answer.organization_question_text,
                answer_text=old_answer.organization_answer_text,
                archived_at=archive_time)
            old_answer.delete()


class QuizUpdateSlideView(CheckInviteMixin, TemplateView):
    template_name = 'quiz_slide.html'

    def get_context_data(self, **kwargs):
        context = super(QuizUpdateSlideView, self).get_context_data(**kwargs)

        quiz_result_id = self.request.session.get('quiz_result_id')
        if quiz_result_id:
            quiz_result = QuizResult.objects.get(id=quiz_result_id)
            traitify = Traitify(secret_key=settings.TRAITIFY_SECRET_KEY, host=settings.TRAITIFY_HOST)
            slides = traitify.get_slides(quiz_result.traitify_assessment_id)
            context['question_count'] = len(slides)

            for slide in slides:
                if slide.response is None:
                    context['slide'] = slide
                    context['percent'] = str(float(slide.position) / float(len(slides)) * 100)
                    return context
        return context

    def post(self, request, *args, **kwargs):
        form_data = request.POST

        quiz_result_id = self.request.session.get('quiz_result_id')
        if quiz_result_id:
            quiz_result = QuizResult.objects.get(id=quiz_result_id)
            traitify = Traitify(secret_key=settings.TRAITIFY_SECRET_KEY, host=settings.TRAITIFY_HOST)
            invite = Invitation.objects.get(id=self.request.session['invite_id'])
            slides = traitify.get_slides(quiz_result.traitify_assessment_id)
            for slide in slides:
                if slide.id == form_data.get('slide_id'):
                    slide.response = (form_data.get('selection') == "True")
                    slide.time_taken = form_data.get('timer', 1000)
                    traitify.update_slide(quiz_result.traitify_assessment_id, slide)
                    if slides.index(slide) == len(slides) - 1:
                        profile = quiz_result.user.profile
                        profile.personality_types = pickle.dumps(
                            traitify.get_personality_types(quiz_result.traitify_assessment_id)).decode('ascii', 'ignore')
                        profile.personality_traits = pickle.dumps(
                            traitify.get_personality_traits(quiz_result.traitify_assessment_id)).decode('ascii', 'ignore')
                        profile.save()
                        action.send(invite.user, verb='finished quiz', target=invite.agreement_period.property)
                        return HttpResponseRedirect(reverse('user_profile_v2'))
            return HttpResponseRedirect(reverse('quiz_slide'))

        return Http404()


class QuizFinishedView(TemplateView):
    template_name = 'quiz.html'

    def get_context_data(self, **kwargs):
        traitify = Traitify(secret_key=settings.TRAITIFY_SECRET_KEY, host=settings.TRAITIFY_HOST)
        context = super(QuizFinishedView, self).get_context_data(**kwargs)

        quiz_result_id = self.request.session.get('quiz_result_id')
        if quiz_result_id:
            quiz_result = QuizResult.objects.get(id=quiz_result_id)
            if not quiz_result.result_json:
                assessment = traitify.get_assessment(quiz_result.traitify_assessment_id)
                quiz_result.result_json = json.dumps(assessment.__dict__)
                quiz_result.save()

            context['personality_types'] = traitify.get_personality_types(quiz_result.traitify_assessment_id)
            context['careers'] = traitify.career_matches(quiz_result.traitify_assessment_id)
        return context


# ToDo: Maybe need to remove this, I don't know where it used
class RoomMateMatchView(CheckInviteMixin, TemplateView):
    template_name = 'roommate_dashboard.html'
    traitify = None

    def get_context_data(self, **kwargs):
        context = super(RoomMateMatchView, self).get_context_data(**kwargs)

        self.traitify = Traitify(secret_key=settings.TRAITIFY_SECRET_KEY, host=settings.TRAITIFY_HOST)

        quiz_result = QuizResult.objects.get(id=self.request.session.get('quiz_result_id'))
        invite = Invitation.objects.get(id=self.request.session.get('invite_id'))
        org_matched_users = Invitation.objects.filter(agreement_period=invite.agreement_period).exclude(
            user=invite.user
        ).values('user__id')

        approved_users = Relationship.objects.filter(
            invitation=invite,
            start_user=invite.user,
            end_user_approved=True,
        )

        pending_users = Relationship.objects.filter(
            invitation=invite,
            start_user=invite.user,
            approval_datetime__isnull=True,
        )

        approved_users = [user.end_user.id for user in approved_users]
        pending_users = [user.end_user.id for user in pending_users]

        context['approved_relationships'] = self._get_users(approved_users)
        context['pending_relationships'] = self._get_users(pending_users)

        unavailible_users = []
        unavailible_users.extend(approved_users)
        unavailible_users.extend(pending_users)

        context['matches'] = self._get_users([user.get('user__id') for user in org_matched_users])

        return context

    def _get_users(self, users, exclude_users=()):
        matches = QuizResult.objects.filter(
            user__in=users,
        )
        if len(exclude_users) > 0:
            matches = matches.exclude(
                user__in=exclude_users
            )
        matches = list(matches)
        for result in matches:
            try:
                result.personality_types = self.traitify.get_personality_types(result.traitify_assessment_id)
            except:
                matches.pop(matches.index(result))
        return matches

    def post(self, request, *args, **kwargs):
        form_data = request.POST

        quiz_result_id = self.request.session.get('quiz_result_id')
        if quiz_result_id:
            quiz_result = QuizResult.objects.get(id=quiz_result_id)
            relationship, created = Relationship.objects.get_or_create(
                invitation_id=self.request.session.get('invite_id'),
                start_user=quiz_result.user,
                end_user_id=form_data.get('end_user_id'),
            )

            return HttpResponseRedirect(reverse('roommate_selection'))

        return Http404()


class UserConversationView(TemplateView):
    def get(self, request, *args, **kwargs):
        conversation_uuid = kwargs.get('conversation_uuid')

        conversation = UserConversation.objects.filter(
            conversation_uuid=conversation_uuid,
        ).first()
        if conversation:
            data = self._serialize_messages(conversation)
        else:
            data = {
                'conversation_uuid': conversation_uuid,
                'messages': []
            }
        return JsonResponse(data)

    def post(self, request, *args, **kwargs):
        form_data = request.POST
        conversation_uuid = kwargs.get('conversation_uuid')
        user1_id = form_data.get('user1_id')
        user2_id = form_data.get('user2_id')
        message = form_data.get('message')

        if not message:
            # Empty messages aren't stored
            return HttpResponseRedirect(reverse('user_profile_v2'))

        try:
            user1 = User.objects.get(id=user1_id)
            user2 = User.objects.get(id=user2_id)
        except User.DoesNotExist:
            return Http404()

        conversation = UserConversation.objects.filter(
            conversation_uuid=conversation_uuid,
        ).first()

        if not conversation:
            conversation = UserConversation.objects.create(
                user1=user1,
                user2=user2,
                conversation_uuid=conversation_uuid,
            )

        ConversationMessage.objects.create(
            conversation=conversation,
            user=user1,
            message=message,
        )
        invite = Invitation.objects.filter(user=user1).select_related('agreement_period__property').first()
        action.send(user1, verb='sent message to', action_object=user2,
                    target=invite.agreement_period.property if invite else None)
        if request.is_ajax:
            return JsonResponse(self._serialize_messages(conversation))
        return HttpResponseRedirect(reverse('user_profile_v2'))

    @staticmethod
    def _serialize_messages(conversation):
        data = {
            'conversation_uuid': str(conversation.conversation_uuid),
            'messages': []
        }
        messages = ConversationMessage.objects.filter(
            conversation=conversation
        ).select_related('user').order_by('created_at')
        for message in messages:
            data['messages'].append({
                'userId': message.user.id,
                'userName': u'{} {}'.format(message.user.first_name, message.user.last_name),
                'message': message.message,
                'date': message.created_at,
            })
        return data


class PeerProfileAjaxView(CheckInviteMixin, TemplateView):
    template_name = 'user_profile_ajax.html'

    def get_context_data(self, **kwargs):
        context = super(PeerProfileAjaxView, self).get_context_data(**kwargs)

        try:
            peer_id = kwargs.get('peer_id')
            invite_id = self.request.session.get('invite_id', None)
            invite = Invitation.objects.filter(id=invite_id).select_related('assignment').first()
            if not invite:
                raise Invitation.DoesNotExist
            peer_user = User.objects.get(id=peer_id)
            # Peer user must be in the same organization, agreement period, property and group as the user
            peer_invite = Invitation.objects.filter(user=peer_user).select_related('assignment').first()
            if peer_invite:
                self._set_user_personality(peer_invite.user)
                self._set_peer_compatibility(peer_invite, invite.user)
                self._set_peer_conversation(peer_invite, invite.user)
                peer_invite.has_relation = self._check_relation(peer_invite.user, invite.user)
                context["peer"] = peer_invite
                #  check that the invitation of this user will not overflow group
                peer_unit = peer_invite.assignment.unit if hasattr(peer_invite, 'assignment') and peer_invite.assignment.unit \
                    else None
                user_unit = invite.assignment.unit if hasattr(invite, 'assignment') and invite.assignment.unit else None
                can_invite = False if peer_unit and user_unit and peer_unit != user_unit else True
                if can_invite:
                    max_group_allowed_size = invite.unit_type.max_occupancy - \
                                             get_group_size(get_related_user_ids(invite.user))
                    peer_group_size = get_group_size(get_related_user_ids(peer_invite.user))
                    can_invite = max_group_allowed_size >= peer_group_size
                if can_invite:
                    property_stage = invite.agreement_period.property.current_stage
                    if Stage.MATCH_ONLY_WITH_USERS_WITH_UNITS in property_stage.restrictions:
                        can_invite = hasattr(peer_invite, 'assignment') and peer_invite.assignment.unit is not None
                    elif invite.user.points < property_stage.min_points:
                        can_invite = False
                context["can_invite"] = can_invite
            else:
                raise Http404('Organization Invitation does not exist')
        except User.DoesNotExist:
            raise Http404('Peer user does not exist')

        return context

    def _check_relation(self, peer, user):
        if Relationship.objects.filter(Q(start_user=user, end_user=peer) | Q(start_user=peer, end_user=user)):
            return True

        return False

    def _set_user_personality(self, user):
        """ Set user's personality attributes dict.
        """
        personality = []
        answers = UserOrganizationSocialHabitAnswers.objects.filter(userprofile=user.profile).order_by('sort_order')
        for answer in answers:
            personality.append((answer.organization_question_text, answer.organization_answer_text))

        personality.append({'': ''})
        user.profile.personality = zip(personality[::2], personality[1::2])

    def _set_peer_compatibility(self, peer, user):
        user_personality_dict = compatibility.get_user_personality(user.profile)
        user_personality_ideal = compatibility.get_ideal_personality(user_personality_dict)

        peer_personality_dict = compatibility.get_user_personality(peer.user.profile)
        peer_personality_ideal = compatibility.get_ideal_personality(peer_personality_dict)
        # distance between user's ideal and peer
        distance1 = compatibility.get_personalities_distance(user_personality_ideal, peer_personality_dict)

        # distance between peer's ideal and user
        distance2 = compatibility.get_personalities_distance(peer_personality_ideal, user_personality_dict)

        peer.compatibility_score = compatibility.get_compatibility_score(distance1, distance2)
        peer.compatibility_style = compatibility.get_compatibility_style(peer.compatibility_score)

    def _set_peer_conversation(self, peer, user):
        conversation = UserConversation.objects.filter(
            Q(user1=user, user2=peer.user) | Q(user1=peer.user, user2=user)
        ).first()
        if conversation:
            peer.conversation_url = '/user-conversation/{}/'.format(conversation.conversation_uuid)
        else:
            peer.conversation_url = '/user-conversation/{}/'.format(uuid4())


#  ToDo: Replace this methods with RoomMateMatcher class
class SearchPeersAjaxView(CheckInviteMixin, TemplateView):
    template_name = 'search_peers_ajax.html'
    PAGE_SIZE = 20

    def get_context_data(self, **kwargs):
        context = super(SearchPeersAjaxView, self).get_context_data(**kwargs)
        search = kwargs.get('search', None)
        invite = kwargs.get('invite')
        if invite is None or not hasattr(invite.user, 'profile') or not invite.user.profile.personality_traits or \
                search is None:
            context['peers'] = []
        else:
            context['search_type'] = search.get('peers')
            # Filter search parameters
            search_name = search.get('name')
            search_habits = search.get('habits')

            try:
                current_group = Group.objects.get(agreement_period=invite.agreement_period, users__in=[invite.user])
            except Group.DoesNotExist:
                current_group = None

            print(current_group)

            if hasattr(invite, 'contract_type') and hasattr(invite.contract_type, 'name') and invite.contract_type.name == 'Private':
                private_contract = True
            else:
                private_contract = False

            peer_users = self._get_all_users(
                invite=invite, 
                exclude_users=self._get_no_quiz_users(), 
                user_group=current_group,
                private_only=private_contract)

            print(peer_users)

            users_already_related = Relationship.objects.filter(start_user=invite.user).values_list('end_user_id', flat=True)

            print(users_already_related)

            peer_users = peer_users.exclude(id__in=users_already_related)

            users_already_related = Relationship.objects.filter(end_user=invite.user).values_list('start_user_id', flat=True)

            print(users_already_related)

            peer_users = peer_users.exclude(id__in=users_already_related)

            print("same_unit_users: ")
            same_unit_users = self._get_sameunit_users(invite)



            print(same_unit_users)
            print("peer users before:")
            print(peer_users)
            peer_users = peer_users.exclude(id__in=same_unit_users)
            print("peer users after: ")
            print(peer_users)

            if search_name or search_habits:
                if search_name:
                    peer_users = self._filter_by_name(peer_users, search_name)

                if search_habits:
                    peer_user_ids = peer_users.values_list('id', flat=True)
                    search_habits = self._parse_search_habits(search_habits)
                    peer_user_ids = self._filter_by_habits(peer_user_ids, search_habits)
                    peer_users = User.objects.filter(id__in=peer_user_ids)

            peer_users = peer_users.prefetch_related(
                Prefetch('groups', Group.objects.filter(agreement_period=invite.agreement_period).
                         prefetch_related(
                    Prefetch('users', User.objects.prefetch_related(
                        Prefetch('invites',
                                 Invitation.objects.filter(agreement_period=invite.agreement_period).select_related(
                                     'contract_type')))))),
                Prefetch('invites', Invitation.objects.filter(agreement_period=invite.agreement_period).
                         select_related('contract_type'))
            ).select_related('profile')

            if search.get('peers') != 'suggested':
                try:
                    page = int(search.get('page', ''))
                except ValueError:
                    page = 1
                context['total_pages'] = range(1, len(peer_users) / self.PAGE_SIZE +
                                               (2 if len(peer_users) % self.PAGE_SIZE != 0 else 1))
                context['current_page'] = page
                offset = (page - 1) * self.PAGE_SIZE
                peer_users = peer_users[offset: offset + self.PAGE_SIZE]

            self._set_peer_compatibility(peer_users, invite.user)

            if search.get('peers') == 'suggested':
                peer_users = self._get_suggested_roommates(peer_users)

            relation_users_ids = current_group.users.values_list('id', flat=True) if current_group else []
            self._set_peers_relations(peer_users, relation_users_ids)
            self._set_peer_conversations(peer_users, invite.user)
            print(peer_users)
            
            context['peers'] = peer_users
            context['invite'] = invite

        return context

    def post(self, request, *args, **kwargs):
        search = json.loads(request.body)
        invite = self._get_user_invite(search.get('invite_id', 0))
        context = self.get_context_data(search=search, invite=invite)
        return self.render_to_response(context)

    def get(self, request, *args, **kwargs):
        print("get")
        search = {u'peers': u'suggested', u'habits': [], u'name': u'', u'page': 1}
        invite = self._get_user_invite(search.get('invite_id', 0))
        context = self.get_context_data(search=search, invite=invite)
        return self.render_to_response(context)

    def _get_user_invite(self, invite_id=0):
        # NOTE: invite_id must not be used here. It must be always taken from the session. It was added here because of
        # search tests.
        invite_id = self.request.session.get('invite_id') or invite_id
        return Invitation.objects.filter(id=invite_id).select_related('agreement_period__property__current_stage',
            'assignment', 'agreement_period', 'agreement_period__property').first()

    @staticmethod
    def _get_no_quiz_users():
        return UserProfile.objects.filter(personality_types__isnull=True).values_list('user', flat=True)

    def _set_peer_compatibility(self, peers, user):
        """ Calculate compatibility score and set appropriate progress bar style for each peer.
            Compatibility score progress bar style is used for the UI templates only.
        """
        user_personality_dict = compatibility.get_user_personality(user.profile)
        user_personality_ideal = compatibility.get_ideal_personality(user_personality_dict)
        for peer in peers:
            peer_personality_dict = compatibility.get_user_personality(peer.profile)
            peer_personality_ideal = compatibility.get_ideal_personality(peer_personality_dict)
            # distance between user's ideal and peer
            distance1 = compatibility.get_personalities_distance(user_personality_ideal, peer_personality_dict)

            # distance between peer's ideal and user
            distance2 = compatibility.get_personalities_distance(peer_personality_ideal, user_personality_dict)

            peer.compatibility_score = compatibility.get_compatibility_score(distance1, distance2)
            peer.compatibility_style = compatibility.get_compatibility_style(peer.compatibility_score)

    @staticmethod
    def _get_all_users(invite, exclude_users, user_group, private_only=False):
        """ 
        Get all users for invite organization, agreement period, property and group type.
        """
        print("_get_all_users")

        property_stage = invite.agreement_period.property.current_stage
        stage_max_points = invite.user.stage.max_points
        if invite.user.stage == property_stage:
            stage_max_points = 10000

        if user_group:
            print("if user_group")
            max_allowed_group_size = user_group.empty_places
            user_group_unit = user_group.unit
        else:
            print("else user_group")
            max_allowed_group_size = invite.unit_type.max_occupancy - (invite.contract_type.occupied_places if invite.contract_type else 1 )
            print(max_allowed_group_size)
            assignment = PendingAssignment.objects.\
                filter(assignment_period=invite.agreement_period, user=invite.user).first()
            user_group_unit = assignment.unit if assignment else None

        if max_allowed_group_size < 1:
            return User.objects.none()

        results = Invitation.objects.filter(agreement_period=invite.agreement_period, unit_type=invite.unit_type).\
            exclude(user_id__in=exclude_users).exclude(user=invite.user).filter(locked=False).\
            exclude(user__hidden=True).exclude(user__profile__personality_types__isnull=True).\
            exclude(contract_type__occupied_places__gt=max_allowed_group_size)

        print(user_group)
        if user_group: # this failing is where we are getting duplicates
            print("if user_group again")
            #print(results)
            user_group_users = user_group.users.all()
            print(user_group_users)
            results = results.exclude(user__in=user_group_users)
            #print(results)

        results = results.filter(user__points__lte=stage_max_points)

        if Stage.MATCH_INSIDE_UNIT in property_stage.restrictions:
            return User.objects.none()
        if Stage.MATCH_ONLY_WITH_USERS_WITH_UNITS in property_stage.restrictions:
            results = results.filter(assignment__unit__isnull=False)
            print(results)

        enable_co_educational_groups = invite.agreement_period.property.enable_co_educational_groups
        if enable_co_educational_groups and invite.user.allow_co_educational_groups:
            results = results.filter(Q(user__gender=invite.user.gender) |
                                                Q(
                                                    ~Q(user__gender=invite.user.gender),
                                                    user__allow_co_educational_groups=True,
                                                ))
        else:
            results = results.filter(user__gender=invite.user.gender)

        if private_only:
            private_unavailable = []
            for each_invite in results:
                print("if private_only")
                if hasattr(each_invite, 'assignment') and hasattr(each_invite.assignment, 'unit'):
                    unit = each_invite.assignment.unit
                    if not unit.private_room_available(invite.agreement_period):
                        private_unavailable.append(each_invite.id)

            results = results.exclude(id__in=private_unavailable)

        potential_roommates = results.values_list('user', flat=True)
        users = User.objects.filter(id__in=potential_roommates)
        users = users.filter(Q(groups__agreement_period=invite.agreement_period) | Q(groups__isnull=True))

        if user_group_unit:
            users = users.filter(Q(groups__isnull=True) | Q(groups__unit__isnull=True, groups__size__lte=F('groups__unit_type__max_occupancy') - max_allowed_group_size))
        else:
            users = users.filter(Q(groups__isnull=True) | Q(groups__size__lte=F('groups__unit_type__max_occupancy') - max_allowed_group_size))

        return users

    def _get_sameunit_users(self, user_invite):
        print("_get_sameunit_users")
        unituser=[]
        userunit = PendingAssignment.objects.filter(
                user=user_invite.user,
                assignment_period=user_invite.agreement_period
            ).exclude(
                unit__isnull=True
            ).select_related(
                'unit',
                'room',
                'bed').first()
        if userunit:
            unituser = PendingAssignment.objects.filter(unit=userunit.unit).exclude(
                user=user_invite.user
            ).select_related('user').values_list('user__id', flat=True)

        return unituser

    def _get_suggested_roommates(self, peers):
        return sorted(peers, key=lambda x: x.compatibility_score, reverse=True)[:settings.SUGGESTED_ROOMMATES_CNT]

    def _set_peers_relations(self, peers, relation_users_ids):
        """ Set has_relation attribute for each user which has any relations.
        """
        for user in peers:
            user.has_relation = (user.id in relation_users_ids)

    def _set_peer_conversations(self, peers, user):
        """ Set conversation URL for each peer.
        """
        for peer in peers:
            conversation = UserConversation.objects.filter(
                Q(user1=user, user2=peer) | Q(user1=peer, user2=user)
            ).first()
            if conversation:
                peer.conversation_url = '/user-conversation/{}/'.format(conversation.conversation_uuid)
            else:
                peer.conversation_url = '/user-conversation/{}/'.format(uuid4())

    def _filter_by_name(self, peers, search_name):
        """ Filter all or suggested peers by name
        """
        queryset = peers
        if search_name:
            search_fields = ['first_name', 'last_name', 'email']
            orm_lookups = ["{}__icontains".format(search_field) for search_field in search_fields]
            for name_part in search_name.split():
                or_queries = [Q(**{orm_lookup: name_part}) for orm_lookup in orm_lookups]
                queryset = queryset.filter(reduce(operator.or_, or_queries))
        return queryset

    def _parse_search_habits(self, search_habits):
        """ Parse search habits represented as pair of strings like ('question-1', 'answer-1) into dict of lists.
            For example:
            {1: [2, 3, 4], 5: [6], 7: [8, 9]}
            here 1, 5, 7 are question IDs, 2, 3, 4, 6, 8, 9 are appropriate answers IDs.
        """
        parsed_habits = defaultdict(list)
        for search_habit in search_habits:
            try:
                question, answer = search_habit
                if 'question-' not in question:
                    continue
                if 'answer-' not in answer:
                    continue
                question_id = int(question.split('-')[-1])
                answer_id = int(answer.split('-')[-1])
                parsed_habits[question_id].append(answer_id)
            except ValueError:
                continue

        return parsed_habits

    def _filter_by_habits(self, peer_ids, search_habits):
        """ Filter users by their social habits.
            Search habits are represented as a dict of lists. See _parse_search_habits method for details.

            Filter logic is follow:
            - several answers for one question are grouped by OR
            - answers grouped by AND
            This way quesries like this are possible
            'Q:Interests - Cooking' OR 'Q:Interests - Sport' AND 'Q:Smoking - No' (NOTE: this isn't an actual query, the
            real query consists of IDs only)
        """
        result = []
        peers_result = []

        # NOTE: code below is a manual python-based filtering. SQL must be the much better solution but it doesn't work
        # for me at the moment.
        search_query = UserOrganizationSocialHabitAnswers.objects.filter(userprofile__user_id__in=peer_ids)
        answers_grp = defaultdict(list)
        for answer in search_query:
            answers_grp[answer.userprofile_id].append(answer.organization_answer_id)

        for user_id, answers_list in answers_grp.items():
            good_user = True
            for q_id, a_list in search_habits.items():
                if not set(a_list).intersection(answers_list):
                    good_user = False
                    break
            if good_user:
                peers_result.append(user_id)

        for userprofile in UserProfile.objects.filter(id__in=peers_result):
            result.append(userprofile.user.id)

        return result


class UserMessagesAjax(CheckInviteMixin, ListView):
    model = UserConversation
    template_name = 'user_messages_ajax.html'
    context_object_name = 'messages'
    paginate_by = 5

    def get_queryset(self):
        invite = Invitation.objects.filter(id=self.request.session['invite_id']).first()
        if invite:
            user_conversations = UserConversation.objects.filter(Q(user1=invite.user) | Q(user2=invite.user))
            return ConversationMessage.objects.filter(conversation__in=user_conversations).exclude(user=invite.user).\
                select_related('conversation').order_by('unread', '-created_at')
        else:
            raise PermissionDenied

    def get_context_data(self, **kwargs):
        context = super(UserMessagesAjax, self).get_context_data(**kwargs)
        start = (context['page_obj'].number - 1) * context['paginator'].per_page
        end = context['page_obj'].number * context['paginator'].per_page
        context['start'] = start if start > 0 else (0 if context['paginator'].count == 0 else 1)
        context['end'] = end if end < context['paginator'].count else context['paginator'].count
        return context


class SelectUnitBedMixin(object):
    def _get_invite(self):
        invite = Invitation.objects.filter(id=self.request.session['invite_id']).\
            select_related('unit_type', 'agreement_period', 'agreement_period__property',
                           'agreement_period__property__current_stage').first()
        if not invite:
            raise PermissionDenied
        return invite

    def _get_available_units(self, invite, group=None):
        if group:
            units = group.allowed_units()
        else:
            units = Unit.objects.filter(unit_type=invite.unit_type)
            blocked_beds = invite.agreement_period.blocked_beds.values_list('id', flat=True)
            units_with_blocked_beds = units.annotate(
                blocked_beds=Sum(
                    Case(
                        When(
                            rooms__beds__id__in=blocked_beds,
                            then=1
                        ),
                        default=0,
                        output_field=models.IntegerField()
                    )
                ),
            ).values('id', 'blocked_beds').order_by('id')
            units_with_free_places = units.annotate(
                free_places=F('unit_type__max_occupancy') - Sum(
                    Case(
                        When(
                            pending_assignments__assignment_period=invite.agreement_period,
                            then=F('pending_assignments__invite__contract_type__occupied_places'),
                        ),
                        default=0,
                        output_field=models.IntegerField()
                    )
                )
            ).values('id', 'free_places').order_by('id')
            units_ids = []
            for blocked_beds, free_places in zip(units_with_blocked_beds, units_with_free_places):
                if free_places['free_places'] - blocked_beds['blocked_beds'] > 0:
                    units_ids.append(free_places['id'])
            units = units.filter(id__in=units_ids)

        # TODO occupied list like following is not needed right now.
        # occupied_units = PendingAssignment.objects.filter(assignment_period=invite.agreement_period,
        #                                                   unit__isnull=False).values_list('unit', flat=True)
        # units = units.exclude(id__in=occupied_units)
        units = units.order_by('name')
        return units

    def _get_group(self, invite):
        all_groups = Group.objects.filter(agreement_period=invite.agreement_period)
        group = all_groups.filter(leader=invite.user).first()
        if not group:
            if all_groups.filter(users__in=[invite.user]).exists():
                raise PermissionDenied("You are not group leader")
        return group

    def _get_available_units_by_size(self, invite, size):
        """
        Get units with free spaces within invite agreement period
        :param invite: user invite info
        :param size: free space size
        :return: available units
        """
        units = Unit.objects.filter(unit_type=invite.unit_type)
        blocked_beds = invite.agreement_period.blocked_beds.values_list('id', flat=True)
        units_with_blocked_beds = units.annotate(
            blocked_beds=Sum(
                Case(
                    When(
                        rooms__beds__id__in=blocked_beds,
                        then=1
                    ),
                    default=0,
                    output_field=models.IntegerField()
                )
            ),
        ).values('id', 'blocked_beds').order_by('id')
        units_with_free_places = units.annotate(
            free_places=F('unit_type__max_occupancy') - Sum(
                Case(
                    When(
                        pending_assignments__assignment_period=invite.agreement_period,
                        then=F('pending_assignments__invite__contract_type__occupied_places'),
                    ),
                    default=0,
                    output_field=models.IntegerField()
                )
            )
        ).values('id', 'free_places').order_by('id')
        units_ids = []
        for blocked_beds, free_places in zip(units_with_blocked_beds, units_with_free_places):
            if free_places['free_places'] - blocked_beds['blocked_beds'] > 0:
                unit = Unit.objects.prefetch_related(
                    Prefetch('rooms', Room.objects.prefetch_related(
                        Prefetch('beds', Bed.objects.order_by('name'))).order_by('name'))).get(id=free_places['id'])
                available_beds = self._get_available_beds(unit, invite)
                if len(available_beds) >= size:
                    units_ids.append(free_places['id'])
        units = units.filter(id__in=units_ids)
        units = units.order_by('name')
        return units

    def _get_available_beds(self, unit, invite):
        rooms = unit.rooms
        if False and invite.room_type:
            rooms = rooms.filter(Q(room_type=invite.room_type) | Q(room_type__isnull=True))
        rooms = rooms.all()

        places = invite.contract_type.occupied_places if invite.contract_type else 1
        if places == 1 and Relationship.objects.filter(invitation__agreement_period=invite.agreement_period).\
                filter(Q(start_user=invite.user) | Q(end_user=invite.user)).\
                filter(is_roommate=Relationship.DIRECT_ROOMMATE).exists():
            places = 2

        beds_list = map(lambda room: room.get_available_beds(invite.agreement_period), rooms)
        beds_list = filter(lambda beds: len(beds) >= places, beds_list)
        return list(chain(*beds_list))


class SelectUnitView(CheckInviteMixin, SelectUnitBedMixin, View):
    def get(self, request, *args, **kwargs):
        size = request.GET.get('size')
        invite = self._get_invite()
        group = self._get_group(invite)
        units = self._get_available_units(invite, group).values('id', 'name')
        if size:
            filtered_units = self._get_available_units_by_size(invite, int(size)).values('id', 'name')
            return JsonResponse(list(filtered_units), safe=False)
        else:
            return JsonResponse(list(units), safe=False)

    def post(self, request, *args, **kwargs):
        unit = request.POST.get('unit')
        if not unit:
            raise SuspiciousOperation('Unit not found')
        invite = self._get_invite()
        if Stage.CAN_SELECT_UNIT not in invite.agreement_period.property.current_stage.restrictions:
            raise SuspiciousOperation("You can't select Unit on this stage")
        units = self._get_available_units(invite)
        selected_unit = units.filter(id=unit).first()
        if not selected_unit:
            raise PermissionDenied("This unit occupied")
        return HttpResponse(selected_unit.name)

    def _get_invite(self):
        invite = Invitation.objects.filter(id=self.request.session['invite_id']).\
            select_related('unit_type', 'agreement_period', 'agreement_period__property',
                           'agreement_period__property__current_stage').first()
        if not invite:
            raise PermissionDenied
        return invite


class SelectBedView(CheckInviteMixin, SelectUnitBedMixin, View):
    def get(self, request, *args, **kwargs):
        unit = request.GET.get('unit')
        invite = self._get_invite()
        if not unit:
            raise SuspiciousOperation("Unit is required!")
        unit_id = int(unit)
        unit = Unit.objects.prefetch_related(
            Prefetch('rooms', Room.objects.prefetch_related(
                Prefetch('beds', Bed.objects.order_by('name'))).order_by('name'))).get(id=unit_id)
        available_beds = self._get_available_beds(unit, invite)
        rooms = unit.rooms.all()
        for room in rooms:
            for bed in room.beds.all():
                assignment = PendingAssignment.objects.filter(assignment_period=invite.agreement_period, bed=bed).\
                    select_related('user').first()
                bed.user = assignment.user if assignment else None
                bed.is_available = bed in available_beds
        beds = list(map(lambda room: SelectBedSerializer(room.beds.all(), many=True).data, rooms))
        return JsonResponse(beds, safe=False)

    def post(self, request, *args, **kwargs):
        bed_id = request.POST.get('bed')
        unit_id = request.POST.get('unit')
        if not unit_id:
            raise SuspiciousOperation('Unit not selected')
        if not bed_id:
            raise SuspiciousOperation('Bed not found')
        try:
            bed = Bed.objects.get(id=bed_id)
        except Bed.DoesNotExist:
            raise SuspiciousOperation('Bed not found')

        invite = self._get_invite()
        restrictions = invite.agreement_period.property.current_stage.restrictions
        if Stage.CAN_SELECT_UNIT not in restrictions:
            raise SuspiciousOperation("You can't select unit on this stage")
        if Stage.CAN_SELECT_BED not in restrictions:
            raise SuspiciousOperation("You can't select bed on this stage")

        group = self._get_group(invite)
        units = self._get_available_units(invite)
        selected_unit = units.filter(id=unit_id).first()
        if not selected_unit:
            raise PermissionDenied("This unit occupied")

        available_beds = self._get_available_beds(selected_unit, invite)
        if bed not in available_beds:
            raise PermissionDenied("This bed occupied")

        self._change_unit(selected_unit, invite, group)
        self._set_bed(bed, invite)

        return HttpResponse(bed.name)

    def _set_bed(self, bed, invite):
        current_assignments = PendingAssignment.objects.filter(assignment_period=invite.agreement_period)
        current_assignments.filter(user=invite.user).update(bed=bed, room=bed.room)
        group = Group.objects.filter(users__in=[invite.user], agreement_period=invite.agreement_period).first()
        if group:
            group.group_roommates(invite.user)

    def _change_unit(self, selected_unit, invite, group):
        if group:
            group.change_unit(selected_unit)
        else:
            user_assignment = PendingAssignment.objects.filter(invite=invite).first()
            if not user_assignment:
                user_assignment_create = PendingAssignment.objects.get_or_create(user=invite.user,
                                                                                 assignment_period=invite.agreement_period,
                                                                                 invite=invite)
                user_assignment = user_assignment_create[0]

            if user_assignment:
                user_assignment.unit = selected_unit
                user_assignment.room = None
                user_assignment.bed = None
                user_assignment.save()
            else:
                raise SuspiciousOperation("PendingAssignment does not exists")


class GoogleVerification(TemplateView):
    template_name = 'google0f68347ff74b158a.html'
