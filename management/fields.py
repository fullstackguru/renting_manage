from django import forms
from django.forms import models
from django.forms.fields import ChoiceField
from django.contrib.postgres.fields import ArrayField

from widgets import ChainedSelect


class ModelChoiceIteratorWithObj(models.ModelChoiceIterator):
    '''
    Works as a normal ModelChoiceIterator, but adds object to a tuple.
    '''
    def __iter__(self):
        if self.field.empty_label is not None:
            yield ("", self.field.empty_label, None)
        if self.field.cache_choices:
            if self.field.choice_cache is None:
                self.field.choice_cache = [
                    self.choice(obj) for obj in self.queryset.iterator()
                ]
            for choice in self.field.choice_cache:
                yield choice
        else:
            method = 'all' if self.queryset._prefetch_related_lookups else 'iterator'
            for obj in getattr(self.queryset, method)():
                yield self.choice(obj)

    def choice(self, obj):
        return self.field.prepare_value(obj), self.field.label_from_instance(obj), obj


class ChainedModelChoiceField(models.ModelChoiceField):
    '''
    This Field works like a normal ModelChoiceField, but it's uses custom ChainedSelect widget,
    and ModelChoiceIteratorWithObj for working with this widget. It's uses ChainedSelect widget for
    adding class attribute to option tags. (It used for chained selects with using
    https://github.com/tuupola/jquery_chained). It takes additional parameter - get_option_class_value.
    It is a function that accept only one obj argument (it takes object from initial queryset)
    and returns string which will be added to class attribute of option tag for specified obj.
    '''
    def __init__(self, queryset, get_option_class_value=None, get_label_from_instance=None, *args, **kwargs):
        super(ChainedModelChoiceField, self).__init__(queryset, *args, **kwargs)
        self.get_label_from_instance = get_label_from_instance
        if not kwargs.get('widget'):
            self.widget = ChainedSelect(get_option_class_value=get_option_class_value)

    def label_from_instance(self, obj):
        if self.get_label_from_instance:
            return self.get_label_from_instance(obj)
        return super(ChainedModelChoiceField, self).label_from_instance(obj)

    def _get_choices(self):
        if hasattr(self, '_choices'):
            return self._choices
        return ModelChoiceIteratorWithObj(self)

    choices = property(_get_choices, ChoiceField._set_choices)


class ChoiceArrayField(ArrayField):
    def formfield(self, **kwargs):
        defaults = {
            'form_class': forms.MultipleChoiceField,
            'choices': self.base_field.choices,
        }
        defaults.update(kwargs)
        return super(ArrayField, self).formfield(**defaults)

    def to_python(self, value):
        if isinstance(value, list):
            value = map(lambda val: self.base_field.to_python(val), value)
        return super(ChoiceArrayField, self).to_python(value)
