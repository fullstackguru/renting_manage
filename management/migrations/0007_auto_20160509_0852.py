# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('management', '0006_make_room_type_nullable'),
    ]

    operations = [
        migrations.AddField(
            model_name='property',
            name='email_subject',
            field=models.CharField(max_length=250, blank=True),
        ),
        migrations.AddField(
            model_name='property',
            name='from_name',
            field=models.CharField(max_length=250, blank=True),
        ),
    ]
