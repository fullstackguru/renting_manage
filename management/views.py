import urlparse
from datetime import datetime
import operator
import itertools
from functools import partial

from django.conf import settings
from django.contrib.auth import REDIRECT_FIELD_NAME, login, logout
from django.contrib.auth.decorators import login_required
from django.contrib.auth.forms import AuthenticationForm
from django.contrib.contenttypes.models import ContentType
from django.core.exceptions import PermissionDenied, SuspiciousOperation
from django.core.urlresolvers import reverse
from django.db.models import Prefetch, Q, Case, When, BooleanField
from django.forms.models import inlineformset_factory, ValidationError
from django.http import HttpResponse, HttpResponseBadRequest, JsonResponse
from django.shortcuts import get_object_or_404, redirect
from django.utils.decorators import method_decorator
from django.views.decorators.cache import never_cache
from django.views.decorators.csrf import csrf_protect
from django.views.generic.base import TemplateView, View
from django.views.generic.edit import FormView, CreateView, UpdateView, DeleteView, FormMixin, DeletionMixin
from django.views.generic.list import ListView
from django.http import QueryDict
from dbmail.models import MailTemplate
from actstream import action
from actstream.models import Action
from nested_formset import nestedformset_factory

from management.forms import InvitationManagerForm, UnitTypeManagerForm, RoomTypeManagerForm, \
    OrganizationSocialHabitQuestionManagerForm, OrganizationSocialHabitAnswerInline, ResidentImportForm, \
    MailTemplateManagerForm, StageForm, RealtyImportForm, UnitForm, RoomForm, BedForm, BedFormSet, RoomFormSet, \
    PropertySettingsForm, PropertySocialNetworkManagerForm
from management.models import Property, Invitation, UnitType, RoomType, OrganizationSocialHabitQuestion, \
    OrganizationSocialHabitAnswer, ResidentImport, PendingAssignment, AssignmentPeriod, Unit, Bed, Room, RealtyImport, \
    Stage, PropertySocialNetwork
from management.mixins import CSVResponseMixin, DataTableMixin, PropertyPermissionMixin, ImportViewMixin
from management.tasks import parse_invites, parse_realty
from management.management_filters import UserListFilter, UnitTypeFilter, RoomTypeFilter, GroupFilter, InviteFilter, \
    PropertySocialNetworkFilter
from utils.fake_group import FakeGroup
from management.actions import resend_invites, account_reminder_emails, generate_profile_report, send_custom_email, \
    export_users,delete_users
from serializers import UserSerializer, InvitationSerializer, UnitTypeSerializer, RoomTypeSerializer, MatchSerializer, \
    OrganizationSocialHabitQuestionSerializer, SuggestedRoommatesSerializer, ResidentImportRecordSerializer, \
    EmailSerializer, FeedSerializer, SearchResultSerializer, UnitSerializer, LiteUserSerializer, RealtyImportSerializer, \
    PropertySocialNetworkSerializer
from utils.compatibility import RoommateMatcher
from utils.services import lock_group
from utils.chart_builder import ChartBuilder
from website.forms import ManagerUserCreationForm, ManagerUserChangeForm
from website.models import User, Relationship, Group


class LoginView(FormView):
    """
    This is a class based version of django.contrib.auth.views.login.
    Usage:
        in urls.py:
            url(r'^login/$',
                AuthenticationView.as_view(
                    form_class=MyCustomAuthFormClass,
                    success_url='/my/custom/success/url/),
                name="login"),
    """
    form_class = AuthenticationForm
    redirect_field_name = REDIRECT_FIELD_NAME
    template_name = 'management/manager_login.html'

    @method_decorator(csrf_protect)
    @method_decorator(never_cache)
    def dispatch(self, *args, **kwargs):
        return super(LoginView, self).dispatch(*args, **kwargs)

    def form_valid(self, form):
        """
        The user has provided valid credentials (this was checked in AuthenticationForm.is_valid()). So now we
        can log him in.
        """
        login(self.request, form.get_user())
        return redirect(self.get_success_url())

    def get_success_url(self):
        if self.success_url:
            redirect_to = self.success_url
        else:
            redirect_to = self.request.REQUEST.get(self.redirect_field_name, '')

        netloc = urlparse.urlparse(redirect_to)[1]
        if not redirect_to:
            redirect_to = settings.LOGIN_REDIRECT_URL
        # Security check -- don't allow redirection to a different host.
        elif netloc and netloc != self.request.get_host():
            redirect_to = settings.LOGIN_REDIRECT_URL
        return redirect_to

    def set_test_cookie(self):
        self.request.session.set_test_cookie()

    def check_and_delete_test_cookie(self):
        if self.request.session.test_cookie_worked():
            self.request.session.delete_test_cookie()
            return True
        return False

    def get(self, request, *args, **kwargs):
        """
        Same as django.views.generic.edit.ProcessFormView.get(), but adds test cookie stuff
        """
        self.set_test_cookie()
        return super(LoginView, self).get(request, *args, **kwargs)

    def post(self, request, *args, **kwargs):
        """
        Same as django.views.generic.edit.ProcessFormView.post(), but adds test cookie stuff
        """
        form_class = self.get_form_class()
        form = self.get_form(form_class)
        print 'POST'
        if form.is_valid():
            self.check_and_delete_test_cookie()
            return self.form_valid(form)
        else:
            print form.errors
            self.set_test_cookie()
            return self.form_invalid(form)


class MainView(TemplateView):
    template_name = 'management/manager_base.html'

    def get_context_data(self, **kwargs):
        context = super(MainView, self).get_context_data(**kwargs)
        context["user"] = self.request.user
        properties = self.request.user.managed_properties.all()
        context["properties"] = properties
        return context

    def get(self, request, *args, **kwargs):
        """
        Same as django.views.generic.edit.ProcessFormView.get(), but adds test cookie stuff
        """
        if self.request.user.is_anonymous():
            return redirect('management:login')
        properties = self.request.user.managed_properties.all()
        if len(properties) > 0:
            return redirect('management:property', pk=properties[0].id)
        return super(MainView, self).get(request, *args, **kwargs)


class DashboardView(PropertyPermissionMixin, TemplateView):
    template_name = 'management/manager_dashboard.html'

    def get_context_data(self, **kwargs):
        context = super(DashboardView, self).get_context_data(**kwargs)
        context["user"] = self.request.user
        properties = self.request.user.managed_properties.all()
        context["properties"] = properties
        context['property'] = self.property
        context['periods'] = AssignmentPeriod.objects.filter(property=self.property)
        return context

    def get(self, request, *args, **kwargs):
        """
        Same as django.views.generic.edit.ProcessFormView.get(), but adds test cookie stuff
        """
        #properties = self.request.user.managed_properties.all()
        #if len(properties) == 1:
        #    return redirect('management:property', pk=properties[0].id)
        return super(DashboardView, self).get(request, *args, **kwargs)


class DashboardChartsView(PropertyPermissionMixin, View):
    chart_colors = ["#FF6384", "#36A2EB", "#FFCE56"]

    def get_students_chart(self, period):
        managers_ids = self.property.managers.values_list('id', flat=True)
        users_ids = Invitation.objects.filter(agreement_period=period).exclude(user_id__in=managers_ids).\
            values_list('user', flat=True)
        users = User.objects.filter(id__in=users_ids)
        chart = ChartBuilder(3, {'count': users.count()})
        chart.set_labels(['Quiz', 'Assignment', 'Roommate Request'])
        chart.set_data([
            users.filter(profile__personality_types__isnull=False).count(),
            PendingAssignment.objects.filter(unit__isnull=False, user_id__in=users_ids).count(),
            Relationship.objects.filter(start_user_id__in=users_ids, end_user_id__in=users_ids, end_user_approved=True,
                                        approval_datetime__isnull=False, is_roommate=Relationship.DIRECT_ROOMMATE).
                count() * 2
        ])
        chart.set_background_color(self.chart_colors)
        chart.set_hover_background_colors(self.chart_colors)
        return chart.build()

    def get_placement_chart(self, period):
        property_beds = Bed.objects.filter(room__unit__unit_type__property=self.property)
        blocked_beds = period.blocked_beds.all()
        blocked_beds_ids = blocked_beds.values_list('id', flat=True)
        assignments = PendingAssignment.objects.filter(assignment_period=period).exclude(bed_id__in=blocked_beds_ids)
        beds_with_assignments_ids = assignments.filter(bed__isnull=False).values_list('bed', flat=True)
        beds_with_assignments = Bed.objects.filter(id__in=beds_with_assignments_ids)
        open_beds = property_beds.exclude(id__in=blocked_beds_ids)\
            .exclude(id__in=beds_with_assignments_ids)
        chart = ChartBuilder(3, {'count': property_beds.count()})
        chart.set_labels(['Blocked', 'Open', 'Occupied'])
        chart.set_data([blocked_beds.count(), open_beds.count(), beds_with_assignments.count()])
        colors = ["#FF6384", "#46c37b", "#909090"]
        chart.set_background_color(colors)
        chart.set_hover_background_colors(colors)
        return chart.build()

    def get_stages(self):
        stages = Stage.objects.values('name').order_by('name')
        current_stage = self.property.current_stage
        chart = ChartBuilder(4)
        chart.set_data([1 for _ in range(0, stages.count())])
        chart.set_labels([stage['name'] for stage in stages])
        colors = []
        completed = True
        for stage in stages:
            if current_stage.name == stage['name']:
                colors.append('#f3b760')
                completed = False
                continue
            colors.append('#46c37b' if completed else '#909090')
        chart.set_background_color(colors)
        return chart.build()

    def get_real_estate(self):
        units = Unit.objects.filter(unit_type__property=self.property)
        rooms = Room.objects.filter(unit__in=units)
        beds = Bed.objects.filter(room__in=rooms)
        return {
            'units': units.count(),
            'rooms': rooms.count(),
            'beds': beds.count()
        }

    def get(self, request, *args, **kwargs):
        period_id = request.GET.get('period')
        if not period_id:
            raise SuspiciousOperation
        period = AssignmentPeriod.objects.filter(id=period_id, property=self.property).first()
        if not period:
            raise SuspiciousOperation
        charts = {
            'students': self.get_students_chart(period),
            'placements': self.get_placement_chart(period),
            'stages': self.get_stages(),
            'real_estate': self.get_real_estate()
        }
        return JsonResponse(charts)


class PropertyView(TemplateView):
    template_name = 'management/manager_properties.html'

    @method_decorator(login_required)
    def dispatch(self, request, *args, **kwargs):
        user = request.user
        user_properties = user.managed_properties.all().values_list('id', flat=True)
        prop = kwargs.get('pk')
        try:
            if int(prop) not in user_properties:
                raise PermissionDenied
        except ValueError:
            raise PermissionDenied
        return super(PropertyView, self).dispatch(request, *args, **kwargs)

    def get_context_data(self, **kwargs):
        context = super(PropertyView, self).get_context_data(**kwargs)
        property = get_object_or_404(Property, pk=kwargs['pk'])
        context["user"] = self.request.user
        properties = self.request.user.managed_properties.all()
        context["properties"] = properties
        context["property"] = property
        context['breadcrumbs'] = [
            {
                'name': property.name,
                'url': reverse('management:property', kwargs={'pk': property.id})
            }
        ]
        print property
        print context, "PROPERTY VIEW"
        return context

    # def get(self, request, **kwargs):
    #     # relationship = self._update_relationship(self.request.GET)
    #     # if relationship:
    #     #     return HttpResponseRedirect(
    #     #         reverse('getting-started', kwargs={'invite_code': self.request.GET.get('invite_code')}))
    #
    #     return HttpResponseRedirect('code_index')


class InvitationListView(PropertyPermissionMixin, DataTableMixin, CSVResponseMixin, ListView):
    model = Invitation
    template_name = 'management/manager_invitations.html'
    context_object_name = 'invitations'
    paginate_by = 100
    search_fields = ('user__email', 'user__first_name', 'user__last_name', 'assignment__unit__name')
    order_columns = ('agreement_period__name', 'assignment__unit__name', '', 'user__first_name', 'user__last_name',
                     'user__email', 'unit_type__name', 'room_type__name', 'user__gender')
    serializer_class = InvitationSerializer

    def get_csv_filename(self):
        prop = self.property
        if prop:
            return "{}_invitations_{}.csv".format(prop.name, datetime.now())
        else:
            return super(InvitationListView, self).get_csv_filename()

    def get(self, request, **kwargs):
        response_format = request.GET.get('format', None)
        if response_format == 'csv':
            queryset = self.get_queryset()
            data = [['Agreement Period', 'Unit', 'Room', 'Bed', 'First Name', 'Last Name', 'Email', 'Points',
                     'Unit Type', 'Room Type', 'Gender Restriction']]
            for invite in queryset:
                user = invite.user
                if hasattr(invite, 'assignment'):
                    unit = invite.assignment.unit.name if invite.assignment.unit else 'WAITLIST'
                    room = invite.assignment.room.name if invite.assignment.room else 'WAITLIST'
                    bed = invite.assignment.bed.name if invite.assignment.bed else 'WAITLIST'
                else:
                    unit = 'WAITLIST'
                    room = 'WAITLIST'
                    bed = 'WAITLIST'
                room_type = invite.room_type.name if invite.room_type else 'Open Room'
                data.append([invite.agreement_period.name, unit, room, bed, user.first_name, user.last_name, user.email,
                             user.points, invite.unit_type.name, room_type, user.get_gender_display()])
            return self.render_to_csv(data)
        else:
            return super(InvitationListView, self).get(request, **kwargs)

    def get_queryset(self):
        property_id = self.kwargs.get('property_id')
        return Invitation.objects.select_related('user').filter(agreement_period__property__id=property_id)\
            .select_related('unit_type', 'room_type', 'agreement_period', 'assignment', 'assignment__unit',
                            'assignment__bed', 'assignment__room')

    def get_context_data(self, **kwargs):
        context = super(InvitationListView, self).get_context_data(**kwargs)
        context["user"] = self.request.user
        properties = self.request.user.managed_properties.all()
        context["properties"] = properties
        context['property'] = self.property
        context['breadcrumbs'] = [
            {
                'name': 'Invitations',
                'url': reverse('management:invitation-list', kwargs={'property_id': self.kwargs.get('property_id')})
            }
        ]
        return context


class UserListView(PropertyPermissionMixin, DataTableMixin, CSVResponseMixin, ListView):
    model = User
    template_name = 'management/manager_users.html'
    context_object_name = 'users'
    search_fields = ('email', 'first_name', 'last_name', 'assignments__unit__name')
    order_columns = ('', 'email', 'first_name', 'last_name', 'last_login', 'assignments__unit__name')
    filters = [UserListFilter]
    serializer_class = UserSerializer
    actions = [
        ('Resend Invite', resend_invites,),
        ('Send Reminder Email', account_reminder_emails),
        ('Generate Profile Report', generate_profile_report),
        ('Export users', export_users),
        ('Delete users', delete_users),
    ]

    def get_actions(self, property):
        actions = self.actions[:]
        custom_emails = property.emails.exclude(category__name__in=settings.DB_MAILER_READ_ONLY_CATEGORIES)
        for email in custom_emails:
            actions.append(('Send ' + email.name, partial(send_custom_email, template_id=email.id)))
        return actions

    def get_csv_filename(self):
        prop = self.property
        if prop:
            return "{}_users_{}.csv".format(prop.name, datetime.now())
        else:
            return super(UserListView, self).get_csv_filename()

    def get(self, request, **kwargs):
        response_format = request.GET.get('format', None)
        if response_format == 'csv':
            queryset = self.get_queryset()
            data = [['Email', 'First Name', 'Last Name', 'Last Login', 'Unit', 'Room', 'Bed']]
            for user in queryset:
                assignment = user.assignments.first()
                if assignment:
                    unit = assignment.unit.name if assignment.unit else 'WAITLIST'
                    room = assignment.room.name if assignment.room else 'WAITLIST'
                    bed = assignment.bed.name if assignment.bed else 'WAITLIST'
                else:
                    unit = 'WAITLIST'
                    room = 'WAITLIST'
                    bed = 'WAITLIST'
                data.append([user.email, user.first_name, user.last_name, user.last_login, unit, room, bed])
            return self.render_to_csv(data)
        else:
            return super(UserListView, self).get(request, **kwargs)

    def post(self, request, **kwargs):
        '''
        POST requests used for processing actions, like send welcome email etc.
        '''
        property_id = self.kwargs.get('property_id')
        property = self.property
        actions = self.get_actions(property)
        try:
            action_id = int(request.POST.get('action', None))
            action = actions[action_id][1]
        except (ValueError, TypeError, KeyError):
            raise SuspiciousOperation('Incorrect action id!')
        all_users = request.POST.get('all', None)
        if all_users:
            queryset = User.objects.filter(available_properties__id__exact=property_id)
            queryset = self.filter(queryset)
            queryset = self.search(queryset)
            user_ids = queryset.values_list('id', flat=True)
        else:
            try:
                user_ids = request.POST.getlist('ids[]', None)
                user_ids = map(int, user_ids)
            except (ValueError, TypeError):
                raise SuspiciousOperation('Incorrect user ids!')
        try:
            response = action(request.user, user_ids, property)
            if response:
                return response
            return HttpResponse('Sent emails!')
        except TypeError:
            raise SuspiciousOperation('Incorrect action!')

    def get_queryset(self):
        managers = self.property.managers.all().values_list('id', flat=True)
        queryset = User.objects.filter(available_properties__exact=self.property).exclude(id__in=managers).\
            prefetch_related(Prefetch('assignments', queryset=PendingAssignment.objects.
                                      filter(assignment_period__property=self.property)
                                      .select_related('bed', 'room', 'user', 'unit')
                                      .order_by('assignment_period__end_date'))
                             )
        return queryset

    def get_context_data(self, **kwargs):
        context = super(UserListView, self).get_context_data(**kwargs)
        property = self.property
        properties = self.request.user.managed_properties.all()
        filters = UserListFilter(property=property, data=self.request.GET, queryset=User.objects.all())
        actions = self.get_actions(property)
        context['filters'] = filters
        context["user"] = self.request.user
        context["properties"] = properties
        context['property'] = property
        context['actions'] = actions
        context['breadcrumbs'] = [
            {
                'name': 'Users',
                'url': reverse('management:user-list', kwargs={'property_id': self.kwargs.get('property_id')})
            }
        ]
        return context


class UserUpdateView(PropertyPermissionMixin, UpdateView):
    model = User
    template_name = 'management/manager_user_update_form.html'
    form_class = ManagerUserChangeForm

    def dispatch(self, request, *args, **kwargs):
        self.invite_form = {}
        return super(UserUpdateView, self).dispatch(request, *args, **kwargs)

    def form_valid(self, form):
        invite_form = self.get_invite_form()
        if invite_form and not invite_form.is_valid():
            if (not invite_form.instance.pk and invite_form.cleaned_data['create']) or invite_form.instance.pk:
                return self.form_invalid(form)
        invite_form.save()
        obj = super(UserUpdateView, self).form_valid(form)
        action.send(self.request.user, verb='updated', action_object=form.instance, target=self.property)
        return obj

    def get_success_url(self):
        return reverse('management:user-update', kwargs={'property_id': self.kwargs.get('property_id'),
                                                         'pk': self.object.id})

    def get_invite_form(self):
        if not self.invite_form:
            period = self.property.current_agreement_period
            if period:
                invite = Invitation.objects.filter(agreement_period=period, user=self.object).last()
                params = {
                    'property': self.property,
                    'instance': invite if invite else Invitation(user=self.object, agreement_period=period),
                }
                if self.request.POST:
                    params['data'] = self.request.POST
                invite_form = InvitationManagerForm(**params)
                self.invite_form = invite_form
            else:
                self.invite_form = None
        return self.invite_form

    def get_context_data(self, **kwargs):
        context = super(UserUpdateView, self).get_context_data(**kwargs)
        context["user"] = self.request.user
        properties = self.request.user.managed_properties.all()
        context['property'] = self.property
        context["properties"] = properties
        context["title"] = "Update User"
        context["student"] = self.object
        context['invite_form'] = self.get_invite_form()
        context["period"] = self.property.current_agreement_period
        context['breadcrumbs'] = [
            {
                'name': 'Users',
                'url': reverse('management:user-list', kwargs={'property_id': self.kwargs.get('property_id')})
            },
            {
                'name': self.object.email,
                'url': reverse('management:user-update', kwargs={
                    'property_id': self.kwargs.get('property_id'),
                    'pk': self.object.id
                })
            }
        ]
        return context


class UserDeleteView(PropertyPermissionMixin, DeleteView):
    model = User

    def delete(self, request, *args, **kwargs):
        if request.is_ajax:
            obj = get_object_or_404(self.model, pk=kwargs['pk'])
            message = 'User {} was deleted!'.format(obj.email)
            action.send(self.request.user, verb='deleted user {}'.format(obj.email), target=self.property)
            obj.delete()
            return HttpResponse(message)
        else:
            raise SuspiciousOperation('Request must be AJAX')


class UserCreateView(PropertyPermissionMixin, CreateView):
    model = User
    template_name = 'management/manager_user_create_form.html'
    form_class = ManagerUserCreationForm

    def get_form_kwargs(self):
        kwargs = super(UserCreateView, self).get_form_kwargs()
        kwargs['property'] = self.property
        return kwargs

    def form_valid(self, form):
        obj = super(UserCreateView, self).form_valid(form)
        action.send(self.request.user, verb='created', action_object=form.instance, target=self.property)
        return obj

    def get_success_url(self):
        return reverse('management:user-update', kwargs={'property_id': self.kwargs.get('property_id'),
                                                         'pk': self.object.id})

    def get_context_data(self, **kwargs):
        context = super(UserCreateView, self).get_context_data(**kwargs)
        context["user"] = self.request.user
        properties = self.request.user.managed_properties.all()
        context['property'] = self.property
        context["properties"] = properties
        context["title"] = "Create User"
        context["submit"] = "Save"
        context['breadcrumbs'] = [
            {
                'name': 'Users',
                'url': reverse('management:user-list', kwargs={'property_id': self.kwargs.get('property_id')})
            },
            {
                'name': "Add user",
                'url': reverse('management:user-add', kwargs={'property_id': self.kwargs.get('property_id')})
            }
        ]
        return context


class MatchesListView(PropertyPermissionMixin, DataTableMixin, CSVResponseMixin, ListView):
    model = Invitation
    template_name = 'management/manager_matches.html'
    filters = [GroupFilter]
    context_object_name = 'matches'
    serializer_class = MatchSerializer

    def get_csv_filename(self):
        prop = self.property
        if prop:
            return "{}_match_report_{}.csv".format(prop.name, datetime.now())
        else:
            return super(MatchesListView, self).get_csv_filename()

    @staticmethod
    def __print_user(user):
        #print user
        if user.gender == User.GENDER_MALE:
            gender = "Male"
        elif user.gender == User.GENDER_FEMALE:
            gender = "Female"
        else:
            gender = "Unknown"
    
        return u"    {} ({}), {}".format(user.full_name, gender,
                                                                              user.email)

    def get(self, request, *args, **kwargs):
        if request.is_ajax():
            queryset = self.get_queryset()
            queryset = self.process_queryset(queryset)
            serializer = self.serializer_class(instance=queryset, many=True, context={'property': self.property})
            return JsonResponse({
                'data': serializer.data,
            }, safe=False)
        else:
            response_format = request.GET.get('format', None)
            if response_format == 'csv':
                queryset = self.get_queryset()
                queryset = self.process_queryset(queryset)
                data = [['Property', 'Agreement Period', 'Unit Type', 'Group Size', 'Proposed Group', 'Unit']]
                for group in queryset:
                    roommates = group.roommates
                    suitemates = group.suitemates
                    unit = group.unit
                    proposed_group = [u"Roommates:"]
                    for roommates_group in roommates:
                        for member in roommates_group:
                            proposed_group.append(self.__print_user(member))
                    proposed_group.append(u"Suite mates:")
                    for member in suitemates:
                        proposed_group.append(self.__print_user(member))
                    data.append([group.agreement_period.property.name, group.agreement_period.name,
                                 group.unit_type.name, group.size, '\n'.join(proposed_group), unit])
                return self.render_to_csv(data)
            else:
                return super(MatchesListView, self).get(request, **kwargs)

    def get_queryset(self):
        property = self.property
        groups = Group.objects.filter(agreement_period__property=property).\
            select_related('agreement_period', 'unit_type', 'unit').\
            prefetch_related('users', Prefetch('relations',
                                               queryset=Relationship.objects.select_related('start_user', 'end_user'))
        )
        return groups

    def process_queryset(self, queryset):
        params_dict = self.request.GET
        users = []
        f = GroupFilter(params_dict, queryset)
        groups = f.qs
        for group in groups:
            roommates_groups = []
            for relation in group.relations.all():
                if relation.is_roommate == Relationship.DIRECT_ROOMMATE:
                    roommates_groups.append([relation.start_user, relation.end_user])
            suitemates = filter(lambda u: not any([u in g for g in roommates_groups]), group.users.all())
            group.roommates = roommates_groups
            group.suitemates = suitemates
            for u in group.users.all():
                users.append(u)
        invites = Invitation.objects.filter(agreement_period__property=self.property).exclude(user__in=users).\
            select_related('assignment', 'assignment__unit', 'contract_type', 'unit_type', 'agreement_period',  'user')
        f = InviteFilter(params_dict, invites)
        invites = f.qs
        single_groups = []
        for i in invites.iterator():
            single_groups.append(FakeGroup(self.property, i.agreement_period, i.unit_type,
                                           i.contract_type.occupied_places if i.contract_type else 1, i.user,
                                           i.assignment.unit if hasattr(i, 'assignment') else None, i.locked))
        return list(groups) + single_groups

    def post(self, *args, **kwargs):
        group_id = self.request.POST.get('groupId', None)
        if not group_id or group_id=="undefined":
            raise SuspiciousOperation('Wrong group id!')
        group = Group.objects.filter(id=group_id).first()
        if not group:
            raise SuspiciousOperation('Wrong group id!')
        try:
            group.lock()
        except Exception as e:
            raise SuspiciousOperation(e)
        return HttpResponse('Successfully locked group!')

    def get_context_data(self, **kwargs):
        context = super(MatchesListView, self).get_context_data(**kwargs)
        context["user"] = self.request.user
        properties = self.request.user.managed_properties.all()
        context['property'] = self.property
        context["properties"] = properties
        filters = GroupFilter(property=self.property, data=self.request.GET, queryset=Group.objects.all())
        context['filters'] = filters
        context['breadcrumbs'] = [
            {
                'name': 'Matches',
                'url': reverse('management:match-list', kwargs={'property_id': self.kwargs.get('property_id')})
            }
        ]
        return context


class SuggestedRoommateListView(PropertyPermissionMixin, DataTableMixin, ListView):
    model = User
    template_name = 'management/manager_suggested_roommates.html'
    context_object_name = 'users'
    search_fields = ('email', )
    order_columns = ('email', )
    serializer_class = SuggestedRoommatesSerializer

    def process_queryset(self, queryset):
        relations = Relationship.objects.filter(invitation__agreement_period=self.agreement_period)
        matcher = RoommateMatcher(queryset, self.all_users, relations)
        queryset = matcher.get_suggested_users()
        return queryset

    def get_queryset(self):
        property_id = self.kwargs.get('property_id')
        property = Property.objects.filter(id=property_id).prefetch_related('assignment_period'). \
            select_related('organization').first()
        self.agreement_period = property.assignment_period.first()
        ids = Invitation.objects.filter(agreement_period=self.agreement_period).values_list('user_id', flat=True)
        self.all_users = User.objects.filter(id__in=ids).select_related('profile'). \
            filter(profile__personality_types__isnull=False)
        return self.all_users

    def get_context_data(self, **kwargs):
        context = super(SuggestedRoommateListView, self).get_context_data(**kwargs)
        context["user"] = self.request.user
        properties = self.request.user.managed_properties.all()
        context['property'] = self.property
        context["properties"] = properties
        context['breadcrumbs'] = [
            {
                'name': 'Suggested Roommates Report',
                'url': reverse('management:suggested-roommates-list',
                               kwargs={'property_id': self.kwargs.get('property_id')})
            }
        ]
        return context


class UnitTypeListView(PropertyPermissionMixin, DataTableMixin, CSVResponseMixin, ListView):
    model = UnitType
    template_name = 'management/manager_unit_types.html'
    context_object_name = 'unit_types'
    filters = [UnitTypeFilter]
    search_fields = ('name', )
    order_columns = ('name', 'max_occupancy')
    serializer_class = UnitTypeSerializer

    def get_csv_filename(self):
        prop = self.property
        if prop:
            return "{}_unit_types_{}.csv".format(prop.name, datetime.now())
        else:
            return super(UnitTypeListView, self).get_csv_filename()

    def get(self, request, **kwargs):
        response_format = request.GET.get('format', None)
        if response_format == 'csv':
            queryset = self.get_queryset()
            data = [['Name', 'Max Occupancy']]
            for unit_type in queryset:
                data.append([unit_type.name, unit_type.max_occupancy])
            return self.render_to_csv(data)
        else:
            return super(UnitTypeListView, self).get(request, **kwargs)

    def get_queryset(self):
        property_id = self.kwargs.get('property_id')
        return UnitType.objects.filter(property_id=property_id)

    def get_context_data(self, **kwargs):
        context = super(UnitTypeListView, self).get_context_data(**kwargs)
        context["user"] = self.request.user
        properties = self.request.user.managed_properties.all()
        context["properties"] = properties
        context['property'] = self.property
        context['breadcrumbs'] = [
            {
                'name': 'Unit Types',
                'url': reverse('management:unit-type-list', kwargs={'property_id': self.kwargs.get('property_id')})
            }
        ]
        return context


class UnitTypeCreateView(PropertyPermissionMixin, CreateView):
    model = UnitType
    template_name = 'management/manager_unit_type_form.html'
    form_class = UnitTypeManagerForm

    def get_form_kwargs(self):
        kwargs = super(UnitTypeCreateView, self).get_form_kwargs()
        kwargs['instance'] = UnitType(property_id=self.kwargs.get('property_id'))
        return kwargs

    def form_valid(self, form):
        obj = super(UnitTypeCreateView, self).form_valid(form)
        action.send(self.request.user, verb='created', action_object=form.instance, target=self.property)
        return obj

    def get_success_url(self):
        return reverse('management:unit-type-list', kwargs={'property_id': self.kwargs.get('property_id')})

    def get_context_data(self, **kwargs):
        context = super(UnitTypeCreateView, self).get_context_data(**kwargs)
        context["user"] = self.request.user
        properties = self.request.user.managed_properties.all()
        context["properties"] = properties
        context['property'] = self.property
        context["title"] = "Create Unit Type"
        context["submit"] = "Save"
        context['breadcrumbs'] = [
            {
                'name': 'Unit Types',
                'url': reverse('management:unit-type-list', kwargs={'property_id': self.kwargs.get('property_id')})
            },
            {
                'name': "Add Unit Type",
                'url': reverse('management:unit-type-add', kwargs={'property_id': self.kwargs.get('property_id')})
            }
        ]
        return context


class UnitTypeUpdateView(PropertyPermissionMixin, UpdateView):
    model = UnitType
    template_name = 'management/manager_unit_type_form.html'
    form_class = UnitTypeManagerForm

    def form_valid(self, form):
        obj = super(UnitTypeUpdateView, self).form_valid(form)
        action.send(self.request.user, verb='updated', action_object=form.instance, target=self.property)
        return obj

    def get_success_url(self):
        return reverse('management:unit-type-list', kwargs={'property_id': self.kwargs.get('property_id')})

    def get_context_data(self, **kwargs):
        context = super(UnitTypeUpdateView, self).get_context_data(**kwargs)
        context["user"] = self.request.user
        properties = self.request.user.managed_properties.all()
        context["properties"] = properties
        context['property'] = self.property
        context["title"] = "Update Unit Type"
        context["submit"] = "Update"
        context['breadcrumbs'] = [
            {
                'name': 'Unit Types',
                'url': reverse('management:unit-type-list', kwargs={'property_id': self.kwargs.get('property_id')})
            },
            {
                'name': self.object.name,
                'url': reverse('management:unit-type-update', kwargs={
                    'property_id': self.kwargs.get('property_id'),
                    'pk': self.object.id
                })
            }
        ]
        return context


class PropertySocialNetworkListView(PropertyPermissionMixin, DataTableMixin, CSVResponseMixin, ListView):
    model = PropertySocialNetwork
    template_name = 'management/manager_social_networks.html'
    context_object_name = 'social-networks'
    filters = [PropertySocialNetworkFilter]
    search_fields = ('network_name', )
    order_columns = ('network_name', )
    serializer_class = PropertySocialNetworkSerializer

    def get_csv_filename(self):
        prop = self.property
        if prop:
            return "{}_social_networks_{}.csv".format(prop.name, datetime.now())
        else:
            return super(PropertySocialNetworkListView, self).get_csv_filename()

    def get(self, request, **kwargs):
        response_format = request.GET.get('format', None)
        if response_format == 'csv':
            queryset = self.get_queryset()
            data = [['Network Name']]
            for social_network in queryset:
                data.append([social_network.network_name])
            return self.render_to_csv(data)
        else:
            return super(PropertySocialNetworkListView, self).get(request, **kwargs)

    def get_queryset(self):
        property_id = self.kwargs.get('property_id')
        return PropertySocialNetwork.objects.filter(property_id=property_id)

    def get_context_data(self, **kwargs):
        context = super(PropertySocialNetworkListView, self).get_context_data(**kwargs)
        context["user"] = self.request.user
        properties = self.request.user.managed_properties.all()
        context["properties"] = properties
        context['property'] = self.property
        context['breadcrumbs'] = [
            {
                'name': 'Social Networks',
                'url': reverse('management:social-networks-list', kwargs={'property_id': self.kwargs.get('property_id')})
            }
        ]
        return context


class PropertySocialNetworkCreateView(PropertyPermissionMixin, CreateView):
    model = PropertySocialNetwork
    template_name = 'management/manager_social_networks_form.html'
    form_class = PropertySocialNetworkManagerForm

    def get_form_kwargs(self):
        kwargs = super(PropertySocialNetworkCreateView, self).get_form_kwargs()
        kwargs['instance'] = PropertySocialNetwork(property_id=self.kwargs.get('property_id'))
        return kwargs

    def form_valid(self, form):
        obj = super(PropertySocialNetworkCreateView, self).form_valid(form)
        return obj

    def get_success_url(self):
        return reverse('management:social-networks-list', kwargs={'property_id': self.kwargs.get('property_id')})

    def get_context_data(self, **kwargs):
        context = super(PropertySocialNetworkCreateView, self).get_context_data(**kwargs)
        context["user"] = self.request.user
        properties = self.request.user.managed_properties.all()
        context["properties"] = properties
        context['property'] = self.property
        context["title"] = "Create Social Network"
        context["submit"] = "Save"
        context['breadcrumbs'] = [
            {
                'name': 'Social Network',
                'url': reverse('management:social-networks-list', kwargs={'property_id': self.kwargs.get('property_id')})
            },
            {
                'name': "Add Social Network",
                'url': reverse('management:social-networks-add', kwargs={'property_id': self.kwargs.get('property_id')})
            }
        ]
        return context


class PropertySocialNetworkUpdateView(PropertyPermissionMixin, UpdateView):
    model = PropertySocialNetwork
    template_name = 'management/manager_social_networks_form.html'
    form_class = PropertySocialNetworkManagerForm

    def form_valid(self, form):
        obj = super(PropertySocialNetworkUpdateView, self).form_valid(form)
        return obj

    def get_success_url(self):
        return reverse('management:social-networks-list', kwargs={'property_id': self.kwargs.get('property_id')})

    def get_context_data(self, **kwargs):
        context = super(PropertySocialNetworkUpdateView, self).get_context_data(**kwargs)
        context["user"] = self.request.user
        properties = self.request.user.managed_properties.all()
        context["properties"] = properties
        context['property'] = self.property
        context["title"] = "Update Room Type"
        context["submit"] = "Update"
        context['breadcrumbs'] = [
            {
                'name': 'Social Network',
                'url': reverse('management:social-networks-list', kwargs={'property_id': self.kwargs.get('property_id')})
            },
            {
                'name': self.object.network_name,
                'url': reverse('management:social-networks-update', kwargs={
                    'property_id': self.kwargs.get('property_id'),
                    'pk': self.object.id
                })
            }
        ]
        return context

class PropertySocialNetworkDeleteView(PropertyPermissionMixin, DeleteView):
    model = PropertySocialNetwork

    def delete(self, request, *args, **kwargs):
        if request.is_ajax:
            socialnetwork = get_object_or_404(self.model, pk=kwargs['pk'])
            if not socialnetwork.property:
                raise SuspiciousOperation('Wrong property!')
            message = 'Social Network {} was deleted!'.format(socialnetwork.network_name)
            socialnetwork.delete()
            return HttpResponse(message)
        else:
            raise SuspiciousOperation('Request must be AJAX')


class RoomTypeListView(PropertyPermissionMixin, DataTableMixin, CSVResponseMixin, ListView):
    model = RoomType
    template_name = 'management/manager_room_types.html'
    context_object_name = 'room_types'
    filters = [RoomTypeFilter]
    search_fields = ('name', )
    order_columns = ('name', 'max_occupancy')
    serializer_class = RoomTypeSerializer

    def get_csv_filename(self):
        prop = self.property
        if prop:
            return "{}_room_types_{}.csv".format(prop.name, datetime.now())
        else:
            return super(RoomTypeListView, self).get_csv_filename()

    def get(self, request, **kwargs):
        response_format = request.GET.get('format', None)
        if response_format == 'csv':
            queryset = self.get_queryset()
            data = [['Name', 'Max Occupancy']]
            for room_type in queryset:
                data.append([room_type.name, room_type.max_occupancy])
            return self.render_to_csv(data)
        else:
            return super(RoomTypeListView, self).get(request, **kwargs)

    def get_queryset(self):
        property_id = self.kwargs.get('property_id')
        return RoomType.objects.filter(property_id=property_id)

    def get_context_data(self, **kwargs):
        context = super(RoomTypeListView, self).get_context_data(**kwargs)
        context["user"] = self.request.user
        properties = self.request.user.managed_properties.all()
        context["properties"] = properties
        context['property'] = self.property
        context['breadcrumbs'] = [
            {
                'name': 'Room Types',
                'url': reverse('management:room-type-list', kwargs={'property_id': self.kwargs.get('property_id')})
            }
        ]
        return context


class RoomTypeCreateView(PropertyPermissionMixin, CreateView):
    model = RoomType
    template_name = 'management/manager_room_type_form.html'
    form_class = RoomTypeManagerForm

    def get_form_kwargs(self):
        kwargs = super(RoomTypeCreateView, self).get_form_kwargs()
        kwargs['instance'] = RoomType(property_id=self.kwargs.get('property_id'))
        return kwargs

    def form_valid(self, form):
        obj = super(RoomTypeCreateView, self).form_valid(form)
        action.send(self.request.user, verb='created', action_object=form.instance, target=self.property)
        return obj

    def get_success_url(self):
        return reverse('management:room-type-list', kwargs={'property_id': self.kwargs.get('property_id')})

    def get_context_data(self, **kwargs):
        context = super(RoomTypeCreateView, self).get_context_data(**kwargs)
        context["user"] = self.request.user
        properties = self.request.user.managed_properties.all()
        context["properties"] = properties
        context['property'] = self.property
        context["title"] = "Create Room Type"
        context["submit"] = "Save"
        context['breadcrumbs'] = [
            {
                'name': 'Room Types',
                'url': reverse('management:room-type-list', kwargs={'property_id': self.kwargs.get('property_id')})
            },
            {
                'name': "Add Room Type",
                'url': reverse('management:room-type-add', kwargs={'property_id': self.kwargs.get('property_id')})
            }
        ]
        return context


class RoomTypeUpdateView(PropertyPermissionMixin, UpdateView):
    model = RoomType
    template_name = 'management/manager_room_type_form.html'
    form_class = RoomTypeManagerForm

    def form_valid(self, form):
        obj = super(RoomTypeUpdateView, self).form_valid(form)
        action.send(self.request.user, verb='updated', action_object=form.instance, target=self.property)
        return obj

    def get_success_url(self):
        return reverse('management:room-type-list', kwargs={'property_id': self.kwargs.get('property_id')})

    def get_context_data(self, **kwargs):
        context = super(RoomTypeUpdateView, self).get_context_data(**kwargs)
        context["user"] = self.request.user
        properties = self.request.user.managed_properties.all()
        context["properties"] = properties
        context['property'] = self.property
        context["title"] = "Update Room Type"
        context["submit"] = "Update"
        context['breadcrumbs'] = [
            {
                'name': 'Room Types',
                'url': reverse('management:room-type-list', kwargs={'property_id': self.kwargs.get('property_id')})
            },
            {
                'name': self.object.name,
                'url': reverse('management:room-type-update', kwargs={
                    'property_id': self.kwargs.get('property_id'),
                    'pk': self.object.id
                })
            }
        ]
        return context


class OrganizationSocialHabitQuestionListView(PropertyPermissionMixin, DataTableMixin, CSVResponseMixin, ListView):
    model = OrganizationSocialHabitQuestion
    template_name = 'management/manager_org_questions.html'
    context_object_name = 'questions'
    search_fields = ('question', )
    order_columns = ('question', 'required', 'sort_order', 'status', 'created_at', 'updated_at')
    serializer_class = OrganizationSocialHabitQuestionSerializer

    def get_csv_filename(self):
        prop = self.property
        if prop:
            return "{}_org_questions_{}.csv".format(prop.name, datetime.now())
        else:
            return super(OrganizationSocialHabitQuestionListView, self).get_csv_filename()

    def get(self, request, **kwargs):
        response_format = request.GET.get('format', None)
        if response_format == 'csv':
            queryset = self.get_queryset()
            data = [['Question', 'Required', 'Sort Order', 'Status', 'Created At', 'Updated At']]
            for question in queryset:
                data.append([question.question, question.required, question.sort_order, question.status,
                             question.created_at, question.updated_at])
            return self.render_to_csv(data)
        else:
            return super(OrganizationSocialHabitQuestionListView, self).get(request, **kwargs)

    def get_queryset(self):
        property = self.property
        if property:
            organization = property.organization
            return OrganizationSocialHabitQuestion.objects.filter(organization=organization)
        else:
            return OrganizationSocialHabitQuestion.objects.none()

    def get_context_data(self, **kwargs):
        context = super(OrganizationSocialHabitQuestionListView, self).get_context_data(**kwargs)
        context["user"] = self.request.user
        properties = self.request.user.managed_properties.all()
        context["properties"] = properties
        context['property'] = self.property
        context['breadcrumbs'] = [
            {
                'name': 'Organization Questions',
                'url': reverse('management:org-questions-list', kwargs={'property_id': self.kwargs.get('property_id')})
            }
        ]
        return context


class OrganizationSocialHabitQuestionCreateView(PropertyPermissionMixin, CreateView):
    model = OrganizationSocialHabitQuestion
    template_name = 'management/manager_org_question_form.html'
    form_class = OrganizationSocialHabitQuestionManagerForm

    def get_form_kwargs(self):
        kwargs = super(OrganizationSocialHabitQuestionCreateView, self).get_form_kwargs()
        property = self.property
        if property:
            kwargs['instance'] = OrganizationSocialHabitQuestion(organization=property.organization)
        return kwargs

    def form_valid(self, form):
        context = self.get_context_data()
        answers = context['answers_formset']
        self.object = form.save()
        if answers.is_valid():
            answers.instance = self.object
            answers.save()
        action.send(self.request.user, verb='created', action_object=form.instance, target=self.property)
        return super(OrganizationSocialHabitQuestionCreateView, self).form_valid(form)

    def get_success_url(self):
        return reverse('management:org-questions-list', kwargs={'property_id': self.kwargs.get('property_id')})

    def get_context_data(self, **kwargs):
        context = super(OrganizationSocialHabitQuestionCreateView, self).get_context_data(**kwargs)
        context["user"] = self.request.user
        properties = self.request.user.managed_properties.all()
        context["properties"] = properties
        context['property'] = self.property
        context["title"] = "Create Social Habit Question"
        context["submit"] = "Save"
        OrganizationQuestionsFormSet = inlineformset_factory(OrganizationSocialHabitQuestion,
                                                             OrganizationSocialHabitAnswer,
                                                             form=OrganizationSocialHabitAnswerInline,
                                                             can_delete=False,
                                                             extra=0)
        if self.request.POST:
            context['answers_formset'] = OrganizationQuestionsFormSet(self.request.POST)
        else:
            context['answers_formset'] = OrganizationQuestionsFormSet()
        context['breadcrumbs'] = [
            {
                'name': 'Social Habit Questions',
                'url': reverse('management:org-questions-list', kwargs={'property_id': self.kwargs.get('property_id')})
            },
            {
                'name': "Add Social Habit Question",
                'url': reverse('management:org-question-add', kwargs={'property_id': self.kwargs.get('property_id')})
            }
        ]
        return context


class OrganizationSocialHabitQuestionUpdateView(PropertyPermissionMixin, UpdateView):
    model = OrganizationSocialHabitQuestion
    template_name = 'management/manager_org_question_form.html'
    form_class = OrganizationSocialHabitQuestionManagerForm

    def form_valid(self, form):
        context = self.get_context_data()
        answers = context['answers_formset']
        self.object = form.save()
        if answers.is_valid():
            answers.instance = self.object
            answers.save()
        action.send(self.request.user, verb='updated', action_object=form.instance, target=self.property)
        return super(OrganizationSocialHabitQuestionUpdateView, self).form_valid(form)

    def get_success_url(self):
        return reverse('management:org-questions-list', kwargs={'property_id': self.kwargs.get('property_id')})

    def get_context_data(self, **kwargs):
        context = super(OrganizationSocialHabitQuestionUpdateView, self).get_context_data(**kwargs)
        context["user"] = self.request.user
        properties = self.request.user.managed_properties.all()
        context["properties"] = properties
        context['property'] = self.property
        context["title"] = "Update Social Habit Question"
        context["submit"] = "Update"
        OrganizationQuestionsFormSet = inlineformset_factory(OrganizationSocialHabitQuestion,
                                                             OrganizationSocialHabitAnswer,
                                                             form=OrganizationSocialHabitAnswerInline,
                                                             can_delete=False,
                                                             extra=0)
        if self.request.POST:
            context['answers_formset'] = OrganizationQuestionsFormSet(self.request.POST, instance=self.object)
        else:
            context['answers_formset'] = OrganizationQuestionsFormSet(instance=self.object)

        context['breadcrumbs'] = [
            {
                'name': 'Social Habit Questions',
                'url': reverse('management:org-questions-list', kwargs={'property_id': self.kwargs.get('property_id')})
            },
            {
                'name': self.object.id,
                'url': reverse('management:org-question-update', kwargs={
                    'property_id': self.kwargs.get('property_id'),
                    'pk': self.object.id
                })
            }
        ]
        return context


class OrganizationSocialHabitQuestionDeleteView(PropertyPermissionMixin, DeleteView):
    model = OrganizationSocialHabitQuestion

    def delete(self, request, *args, **kwargs):
        if request.is_ajax:
            question = get_object_or_404(self.model, pk=kwargs['pk'])
            property = Property.objects.filter(pk=kwargs['property_id']).prefetch_related('emails').first()
            if not property:
                raise SuspiciousOperation('Wrong property!')
            if property.organization != question.organization:
                raise PermissionDenied
            action.send(self.request.user, verb='deleted "{}" question'.format(question.question),
                        target=self.property)
            OrganizationSocialHabitAnswer.objects.filter(question=question).delete()
            question.delete()
            return HttpResponse(reverse('management:org-questions-list', kwargs={'property_id': property.id}))
        else:
            raise SuspiciousOperation('Request must be AJAX')


class ResidentImportListView(PropertyPermissionMixin, DataTableMixin, ImportViewMixin, ListView):
    model = ResidentImport
    template_name = 'management/resident_import.html'
    context_object_name = 'records'
    search_fields = None
    order_columns = ('file', 'created_at', 'status')
    serializer_class = ResidentImportRecordSerializer
    form_class = ResidentImportForm
    task = parse_invites
    sample_columns = [
                (u"Agreement Period\u002A", 20),
                (u"Unit", 10),
                (u"Room", 10),
                (u"Bed", 10),
                (u"First name\u002A", 20),
                (u"Last name\u002A", 20),
                (u"Email Address\u002A", 20),
                (u"Points\u002A", 10),
                (u"Unit Type\u002A", 10),
                (u"Room Type", 13),
                (u"Gender Restriction", 20),
                (u"Contract Type", 13),
                (u"Locked", 10),
            ]

    def get_context_data(self, **kwargs):
        context = super(ResidentImportListView, self).get_context_data(**kwargs)
        context["user"] = self.request.user
        properties = self.request.user.managed_properties.all()
        context["properties"] = properties
        context['property'] = self.property
        context['file_form'] = ResidentImportForm()
        context['breadcrumbs'] = [
            {
                'name': 'Import Residents',
                'url': reverse('management:resident-import-list', kwargs={'property_id': self.kwargs.get('property_id')})
            }
        ]
        return context


class RealtyImportListView(PropertyPermissionMixin, DataTableMixin, ImportViewMixin, ListView):
    model = RealtyImport
    template_name = 'management/realty_import.html'
    context_object_name = 'records'
    search_fields = None
    order_columns = ('file', 'created_at', 'status')
    serializer_class = RealtyImportSerializer
    form_class = RealtyImportForm
    task = parse_realty
    sample_columns = [
                (u"Unit", 10),
                (u"Unit Type", 20),
                (u"Room", 10),
                (u"Room Type", 20),
                (u"Bed", 10),
            ]

    def get_context_data(self, **kwargs):
        context = super(RealtyImportListView, self).get_context_data(**kwargs)
        context["user"] = self.request.user
        properties = self.request.user.managed_properties.all()
        context["properties"] = properties
        context['property'] = self.property
        context['file_form'] = self.form_class()
        context['breadcrumbs'] = [
            {
                'name': 'Realty Import',
                'url': reverse('management:realty-import-list', kwargs={'property_id': self.kwargs.get('property_id')})
            }
        ]
        return context


class MailTemplateListView(PropertyPermissionMixin, DataTableMixin, ListView):
    model = MailTemplate
    template_name = 'management/manager_mail_templates.html'
    context_object_name = 'emails'
    search_fields = ('name', )
    order_columns = ('name', )
    serializer_class = EmailSerializer

    def get_queryset(self):
        property = self.property
        if property:
            return property.emails.all().exclude(slug__startswith=settings.DB_MAILER_DEFAULT_TEMPLATE_PREFIX)
        return MailTemplate.objects.none()

    def get_context_data(self, **kwargs):
        context = super(MailTemplateListView, self).get_context_data(**kwargs)
        context["user"] = self.request.user
        properties = self.request.user.managed_properties.all()
        context["properties"] = properties
        context['property'] = self.property
        context['breadcrumbs'] = [
            {
                'name': 'Email Templates',
                'url': reverse('management:mail-template-list', kwargs={'property_id': self.kwargs.get('property_id')})
            }
        ]
        return context


class MailTemplateCreateView(PropertyPermissionMixin, CreateView):
    model = MailTemplate
    template_name = 'management/manager_mail_template_form.html'
    form_class = MailTemplateManagerForm

    def get_form_kwargs(self):
        kwargs = super(MailTemplateCreateView, self).get_form_kwargs()
        kwargs['property'] = self.property
        return kwargs

    def form_valid(self, form):
        obj = super(MailTemplateCreateView, self).form_valid(form)
        action.send(self.request.user, verb='created', action_object=form.instance, target=self.property)
        return obj

    def get_success_url(self):
        return reverse('management:mail-template-list', kwargs={'property_id': self.kwargs.get('property_id')})

    def get_context_data(self, **kwargs):
        context = super(MailTemplateCreateView, self).get_context_data(**kwargs)
        context["user"] = self.request.user
        properties = self.request.user.managed_properties.all()
        context["properties"] = properties
        context['property'] = self.property
        context["title"] = "Create Mail Template"
        context["submit"] = "Save"
        context['breadcrumbs'] = [
            {
                'name': 'Email Templates',
                'url': reverse('management:mail-template-list', kwargs={'property_id': self.kwargs.get('property_id')})
            },
            {
                'name': "Add Room Type",
                'url': reverse('management:mail-template-add', kwargs={'property_id': self.kwargs.get('property_id')})
            }
        ]
        return context


class MailTemplateUpdateView(PropertyPermissionMixin, UpdateView):
    model = MailTemplate
    template_name = 'management/manager_mail_template_form.html'
    form_class = MailTemplateManagerForm

    def get_form_kwargs(self):
        kwargs = super(MailTemplateUpdateView, self).get_form_kwargs()
        kwargs['property'] = self.property
        return kwargs

    def form_valid(self, form):
        obj = super(MailTemplateUpdateView, self).form_valid(form)
        action.send(self.request.user, verb='updated', action_object=form.instance, target=self.property)
        return obj

    def get_success_url(self):
        return reverse('management:mail-template-list', kwargs={'property_id': self.kwargs.get('property_id')})

    def get_context_data(self, **kwargs):
        context = super(MailTemplateUpdateView, self).get_context_data(**kwargs)
        context["user"] = self.request.user
        properties = self.request.user.managed_properties.all()
        context["properties"] = properties
        context['property'] = self.property
        context["title"] = "Update Email Template"
        context["submit"] = "Update"
        context['breadcrumbs'] = [
            {
                'name': 'Email Templates',
                'url': reverse('management:mail-template-list', kwargs={'property_id': self.kwargs.get('property_id')})
            },
            {
                'name': self.object.name,
                'url': reverse('management:mail-template-update', kwargs={
                    'property_id': self.kwargs.get('property_id'),
                    'pk': self.object.id
                })
            }
        ]
        return context


class MailTemplateDeleteView(PropertyPermissionMixin, DeleteView):
    model = MailTemplate

    def delete(self, request, *args, **kwargs):
        if request.is_ajax:
            template = get_object_or_404(self.model, pk=kwargs['pk'])
            property = Property.objects.filter(pk=kwargs['property_id']).prefetch_related('emails').first()
            if not property:
                raise SuspiciousOperation('Wrong property!')
            if template not in property.emails.all():
                raise PermissionDenied
            message = 'Email Template {} was deleted!'.format(template.name)
            action.send(self.request.user, verb='deleted "{}" email template'.format(template.name),
                        target=self.property)
            template.delete()
            return HttpResponse(message)
        else:
            raise SuspiciousOperation('Request must be AJAX')


class PropertyFeedListView(PropertyPermissionMixin, DataTableMixin, ListView):
    model = Action
    template_name = 'management/manager_feed.html'
    context_object_name = 'actions'
    search_fields = None
    order_columns = None
    serializer_class = FeedSerializer

    def get_queryset(self):
        if self.property:
            content_type = ContentType.objects.get_for_model(self.property)
            return Action.objects.filter(target_content_type=content_type, target_object_id=self.property.id).\
                order_by('-timestamp')
        return Action.objects.none()

    def get_context_data(self, **kwargs):
        context = super(PropertyFeedListView, self).get_context_data(**kwargs)
        context["user"] = self.request.user
        properties = self.request.user.managed_properties.all()
        context["properties"] = properties
        context['property'] = self.property
        context['breadcrumbs'] = [
            {
                'name': 'Activity Feed',
                'url': reverse('management:feed', kwargs={'property_id': self.kwargs.get('property_id')})
            }
        ]
        return context


class GlobalSearchView(TemplateView):
    template_name = 'management/manager_search_result.html'
    serializer_class = SearchResultSerializer
    user_search_fields = ['first_name', 'last_name', 'email']

    def search(self, queryset, search_fields, search_value):
        if search_value and search_fields:
            orm_lookups = ["{}__icontains".format(search_field) for search_field in search_fields]
            for search_part in search_value.split():
                or_queries = [Q(**{orm_lookup: search_part}) for orm_lookup in orm_lookups]
                queryset = queryset.filter(reduce(operator.or_, or_queries))
        return queryset

    def get(self, request, **kwargs):
        if request.is_ajax():
            search_value = request.GET.get('search', None)
            if not search_value:
                raise SuspiciousOperation('Please pass search param.')
            user = request.user
            user_properties = user.managed_properties.all().values_list('id', flat=True)
            users = User.objects.filter(available_properties__in=user_properties)
            users = self.search(users, self.user_search_fields, search_value)
            user_ids = users.values_list('id', flat=True)
            invites = Invitation.objects.filter(user_id__in=user_ids).select_related('user', 'agreement_period__property')
            results = []
            for invite in invites:
                property = invite.agreement_period.property
                user = invite.user
                results.append({
                    'user': user.full_name,
                    'property': property.name,
                    'user_url': reverse('management:user-update', kwargs={'property_id': property.id, 'pk': user.id}),
                })
            serializer = SearchResultSerializer(instance=results, many=True)
            return JsonResponse({'data': serializer.data}, safe=False)
        else:
            return super(GlobalSearchView, self).get(request, **kwargs)

    def get_context_data(self, **kwargs):
        context = super(GlobalSearchView, self).get_context_data(**kwargs)
        search_value = self.request.GET.get('search', None)
        context['search_value'] = search_value if search_value else ''
        context["user"] = self.request.user
        properties = self.request.user.managed_properties.all()
        context["properties"] = properties
        return context


class PlacementListView(PropertyPermissionMixin, DataTableMixin, ListView):
    model = Unit
    template_name = 'management/manager_placement.html'
    context_object_name = 'units'
    search_fields = ['name']
    order_columns = ['name', 'unit_type__name']
    serializer_class = UnitSerializer

    def get_queryset(self):
        try:
            period_id = int(self.request.GET.get('period'))
            agreement_period = AssignmentPeriod.objects.get(property=self.property, id=period_id)
        except AssignmentPeriod.DoesNotExist:
            raise SuspiciousOperation
        except (ValueError, TypeError):
            agreement_period = AssignmentPeriod.objects.filter(property=self.property).latest('end_date')
        if not agreement_period:
            raise SuspiciousOperation
        blocked_beds = agreement_period.blocked_beds.values_list('id', flat=True)
        qs = Unit.objects.only('name', 'unit_type__name').filter(unit_type__property=self.property).select_related(
            'unit_type').prefetch_related(
            Prefetch('rooms', queryset=Room.objects.only('name', 'unit').all().order_by('name')),
            Prefetch('rooms__beds', queryset=Bed.objects.only('name', 'room').annotate(
                is_blocked=Case(
                    When(id__in=blocked_beds, then=True),
                    default=False,
                    output_field=BooleanField()
                )
            ).order_by('name')),
            Prefetch('rooms__beds__assignments', queryset=PendingAssignment.objects.
                     filter(assignment_period=agreement_period).only(
                            'bed', 'user', 'invite__contract_type', 'user__first_name', 'user__last_name',
                            'user__points', 'user__email').select_related('user', 'invite__contract_type')
                     )
        )
        return qs

    def get_serializer_context(self):
        stages = Stage.objects.all()
        return {'stages': stages}

    def get_context_data(self, **kwargs):
        context = super(PlacementListView, self).get_context_data(**kwargs)
        context["user"] = self.request.user
        properties = self.request.user.managed_properties.all()
        context["properties"] = properties
        context['property'] = self.property
        context['periods'] = AssignmentPeriod.objects.filter(property=self.property).order_by('-end_date')
        context['breadcrumbs'] = [
            {
                'name': 'Placement',
                'url': reverse('management:placement_details', kwargs={'property_id': self.property.id})
            },
        ]

        return context


class UnitBaseFormView(PropertyPermissionMixin, FormMixin):
    model = Unit
    template_name = 'management/manager_unit_form.html'

    def get_form_class(self):
        PropertyRoomForm = type('PropertyRoomForm', (RoomForm, ), {
            'property': self.property,
            '__module__': __name__,
        })
        bed_formset = inlineformset_factory(
                Room,
                Bed,
                formset=BedFormSet,
                form=BedForm,
                extra=0,
                can_delete=False
            )
        return nestedformset_factory(
            Unit,
            Room,
            form=PropertyRoomForm,
            formset=RoomFormSet,
            exclude=['square'],
            extra=0,
            nested_formset=bed_formset,
            can_delete=False
        )

    def form_valid(self, form):
        context = self.get_context_data()
        unit_form = context['unit_form']
        if not unit_form.is_valid():
            return self.form_invalid(form)
        if form.is_valid():
            unit = unit_form.save()
            form.instance = unit
        return super(UnitBaseFormView, self).form_valid(form)

    def get_success_url(self):
        return reverse('management:placement_details', kwargs={'property_id': self.kwargs.get('property_id')})


class UnitCreateView(UnitBaseFormView, CreateView):

    def get_context_data(self, **kwargs):
        context = super(UnitCreateView, self).get_context_data(**kwargs)
        context["user"] = self.request.user
        properties = self.request.user.managed_properties.all()
        context["properties"] = properties
        context['property'] = self.property
        context["title"] = "Create Unit"
        context["submit"] = "Save"
        if self.request.POST:
            context['unit_form'] = UnitForm(self.property, self.request.POST)
        else:
            context['unit_form'] = UnitForm(self.property)
        return context


class UnitUpdateView(UnitBaseFormView, UpdateView):

    def dispatch(self, request, *args, **kwargs):
        if not Unit.objects.filter(id=kwargs['pk'], unit_type__property_id=kwargs['property_id']).exists():
            raise PermissionDenied
        return super(UnitUpdateView, self).dispatch(request, *args, **kwargs)

    def get_context_data(self, **kwargs):
        context = super(UnitUpdateView, self).get_context_data(**kwargs)
        context["user"] = self.request.user
        properties = self.request.user.managed_properties.all()
        context["properties"] = properties
        context['property'] = self.property
        context["title"] = "Update Unit"
        context["submit"] = "Update"
        if self.request.POST:
            context['unit_form'] = UnitForm(self.property, self.request.POST, instance=self.object)
        else:
            context['unit_form'] = UnitForm(self.property, instance=self.object)
        return context


class PlacementUsersView(PropertyPermissionMixin, View):

    def get(self, request, *args, **kwargs):
        bed_id = request.GET.get('bed_id')
        if not bed_id:
            raise SuspiciousOperation("You must pass bed id!")
        with_group = request.GET.get('with_group', False)
        with_beds = request.GET.get('with_beds', False)
        try:
            bed = Bed.objects.get(id=bed_id)
        except Bed.DoesNotExist:
            raise SuspiciousOperation("Bed does not exists")
        users = self._users_can_place(bed, kwargs["period_id"], with_group, with_beds)
        return JsonResponse(LiteUserSerializer(users, many=True).data, safe=False)

    def post(self, request, *args, **kwargs):
        bed_id = request.POST.get('bed_id')
        if not bed_id:
            raise SuspiciousOperation("You must pass bed id!")
        with_group = request.POST.get('with_group', False)
        with_beds = request.POST.get('with_beds', False)
        try:
            bed = Bed.objects.get(id=bed_id)
        except Bed.DoesNotExist:
            raise SuspiciousOperation("Bed does not exists")
        user_id = request.POST.get("user_id")
        if not user_id:
            raise SuspiciousOperation("You must pass user id!")
        try:
            user = User.objects.get(id=user_id)
        except User.DoesNotExist:
            raise SuspiciousOperation("User does not exists")
        period_id = kwargs["period_id"]
        users = self._users_can_place(bed, period_id, with_group, with_beds)
        if user not in users:
            raise SuspiciousOperation("This user can't occupy this bed!")
        # change assignment
        user_assignment = PendingAssignment.objects.filter(user=user, assignment_period=period_id).first()
        if not user_assignment:
            invite = Invitation.objects.get(user=user)
            assignment_period = AssignmentPeriod.objects.get(id=period_id)
            room = bed.room
            user_assignment_create = PendingAssignment.objects.get_or_create(user=user, assignment_period=assignment_period,
                                                invite=invite, room_type=room.room_type, room=room, unit=room.unit)
            user_assignment = user_assignment_create[0]
 
        if user_assignment:
            user_assignment.bed = bed
            user_assignment.save()
        else:
            raise SuspiciousOperation("PendingAssignment does not exists") 

        if with_group:
            group = Group.objects.filter(agreement_period=period_id, users__in=[user]).first() or []
            try:
                group = group.users.exclude(id=user.id)
            except ValueError:
                group = []
            room = bed.room
            unit = room.unit
            rooms = unit.rooms.all()
            rooms = map(lambda r: r.get_available_beds(period_id), rooms)
            print rooms,group
            i = 0
            roomoccupy = [] 
            useroccupy = []
            for user in group:
              for roombed in rooms:
                if len(roombed) > 0 and roombed not in roomoccupy and user.id not in useroccupy:
                  roomoccupy.append(roombed)
                  useroccupy.append(user.id)
                  self.postpatchgroup(roombed[0].id, user.id, period_id , False, with_beds)
            group_invites = Invitation.objects.filter(user__in=group).select_related('user', 'contract_type')
            invites_with_double_bed = filter(lambda invite: invite.contract_type.occupied_places == 2, group_invites)
            for invite in invites_with_double_bed:
                double_rooms = filter(lambda r: len(r) >= 2, rooms)
                if double_rooms:
                    assignment = PendingAssignment.objects.filter(user=invite.user, assignment_period=period_id).first()
                    assignment.bed = double_rooms[0][0]
                    assignment.save()
                    rooms.remove(double_rooms[0])
            invites_with_single_bed = filter(lambda invite: invite.contract_type.occupied_places == 1, group_invites)
            beds = list(itertools.chain(*rooms))
            for invite, b in zip(invites_with_single_bed, beds):
                if not invite:
                    break
                assignment = PendingAssignment.objects.filter(user=invite.user, assignment_period=period_id).first()
                if not assignment:
                    continue
                assignment.bed = b
                assignment.save()
        else:
            self._remove_from_group(user)
            self._add_to_group(user_assignment)
        return JsonResponse(LiteUserSerializer(user).data, safe=False)

    def postpatchgroup(self, bed_id, user_id, period_id , with_group, with_beds):
        if not bed_id:
            raise SuspiciousOperation("You must pass bed id!")
        try:
            bed = Bed.objects.get(id=bed_id)
        except Bed.DoesNotExist:
            raise SuspiciousOperation("Bed does not exists")

        if not user_id:
            raise SuspiciousOperation("You must pass user id!")
        try:
            user = User.objects.get(id=user_id)
        except User.DoesNotExist:
            raise SuspiciousOperation("User does not exists")

        users = self._users_can_place(bed, period_id, with_group, with_beds)
        if user not in users:
            raise SuspiciousOperation("This user can't occupy this bed!")
        # change assignment
        user_assignment = PendingAssignment.objects.filter(user=user, assignment_period=period_id).first()
        if not user_assignment:
            invite = Invitation.objects.get(user=user)
            assignment_period = AssignmentPeriod.objects.get(id=period_id)
            room = bed.room
            user_assignment_create = PendingAssignment.objects.get_or_create(user=user, assignment_period=assignment_period,
                                                invite=invite, room_type=room.room_type, room=room, unit=room.unit)
            user_assignment = user_assignment_create[0]
 
        if user_assignment:
            user_assignment.bed = bed
            user_assignment.save()
        else:
            raise SuspiciousOperation("PendingAssignment does not exists") 

        if with_group:
            group = Group.objects.filter(agreement_period=period_id, users__in=[user]).first() or []
            try:
                group = group.users.exclude(id=user.id)
            except ValueError:
                group = []
            room = bed.room
            unit = room.unit
            rooms = unit.rooms.all()
            rooms = map(lambda r: r.get_available_beds(period_id), rooms)
            group_invites = Invitation.objects.filter(user__in=group).select_related('user', 'contract_type')
            invites_with_double_bed = filter(lambda invite: invite.contract_type.occupied_places == 2, group_invites)
            for invite in invites_with_double_bed:
                double_rooms = filter(lambda r: len(r) >= 2, rooms)
                if double_rooms:
                    assignment = PendingAssignment.objects.filter(user=invite.user, assignment_period=period_id).first()
                    assignment.bed = double_rooms[0][0]
                    assignment.save()
                    rooms.remove(double_rooms[0])
            invites_with_single_bed = filter(lambda invite: invite.contract_type.occupied_places == 1, group_invites)
            beds = list(itertools.chain(*rooms))
            for invite, b in zip(invites_with_single_bed, beds):
                if not invite:
                    break
                assignment = PendingAssignment.objects.filter(user=invite.user, assignment_period=period_id).first()
                if not assignment:
                    continue
                assignment.bed = b
                assignment.save()
        else:
            self._remove_from_group(user)
            self._add_to_group(user_assignment)
        return JsonResponse(LiteUserSerializer(user).data, safe=False)


    def patch(self, request, *args, **kwargs):
        params = QueryDict(request.body)
        bed_id = params.get('bed_id')
        if not bed_id:
            raise SuspiciousOperation("You must pass bed id!")
        try:
            bed = Bed.objects.get(id=bed_id)
        except Bed.DoesNotExist:
            raise SuspiciousOperation("Bed does not exists")
        try:
            period = AssignmentPeriod.objects.get(id=kwargs['period_id'])
        except AssignmentPeriod.DoesNotExist:
            raise SuspiciousOperation("Assignment period does not exists")
        assignment = PendingAssignment.objects.filter(bed=bed, assignment_period=period).first()
        if assignment:
            assignment.unit = None
            assignment.room = None
            assignment.bed = None
            assignment.save()
            user = assignment.user
            self._remove_from_group(user)
        if period not in bed.blocked.all():
            bed.blocked.add(period)
        else:
            bed.blocked.remove(period)
        return HttpResponse('Blocked!')

    def delete(self, request, *args, **kwargs):
        params = QueryDict(request.body)
        bed_id = params.get('bed_id')
        if not bed_id:
            raise SuspiciousOperation("You must pass bed id!")
        try:
            bed = Bed.objects.get(id=bed_id)
        except Bed.DoesNotExist:
            raise SuspiciousOperation("Bed does not exists")
        assignment = PendingAssignment.objects.filter(bed=bed, assignment_period_id=kwargs['period_id']).first()
        if not assignment:
            raise SuspiciousOperation("Assignment does not exist")
        assignment.unit = None
        assignment.room = None
        assignment.bed = None
        assignment.save()
        user = assignment.user
        self._remove_from_group(user)
        return HttpResponse('Deleted!')

    def _users_can_place(self, bed, assignment_period, with_group, with_beds):
        if PendingAssignment.objects.filter(bed=bed, assignment_period=assignment_period).exists():
            raise SuspiciousOperation("This bed already occupied")
        user_ids = User.objects.filter(available_properties__in=[self.property]).\
            exclude(id__in=self.property.managers.values_list('id', flat=True)).values_list('id', flat=True)
        room = bed.room
        num_of_available_beds = len(room.get_available_beds(assignment_period))
        available_user_ids = Invitation.objects.filter(user_id=user_ids, agreement_period=assignment_period,
                                                       contract_type__occupied_places__lte=num_of_available_beds,
                                                       room_type=room.room_type, unit_type=room.unit.unit_type).\
            values_list('user', flat=True)
        if not with_beds:
            available_user_ids = available_user_ids.exclude(assignment__bed__isnull=False).values_list('user', flat=True)
        else:
            available_user_ids = PendingAssignment.objects.filter(user__in=available_user_ids, bed__isnull=False).\
                values_list('user', flat=True)
					
        if with_group:
            unit = bed.room.unit
            unit_rooms = unit.rooms.all()
            num_of_available_beds = map(lambda room: len(room.get_available_beds(assignment_period)), unit_rooms)
            total_beds = sum(num_of_available_beds)
            # ToDo : check empty rooms and users with double xl contract
            empty_rooms = filter(lambda room: room > 1, num_of_available_beds)
            available_user_ids = Group.objects.filter(users__in=available_user_ids, size__lte=total_beds,
                                                      agreement_period=assignment_period).\
                values_list('users', flat=True)
        return User.objects.filter(id__in=available_user_ids)

    def _remove_from_group(self, user):
        group = Group.objects.filter(users__in=[user], agreement_period=self.kwargs['period_id']).first()
        if group:
            users = group.users.all()
            relations = Relationship.objects.filter(Q(start_user=user, end_user__in=users) |
                                                    Q(start_user__in=users, end_user=user))
            relations.delete()
            group.users.remove(user)
            group.save()

    def _add_to_group(self, assignment):
        user = assignment.user
        unit = assignment.unit
        group = Group.objects.filter(unit=unit, agreement_period=self.kwargs['period_id']).first()
        if group:
            group_users = group.users.all()
            for u in group_users:
                relation = Relationship(start_user=user, end_user=u, invitation=assignment.invite, read_only=True)
                relation.save()
            group.users.add(user)
            group.save()


class StageView(PropertyPermissionMixin, UpdateView):
    model = Property
    template_name = 'management/manager_stages_form.html'
    form_class = StageForm
    pk_url_kwarg = 'property_id'

    def form_valid(self, form):
        obj = super(StageView, self).form_valid(form)
        action.send(self.request.user, verb='updated', action_object=form.instance, target=self.property)
        return HttpResponse('Changed')

    def get_success_url(self):
        return reverse('management:stages', kwargs={'property_id': self.property.id})

    def get_context_data(self, **kwargs):
        context = super(StageView, self).get_context_data(**kwargs)
        context["user"] = self.request.user
        properties = self.request.user.managed_properties.all()
        context["properties"] = properties
        context['property'] = self.property
        context["title"] = "Stages"
        context["submit"] = "Save"
        context['breadcrumbs'] = [
            {
                'name': 'Settings',
                'url': reverse('management:stages', kwargs={'property_id': self.kwargs.get('property_id')})
            },
        ]
        return context


class PropertySettingsView(PropertyPermissionMixin, UpdateView):
    model = Property
    template_name = 'management/property_settings_form.html'
    form_class = PropertySettingsForm
    pk_url_kwarg = 'property_id'

    def form_valid(self, form):
        obj = super(PropertySettingsView, self).form_valid(form)
        action.send(self.request.user, verb='updated settings for', action_object=form.instance, target=self.property)
        return obj

    def get_success_url(self):
        return reverse('management:property_settings', kwargs={'property_id': self.property.id})

    def get_context_data(self, **kwargs):
        context = super(PropertySettingsView, self).get_context_data(**kwargs)
        context["user"] = self.request.user
        properties = self.request.user.managed_properties.all()
        context["properties"] = properties
        context['property'] = self.property
        context["title"] = "Property settings"
        context["submit"] = "Save"
        context['breadcrumbs'] = [
            {
                'name': "Property settings",
                'url': reverse('management:property_settings', kwargs={'property_id': self.kwargs.get('property_id')})
            },
        ]
        return context


def logout_view(request):
    logout(request)
    return redirect(settings.LOGIN_URL)
