# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    def add_contract_types(apps, schema_editor):
        ContractType = apps.get_model("management", "ContractType")
        ContractType.objects.create(name="Single", occupied_places=1)
        ContractType.objects.create(name="Double", occupied_places=1)
        ContractType.objects.create(name="Double XL", occupied_places=2)

    dependencies = [
        ('management', '0020_add_contract_type_model'),
    ]

    operations = [
        migrations.RunPython(add_contract_types),
    ]

