var submitted = false;


jQuery('#profile-form').validate({
    ignore: [],
    errorClass: "help-block animated fadeInDown",
    errorElement: "div",
    errorPlacement: function (e, r) {
        jQuery(r).parents(".question-container > div, .form-field").append(e)
    },
    highlight: function (e) {
        var r = jQuery(e);
        r.closest(".question-container, .form-field").removeClass("has-error").addClass("has-error"), r.closest(".help-block").remove()
    },
    success: function (e) {
        var r = jQuery(e);
        r.closest(".question-container, .form-field").removeClass("has-error"), r.closest(".help-block").remove()
    },
    showErrors: function (errorMap, errorList) {
        this.defaultShowErrors();
        if (jQuery('#simple-step1').find('.has-error').length != 0 && submitted) {
            submitted = false;
            jQuery('#step1-link').trigger('click');
        }
    }
});
$('.nav-tabs li a[href="#simple-step2"]').click(function(){
    var form = $('#profile-form');
    var validator = form.validate();
    var firstTabValid = true;
    $(".first-tab-element").each(function () {
        if (!validator.element($(this))) {
            firstTabValid = false;
        }
    });
    if (firstTabValid) {
        $(this).tab('show');
        validator.resetForm();
    }
    return false;
});


$('#submit-btn').on('click', function () {
    submitted = true;
});

$.validator.addMethod("valueNotEquals", function(value, element, arg){
  return arg != value;
 }, "Value must not equal arg.");

$('.required').each(function () {
    $(this).rules('add', {
        required: true,
        messages: {
            required: "This field is required!"
        }
    });
});

$('.url-field').each(function () {
    $(this).rules('add', {
        url: true,
        messages: {
            required: "Enter valid url!"
        }
    });
});

$('.required-select').each(function () {
   $(this).rules('add', {
       valueNotEquals: "",
       messages: {
           valueNotEquals: "Please select an item!"
       }
   })
});