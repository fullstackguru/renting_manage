from django.conf import settings
from django.db.models import Sum
from django.db.models.signals import post_save, m2m_changed
from utils.emails import send_relationship_email, send_in_app_message
from website.models import UserProfile, Relationship, ConversationMessage, Group
from management.models import PendingAssignment, Unit


def send_relationship_email_signal(sender, instance, **kwargs):
    # raw parameter is set to True during loaddata management operation.
    if kwargs.get('raw'):
        return
    print(kwargs)
    send_relationship_email(instance, kwargs.get('created'))


def send_in_app_message_signal(sender, instance, **kwargs):
    # raw parameter is set to True during loaddata management operation.
    if kwargs.get('raw'):
        return
    send_in_app_message(instance, kwargs.get('created'))


def create_user_profile(sender, instance, **kwargs):
    if kwargs.get('created'):
        print 'USER CREATED SIGNAL'
        user_profile = UserProfile.objects.get_or_create(user=instance)


def update_group(sender, instance, action, model, **kwargs):
    if action in ["post_add", "post_remove", "post_clear"]:
        users = instance.users.all()
        instance.size = users.filter(invites__agreement_period=instance.agreement_period).aggregate(
            group_size=Sum('invites__contract_type__occupied_places')
        )['group_size'] or 0
        instance.leader = users.latest('points') if users else None
        instance.save()
        if instance.unit:
            assignments = PendingAssignment.objects.filter(assignment_period=instance.agreement_period)
            if action == 'post_add':
                assignments.filter(user__in=users).exclude(unit=instance.unit).\
                    update(unit=instance.unit, room=None, bed=None)
            else:
                assignments.filter(user=model).update(unit=None, room=None, bed=None)


post_save.connect(send_relationship_email_signal, sender=Relationship, dispatch_uid="send_relationship_email")
post_save.connect(send_in_app_message_signal, sender=ConversationMessage, dispatch_uid="send_in_app_message")
post_save.connect(create_user_profile, sender=settings.AUTH_USER_MODEL, dispatch_uid="create_user_profile")
m2m_changed.connect(update_group, sender=Group.users.through, dispatch_uid="update_group")
