from datetime import datetime, timedelta
import uuid
from django.db import models
from django.db.models import Model
from django.utils import timezone
from ckeditor.fields import RichTextField
from django.conf import settings
from dbmail.models import MailTemplate,MailFromEmail
from .fields import ChoiceArrayField
User = settings.AUTH_USER_MODEL


INVITATION_TYPE = (
    ('M', 'Manual'),
    ('A', 'Auto'),
)


class Organization(Model):
    name = models.CharField(null=False, max_length=250)

    def __str__(self):
        return self.name


class ContractType(Model):
    name = models.CharField(max_length=64)
    occupied_places = models.IntegerField(default=1)

    def __unicode__(self):
        return u'{} - {}'.format(self.name, self.occupied_places)


class Property(Model):
    organization = models.ForeignKey(Organization)
    name = models.CharField(null=False, max_length=250)
    managers = models.ManyToManyField(settings.AUTH_USER_MODEL, related_name='managed_properties')
    email_subject = models.CharField(blank=True, null=False, max_length=250)
    from_name = models.CharField(blank=True, null=False, max_length=250)
    from_email = models.EmailField(max_length=255, default=settings.DEFAULT_FROM_EMAIL)
    welcome_email_body = RichTextField(blank=True, null=True)
    enable_email_sending = models.BooleanField(default=True, null=False)
    disable_email_resident = models.BooleanField(default=False, null=False)
    logo = models.ImageField(blank=True, null=True, upload_to='uploads/property-logos/')
    emails = models.ManyToManyField(MailTemplate, blank=True)
    contract_type = models.ManyToManyField(ContractType, blank=True)
    from_email_host = models.ManyToManyField(MailFromEmail, blank=True)
    enable_co_educational_groups = models.BooleanField(default=False)
    enable_social_network = models.BooleanField(default=False)
    enable_star_setting = models.BooleanField(default=False)
    show_placement_tab = models.BooleanField(default=True)
    current_stage = models.ForeignKey('Stage', default=1, related_name='properties')
    file = models.FileField(upload_to='uploads/resources_link/', blank=True, null=True)

    def __str__(self):
        return self.name

    def __get_email(self, category):
        property_email = self.emails.filter(category__name=category).first()
        if property_email:
            return property_email
        default_email = MailTemplate.objects.filter(slug__startswith=settings.DB_MAILER_DEFAULT_TEMPLATE_PREFIX,
                                                    category__name=category).first()
        return default_email

    def get_welcome_email(self):
        return self.__get_email(settings.DB_MAILER_WELCOME_EMAIL_CATEGORY_NAME)

    def get_reminder_email(self):
        return self.__get_email(settings.DB_MAILER_REMINDER_EMAIL_CATEGORY_NAME)

    def get_accept_relationship_email(self):
        return self.__get_email(settings.DB_MAILER_ACCEPT_RELATIONSHIP_CATEGORY_NAME)

    def get_reject_relationship_email(self):
        return self.__get_email(settings.DB_MAILER_REJECT_RELATIONSHIP_CATEGORY_NAME)

    def get_request_relationship_email(self):
        return self.__get_email(settings.DB_MAILER_REQUEST_RELATIONSHIP_CATEGORY_NAME)

    def get_message_notification_email(self):
        return self.__get_email(settings.DB_MAILER_MESSAGE_NOTIFICATION_CATEGORY_NAME)

    @property
    def current_agreement_period(self):
        return self.assignment_period.order_by('-end_date').first()

    class Meta:
        verbose_name_plural = 'Properties'
        ordering = ('name', )


class AssignmentPeriod(Model):
    property = models.ForeignKey(Property, related_name='assignment_period')
    name = models.CharField(null=False, max_length=250)
    start_date = models.DateTimeField(null=False)
    end_date = models.DateTimeField(null=False)

    class Meta:
        unique_together = ('property', 'name')

    def __str__(self):
        return '{} :: {}'.format(self.property, self.name)


class UnitType(Model):
    property = models.ForeignKey(Property)
    name = models.CharField(null=False, max_length=250)
    max_occupancy = models.IntegerField(null=False, blank=False, default=1)
    max_rooms = models.IntegerField(default=1)

    def __str__(self):
        return '{}, max occupancy: {}, max rooms: {}'.format(self.name, self.max_occupancy, self.max_rooms)


class RoomType(Model):
    property = models.ForeignKey(Property)
    name = models.CharField(null=False, max_length=250)
    max_occupancy = models.IntegerField(null=False, blank=False, default=1)

    def __str__(self):
        return '{}, max beds: {}'.format(self.name, self.max_occupancy)


class Unit(Model):
    name = models.CharField(null=False, max_length=250)
    unit_type = models.ForeignKey(UnitType)
    unit_image = models.ImageField(blank=True, null=True, upload_to='uploads/unit_image/%Y/%m/%d/')

    class Meta:
        unique_together = (('name', 'unit_type'), )

    def __str__(self):
        return '{}, {}, occupancy: {}'.format(self.name, self.unit_type.name, self.unit_type.max_occupancy)

    def get_suitmates(self):
        return self.pending_assignments

    def private_room_available(self, agreement_period):
        for each_room in self.rooms.all(): 
            if each_room.is_empty(agreement_period):
                return True
        return False


class Room(Model):
    unit = models.ForeignKey(Unit, related_name='rooms')
    name = models.CharField(max_length=100, null=True)
    room_type = models.ForeignKey(RoomType, null=True)
    square = models.IntegerField(null=False, blank=False, default=0)

    def __str__(self):
        return 'name {} unit - {}, type {}, square {}'.format(self.name, self.unit.name, self.room_type.name if self.room_type else '', self.square)

    def get_available_beds(self, assignment_period):
        all_beds = self.beds.values_list('id', flat=True)
        assignments = PendingAssignment.objects.filter(bed__in=all_beds, assignment_period=assignment_period)
        occupied_beds = assignments.values_list('bed', flat=True)
        num_of_double_beds = assignments.filter(invite__contract_type__occupied_places__gte=2).count()
        beds = list(Bed.objects.filter(id__in=set(all_beds) - set(occupied_beds)).
                    exclude(blocked__in=[assignment_period]))
        return beds[:-num_of_double_beds] if num_of_double_beds else beds

    def get_beds(self):
        print("get_beds")
        all_beds = self.beds.values_list('id', flat=True)
        print(all_beds)
        beds = Bed.objects.filter(id__in=all_beds)
        return beds

    def get_bed_count(self):
        print("get_bed_count")
        bed_count = self.beds.count()
        print(bed_count)
        return bed_count

    def is_empty(self, assignment_period):
        all_beds = self.beds.values_list('id', flat=True)
        assignments = PendingAssignment.objects.filter(bed__in=all_beds, assignment_period=assignment_period)
        if assignments:
            return False
        else:
            return True


class Bed(Model):
    name = models.CharField(max_length=64)
    room = models.ForeignKey(Room, related_name='beds')
    blocked = models.ManyToManyField(AssignmentPeriod, related_name='blocked_beds',
                                     verbose_name='Periods where this bed blocked')

    class Meta:
        unique_together = ('name', 'room')

    def __str__(self):
        return 'Bed {} in {}'.format(self.name, self.room)




def get_invite_date():
    return datetime.now().date()


def get_expire_date():
    return datetime.now().date() + timedelta(days=30)


class Invitation(Model):
    MATCHING_RESTRICTION_ALL = 'A'
    MATCHING_RESTRICTION_WITH_ASSIGNMENTS = 'Y'
    MATCHING_RESTRICTION_WITHOUT_ASSIGNMENTS = 'N'
    MATCHING_RESTRICTION_CHOICES = (
        (MATCHING_RESTRICTION_ALL, 'All'),
        (MATCHING_RESTRICTION_WITH_ASSIGNMENTS, 'With Assignments'),
        (MATCHING_RESTRICTION_WITHOUT_ASSIGNMENTS, 'Without Assignments')
    )

    user = models.ForeignKey(settings.AUTH_USER_MODEL, related_name='invites')
    agreement_period = models.ForeignKey(AssignmentPeriod)
    invite_code = models.CharField(null=False, max_length=250, default=uuid.uuid4)
    invitation_type = models.CharField(null=False, max_length=250, choices=INVITATION_TYPE, default='M')
    invitation_date = models.DateTimeField(default=get_invite_date)
    expire_date = models.DateTimeField(default=get_expire_date)
    unit_type = models.ForeignKey(UnitType)
    room_type = models.ForeignKey(RoomType, blank=True, null=True)
    contract_type = models.ForeignKey(ContractType, blank=True, null=True, default=1)
    activated_time = models.DateTimeField(null=True, blank=True, default=None)
    locked = models.BooleanField(default=False)  # True if manager lock group with this user
    matching_restriction = models.CharField(max_length=1, choices=MATCHING_RESTRICTION_CHOICES,
                                            default=MATCHING_RESTRICTION_ALL)

    class Meta:
        unique_together = (('user', 'agreement_period'), )

    def save(self, force_insert=False, force_update=False, using=None,
             update_fields=None):
        super(Invitation, self).save(force_insert, force_update, using, update_fields)
        self.user.available_properties.add(self.agreement_period.property)

    def __str__(self):
        return '{} - {} :: {}, {}'.format(self.agreement_period.property, self.agreement_period, self.invite_code,
                                          self.user.email)

    def feed_str(self):
        return u"invite for user {}".format(self.user.full_name)


class PendingAssignment(Model):
    user = models.ForeignKey(settings.AUTH_USER_MODEL, related_name='assignments')
    unit = models.ForeignKey(Unit, null=True, blank=True, related_name='pending_assignments')
    room = models.ForeignKey(Room, null=True, blank=True)
    bed = models.ForeignKey(Bed, null=True, blank=True, related_name='assignments')
    room_type = models.ForeignKey(RoomType, null=True)
    assignment_period = models.ForeignKey(AssignmentPeriod)
    invite = models.OneToOneField(Invitation, null=True, blank=True, related_name='assignment')

    class Meta:
        ordering = ['unit', 'room_type']
        unique_together = ("user", "assignment_period")

    def __str__(self):
        return u'period - {}, unit {}, user {}'.format(self.assignment_period.name, self.user.email,
                                                       self.unit.name if self.unit else 'WAITLIST')

    def save(self, **kwargs):
        if self.bed:
            self.room = self.bed.room
            self.room_type = self.room.room_type
            self.unit = self.room.unit
        return super(PendingAssignment, self).save(**kwargs)


class PropertySocialNetwork(Model):
    property = models.ForeignKey(Property)
    network_name = models.CharField(null=False, max_length=50)

    def __unicode__(self):
        return u"{} : {}".format(self.property,self.network_name)


class OrganizationSocialHabitQuestion(Model):
    organization = models.ForeignKey(Organization)
    question = models.CharField(null=False, max_length=250)
    sort_order = models.IntegerField(default=1)
    status = models.BooleanField(default=True)
    required = models.BooleanField(default=True)
    created_at = models.DateTimeField(default=timezone.now, editable=False, db_index=True)
    updated_at = models.DateTimeField(default=timezone.now, editable=False, db_index=True)

    def __unicode__(self):
        return u"Question: {}".format(self.question)


class OrganizationSocialHabitAnswer(Model):
    question = models.ForeignKey(OrganizationSocialHabitQuestion, related_name='answers')
    answer = models.CharField(null=False, max_length=250)
    sort_order = models.IntegerField(default=1)
    status = models.BooleanField(default=True)
    created_at = models.DateTimeField(default=timezone.now, editable=False, db_index=True)
    updated_at = models.DateTimeField(default=timezone.now, editable=False, db_index=True)


class ImportModel(Model):
    QUEUED = 'Q'
    IN_PROGRESS = 'P'
    COMPLETED = 'C'
    FAILED = 'F'
    STATUS_CHOICES = (
        (QUEUED, 'Queued'),
        (IN_PROGRESS, 'In Progress'),
        (COMPLETED, 'Completed'),
        (FAILED, 'Failed'),
    )
    property = models.ForeignKey(Property)
    created_at = models.DateTimeField(default=datetime.now)
    added_by = models.ForeignKey(User)
    status = models.CharField(max_length=1, choices=STATUS_CHOICES)
    error = models.CharField(max_length=256, null=True)

    class Meta:
        abstract = True


class ResidentImport(ImportModel):
    file = models.FileField(upload_to='uploads/resident_import/')

    def __unicode__(self):
        return u"Resident import for {}, created at {}".format(self.property, self.created_at)


class RealtyImport(ImportModel):
    file = models.FileField(upload_to='uploads/realty_import/')

    def __unicode__(self):
        return u"Realty import for {}, created at {}".format(self.property, self.created_at)


class Stage(Model):
    MATCH_INSIDE_UNIT = 0
    MATCH_ONLY_WITH_USERS_WITH_UNITS = 1
    CAN_SELECT_UNIT = 2
    CAN_SELECT_BED = 3

    RESTRICTIONS = (
        (MATCH_INSIDE_UNIT, 'Allow matching only inside unit'),
        (MATCH_ONLY_WITH_USERS_WITH_UNITS, 'Allow matching only with users who have a unit assignment'),
        (CAN_SELECT_UNIT, 'Group leader can select unit'),
        (CAN_SELECT_BED, 'Users with unit can select bed')
    )
    name = models.CharField(max_length=32)
    label = models.CharField(max_length=256, blank=True, null=True)
    min_points = models.PositiveSmallIntegerField()
    max_points = models.PositiveSmallIntegerField()
    restrictions = ChoiceArrayField(
        models.SmallIntegerField(blank=True, null=True, choices=RESTRICTIONS),
        blank=True
    )

    class Meta:
        ordering = ['name']

    def __unicode__(self):
        return self.name

