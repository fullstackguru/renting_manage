# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('website', '0028_add_hidden_field_to_user'),
    ]

    operations = [
        migrations.AddField(
            model_name='user',
            name='disable_emails',
            field=models.BooleanField(default=False),
        ),
    ]
