from datetime import datetime
from django.conf import settings
from dbmail import send_db_mail


def send_welcome_email(invite):
    if not invite.agreement_period.property.enable_email_sending or invite.user.disable_emails:
        return

    property = invite.agreement_period.property
    welcome_email = property.get_welcome_email()
    if not welcome_email:
        return

    context = {
        'server_url': settings.SERVER_URL,
        'property': property,
        'user': invite.user,
        'logo_url': property.logo.url if property.logo else None,
        'subject': 'Welcome to {} roommate matching'.format(property),
        'invite': invite,
        'year': datetime.now().strftime('%Y'),
        'company': invite.agreement_period.property.name,
    }
    email_to = invite.user.email
    send_db_mail(welcome_email.slug, email_to, context)


def send_account_reminder_email(invite):
    if not invite.agreement_period.property.enable_email_sending or invite.user.disable_emails:
        return
    property = invite.agreement_period.property
    reminder_email = property.get_reminder_email()
    if not reminder_email:
        return
    context = {
        'subject': "Account Details!",
        'invite': invite,
        'user': invite.user,
        'property': property,
        'logo_url': property.logo.url if property.logo else '',
        'server_url': settings.SERVER_URL,
        'year': datetime.now().strftime('%Y'),
        'company': invite.agreement_period.property.name,
    }
    send_db_mail(reminder_email.slug, invite.user.email, context)


def send_relationship_email(relationship, created):
    from management_models import Invitation
    #  created by admin or manage.py command
    if relationship.read_only or relationship.via_user:
        return

    end_user_invitation = Invitation.objects.filter(user_id=relationship.end_user.id).\
        select_related('agreement_period__property').first()
    start_user_invitation = Invitation.objects.filter(user_id=relationship.start_user.id).\
        select_related('agreement_period__property').first()
    if not end_user_invitation or not start_user_invitation:
        return
    property = end_user_invitation.agreement_period.property
    if not property.enable_email_sending:
        return

    if created:
        request_relationship_email = property.get_request_relationship_email()
        if not request_relationship_email or relationship.end_user.disable_emails:
            return

        context = {
            'subject': "Roommate Request!",
            'relationship': relationship,
            'start_user': relationship.start_user,
            'end_user': relationship.end_user,
            'code': end_user_invitation.invite_code,
            'server_url': settings.SERVER_URL,
        }
        send_db_mail(request_relationship_email.slug, relationship.end_user.email, context)
    else:
        if relationship.end_user_approved:
            accept_relationship_email = property.get_accept_relationship_email()
            if not accept_relationship_email or relationship.end_user.disable_emails:
                return
            context = {
                'subject': 'Roommate Request Accepted',
                'start_user': relationship.start_user,
                'end_user': relationship.end_user,
                'code': start_user_invitation.invite_code,
                'server_url': settings.SERVER_URL,
            }
            send_db_mail(accept_relationship_email.slug, relationship.start_user.email, context)
        else:
            reject_relationship_email = property.get_reject_relationship_email()
            if not reject_relationship_email or relationship.end_user.disable_emails:
                return
            context = {
                'subject': 'Roommate Request Rejected',
                'start_user': relationship.start_user,
                'end_user': relationship.end_user,
                'code': start_user_invitation.invite_code,
                'server_url': settings.SERVER_URL,
            }
            send_db_mail(reject_relationship_email.slug, relationship.start_user.email, context)


def send_in_app_message(message, created):
    from management_models import Invitation
    if created:
        user = message.user
        from_user = message.conversation.user1
        to_user = message.conversation.user2
        if to_user.disable_emails:
            return
        if from_user.id != user.id:
            from_user, to_user = to_user, from_user
        to_user_invitation = Invitation.objects.filter(user_id=to_user.id).\
            select_related('agreement_period__property').first()
        if not to_user_invitation:
            return
        property = to_user_invitation.agreement_period.property
        message_notification_email = property.get_message_notification_email()
        if not message_notification_email or not property.enable_email_sending:
            return
        context = {
            'subject': 'New Message',
            'from_user': from_user,
            'to_user': to_user,
            'code': to_user_invitation.invite_code,
            'server_url': settings.SERVER_URL,
        }
        send_db_mail(message_notification_email.slug, to_user.email, context)


def send_email(template, invite):
    property = invite.agreement_period.property
    if not property.enable_email_sending or not template or invite.user.disable_emails:
        return
    context = {
        'server_url': settings.SERVER_URL,
        'property': property,
        'user': invite.user,
        'logo_url': property.logo.url if property.logo else None,
        'subject': template.subject,
        'invite': invite,
        'year': datetime.now().strftime('%Y'),
        'company': invite.agreement_period.property.name,
    }
    email_to = invite.user.email
    send_db_mail(template.slug, email_to, context)
