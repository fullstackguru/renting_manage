from rest_framework import serializers
from django.db.models import Model
from dbmail.models import MailTemplate
from actstream.models import Action
from website.models import User, Group
from management.models import Bed, Invitation, UnitType, RoomType, OrganizationSocialHabitQuestion, ResidentImport, \
    PendingAssignment, Unit, Room, ContractType, RealtyImport, PropertySocialNetwork


class AssignmentSerializer(serializers.ModelSerializer):
    unit = serializers.SerializerMethodField()
    bed = serializers.SerializerMethodField()

    def get_unit(self, obj):
        return obj.unit.name

    def get_bed(self, obj):
        return obj.bed.name

    class Meta:
        model = PendingAssignment
        fields = ('bed', 'unit')


class UserSerializer(serializers.ModelSerializer):
    unit = serializers.SerializerMethodField()
    room = serializers.SerializerMethodField()
    bed = serializers.SerializerMethodField()

    def get_unit(self, obj):
        assignment = obj.assignments.first()
        if assignment and assignment.unit:
            return assignment.unit.name
        return None

    def get_room(self, obj):
        assignment = obj.assignments.first()
        if assignment and assignment.room:
            return assignment.room.name
        return None

    def get_bed(self, obj):
        assignment = obj.assignments.first()
        if assignment and assignment.bed:
            return assignment.bed.name
        return None

    class Meta:
        model = User
        fields = ('email', 'first_name', 'last_name', 'last_login', 'unit', 'room', 'bed', 'id')


class LiteUserSerializer(serializers.ModelSerializer):
    class Meta:
        model = User
        fields = ('email', 'first_name', 'last_name', 'id','gender')


class InvitationSerializer(serializers.ModelSerializer):
    unit = serializers.SerializerMethodField()
    room = serializers.SerializerMethodField()
    bed = serializers.SerializerMethodField()
    agreement_period = serializers.SerializerMethodField()
    first_name = serializers.SerializerMethodField()
    last_name = serializers.SerializerMethodField()
    email = serializers.SerializerMethodField()
    unit_type = serializers.SerializerMethodField()
    room_type = serializers.SerializerMethodField(required=False)
    gender = serializers.SerializerMethodField()

    def get_agreement_period(self, obj):
        return obj.agreement_period.name

    def get_unit(self, obj):
        if not hasattr(obj, 'assignment') or not obj.assignment.unit:
            return None
        return obj.assignment.unit.name

    def get_room(self, obj):
        if not hasattr(obj, 'assignment') or not obj.assignment.room:
            return None
        return obj.assignment.room.name

    def get_bed(self, obj):
        if not hasattr(obj, 'assignment') or not obj.assignment.bed:
            return None
        return obj.assignment.bed.name

    def get_first_name(self, obj):
        return obj.user.first_name

    def get_last_name(self, obj):
        return obj.user.last_name

    def get_email(self, obj):
        return obj.user.email

    def get_unit_type(self, obj):
        return obj.unit_type.name

    def get_room_type(self, obj):
        if obj.room_type:
            return obj.room_type.name
        else:
            return None

    def get_gender(self, obj):
        return obj.user.gender

    class Meta:
        model = Invitation
        fields = ('id', 'agreement_period', 'unit', 'room', 'bed', 'first_name', 'last_name', 'user',
                  'email', 'unit_type', 'room_type', 'gender')


class UnitTypeSerializer(serializers.ModelSerializer):
    class Meta:
        model = UnitType
        fields = ('name', 'max_occupancy', 'id', 'max_rooms')


class RoomTypeSerializer(serializers.ModelSerializer):
    class Meta:
        model = RoomType
        fields = ('name', 'max_occupancy', 'id')


class MatchUserSerializer(serializers.Serializer):
    name = serializers.SerializerMethodField()
    gender = serializers.SerializerMethodField()
    email = serializers.SerializerMethodField()

    def get_name(self, obj):
        return obj.full_name

    def get_gender(self, obj):
        if obj.gender == User.GENDER_FEMALE:
            return "Female"
        elif obj.gender == User.GENDER_MALE:
            return "Male"
        else:
            return "Unknown"

    def get_email(self, obj):
        return obj.email


class MatchSerializer(serializers.ModelSerializer):
    property = serializers.SerializerMethodField()
    agreement_period = serializers.CharField(source='agreement_period.name')
    unit_type = serializers.CharField(source='unit_type.name')
    group_count = serializers.IntegerField(source='size')
    suitemates = serializers.ListField(child=MatchUserSerializer())
    roommates = serializers.ListField(child=serializers.ListField(child=MatchUserSerializer()))
    unit = serializers.CharField(source='unit.name')
    locked = serializers.BooleanField(source='is_locked')

    def get_property(self, obj):
        if 'property' in self.context:
            return self.context['property'].name
        if isinstance(obj, Model):
            return obj.agreement_period.property.name
        return obj.property.name

    class Meta:
        model = Group
        fields = ('property', 'agreement_period', 'unit_type', 'group_count', 'suitemates', 'roommates',
                  'unit', 'id', 'locked')


class RoomMateSerializer(serializers.ModelSerializer):
    score = serializers.SerializerMethodField()

    def get_score(self, obj):
        return obj.compatibility_score

    class Meta:
        model = User
        fields = ('email', 'first_name', 'last_name', 'score')


class SuggestedRoommatesSerializer(serializers.ModelSerializer):
    suggested_roommates = serializers.SerializerMethodField()

    def get_suggested_roommates(self, obj):
        obj.suggested_roommates = sorted(obj.suggested_roommates, key=lambda r: -r.compatibility_score)
        serializer = RoomMateSerializer(instance=obj.suggested_roommates, many=True)
        return serializer.data

    class Meta:
        model = User
        fields = ('email', 'first_name', 'last_name', 'suggested_roommates')

class PropertySocialNetworkSerializer(serializers.ModelSerializer):
    class Meta:
        model = PropertySocialNetwork
        fields = ('network_name', 'id')


class OrganizationSocialHabitQuestionSerializer(serializers.ModelSerializer):

    class Meta:
        model = OrganizationSocialHabitQuestion
        fields = ('question', 'required', 'sort_order', 'status', 'created_at', 'updated_at', 'id')


class ImportSerializer(serializers.ModelSerializer):
    file_url = serializers.URLField(read_only=True, source='file.url')
    file = serializers.SerializerMethodField()
    status = serializers.SerializerMethodField()

    def get_file(self, obj):
        return obj.file.name.split('/')[-1]

    def get_status(self, obj):
        return obj.get_status_display()


class ResidentImportRecordSerializer(ImportSerializer):
    class Meta:
        model = ResidentImport
        fields = ('file', 'created_at', 'status', 'file_url', 'error')


class RealtyImportSerializer(ImportSerializer):
    class Meta:
        model = RealtyImport
        fields = ('file', 'created_at', 'status', 'file_url', 'error')


class EmailSerializer(serializers.ModelSerializer):
    class Meta:
        model = MailTemplate
        fields = ('name', 'id')


class FeedSerializer(serializers.ModelSerializer):
    actor = serializers.SerializerMethodField()
    object = serializers.SerializerMethodField()

    def get_actor(self, obj):
        if hasattr(obj.actor, 'feed_str'):
            return obj.actor.feed_str()
        return str(obj.actor)

    def get_object(self, obj):
        if not obj.action_object:
            return ''
        if hasattr(obj.action_object, 'feed_str'):
            return obj.action_object.feed_str()
        return str(obj.action_object)

    class Meta:
        model = Action
        fields = ('actor', 'verb', 'object', 'timestamp')


class BedContractTypeSerializer(serializers.ModelSerializer):
    class Meta:
        model = ContractType
        fields = ("name", "occupied_places")


class BedSerializer(serializers.ModelSerializer):
    user = serializers.SerializerMethodField()
    blocked = serializers.SerializerMethodField()
    contract_type = serializers.SerializerMethodField()
    user_stage = serializers.SerializerMethodField()

    def get_user(self, obj):
        assignment = obj.assignments.first()
        if assignment:
            return LiteUserSerializer(assignment.user).data
        return None

    def get_user_stage(self, obj):
        assignment = obj.assignments.first()
        if assignment:
            user = assignment.user
            for s in self.context.get('stages', []):
                if s.min_points <= user.points <= s.max_points:
                    return s.name
        return None

    def get_contract_type(self, obj):
        assignment = obj.assignments.first()
        if assignment:
            return BedContractTypeSerializer(assignment.invite.contract_type).data
        return None

    def get_blocked(self, obj):
        return obj.is_blocked

    class Meta:
        model = Bed
        fields = ("id", "name", "user", "user_stage", "contract_type", "blocked")


class RoomSerializer(serializers.ModelSerializer):
    beds = BedSerializer(many=True)

    class Meta:
        model = Room
        fields = ("beds", "name", "id")


class SearchResultSerializer(serializers.Serializer):
    user = serializers.CharField()
    property = serializers.CharField()
    user_url = serializers.URLField()


class UnitSerializer(serializers.ModelSerializer):
    unit_type = serializers.CharField(source='unit_type.name')
    rooms = RoomSerializer(many=True)

    class Meta:
        model = Unit
        fileds = ('name', 'unit_type', 'rooms')
